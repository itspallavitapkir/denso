<?php
	require_once("class.phpmailer.php");
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "localhost";
		const DB_USER = "etplpm_denso";
		const DB_PASSWORD = "denso@123";
		const DB = "etplpm_denso_dev";
		const HOST = "http://192.199.240.234/~densoamp/denso_dev/";
//		const FROM_EMAIL ='mat@fynch.com';
		const FROM_EMAIL ='swapnilrtayade54@gmail.com';
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/* 
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		
		private function login(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				//$this->response('',406);
				$error = array('status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			if(isset($this->_request['email']))
				$email = $this->_request['email'];		
			if(isset($this->_request['pwd']))	
				$password = $this->_request['pwd'];
			
			// Input validations
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT user_id, email_address FROM dn_users WHERE email_address = '$email' AND password = '".md5($password)."' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result['status'] = 'Success';
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){
							$result["Login"] = $row;
							// If success everythig is good send header as "OK" and user details
						}
						$this->response($this->json($result), 200);
						
					}
					else{
						$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
						$this->response($this->json($error), 200);
					}
				}
			}
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		} 
		
		/*forgot password API
		 * 
		 */
		 
		 private function forgotPassword(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			if(isset($this->_request['email']))
				$email = $this->_request['email'];	
			
			// Input validations
			if(!empty($email)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT user_id,first_name,last_name,email_address FROM dn_users WHERE email_address = '$email' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						
						$reset_key  = md5(uniqid(mt_rand(), true));
						
						
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){
							
							mysql_query("update dn_users set dn_resetkey='".$reset_key."' where email_address='".$email."'");
							$uname = $row['first_name']." ".$row['last_name'];
						
							$hostname = self::HOST;
							$mail = new PHPMailer(true);
							$messageBody  = "Hello $uname,<br/><br/>
                            We have received your request to reset your password. <br/> <br/>
                             Please click the link below to reset.<br/><br/>
                             <a href=".$hostname."resetuserpwd/?key=".$reset_key.">".$hostname."resetuserpwd/?key=".$reset_key."</a>
                             <br/><br/>If the link is not working properly, then copy and paste the link in your browser.<br/>
                             If you did not send this request, please ignore this email.
                             <br/><br/>Regards,<br/>DENSO AFTERMARKET WEBSITE";
							
							 
							 $mail->SetFrom(self::FROM_EMAIL, 'DENSO ASIA AFTERMARKET');
							 
							 $mail->AddAddress("$email");
							 
							 $mail->Subject = 'DENSO AFTERMARKET WEBSITE - Reset Password'; 
							 $mail->AltBody = $messageBody; 
							 if ( ! $mail->IsSendmail()){  
								$result['status'] = 'Failed';
								$result["Forgot_password"] = $row;
								$result['msg'] = 'The email can not be send ! Server Error.';
							}
							else{
								$result['status'] = 'Success';
								$result["Forgot_password"] = $row;
								$result['msg'] = 'The reset password link has been sent to your email address.';
							}
							// If success everythig is good send header as "OK" and user details
						}
						$this->response($this->json($result), 200);
						
					}
					else{
						$this->response('', 204);	// If no records "No Content" status
					}
				}
			}
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		} 

		
		
		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>
