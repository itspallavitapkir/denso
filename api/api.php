<?php
error_reporting(1);
	//require_once("class.phpmailer.php");
	require_once("Rest.inc.php");
	include_once("simple_html_dom.php");
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "localhost";
		const DB_USER = "root";
		//const DB_PASSWORD = "denso@100%";
		const DB_PASSWORD = "mysqlrootbitware";
		const DB = "denso_dev_db";
		//const HOST = "http://192.199.240.234/~densoamp/denso_dev";
		//const HOST = "http://aftermarket.denso.com.sg";
		const HOST = "http://103.224.243.154/denso/";
		const FROM_EMAIL ='mat@fynch.com';
		const BACKGROUND = "background_img";
		const SK_FILES = "sk_files";
		const USER_EXIST_MSG = "The user with this account does not exists.";
		const USER_INACTIVE_MSG = "Your account is inactive. Contact your administrator to activate it.";
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		mysql_query("set names utf8");	 
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
				$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
	
		/* 
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		
		private function login(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			if(isset($this->_request['email']))
				$email = $this->_request['email'];		
			if(isset($this->_request['pwd']))	
				$password = $this->_request['pwd'];
			
			// Input validations
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					
					$sql = mysql_query("SELECT * FROM dn_users WHERE email_address = '$email' AND password = '".md5($password)."' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						//$result['Status'] = 'Success';
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){
							
							if($row['dn_status'] == 'Active'){
							 //print_r($userData);die;
$usr_id  = $row["user_id"];
$sql = mysql_query("insert into dn_user_statistics (user_id,login_time,date_modified) values('.$usr_id.','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')", $this->db);

								$login_count = $row['login_count']+1;
								$sql = mysql_query("update dn_users set login_count = $login_count WHERE email_address = '$email'", $this->db);

mysql_query("insert into dn_user_statistics (user_id,login_time,date_modified) values(".$row["user_id"].",'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')", $this->db); 
								$sql_insert_stat = mysql_insert_id();								if($sql_insert_stat){
									$row['stat_id'] = $sql_insert_stat;
								}
								$result = $row;
								$result['Status'] = 'Success';
								$result['is_active'] = 1;
								$result['msg'] = "Successful logged in";
								$this->response($this->json($result), 200);
							}
							elseif($row['dn_status'] == 'Inactive'){
								$result = $row;
								$result['Status'] = 'Success';
								$result['is_active'] = 0;
								$result['msg'] = "Your account is inactive";
								$this->response($this->json($result), 200);
								//$error = array('Status' => "Success", 'is_active' => '0',"msg" => "Your account is inactive");
								//$this->response($this->json($result), 200);
							}
							
							
							
							// If success everythig is good send header as "OK" and user details
						}
						
						
					}
					else{
						//$this->response('', 204);	// If no records "No Content" status
						$error = array('Status' => "Failed", "msg" => "Invalid Email address or Password");
						$this->response($this->json($error), 400);
					}
				}
			}
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('Status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		}  

		private function logout(){
			
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			
			if(isset($this->_request['stat_id']))
				$statid = $this->_request['stat_id'];		
			if(isset($this->_request['user_id']))	
				$userid = $this->_request['user_id'];
			
			if(isset($statid) && isset($userid)){				
					$sql = mysql_query("update dn_user_statistics set logout_time = '".date("Y-m-d H:i:s")."',date_modified = '".date("Y-m-d H:i:s")."' WHERE stat_id = $statid and user_id=$userid", $this->db);
					$result['Status'] = "Success";
					$result['msg'] = "Logout Successfully";
					$this->response($this->json($result), 200);
			}
			else{
					$result['Status'] = "Failed";
					$result['msg'] = "Logout Successfully";
					$this->response($this->json($result), 200);
			}
				
		} 

		
		/*forgot password API
		 * 
		 */
		 
		 private function forgotPassword(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			if(isset($this->_request['email']))
				$email = $this->_request['email'];	
			
			// Input validations
			if(!empty($email)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT user_id,first_name,last_name,email_address FROM dn_users WHERE email_address = '$email' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						
						$reset_key  = md5(uniqid(mt_rand(), true));
						
						
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){
							
							mysql_query("update dn_users set dn_resetkey='".$reset_key."' where email_address='".$email."'");
							$uname = $row['first_name']." ".$row['last_name'];
							$email  = $row['email_address'];
							$hostname = self::HOST;
							//$mail = new PHPMailer(true);
							$messageBody  = "Hello $uname,<br/><br/>
                            We have received your request to reset your password. <br/> <br/>
                             Please click the link below to reset.<br/><br/>
                             <a href=".$hostname."resetuserpwd/?key=".$reset_key.">".$hostname."resetuserpwd/?key=".$reset_key."</a>
                             <br/><br/>If the link is not working properly, then copy and paste the link in your browser.<br/>
                             If you did not send this request, please ignore this email.
                             <br/><br/>Regards,<br/>DENSO AFTERMARKET WEBSITE";
							
							
							$reply_to  = self::FROM_EMAIL;
							$SetFrom = 'DENSO ASIA AFTERMARKET';
							$headers = "From: $SetFrom <".$reply_to.">" . "\r\n";
										"Reply-To: $reply_to" . "\r\n" .
										"X-Mailer: PHP/" . phpversion();
							$headers  .= "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
							// $mail->SetFrom(self::FROM_EMAIL, 'DENSO ASIA AFTERMARKET');
							 
							// $mail->AddAddress("$email");
							 
							// $mail->Subject = 'DENSO AFTERMARKET WEBSITE - Reset Password'; 
							// $mail->AltBody = $messageBody; 
							
							
							 
							 $to = $email;
							 
							$subject = 'DENSO AFTERMARKET WEBSITE - Reset Password'; 
							// $mail->AltBody = $messageBody; 
							
							 if ( ! mail($to,$subject,$messageBody,$headers)){ 
								$result['Status'] = 'Failed';
								$result["Forgot_password"] = $row;
								$result['msg'] = 'The email can not be send ! Server Error.';
							}
							else{
								$result['Status'] = 'Success';
								$result["Forgot_password"] = $row;
								$result['msg'] = 'The reset password link has been sent to your email address.';
							}
							// If success everythig is good send header as "OK" and user details
						}
						$this->response($this->json($result), 200);
						
					}
					else{
						$error = array('Status' => "Failed", "msg" => "The email address is not found in our database");
						$this->response($this->json($error), 400);

					}
				}
			}
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('Status' => "Failed", "msg" => "The email address is not found in our database");
			$this->response($this->json($error), 400);
		} 
		
		/*category listing*/
		
		private function listCategory(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			 	
			$sql = mysql_query("SELECT v_cat_id,category FROM dn_categories where status_pmk='Active' order by category ASC", $this->db);
				if(mysql_num_rows($sql) > 0){
						$result['Status'] = 'Success';
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){ 
								$result["Category_list"][] = $row;	
						}
						$this->response($this->json($result), 200);
						
			}
			else{
				$this->response('', 204);	// If no records "No Content" status
			} 
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		} 
		
		/*sections on perticular sub category*/
		
		private function getSectionByCatId(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			 	 	
			if(isset($this->_request['user_id'])){
				$user_id = $this->_request['user_id'];	
				$result_user_exists = $this->check_user_exists($user_id);
				
				if($result_user_exists == "2"){
					$error = array('Status' => "Failed", "msg" => self::USER_INACTIVE_MSG);
					$this->response($this->json($error), 400);exit;
				}
				
				if($result_user_exists == 0){
					$error = array('Status' => "Failed", "msg" => self::USER_EXIST_MSG);
					$this->response($this->json($error), 400);exit;
				}
			}
			if(isset($this->_request['v_cat_id']))
				$v_cat_id = $this->_request['v_cat_id'];	
			if(isset($this->_request['os']))
				$os = $this->_request['os'];
			if(isset($this->_request['user_id'])){			
				$sql_country = mysql_query("SELECT country FROM dn_users where user_id=".$user_id."", $this->db);	
				$country_id =0;
				
				if(mysql_num_rows($sql_country) > 0){
						while($row_country = mysql_fetch_array($sql_country,MYSQL_ASSOC)){ 
							$country_id = $row_country['country'];
						}
				}
			}
			
			if(isset($this->_request['user_id']) && $this->_request['user_id']!=""){		
				$sql_user = mysql_query("SELECT *,l.language,c.category FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id left join dn_categories c on p.category=c.v_cat_id where p.category=".$v_cat_id." AND (p.sub_category = '' OR p.sub_category IS NULL OR p.sub_category =0) AND p.lang_id=1", $this->db);	
			}
			else{
				
				$sql_user = mysql_query("SELECT *,l.language,c.category FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id left join dn_categories c on p.category=c.v_cat_id where p.category=".$v_cat_id." AND (p.sub_category = '' OR p.sub_category IS NULL OR p.sub_category =0) AND p.lang_id=1 AND public_access='1'", $this->db);	
			}
			
			if(mysql_num_rows($sql_user) > 0){
						$result['Status'] = 'Success'; 
						$i=0;
						$array_font_size = array("font-size:8px","font-size:9px","font-size:10px","font-size:11px","font-size:12px","font-size:14px","font-size:16px","font-size:18px","font-size:20px","font-size:22px","font-size:24px","font-size:28px","font-size:36px","font-size:48px");
						$arrayreplace_font_size = array("font-size:14px","font-size:15px","font-size:16px","font-size:17px","font-size:18px","font-size:20px","font-size:22px","font-size:24px","font-size:26px","font-size:28px","font-size:30px","font-size:34px","font-size:42px","font-size:54px");
						/*$user_agent     =   $_SERVER['HTTP_USER_AGENT'];
						$user_agent_str  =split(';',$user_agent);
						$user_os  =split(';',$user_agent_str[1]);
						//echo $user_os[0];
						$user_os_name = split(" ",trim($user_os[0]));*/
						while($row = mysql_fetch_array($sql_user,MYSQL_ASSOC)){ 
							
								$k=0;
								$lang_res ='';
								$p =0;
								$j=0;
								$row["language"] = $lang_res;

								
								if($row["background_img"] != ""){
									$row["background_img"] = self::HOST."/".self::BACKGROUND."/".$row["background_img"];
								}
								else{
									$row["background_img"] = "";
								}
								
								$string_section ="";
								
								//print_r($html_data);//die;
								/*foreach($obj->find('table') as $ele):
										$ele->href;
								endforeach;*/
								//if($i===0){
								
								
								//$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
								//$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
								
								if($os =='Android'){
								   $row['page_data'] .="	
									<script type='text/javascript'> 
									 function locationChange(temp) { 
										AndroidFunction.myFunction(temp);
									 }  
									</script>";
								}else{
								$row['page_data'] .="	
									<script type='text/javascript'>
									var section = '';
									 
									 function locationChange(sname) { 
										section = sname;window.location  = 'ios:webToNativeCall';
									 } 
									 function getText(){return section;}
									</script>";
								}

								//}

								$row['page_data'] = str_replace($array_font_size,$arrayreplace_font_size,$row['page_data']);
								//$html_data1 = str_get_html($row['page_data']);
								
								//
								
								if (strpos($row['page_data'] , 'Category_1_') !== false) {

									$data = str_get_html(str_replace("Category_1_","",$row['page_data']));
									foreach($data->find('a') as $node):
										if($node->href!="" && (strpos($node->href , 'www') === false)){
										$v = str_replace("#","",str_replace("_"," ",$node->href));
										$data = str_replace('href="'.$node->href.'"',"onclick='locationChange(\"".$v."\");' href='javascript:void(0);'",$data);
}
									endforeach;
									$row['page_data'] = $data;
								}
								$data='';
								//print_r($html);
								/*foreach($html_data->find('a') as $element):
									$row['page_data'] .= $element->innertext ="onclick='locationChange();'";
									//$html_data->save();
								endforeach;
								*/
								$result["Sections"][] = $row;
								
								$sql_versions = mysql_query("SELECT p.page_id,p.page_description,l.language,l.lang_code FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id left join dn_categories c on p.category=c.v_cat_id where p.category=".$v_cat_id."  AND (p.sub_category = '' OR p.sub_category IS NULL OR p.sub_category =0)  AND p.lang_id!=1 and section_name='".$row['section_name']."'", $this->db);	
								if(mysql_num_rows($sql_versions) > 0){
									while($row_ver = mysql_fetch_array($sql_versions,MYSQL_ASSOC)){
										
										//$str = $row_ver['page_description'];	
										
								if($os =='Android'){
								   $row_ver['page_description'] .="	
									<script type='text/javascript'> 
									 function locationChange(temp) {AndroidFunction.myFunction(temp);}  
									</script>";
								}else{
								$row_ver['page_description'] .="	
									<script type='text/javascript'>
									var section = '';
									 
									 function locationChange(sname) { 
										section = sname;window.location  = 'ios:webToNativeCall';
									 } 
									 function getText(){return section;}
									</script>";
								}

										$row_ver['page_description'] = str_replace($array_font_size,$arrayreplace_font_size,$row_ver['page_description']);
										//$row_ver['page_description'] = str_replace("href","onclick='locationChange(\"".$row_ver['section_name']."\");' data-ref",$row_ver['page_description']);
										if (strpos($row_ver['page_description'], 'Category_1_') !== false) {
											$data_desc = str_get_html(str_replace("Category_1_","",$row_ver['page_description']));
										
//										echo $data_desc->find('a') ;
										
											foreach($data_desc->find('a') as $node1):
												if($node1->href != "" && (strpos($node->href , 'www') === false)){
													$v = str_replace("#","",str_replace("_"," ",$node1->href));
													$data_desc = str_replace('href="'.$node1->href.'"',"onclick='locationChange(\"".$v."\");' href='javascript:void(0);'",$data_desc);
												}
											endforeach;
											$row_ver['page_description'] = $data_desc;
										
										}
										
										
										
										//onclick='locationChange()'
										//	array(get_class($this),'replace_unicode_escape_sequence'), $str);
										 
										$result["Sections"][$i]["versions"][$j] = $row_ver;
										//echo $i;
										$j++;
									}
								}
								 
								//print_r($result);die;
								$sql_lang = mysql_query("SELECT l.language FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id where category=".$v_cat_id."  AND (p.sub_category = '' OR p.sub_category IS NULL)  and section_name='".$row['section_name']."'", $this->db);	
								if(mysql_num_rows($sql_lang) > 0){
									while($row_lang = mysql_fetch_array($sql_lang,MYSQL_ASSOC)){ 
										$result["Sections"][$i]["language"][$p] =  $row_lang['language'];
										$p++;
									}
									//$lang_res = substr($lang_res,0,-1);	
								}
								$resources_ids = substr($row['resources_id'],0,-1);	


								if(isset($this->_request['user_id']) && $this->_request['user_id']!=""){
									$sql_resources = mysql_query("SELECT resources_sk_pdf FROM dn_resource_sk where res_sk_id in (".$resources_ids.") and FIND_IN_SET('".$country_id."',country) and FIND_IN_SET('".$user_id."',user_id)", $this->db);	
								
									if(mysql_num_rows($sql_resources) > 0){
										while($row_res = mysql_fetch_array($sql_resources,MYSQL_ASSOC)){ 
											$result["Sections"][$i]["resources"][$k]["resources_name"] = $row_res["resources_sk_pdf"];
											$result["Sections"][$i]["resources"][$k]["resources_link"] = self::HOST."/".self::SK_FILES."/".$row_res["resources_sk_pdf"];
											$k++;
										} 
									} 
								} 
								elseif($row['public_access'] != '1'){
											$sql_resources = mysql_query("SELECT resources_sk_pdf FROM dn_resource_sk where res_sk_id in (".$resources_ids.")", $this->db);	
											if(mysql_num_rows($sql_resources) > 0){
												while($row_res = mysql_fetch_array($sql_resources,MYSQL_ASSOC)){ 
													$result["Sections"][$i]["resources"][$k]["resources_name"] = $row_res["resources_sk_pdf"];
													$result["Sections"][$i]["resources"][$k]["resources_link"] = self::HOST."/".self::SK_FILES."/".$row_res["resources_sk_pdf"];
													$k++;
												} 
											} 
								}
								else{
									$result["Sections"][$i]["resources"] = array();
								}
								
								$i++;
						}
						 
						$sql_subcat = mysql_query("SELECT v_sub_cat_id,sub_category FROM dn_subcategory where v_cat_id=".$v_cat_id."", $this->db);
						if(mysql_num_rows($sql_subcat) > 0){
								$result['Status'] = 'Success';
								while($row_subcat = mysql_fetch_array($sql_subcat,MYSQL_ASSOC)){ 
										$result["Sub_category_list"][] = $row_subcat;	
								}
								$this->response($this->json($result), 200);
								
						}
						
						$this->response($this->json($result), 200);
			}
			else{ 
				
				$sql_subcat = mysql_query("SELECT v_sub_cat_id,sub_category FROM dn_subcategory where v_cat_id=".$v_cat_id."", $this->db);
				if(mysql_num_rows($sql_subcat) > 0){
						$result['Status'] = 'Success';
						while($row_subcat = mysql_fetch_array($sql_subcat,MYSQL_ASSOC)){ 
								$result["Sub_category_list"][] = $row_subcat;	
						}
						
						//$this->response($this->json($result), 200);
				}		
				else{
				
					$error['Status'] = 'Failed';
					$error['msg']    = 'No record exists';
					$this->response($this->json($error), 404);
				}
				$result['msg'] = 'No record found for this Category. Please check relevant sub-categories.';
				$this->response($this->json($result), 200);
				//$this->response('', 204);	// If no records "No Content" status
			}
			 
			// If invalid inputs "Bad Request" status message and reason
			$this->response('', 204);
			
		}
		
		
		
		private function getSectionBySubCatId(){
			//header('Content-Type: application/json; charset=utf-8');
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			 	 
			if(isset($this->_request['user_id'])){
				$user_id = $this->_request['user_id'];	
				$result_user_exists = $this->check_user_exists($user_id);
				if($result_user_exists == "2"){
					$error = array('Status' => "Failed", "msg" => self::USER_INACTIVE_MSG);
					$this->response($this->json($error), 400);exit;
				}
				if($result_user_exists == 0){
					$error = array('Status' => "Failed", "msg" => self::USER_EXIST_MSG);
					$this->response($this->json($error), 400);exit;
				}
			}	
			if(isset($this->_request['v_sub_cat_id']))
				$v_sub_cat_id = $this->_request['v_sub_cat_id'];	
			if(isset($this->_request['os']))
				$os = $this->_request['os'];

			if(isset($this->_request['user_id'])){						
				$sql_country = mysql_query("SELECT country FROM dn_users where user_id=".$user_id."", $this->db);	
				$country_id =0;
				
				if(mysql_num_rows($sql_country) > 0){
						while($row_country = mysql_fetch_array($sql_country,MYSQL_ASSOC)){ 
							$country_id = $row_country['country'];
						}
				}
			}
			 
			if(isset($this->_request['user_id']) && $this->_request['user_id']!=""){ 		
				$sql_user = mysql_query("SELECT *,l.language,c.category FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id left join dn_categories c on p.category=c.v_cat_id where p.sub_category=".$v_sub_cat_id." AND p.lang_id=1", $this->db);	
			}
			else{
				$sql_user = mysql_query("SELECT *,l.language,c.category FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id left join dn_categories c on p.category=c.v_cat_id where p.sub_category=".$v_sub_cat_id." AND p.lang_id=1 AND public_access='1'", $this->db);	
			}
			if(mysql_num_rows($sql_user) > 0){
						$background = "";
						$sql_background = mysql_query("SELECT background_img FROM dn_subcategory where v_sub_cat_id=".$v_sub_cat_id."", $this->db);	
						if(mysql_num_rows($sql_background) > 0){
								while($row_background = mysql_fetch_array($sql_background,MYSQL_ASSOC)){ 
									$background = $row_background['background_img'];
								}
						}
						
						$result['Status'] = 'Success'; 
						$i=0;
						$array_font_size = array("font-size:8px","font-size:9px","font-size:10px","font-size:11px","font-size:12px","font-size:14px","font-size:16px","font-size:18px","font-size:20px","font-size:22px","font-size:24px","font-size:28px","font-size:36px","font-size:48px");
						$arrayreplace_font_size = array("font-size:14px","font-size:15px","font-size:16px","font-size:17px","font-size:18px","font-size:20px","font-size:22px","font-size:24px","font-size:26px","font-size:28px","font-size:30px","font-size:34px","font-size:42px","font-size:54px");

						while($row = mysql_fetch_array($sql_user,MYSQL_ASSOC)){ 
							  
								$k=0;
								$lang_res ='';
								
								$p =0;
								
								$j=0;
								$row["language"] = $lang_res;
								
								
								if($background != ""){
									$row["background_img"] = self::HOST."/".self::BACKGROUND."/".$background;
								}
								else{
									$row["background_img"] = "";
								}
								//if($i===0){
								if($os =='Android'){
								   $row['page_data'] .="	
									<script type='text/javascript'> 
									 function locationChange(temp) { 
										AndroidFunction.myFunction(temp);
									 }  
									</script>";
								}else{
								$row['page_data'] .="	
									<script type='text/javascript'>
									var section = '';
									 
									 function locationChange(sname) { 
										section = sname;window.location  = 'ios:webToNativeCall';
									 } 
									 function getText(){return section;}
									</script>";
								}

								//}
								$row['page_data'] =str_replace($array_font_size,$arrayreplace_font_size,$row['page_data']);
								if (strpos($row['page_data'] , 'Category_1_') !== false) {
									$data = str_get_html(str_replace("Category_1_","",$row['page_data']));
									foreach($data->find('a') as $node):
if($node->href!="" && (strpos($node->href , 'www') === false)){
										$v = str_replace("#","",str_replace("_"," ",$node->href));
										$data = str_replace('href="'.$node->href.'"',"onclick='locationChange(\"".$v."\");' href='javascript:void(0);'",$data);
}
									endforeach;
									$row['page_data'] = $data;
								}
								$result["Sections"][] = $row;
								$sql_versions = mysql_query("SELECT p.page_id,p.page_description,l.language,l.lang_code FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id left join dn_subcategory c on p.sub_category=c.v_sub_cat_id where p.sub_category=".$v_sub_cat_id." AND p.lang_id!=1 and section_name='".$row['section_name']."'", $this->db);	
								if(mysql_num_rows($sql_versions) > 0){
									while($row_ver = mysql_fetch_array($sql_versions,MYSQL_ASSOC)){ 
										
										//$row_ver['page_description'] = mb_convert_encoding($row_ver['page_description'], 'UTF-8', 'HTML-ENTITIES');	
										//$str = $row_ver['page_description'];		
										//$row_ver['page_description'] = preg_replace_callback('/\\\\u([0-9a-f]{4})/i',
										//	array(get_class($this),'replace_unicode_escape_sequence'), $str);
										//	var_dump($str);
										//echo $row_ver['page_description'];
										
										if($os =='Android'){
								   $row_ver['page_description'] .="	
									<script type='text/javascript'> 
									 function locationChange(temp) {AndroidFunction.myFunction(temp);}  
									</script>";
								}else{
								$row_ver['page_description'] .="	
									<script type='text/javascript'>
									var section = '';
									 
									 function locationChange(sname) { 
										section = sname;window.location  = 'ios:webToNativeCall';
									 } 
									 function getText(){return section;}
									</script>";
								}
										$row_ver['page_description'] = str_replace($array_font_size,$arrayreplace_font_size,$row_ver['page_description']);
										if (strpos($row_ver['page_description'], 'Category_1_') !== false) {
											$data_desc = str_get_html(str_replace("Category_1_","",$row_ver['page_description']));
										
//										echo $data_desc->find('a') ;
										
											foreach($data_desc->find('a') as $node1):
												if($node1->href != "" && (strpos($node->href , 'www') === false)){
													$v = str_replace("#","",str_replace("_"," ",$node1->href));
													echo $data_desc = str_replace('href="'.$node1->href.'"',"onclick='locationChange(\"".$v."\");' href='javascript:void(0);'",$data_desc);
												}
											endforeach;
											$row_ver['page_description'] = $data_desc;
										
										}
										$result["Sections"][$i]["versions"][$j] = $row_ver;
										//$result["Sections"][$i]["versions"][$j]['page_description'] = $row_ver['page_description'];
										//echo $i;
										$j++;
										
									}
								}
								//print_r($result);
								$sql_lang = mysql_query("SELECT l.language FROM dn_pages p left join dn_lang l on p.lang_id=l.lang_id where sub_category=".$v_sub_cat_id." and section_name='".$row['section_name']."'", $this->db);	
								if(mysql_num_rows($sql_lang) > 0){
									while($row_lang = mysql_fetch_array($sql_lang,MYSQL_ASSOC)){ 
										$result["Sections"][$i]["language"][$p] =  $row_lang['language'];
										$p++;
									}
									//$lang_res = substr($lang_res,0,-1);	
								}
								
								$resources_ids = substr($row['resources_id'],0,-1);	
								
								if(isset($this->_request['user_id']) && $this->_request['user_id']!=""){
									$sql_resources = mysql_query("SELECT resources_sk_pdf FROM dn_resource_sk where res_sk_id in (".$resources_ids.") and FIND_IN_SET('".$country_id."',country) and FIND_IN_SET('".$user_id."',user_id)", $this->db);	
								
									if(mysql_num_rows($sql_resources) > 0){
										while($row_res = mysql_fetch_array($sql_resources,MYSQL_ASSOC)){ 
											$result["Sections"][$i]["resources"][$k]["resources_name"] = $row_res["resources_sk_pdf"];
											$result["Sections"][$i]["resources"][$k]["resources_link"] = self::HOST."/".self::SK_FILES."/".$row_res["resources_sk_pdf"];
											$k++;
										} 
									} 
								} 
								elseif($row['public_access'] != '1'){
											$sql_resources = mysql_query("SELECT resources_sk_pdf FROM dn_resource_sk where res_sk_id in (".$resources_ids.")", $this->db);	
											if(mysql_num_rows($sql_resources) > 0){
												while($row_res = mysql_fetch_array($sql_resources,MYSQL_ASSOC)){ 
													$result["Sections"][$i]["resources"][$k]["resources_name"] = $row_res["resources_sk_pdf"];
													$result["Sections"][$i]["resources"][$k]["resources_link"] = self::HOST."/".self::SK_FILES."/".$row_res["resources_sk_pdf"];
													$k++;
												} 
											} 
										}
								else{
									$result["Sections"][$i]["resources"] = array();
									
								}
																
								$i++;
						}
						
						$this->response($this->json($result), 200);
			}
			else{
				$error['Status'] = 'Failed';
					$error['msg']    = 'No record exists';
					$this->response($this->json($error), 404);
				//$this->response('', 204);	// If no records "No Content" status
			}
			 
			// If invalid inputs "Bad Request" status message and reason
			$this->response('', 204);
			
		}
		
		private function listSubCategory(){
			
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}
			
			if(isset($this->_request['v_cat_id']))
				$v_cat_id = $this->_request['v_cat_id'];
			 	  
			$sql = mysql_query("SELECT v_sub_cat_id,v_cat_id,sub_category FROM dn_subcategory where v_cat_id=".$v_cat_id."", $this->db);
				if(mysql_num_rows($sql) > 0){
						$result['status'] = 'Success';
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){ 
								$result["Sub_category_list"][] = $row;	
						}
						$this->response($this->json($result), 200);
						
			}
			else{
				$this->response('', 204);	// If no records "No Content" status
			} 
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('Status' => "Failed", "msg" => "No Records Found");
			$this->response($this->json($error), 400);
		} 
		
		
		/*check user exists*/
		
		
		function check_user_exists($user_id){
			
			if($user_id != ""){
				$sql = mysql_query("SELECT * FROM dn_users WHERE user_id = ".$user_id." ", $this->db);
					if(mysql_num_rows($sql) > 0){
						//echo "SELECT * FROM dn_users WHERE user_id = ".$user_id." AND dn_status='Inactive'";
						$sql = mysql_query("SELECT * FROM dn_users WHERE user_id = ".$user_id." AND dn_status='Inactive'", $this->db);
						if(mysql_num_rows($sql) > 0){
							return "2";
						}
						
						return 1;
					}
					else{
						return 0;
					}
			}
			else{
				return 0;
			}
		}
		
		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>
