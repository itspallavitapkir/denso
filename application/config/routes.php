<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "frontcontroller";
$route['404_override'] = '';

$route["adminlogin"] = "login";
$route["logout"] = "login/logout";

//fronend
/*AMW urls*/

$route["Index"] 	= "frontcontroller/index";
$route["AMW/index"] 	= "frontcontroller/index";
$route["forgotpassword"]= "frontcontroller/forgotUserPassword";
$route["resetuserpwd"]= "frontcontroller/resetUserPassword";
$route["AMW/profile"] = "frontcontroller/profile";
$route["AMW/updateprofile"] = "frontcontroller/updateUserProfile";
$route["AMW/userlogin"] = "frontcontroller/login";
$route["AMW/news"] = "frontcontroller/news";
$route["AMW/news_details"] = "frontcontroller/news_details";
$route["AMW/viewresources"] = "frontcontroller/resources";
$route["AMW/postcomment"] = "frontcontroller/post_comment";
$route["AMW/search"] = "frontcontroller/search";
$route["AMW/details"] = "frontcontroller/details";
$route["AMW/downloadPDFFile"] = "frontcontroller/downloadPDFFile";
$route["AMW/downloadAIFile"] = "frontcontroller/downloadAIFile";

$route["logoutuser"] = "login/logoutuser";

/*SKW urls*/
$route["PMK/index"] = "frontcontroller/sales_kit_home";
$route["PMK/sale-kit-home"] = "frontcontroller/sales_kit_home";
$route["PMK/sections-category"] = "frontcontroller/section_by_category";
$route["PMK/sections-sub-category"] = "frontcontroller/section_by_sub_category";

//added by pallavi
$route["Test"] = "home";
$route["Notify"] = "apicontroller/iosnotification";




/* End of file routes.php */
/* Location: ./application/config/routes.php */
