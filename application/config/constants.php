<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/***************** Database Tables **********************/

define("TB_PUSH_NOTIFICATION","dn_push_notification");
define("TB_SECTION","dn_section");
define("TB_QUESTIONNAIRE","dn_questionnaire");
define("TB_QUE_OPTIONS","dn_que_options");
define("TB_BANNER","dn_banner");
define("TB_USERS","dn_users");
define("TB_COUNTRY","dn_country");
define("TB_ADMIN","dn_admin");
define("TB_BRAND","dn_brand");
define("TB_MODEL","dn_model");
define("TB_CATEGORY","dn_categories");
define("TB_SUBCATEGORY","dn_subcategory");
define("TB_NEWS","dn_news");
define("TB_NEWS_CATEGORY","dn_news_category");
define("TB_RESOURCES","dn_resources");
define("TB_USER_ACCESS","dn_user_access");
define("TB_USER_ROLE"," dn_user_role");
define("TB_VEHICLE","dn_vehicle");
define("TB_YEAR"," dn_year"); 
define("TB_COMMENT","dn_news_comment"); 
define("TB_USER_STAT","dn_user_statistics"); 
define("TB_DOWNLOADS","dn_download"); 
define("TB_VIEWED_NEWS","dn_news_viewed"); 
define("TB_VIEWED_SEARCH","dn_viewed_search"); 
define("TB_VIEWED_PAGE","dn_page_visited");

//PHASE II
define("TB_RESOURCES_SK","dn_resource_sk");
define("TB_LANG","dn_lang");
define("TB_PAGES","dn_pages");
define("TB_PAGES_DATA","dn_pages_data");

define("PER_PAGE",10);
define("PER_PAGE_OPTION",100);

//admin email setting
//define("EMAIL_FROM","mat@fynch.com"); 
define("EMAIL_FROM","asiamarketing@denso.com.sg"); 

//for banner image path
define('UPLOAD_BANNER_PATH','banners_img/');

/***************** Database Tables **********************/


/* End of file constants.php */
/* Location: ./application/config/constants.php */
