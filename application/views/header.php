<?php
$amw ='accordion-toggle menu-open';
$display = 'display:none;'; 
$users =  ($this->uri->segment(2) == "UserAccess" || $this->uri->segment(2) == "getUsers") ? 'accordion-toggle menu-open':'accordion-toggle';

 $sections =  ($this->uri->segment(2) == "getSection" || $this->uri->segment(2) == "getSection") ? 'accordion-toggle menu-open':'accordion-toggle';


$banners =  ($this->uri->segment(2) == "getBanners" || $this->uri->segment(2) == "getBanners") ? 'accordion-toggle menu-open':'accordion-toggle';
$questionnaire =  ($this->uri->segment(2) == "getQuestionnaire" || $this->uri->segment(2) == "getQuestionnaire") ? 'accordion-toggle menu-open':'accordion-toggle';
$pushmanagement =  ($this->uri->segment(2) == "getPushnotification" || $this->uri->segment(2) == "getPushnotification") ? 'accordion-toggle menu-open':'accordion-toggle';

$languageTranslation =  ($this->uri->segment(2) == "getLanguageTranslation" || $this->uri->segment(2) == "getLanguageTranslation") ? 'accordion-toggle menu-open':'accordion-toggle';

$vehicle =  ($this->uri->segment(2) == "getVehicleBrand" || $this->uri->segment(2) == "getVehicleModel"  || $this->uri->segment(2) == "getVehicles" || $this->uri->segment(2) == "importcsvproduct") ? 'accordion-toggle menu-open':'accordion-toggle';
$news =  ($this->uri->segment(2) == "getNewsCategory" || $this->uri->segment(2) == "getNews") ? 'accordion-toggle menu-open':'accordion-toggle';
$resources =  ($this->uri->segment(2) == "getResources") ? 'accordion-toggle menu-open':'accordion-toggle';
$stats =  ($this->uri->segment(2) == "getDownloadStatistics" || $this->uri->segment(2) == "viewdownloadstatistics") ? 'accordion-toggle menu-open':'accordion-toggle';
$genaral =  ($this->uri->segment(2) == "getCountry" || $this->uri->segment(2) == "getVehicleCategory" ||  $this->uri->segment(2) == "getVehicleSubCategory") ? 'accordion-toggle menu-open':'accordion-toggle';
if($this->uri->segment(2) == "getResourcesSK" || $this->uri->segment(2) == "getLanguages" || $this->uri->segment(2) == "getSection"|| $this->uri->segment(2) == "getPagesDataListSK" || $this->uri->segment(2) == "getPagesListSK" || $this->uri->segment(2) == "getPagesSK" || $this->uri->segment(2) == "getPagesDataSK"){ $amw ='accordion-toggle'; $display = 'display:block;'; $skw =  'accordion-toggle menu-open'; }else{ $display = 'display:none;';$skw = 'accordion-toggle';  }

if(checkPermissionHeader() == 1 && $amw != 'accordion-toggle menu-open'){ $display = 'display:block;';}else{$display = 'display:none;';} 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>DENSO Admin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">-->
    <meta name="keywords" content="DENSO Admin" />
    <meta name="description" content="DENSO Admin">
    <meta name="author" content="DENSO Admin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon"  type="image/png" href="<?php echo base_url(); ?>favicon.png">
    <!-- Font CSS (Via CDN) -->
    <!--<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
-->
    <!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/magnific/magnific-popup.css">

     <!-- Datatables CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/theme.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/loader.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/media.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

   <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Admin Modals CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-plugins/admin-modal/adminmodal.css">
    
    <!-- datepicker css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/bootstrap-datetimepicker.min.css" />



    <!-- Favicon -->
    
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
      
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/magnific/jquery.magnific-popup.js"></script>
    <!-- Datatables -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

    <!-- Datatables Tabletools addon -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.clickout.js"></script>
	<style>
		.paginate_button:hover{cursor:pointer;}
		table.dataTable thead > tr > th{ padding-left:10px !important;}
		table.dataTable thead th:first-child{background:transparent !important;}
		div.dataTables_length select{ width: 175px !important;}
		.dt-panelfooter{ display: none;}
		#edit1{display:none}
	</style>
</head>

<body class="dashboard-page datatables-page sb-l-o sb-r-c onload-check" data-spy="scroll" data-target="#nav-spy" data-offset="300">
	<div id="loading">
		<div id="loading-center">
		<div id="loading-center-absolute">
		</div>
		</div>
	</div>

<!-- Start: Main -->
    <div id="main">

        <!-- Start: Header -->
        <header class="navbar bg-light navbar-fixed-top">
            <div class="navbar-branding">
                <a class="navbar-brand" href="<?php echo base_url(); ?>administration/getUsers"> <img src="<?php echo base_url(); ?>assets/img/logos/logo.png" class="img-responsive pull-left" />
					
                </a>
                <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines pull-left"></span>
                <ul class="nav navbar-nav pull-right hidden">
                    <li>
                        <a href="#" class="sidebar-menu-toggle">
                            <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="nav navbar-nav navbar-left pt20 text-logo"></div> 
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown"> 
						<i class="fa fa-user"></i>
                        <span>Administrator Settings</span>
                        <span class="caret caret-tp hidden-xs"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
                        <li class="of-h">
                            <a href="#changepassword" class="fw600 p12 animated animated-short fadeInUp model-open">
                                <span class="fa fa-user pr5"></span> Change Password
                                
                            </a>
                        </li>
                        <li class="br-t of-h">
                            <a href="<?php echo base_url(); ?>logout" class="fw600 p12 animated animated-short fadeInDown">
                                <span class="fa fa-power-off pr5"></span> Logout </a>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>
        <!-- End: Header -->

        <!-- Start: Sidebar -->
        <aside id="sidebar_left" class="nano nano-primary affix has-scrollbar">
            <div class="nano-content">

                <!-- Start: Sidebar Header -->
                <header class="sidebar-header">
                    <div class="user-menu">
                        <div class="row text-center mbn">
                            <div class="col-xs-4">
                                <a href="dashboard.html" class="text-primary" data-toggle="tooltip" data-placement="top" title="Dashboard">
                                    <span class="glyphicons glyphicons-home"></span>                                </a>                            </div>
                            <div class="col-xs-4">
                                <a href="pages_messages.html" class="text-info" data-toggle="tooltip" data-placement="top" title="Messages">
                                    <span class="glyphicons glyphicons-inbox"></span>                                </a>                            </div>
                            <div class="col-xs-4">
                                <a href="pages_profile.html" class="text-alert" data-toggle="tooltip" data-placement="top" title="Tasks">
                                    <span class="glyphicons glyphicons-bell"></span>                                </a>                            </div>
                            <div class="col-xs-4">
                                <a href="pages_timeline.html" class="text-system" data-toggle="tooltip" data-placement="top" title="Activity">
                                    <span class="glyphicons glyphicons-imac"></span>                                </a>                            </div>
                            <div class="col-xs-4">
                                <a href="pages_profile.html" class="text-danger" data-toggle="tooltip" data-placement="top" title="Settings">
                                    <span class="glyphicons glyphicons-settings"></span>                                </a>                            </div>
                            <div class="col-xs-4">
                                <a href="pages_gallery.html" class="text-warning" data-toggle="tooltip" data-placement="top" title="Cron Jobs">
                                    <span class="glyphicons glyphicons-restart"></span>                                </a>                            </div>
                        </div>
                    </div>
                </header>
                <!-- End: Sidebar Header -->

                <!-- sidebar menu -->
                <ul class="nav sidebar-menu">
                 <!-- sidebar resources -->
                   <li>
					   <?php if(checkPermissionHeader() == 1){?>
                        <a class="<?php echo $amw;?>" href="#">
                            <span class="fa fa-user"></span>
                            <span class="sidebar-title">AMW Administration</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" >
							
							<li>
                                <a class="<?php echo $users;?>" href="#">
                                    <span class="glyphicons glyphicons-user"></span>Users
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <!--<li><a href="<?php echo base_url(); ?>administration/UserAccess">Manage Users Access</a> </li>-->
                                    <li><a href="<?php echo base_url(); ?>administration/getUsers">Manage Users</a> </li>
                                 </ul>
                            </li>
                            <!-- quetionariie -->
                            <li>
                                <a class="<?php echo $questionnaire;?>" href="#">
                                    <span class="glyphicons glyphicons-user"></span>Questionnaire
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <!--<li><a href="<?php echo base_url(); ?>administration/UserAccess">Manage Users Access</a> </li>-->
                                    <li><a href="<?php echo base_url(); ?>administration/getQuestionnaire">Manage Questionnaires</a> </li>
                                 </ul>
                            </li>
                             <!-- end quetionariie -->
                             <!-- push managemt -->
                            <li>
                                <a class="<?php echo $pushmanagement;?>" href="#">
                                    <span class="glyphicons glyphicons-user"></span>Push Management
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>administration/getPushnotification">Push Management </a> </li>
                                 </ul>
                            </li>
                             <!-- push managemt -->
                            <li>
                                <a class="<?php echo $genaral;?>" href="#">
                                    <span class="glyphicon glyphicon-th"></span>General
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>administration/getVehicleCategory">Manage Categories</a></li>
                                    <li><a href="<?php echo base_url(); ?>administration/getVehicleSubCategory">Manage Sub-Categories</a></li>
                                    <li><a href="<?php echo base_url(); ?>administration/getCountry">Manage Country</a></li>
                                 </ul>
                            </li>
                            <li>
                                <a class="<?php echo $vehicle;?>" href="#">
                                    <span class="glyphicons glyphicons-car"></span>Vehicle Parts
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    
                                    <li><a href="<?php echo base_url(); ?>administration/getVehicleBrand">Manage Brands</a></li>
                                    <li><a href="<?php echo base_url(); ?>administration/getVehicleModel">Manage Models</a></li>
                                    <li><a href="<?php echo base_url(); ?>administration/getVehicles">Manage Vehicles Parts</a></li>
                                    <li><a href="<?php echo base_url(); ?>administration/importcsvproduct">Import Parts via CSV/EXCEL</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="<?php echo $resources;?>" href="#">
                                    <span class="glyphicons glyphicons-cloud-download"></span>Resources
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>administration/getResources">Manage Resources</a> </li>
                                </ul>
                            </li>
                            <li>
                                <a class="<?php echo $news;?>" href="#">
                                    <span class="fa fa-newspaper-o"></span>News And Updates
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav"> 
									<li><a href="<?php echo base_url(); ?>administration/getNewsCategory">Manage News Category</a> </li>
                                    <li><a href="<?php echo base_url(); ?>administration/getNews">Manage News</a> </li>
                                 </ul>
                            </li>
                            <li>
                                <a class="<?php echo $stats;?>" href="#">
                                    <span class="glyphicon glyphicon-stats"></span>Statistics
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>administration/getDownloadStatistics">Download Statistics</a> </li>
                                </ul>
                            </li>

                            <li>
                                <a class="<?php echo $banners;?>" href="#">
                                    <span class="fa fa-newspaper-o"></span>Banners 
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav"> 
                                    <li><a href="<?php echo base_url(); ?>administration/getBanners">Manage Banners</a> </li>
                                 </ul>
                            </li>
                             
                            
                        </ul>
						<?php } ?>
                    </li>
                     <li>
                        <a class="<?php echo $skw;?>" href="#">
                            <span class="fa fa-male"></span>
                            <span class="sidebar-title">PMK Administration</span>
                            <span class="caret"></span>
                        </a>
                        
                        <ul class="nav sub-nav skw" style="<?php echo $display;?>"> 
                                    <?php if(checkPermissionHeader() == 1){?>
                                    <li <?php echo ($this->uri->segment(2) == "getResourcesSK")?'class="active"':''; ?>><a href="<?php echo base_url(); ?>administration/getResourcesSK"> <span class="glyphicon glyphicon-cloud"></span>Manage Resources PMK</a> </li>
                                    <?php } ?>
                                     </li>

 
                                   <li>
                                        <a class="<?php echo $languageTranslation;?>" href="#">
                                            <span class="glyphicon glyphicon-transfer"></span> Language Translation
                                            <span class="caret"></span> </a>
                                        <ul class="nav sub-nav">
                                            <li <?php echo ($this->uri->segment(2) == "getPagesListSK" || $this->uri->segment(2) == "getPagesSK")?'class="active"':''; ?>><a href="<?php echo base_url(); ?>administration/getLanguages">Manage Language </a> </li>
                                            <li <?php echo ($this->uri->segment(2) == "getPagesListSK" || $this->uri->segment(2) == "getPagesListSK")?'class="active"':''; ?>><a href="<?php echo base_url(); ?>administration/getSection"> Manage Section </a> </li>
                                            <li <?php echo ($this->uri->segment(2) == "getPagesListSK" || $this->uri->segment(2) == "getPagesSK")?'class="active"':''; ?>>
                                    <a href="<?php echo base_url(); ?>administration/getPagesListSK"> Manage Language Translation</a>
                                         </ul>
                                    </li> 
                                
                        </ul>
                    </li>
                 <!-- sidebar bullets -->   
              </ul>
            </div>
      </aside>

    <!-- End: Main -->
