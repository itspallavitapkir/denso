<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    
    <title>Masterpiece Manager</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet">   
    <link href="<?php echo base_url(); ?>css/media-query.css" rel="stylesheet"> 
	 <link rel="icon" href="<?php echo base_url(); ?>assets/img/inner-logo.png" type="image/gif" sizes="16x16"> 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>html5shiv.min.js"></script>
      <script src="<?php echo base_url(); ?>respond.min.js"></script>
    <![endif]-->  
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script> 
    <script src="<?php echo base_url(); ?>js/v11.js"></script>
    <style>
		#loading{
			left: 47%;
			position: absolute;
			top: 50%;
		}
    </style>
</head>
	<body>
		<a href="<?php echo base_url(); ?>logout">logout</a>
