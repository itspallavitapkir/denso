<?php
$email_address = (empty($email_address)) ? "" : $email_address;
?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to DENSO Login</title>
<link rel="icon" href="<?php echo base_url();?>favicon.png" type="image/png">
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url();?>css/denso.css" rel="stylesheet" type="text/css">
<!--<link href="css/font.css" rel="stylesheet" type="text/css">-->
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<link href="<?php echo base_url();?>css/media.css" type="text/css" rel="stylesheet">
<!--Script file-->
<!--Script file-->

</head>

<body>

<div class="wrapper">
<div class="page_width">
 <!--container main_section end here--> 
<header class="headsection"> 
<div class="container main_section">
<div class="row">
<div class="col-lg-6 logo_top_login">
<div class="logo" id="loginpage_logo"><h1><a href="<?php echo base_url();?>index"><img src="<?php echo base_url();?>img/logo.png" border="0" title="DENSO"></a></h1></div>
</div>
    
 </div>  
 </div>
 </header>
 <!--container main_section end here--> 
 
  <!--middle_section Start here--> 
 <div class="middle_section">
  <div class="container Login_Register_section">
 <div class="row">
 <div class="col-lg-12">

 <?php if(!empty($validation_message)){?>
 <div class="form-group col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 mar">
						<div class="alert alert-dismissable alert-danger" role="alert"><?php echo $validation_message; ?></div>
					</div>	
					<div class="col-md-4"></div>
				 </div> 
	 </div> 
 <?php } ?>
 <?php if(!empty($message)){?>
 <div class="form-group col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 mar">
						<div class="alert alert-dismissable alert-success" role="alert"><?php echo $message; ?></div>
					</div>	
					<div class="col-md-4"></div>
				 </div> 
	 </div> 
 <?php } ?>	
 <div class="login_wrapper">				  
<div class="login_detail_section"> 

 <div class="logtitle">Login</div>
 <form method="post" action="<?php echo base_url(); ?>AMW/index" name="frmUserlogin" id="frmUserlogin">
		 <div id="logsectform">
		  <div class="form-group logsect">
		  <input type="text" class="form-control" id="uemail" name="uemail" placeholder="Email Address" value="<?php echo $email_address;?>">
		  <span class="validationerror" id="uemailInfo"></span>
		 </div>
		<div class="form-group">
		  <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Password">
		  <span class="validationerror" id="pwdInfo"></span>
		</div>
		<div class="form-group forgot_pass">
		  <label for="pwd"><a href="<?php echo base_url();?>forgotpassword">Forgot Password?</a></label>
		</div>
		<div class="form-group log_btn">
		   <input type="submit" class="btn btn-primary" value="AMW" name="cmdLogin" id="cmdLogin">   
		</div>
		<div class="form-group log_btn">
		<input type="submit" class="btn btn-primary" value="PMK" name="cmdLoginSales" id="cmdLoginSales">
		</div>
		<div class="form-group pmk_public">
			<a class="btn btn-primary" href="<?php echo base_url();?>PMK/index">PMK Public Access</a>
		</div>
		 </div>
		</div>
 </form>
 </div>
 </div>
 
  </div>
   </div>
    </div>
 
 
 </div>
  <!--middle_section end here-->  
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/validate.js"></script>
<script language="javascript">
var loginemail = $("#uemail");
	var uemailInfo = $("#uemailInfo");		
var pwd = $("#pwd");
	var pwdInfo = $("#pwdInfo");	 


$("#frmUserlogin").submit(function(){ 
	
	var flag=1;
	
	if(!validateEmpty(loginemail, uemailInfo, "the email address")){
		flag = 0;
	}
	
	if(loginemail.val() != ""){
		if(!validateEmail(loginemail, uemailInfo)){
			flag = 0;
		}
	}
	
	if(!validateEmpty(pwd, pwdInfo, "the password")){
		flag = 0;
	}
	
	if(pwd.val()!=""){
		if(CheckPasswordText(pwd, pwdInfo)){
			//alert("test");
			//flag = 1;
		}
		else if(pwd.val().length<3 || pwd.val().length>50){ 
				$('#pwdInfo').html("The password length should be min 3 and max 50");
				flag=0;
		} 
		else{
			flag = 0;
		}
	}
	
	if(flag){ 
		//$("#frmLoginAccount").submit();  
		return true;
	}
	else{
		return false;
	}
});
</script>
<footer class="footer_section">
  <div class="container foot_text">
 <div class="row">
 <div class="col-lg-12">
 <p>Copyright DENSO &copy;2015. All rights reserved</p>
 </div>   
</div>
</div>   
</footer>
  
 
</div>
</div>
<!--Page_width end here--> 
</body>
</html>
