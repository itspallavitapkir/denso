<?php
$CI =& get_instance();
$CI->load->model('administration_model');
?>

<!--container main_section end here--> 
<!--middle_section Start here--> 
 <div class="middle_section searchmiddle" ng-app="myApp">
 
  <div class="container page_title" id="searchsection">
	 <div class="row">
		 <div class="col-lg-12 searchtitle" id="refresh_section">
			<h2>Search By</h2>
			<div class="Search_refresh"> 
		<button type="button" id="refresh" class="search_field srch-btn" onclick="refreshform();">Refresh Search</button>
	  </div>
		 </div>
	 </div>
  </div>
   
  <div class="container search_page_select search-selectionmain" >
 <div class="row">
	
	<form role="form" ng-controller="autocompleteController" id="frmSearchByBrand" name="frmSearchByBrand" class="formsearch-section">
    
	<input type="hidden" id="searchBy" name="searchBy" value="brand"/>
    
    <div class="col-lg-3 col-xs-12"> 
	   <div class="form-group">
		<input type="hidden" id="selectedbrandid" name="selectedbrandid" value="{{selectedBrands.v_brand_id}}"/>   
		<input type="text" autocomplete="off" name="txtbrand" id="txtbrand" class="part_no" placeholder="Vehicle Brand" ng-model="selectedBrands" typeahead="b as b.brand for b in brands | filter:{brand:$viewValue}:startsWithBrand">
		
		<i class="icon-search nav-search-icon"></i>
	  </div> 
    </div>
    <div class="col-lg-3 col-xs-3"> 
       <div class="form-group">
		<input type="hidden" id="selectedmodelid" name="selectedmodelid"  value="{{selectedModels.v_model_id}}"/>   
		<input type="text" autocomplete="off" name="txtmodel" id="txtmodel" class="part_no" placeholder="Vehicle Model" ng-model="selectedModels" typeahead="b as b.v_model for b in models | filter:{v_model:$viewValue}:startsWithModel">
	   </div> 
    </div>
    <div class="col-lg-2 col-xs-3"> 
		<div class="form-group">   
			<input type="hidden" id="selectedyear" name="selectedyear" value="{{selectedYears.year}}"/>
			<input type="text" autocomplete="off" name="selyear" id="selyear" class="part_no" placeholder="Year/Year Range" ng-model="selectedYears" typeahead="b as b.year for b in years | filter:{year:$viewValue}:startsWithYear">
		</div>
   </div>
   
	<div class="col-lg-3 col-xs-12 srchmain-denso">
		<div class="form-group">
		 <button class="search_field" id="searchByBrand" onclick="searchRecordFilter('brand');">Search</button>
	</div>
   </div>
	</form>
 </div>
 </div>
 
  <div class="container page_title search2" id="searchsection">
	<div class="row">
		<div class="col-lg-12 searchtitle">
			<h2>Search By Car Maker Part No.</h2>
		</div>
	</div>
  </div>
  <div class="container search_page_select search-selectionmain">
 <div class="row">
   <div class="col-lg-3 searching-blcok25" >
    <form role="form" ng-controller="autocompleteController2" id="frmcarpn" name="frmcarpn"> 
		<input type="hidden" id="searchBy" name="searchBy" value=""/>
		<div class="form-group">
			<input type="text" autocomplete="off" name="car_maker_part_no" id="car_maker_part_no" class="part_no" placeholder="Vehicle Car Maker Part No." ng-model="selectedCarMaker" typeahead="v as v.car_maker_pn for v in vehicles | filter:$viewValue:startsWithCarMakerPn">
			<span class="validationerror" id="infoCarMaker"></span>
		</div>
		</div>
	<div class="col-lg-6"> 
		<div class="form-group">
			<div class="col-lg-3"> 
				<button type="submit" class="search_field srch-btn" onclick="searchRecordFilter('carpn');">Search</button>
		</div> 
	</div> 
  </form>
</div>
   
  
 </div>
 </div>
 
 <div id="searchsection" class="container page_title search2">
	<div class="row">
		<div class="col-lg-12 searchtitle">
			<h2>Search By Category</h2>
		</div>
	</div>
  </div>
 <div class="container search_page_select search-selectionmain">
 <div class="row">
   <div class="col-lg-3 searching-blcok25" >
    <form role="form" id="frmcategory" name="frmcategory"> 
		<input type="hidden" id="searchBy" name="searchBy" value=""/>
		<div class="form-group">
			<div class="pm"><div class="dataTables_length1">
						<div class="multiple-selection">
						<div class="rt_aw"></div>	
						<input type="hidden" name="txt_resource" id="txt_resource" />	
						<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):
								?>
								<li>
                                <input type="checkbox" name="dd_resources[]" value="<?php echo $cat['id']?>" placeholder="" id="dd_resources" class="events-category">
                               
								<?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?> 
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" value="<?php echo $subcat['id']?>" id="subcategory_<?php echo $subcat['id']?>" class="child events-child-category">
											<span><?php echo $subcat['sub_category'];?></span>
                                        </li>
									 
								 <?php endforeach;
								 ?>
								 </ul>
								 <?php
								    }
								 ?>
								    </li>
								    <?php
								 endforeach;
								 ?>
								 
								 <?php
									}
								?>
							</ul>
						</div>
						</div>
					</div> 
		</div>
   </div>
	<div class="col-lg-6"> 
		<div class="form-group">
			<div class="col-lg-3"> 
				<button type="submit" class="search_field srch-btn" onclick="searchRecordFilter('category');">Search</button>
		</div> 
	</div> 
  </form>
</div>
   
  
 </div>
 </div>
 <div class="container resource_page" id="searchbrabd">
<div class="row">
<div class="col-lg-12">
<input type="hidden" id="start" name="start"/>
<div class="info"><strong>Please enter (on type the autosuggestion list will display) the Vehicle Brand, Model, Year/Year Range and/or Category to search<br/>Or Please enter Car Maker Part Number</strong></div>
<div class="table-responsive" id="search_result" id="table1">
	<div class="ajax-loading"><img src="<?php echo base_url();?>img/ajax-loader.gif" /></div>
	
</div>

  
  
  </div>
  
 </div>
 </div>

</div>
  <!--middle_section end here--> 
  <style>
	  .pm {
position: relative;
}
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
<!--background: url("<?php //echo base_url();?>images/downArrow.gif") no-repeat scroll 95% center #FFFFFF !important;-->
}
.set_category {
    background-color: #fff;
    height: 150px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 31px;
    width: 266px;
    z-index: 99999 !important; border:1px solid #ccc; border-top:none;
}
.set_category li input#dd_resources {
    margin: 3px 8px 14px 0;float: left;
}
.event-children li {
    float: left;
    margin: 0 0 4px;
    width: 100%;
}
.set_category span {
    float: left;
    margin: 4px 0 0;
}
.event-children li input {
    float: left;
    margin: 3px 8px 6px 0;
}
.event-children span {
    float: left;
    margin: 0 !important;
}
#vehicle-categories > li {
    float: left;
    width: 100%;
}
</style>


<script type="text/javascript" src="<?php echo base_url();?>js/angular.min.js"></script> 
<script src="<?php echo base_url();?>js/ui-bootstrap-tpls-0.9.0.js"></script>
<script> 
$(window).load(function() {
	$('.ajax-loading').hide();
	$("#vehicle-categories").hide(); 
	$('.events-category').next('.event-children').css('display', 'none');
});

$(document).click(function(e) {
    if(e.target.id !== "select-category" &&  !$("#vehicle-categories").find(e.target).length && e.target.className !== "rt_aw")
    {
     $("#vehicle-categories").hide(); 
    }
});

$(document).ready(function() {
		$('.ajax-loading').hide(); 
		$("#vehicle-categories").hide();
		$('#select-category,.rt_aw').click( function(){
			
			$("#vehicle-categories").toggle();
			
		});
	 
	 $('.events-category').click( function(){
		var c = this.checked; 
		 //$("#searchBy").val('category'); 
		
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
			//searchRecordFilter($("#searchBy").val());
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
			//searchRecordFilter($("#searchBy").val());
		}
	});
	
	$('.events-category,.events-child-category').click( function(){
		var c = this.checked; 
		 $("#searchBy").val('category'); 
		
		if(this.checked==true){  
			searchRecordFilter($("#searchBy").val());
		}
		else{ 
			searchRecordFilter($("#searchBy").val());
		}
	});
	
	$('.events-category').change( function(){
    var c = this.checked; 
    if( c ){
        $(this).next('.event-children').css('display', 'block'); 
        
    }else{
        $(this).next('.event-children').css('display', 'none');
    }
	});
});
</script>
<script>
	//Define an angular module for our app
var app = angular.module('myApp', ['ui.bootstrap']);
//var appscrl = angular.module('scrollApp', ['infinite-scroll']);

app.controller('autocompleteController', function($scope, $http) {
  getVBrands(); // Load all which is matching brands 
  
  function getVBrands(){  
  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_brand").success(function(data){
        $scope.brands = data; 
       });
  };
  getVModels();
  function getVModels(){  
  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_model").success(function(data){
        $scope.models = data;
       });
  };
  getYear();
  function getYear(){  
  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_year").success(function(data){
        $scope.years = data;
       });
  };
  $scope.startsWithBrand = function(brand, viewValue) { 
		
		return brand.substr(0, viewValue.length).toLowerCase() === viewValue.toLowerCase();
	  
  } 
  $scope.startsWithModel = function(model, viewValue) {
		return model.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
  } 
  $scope.startsWithYear = function(year, viewValue) {
	return year.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
  } 
});
app.controller('autocompleteController2', function($scope, $http) {
 
  getCarMakerPartNo();
  function getCarMakerPartNo(){
	  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_car_maker").success(function(data){ 
			$scope.vehicles = data;
       });
       $scope.startsWithCarMakerPn = function(vehicles, viewValue) {
		return vehicles.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
	  } 
       
  }; 
});

$(document).ready(function(){ 
	
	/*$(window).scroll(function(){ 
		
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			
			 searchRecord($("#searchBy").val()); 
		}
	});*/
});
	
	
	function refreshform(){
		
		$('#frmSearchByBrand')[0].reset();
		$('#frmcarpn')[0].reset();
		$('#frmcategory')[0].reset();
		$('#selectedbrandid').val("");
		$('#selectedmodelid').val(""); 
		$("#search_result").html("");
		$("#vehicle-categories").hide(); 
		$(".events-category").next('.event-children').css('display', 'none');
		
	}
	
	function searchRecordFilter(searchBy){ 
			
			$(".info").html(""); 
			$("#searchBy").val(searchBy); 
			
			$("#start").val("0"); 
			searchRecord($("#searchBy").val());
			
			$(window).scroll(function(){
				if ($(window).scrollTop() == $(document).height() - $(window).height()){ 
						
						searchRecord($("#searchBy").val());
					
				}
			});  
	}
	 

	function searchRecord(searchBy){    
		//$("#vehicle-categories").hide();
		//$('#searchByBrand').on('click', function (e) {
			//alert($("#txtbrand").val());
			
			
				$("#searchBy").val(searchBy); 
			
			if(searchBy=='brand'){
				var formData = new FormData($('#frmSearchByBrand')[0]);
				formData.append("start", $("#start").val());  
			}
			else if(searchBy=='carpn'){
				var formData = new FormData($('#frmcarpn')[0]); 
				formData.append("start", $("#start").val()); 
			}
			else if(searchBy=='category'){
				var formData = new FormData($('#frmcategory')[0]); 
				formData.append("start", $("#start").val()); 
			}
			 

				$('.ajax-loading').show();  
				$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					async: false,
					processData: false,
					url: "<?php echo base_url(); ?>frontcontroller/search_result1",
					data: formData,
					//data:{selectedbrandid:$("#selectedbrandid").val(),selectedmodelid:$("#selectedmodelid").val(),selyear:$("#selyear").val(),dd_resources:dd_category_val,dd_subresources:dd_sub_category_val},
					beforeSend: function(){
						$('.ajax-loading').show();
						$('.ajax-loading-div').show();    
					},
					complete: function(){
						$('.ajax-loading').hide(); 
						$('.ajax-loading-div').remove(); 
					},
					success: function(json){
					$("#start").val(json.paginate); 
					if(json.start != 0){
						//$('.ajax-loading').show();  
						$("#search_result tbody").append(json.table); 
						$('.ajax-loading-div').remove();  
					}
					else{
						$("#search_result").html(json.table);
						$('.ajax-loading-div').remove();  
					}
					},
					error: function(){} 	    
				});
			
		//});
	} 

</script>  
