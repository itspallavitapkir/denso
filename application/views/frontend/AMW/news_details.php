<!--container main_section end here--> 
<!--middle_section Start here--> 
 <div class="middle_section">
 

<div class="container page_title">
<div class="row">
<div class="col-lg-12">
<h2>Denso News</h2>
<a class="back_link" href="<?php echo base_url();?>AMW/news">Back</a>
</div>
</div>
</div>


<div class="container news_page">
 <div class="row">
  <div class="col-lg-8">
<?php if(count($news_details)>0){
	foreach($news_details as $news):
	?>
<h3><?php echo $news['news_heading']?></h3>
<div class="discription">
<?php if($news['news_description'] != ""){
	if(strlen($news['news_description'])>1000){
	?>
<div class="incomplete"><?php echo substr($news['news_description'],0,1000);?>...<br><span class="more" style="cursor:pointer;font-weight:bold;">Read More</span></div>
<div class="complete"><?php echo $news['news_description'];?></div> 
<?php
}
else
{
	?>
	<div><?php echo $news['news_description'];?></div> 
	<?php
}
?>




<?php }else{ ?>
	<p class="text-description">-</p>
<?php } ?>
<?php if($news['news_filename']!=""){?>
	<p>For detailed information please <a target="_blank" href="<?php echo base_url();?>news_files/<?php echo $news["news_filename"];?>">click here</a></p>
<?php }
endforeach;
} ?>
</div>

 </div>
 
<div class="col-lg-4">
<?php echo $html; ?>

</div>
  
 </div>
 </div>
 
 
 
 
 <div class="container categoresection">
<div class="row">
 
<div class="col-lg-8" id="sharing">
<div class="comments">
<input type="hidden" id="newsid" name="newsid" value="<?php echo $_GET['nid'];?>" />	
<?php if(count($news_comments)>0){
	?>
	<h3><?php echo (count($news_comments)>0?count($news_comments):0);?> Comment(s)</h3> 
	<?php
	foreach($news_comments as $comment): 
	?>	

<div data-has-form="false" data-comment-id="1" class="comment">
	<!--<img width="65" height="58" alt="" src="http://exceptionaire.co.in/densodev/img/user_img.png" class="avatar">-->
	<div class="main-comment">
		<div class="user-name"><?php echo $comment['first_name']." ".$comment['last_name']; ?>:</div>
		<div class="user-date"><?php echo date('D, d F H:i:s', strtotime($comment["date_added"]));?><span class="user-site"></span></div>
		<p><?php echo $comment['comments'];?></p>
	</div>
</div>

<?php endforeach;

}?>
</div>

<span>Leave a Comment </span>
<div id="message"></div>
<!--<div class="form_field section">
<label class="fild_leble">Description :</label>
<textarea placeholder="Description" aria-invalid="false" aria-required="true" class="desccription" maxlength="500" rows="5" cols="40" name="your-message"></textarea>
</div>
-->
<div class="form_field section">
<label class="fild_leble">Comment <span class="validationerror">*</span> :</label>
<textarea name="comment" id="comment" placeholder="Comment" aria-invalid="false" aria-required="true" class="desccription" maxlength="500" rows="5" cols="40"></textarea>
<span id="infoComment"  class="text-danger"></span>
</div>

<div class="form_field section">
<p><strong>Enter a captcha</strong></p>
<img src="<?php echo base_url();?>captcha.php" id="captcha" /><br/>
<!-- CHANGE TEXT LINK -->
<a href="javascript:void(0);" onclick="document.getElementById('captcha').src='<?php echo base_url(); ?>captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();"
    id="change-image">Not readable? Change text.</a><br/><br/>
	 <input type="text" class="form-field" name="captcha" id="captcha-form" autocomplete="off" placeholder="Please enter the above word"/>
	 <span id="infoCaptcha"  class="text-danger"></span>
    </div>
<div class="form_field section">
<button class="news_sub" id="btn_save">Submit</button>
</div>

</div>


</div>
</div>
<script src="<?php echo base_url();?>js/validate.js" type="text/javascript"></script>
<script>
$(".complete").hide();     
$(".more").click(function(){
    $(".complete").show(); 
    $(".incomplete").hide(); 
});	

$(document).ready(function(){   
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		flag=1;
		var newsid = $("#newsid").val();
		
		var comment = $("#comment");
			var nhInfo = $("#infoComment");  
		var captcha = $("#captcha-form");
			var captchaInfo = $("#infoCaptcha");  
				
		if(!validateEmpty(comment, nhInfo, "the comment")){
			flag = 0;
		}
		if(!validateEmpty(captcha, captchaInfo, "the captcha")){
			flag = 0;
		}	
		if(flag){
			if(newsid!=""){
				$.ajax({
					type: "POST",
					dataType: "json", 
					url: "<?php echo base_url(); ?>postcomment",
					data: {"nid":newsid,"comment":comment.val(),"captcha":captcha.val()},
				}).success(function (json) {
					//console.log(json);
					if(json.msg){
						$("#message").html(json.msg);
					}
					else if(json.msg1){
						$("#message").html(json.msg1);
					}
					$("#message div").fadeOut(8000);
					if(json.msg){
						 comment.val("");
					 }
					 $("#captcha-form").val("");
				}); 
			} 
		}
		else{
			return false;
		}
	});
});
</script>