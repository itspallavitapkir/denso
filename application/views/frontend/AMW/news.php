<!--container main_section end here--> 
<!--middle_section Start here--> 
 <div class="middle_section">
 
  <div class="container page_title">
 <div class="row">
 <div class="col-lg-12">
 <h2>DENSO News</h2>
 </div>
 </div>
 </div>
 
 <div class="container news_page">
 <div class="row">
  <div class="col-lg-12">
   <?php echo $html; ?>
  </div>
  
 </div>
 </div>
 </div>
 

  
   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" id="login_popup">
    <div class="modal-content">
    
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="<?php echo base_url();?>img/close_img.png" alt=""></span></button>
        <h4 class="modal-title" id="myModalLabel">Login</h4>
      </div>
      
      <div class="modal-body">
      
<div class="form-group">
  <label for="usr">Username:</label>
  <input type="text" class="form-control" id="usr" placeholder="Username">
</div>

<div class="form-group">
  <label for="pwd">Password:</label>
  <input type="password" class="form-control" id="pwd" placeholder="Password">
</div>

<div class="form-group forgot_pass">
  <label for="pwd"><a href="#">Forgot Password?</a></label>

</div>

      </div>
      
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        <button type="button" class="btn btn-primary">Login</button>
      </div>
      
    </div>
    
  </div>
   </div> 
  
  
  
  
  
</div>
  <!--middle_section end here--> 
 
