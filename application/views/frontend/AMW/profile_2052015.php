<!--container main_section end here--> 
 
  <!--middle_section Start here--> 
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(".flip2").click(function(){
$(".pane2").slideToggle("slow");
});

<?php if(!empty($validation_message)) { ?>
	$(".flip2").trigger('click');
<?php } ?>
});

 
</script>
<div class="middle_section"> 
  <div class="container page_title profile_sect">
 <div class="row">
 <div class="col-lg-12">
 <h2>Profile </h2>
 <?php if(!empty($message)) { ?><div class="makeText" id="message"><div class='pull-left makeText  alert alert-success'><?php echo $message;?></div></div><?php } ?>
 <!--Update section-->
 <div class="update_section">
 <ul>
 <li class="left_first">First Name :</li>
  <li><?php echo $firstname; ?></li>
  <li class="left_first">Last Name : </li>
  <li><?php echo $lastname; ?> </li>
  <li class="left_first">Email Address : </li>
  <li><?php echo $email_address; ?></li>
  <li class="left_first">Phone Number :</li>
  <li><?php echo $contact_details; ?> </li>
  <li class="left_first">Country :</li>
  <li><?php echo $country_name; ?> </li>
  <li class="left_first">Company :</li>
  <li><?php echo $company; ?> </li>
 </ul>
 <div class="update_btn"><button type="submit" class="btn btn-default main_profile_btn flip2">Update Profile</button> </div>
 
 </div>
 <!--Update section-->
 
 <div class="comeing_soon pane2" style="display:none;">
 
  <div class="main_profile">
     <div class="row main_profile_title">    
 	 	<h2>Edit Profile</h2>
     </div>
     
     <form class="form-inline" name="frmProfile" id="frmProfile" action="<?php echo base_url(); ?>profile" method="post">
			<?php if(!empty($validation_message)){?>
                <div class="form-group col-md-12">
                    <div class="alert alert-dismissable alert-danger" role="alert"><?php echo $validation_message; ?></div>
                </div>
			<?php } ?>
     	<div class="row field_space">
              <div class="form-group col-xs-6">
            
                    <label class="profile_label profile_field" for="txt_first_name">First Name</label>
                    <input type="text" class="profile_input profile_field" name="txt_first_name" id="txt_first_name" value="<?php echo $firstname;?>">
   
              </div>
              
              <div class="form-group col-xs-6">
             
                    <label class="profile_label profile_field" for="txt_last_name">Last Name</label>
                    <input type="text" class="profile_input profile_field" name="txt_last_name" id="txt_last_name" value="<?php echo $lastname;?>">
            
              </div>
         </div>
          
         
         <div class="row field_space"> 
              <div class="form-group col-xs-6">
                    <label  class="profile_label profile_field" for="country">Country</label>
                    <div class="dropdown  profile_field_select"> 
						  <select class="form-control dropdown-toggle" id="country" name="country">
							  <?php foreach($AllContries as $country):
							  if($country_id  == $country['id']){
							  ?>
								<option value="<?php echo  $country["id"];?>" selected=""><?php echo $country["name"];?></option>
							  <?php 
							  }else{ ?>
							    <option value="<?php echo  $country["id"];?>"><?php echo $country["name"];?></option>
							  <?php 
							  }?>
							  <?php endforeach; ?>	
								
						  </select>
                    </div>
              </div>
               <div class="form-group col-xs-6">
                <!-- <div class=""> -->
                    <label class="profile_label profile_field" for="txt_contactdetails">Contact Number</label>
                    <input type="text" class="profile_input profile_field" name="txt_contact_number" id="txt_contact_number" value="<?php echo $contact_details; ?>">
             
              </div> 
          </div>
           
        
          <div class="row field_space">
              <div class="form-group col-xs-6">
                <!-- <div class=""> -->
                    <label class="profile_label profile_field" for="txt_contactdetails">Company</label>
                    <input type="text" class="profile_input profile_field" name="txt_company" id="txt_company" value="<?php echo $company; ?>">
              </div> 
         </div>
          <div class="row main_profile_submit">
              <button type="submit" class="btn btn-default main_profile_btn">Ok</button>
              <button type="submit" class="btn btn-default main_profile_btn">Close</button>
          </div>
     </form>
 </div>
  
 </div>
 </div>
 </div>
 </div>
 
 
</div>
  <!--middle_section end here--> 
 
