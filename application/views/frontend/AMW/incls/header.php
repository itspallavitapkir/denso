<?php
$resources =  ($this->uri->segment(1) == "viewresources") ? 'class="active"':'';
$userprofile =  ($this->uri->segment(1) == "profile") ? 'class="active"':'';
$news =  ($this->uri->segment(1) == "news" || $this->uri->segment(1) == "news_details") ? 'class="active"':'';
$search =  ($this->uri->segment(1) == "search" || $this->uri->segment(1) == "details") ? 'class="active"':'';
$userdata_header = $this->session->userdata('auth_densouser'); 
?> 
<header class="headsection"> 
<div class="container main_section">
<div class="row">
<div class="col-lg-6"><div class="logo"><h1><a href="<?php echo base_url();?>AMW/index"><img alt="alt="DENSO"" src="<?php echo base_url();?>img/logo.png" border="0" title="DENSO"></a></h1></div></div>
    <div class="col-lg-6 header_rtsection">
    <div class="bs-example">
	<div class="welcome">Welcome, <?php echo $userdata_header['first_name']." ".$userdata_header['last_name'];?>
     <?php if($userdata_header['user_type']==1){
        echo "(Wholeseller)";
        } else if($userdata_header['user_type']==2){
           echo "(Distributor)"; 
        }
        ?>   
    </div>
    <nav role="navigation" class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           <!-- <a href="#" class="navbar-brand">Brand</a>-->
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="firstt_sect"><a <?php echo $search;?> href="<?php echo base_url();?>AMW/search/">Search</a></li>
                <li><a <?php echo $resources;?> href="<?php echo base_url();?>AMW/viewresources/">Resources </a></li>
                <li><a <?php echo $news;?> href="<?php echo base_url();?>AMW/news/">News</a></li>
                <li><a <?php echo $userprofile;?> href="<?php echo base_url();?>AMW/profile/"><span>&nbsp;</span> Profile </a></li>
                <li class="last_sect"><a href="<?php echo base_url();?>logoutuser"><span>&nbsp;</span> Logout </a></li>
            </ul>
           
        </div>
    </nav>
  <!--   <ul class="nav navbar-nav navbar-right profile_sect">
                <li><a href="#" data-toggle="modal" data-target="#myModal"><span>&nbsp;</span> Profile </a></li>

            </ul>-->
</div>
 </div>   
    
 </div>  
 </div>  
</header>
  <!--container main_section end here--> 
