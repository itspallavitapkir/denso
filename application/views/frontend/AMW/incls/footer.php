<script>  
var timer = 0;
function set_interval() {
  
  // the interval 'timer' is set as soon as the page loads
  timer = setInterval("auto_logout()", 1800000); //30 min. calculate 5min = 5x60 = 300 sec = 300,000 millisec
 
}

function reset_interval() { 
	
  if (timer != 0) {
	  
    clearInterval(timer);
    timer = 0; 
    timer = setInterval("auto_logout()", 1800000);//30 min
    
    // completed the reset of the timer
  }
}

function auto_logout() {
  // this function will redirect the user to the logout script
  window.location = "<?php echo base_url();?>logoutuser";
}


</script>
<footer class="footer_section">
  <div class="container foot_text">
 <div class="row">
 <div class="col-lg-12">
 <div class="col-lg-6 footer_amk"><a href="<?php echo base_url();?>PMK/index">Switch To Products Market Kit</a></div>
 <div class="col-lg-6"><p>Copyright DENSO &copy; 2015, All rights reserved. </p></div>	 
 
 </div>   
</div>
</div>   
</footer>
 </div>   
</div>
<!--Page_width end here--> 
</body>
</html>
