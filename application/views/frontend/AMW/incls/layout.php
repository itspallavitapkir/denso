<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title;?></title>
<meta name="description" content="<?php echo $meta_description;?>">
<meta name="keywords" content="<?php echo $meta_keyword;?>">
<link rel="icon" href="<?php echo base_url();?>favicon.png" type="image/png">
<link rel="icon" href="<?php echo base_url();?>favicon.png" type="image/png">
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url();?>css/denso.css" rel="stylesheet" type="text/css">
<!--<link href="css/font.css" rel="stylesheet" type="text/css">-->
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<link href="<?php echo base_url();?>css/media.css" type="text/css" rel="stylesheet">
<!--Script file-->
<!--Script file-->

</head>
<!--[if IE 6]>
<script type="text/javascript">
window.location = "<?php echo base_url();?>ie6-please-update-your-browser.html";
</script>
<![endif]-->
<!--[if IE 7]>
window.location = "<?php echo base_url();?>ie6-please-update-your-browser.html";
<![endif]-->
<body onload="set_interval()" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()" >

<div class="wrapper">
<div class="page_width">
 <!--container main_section end here--> 
 				<?php
 				//echo $b_image;
 				?>		
				<?php $this->load->view("frontend/AMW/incls/header");?>
				<?php if(!empty($b_image)){?>
					<img src="<?php echo base_url().$b_image;?>" class="banner_image" width="100%">
				<?php }else{?>
					<img src="<?php echo base_url();?>/img/default-banner-Image.jpeg" class="banner_image" width="100%">
				<?php } ?>
				
				<?php $this->load->view($main_content);?>
				<?php $this->load->view("frontend/AMW/incls/footer");?>
			
			</div>
    <!-- main content end-->
  </div>
</div>


</body>
</html>			
