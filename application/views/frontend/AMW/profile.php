<!--container main_section end here--> 
 
  <!--middle_section Start here--> 
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(".flip2").click(function(){
$(".pane2").slideToggle("slow");
});

<?php if(!empty($validation_message)) { ?>
	$(".flip2").trigger('click');
<?php } ?>
});

 
</script>
<div class="middle_section"> 
  <div class="container page_title profile_sect">
 <div class="row">
 <div class="col-lg-12">
 <h2>Profile </h2>
 <?php if(!empty($message) && empty($validation_message)) { ?><div class="makeText" id="message"><div class='pull-left makeText  alert alert-success'><?php echo $message;?></div></div><?php } ?>
 <!--Update section-->
 <div class="update_section">
 <div class="set_update_con setupdatemain-top">
  <div class="rowblock-inner">
  	<div class="left_first">First Name :</div>
  	<div class="cont_name"><?php echo $firstname; ?></div>
   </div>
  <div class="rowblock-inner">
  <div class="left_first">Last Name : </div>
  <div class="cont_name"><?php echo $lastname; ?> </div>
  </div>
  <div class="rowblock-inner">
  <div class="left_first email-leftfist">Email Address : </div>
  <div class="cont_name address-leftfist"><?php echo $email_address; ?></div>
  </div>
  <div class="rowblock-inner">
  <div class="left_first">Contact Number :</div>
  <div class="cont_name"><?php echo ($contact_details?$contact_details:"-"); ?> </div>
  </div>
  <div class="rowblock-inner">
  <div class="left_first">Country :</div>
  <div class="cont_name"><?php echo ($country_name?$country_name:"-"); ?> </div>
  </div>
  <div class="rowblock-inner">
  <div class="left_first">Company :</div>
  <div class="cont_name"><?php echo ($company?$company:"-"); ?> </div>
 </div>
  </div>
 <!--<div class="update_btn"><button type="submit" class="btn btn-default main_profile_btn flip2">Update Profile</button> </div>-->
 
 </div>
 <!--Update section-->
 
 <div class="comeing_soon pane2">
 
  <div class="main_profile">
     <div class="row main_profile_title">    
 	 	<h2>Edit Profile</h2>
     </div>
     
     <form class="form-inline" name="frmProfile" id="frmProfile" action="<?php echo base_url(); ?>AMW/profile" method="post">
			<?php if(!empty($validation_message)){?>
                <div class="form-group col-md-12">
                    <div class="alert alert-dismissable alert-danger" role="alert"><?php echo $validation_message; ?></div>
                </div>
			<?php } ?>
     	<!--<div class="row field_space">
              <div class="form-group col-xs-6">
            
                    <label class="profile_label profile_field" for="txt_first_name">First Name</label>
                    <input type="text" class="profile_input profile_field" name="txt_first_name" id="txt_first_name" value="< ?php echo $firstname;?>" placeholder="First Name">
					<span id="fInfo"  class="text-danger"></span>
              </div>
              
              <div class="form-group col-xs-6">
             
                    <label class="profile_label profile_field" for="txt_last_name">Last Name</label>
                    <input type="text" class="profile_input profile_field" name="txt_last_name" id="txt_last_name" value="< ?php echo $lastname;?>" placeholder="Last Name">
					<span id="lInfo" class="text-danger"></span>
              </div>
         </div>
          
         
         <div class="row field_space"> 
              <div class="form-group col-xs-6">
                    <label  class="profile_label profile_field" for="country">Country</label>
                    <div class="dropdown  profile_field_select"> 
						  <select class="form-control dropdown-toggle" id="country" name="country">
							  <option value="">Select Country</option>
							  < ?php foreach($AllContries as $country):
							  if($country_id  == $country['id']){
							  ?>
								<option value="< ?php echo  $country["id"];?>" selected="">< ?php echo $country["name"];?></option>
							  < ?php 
							  }else{ ?>
							    <option value="< ?php echo  $country["id"];?>">< ?php echo $country["name"];?></option>
							  < ?php 
							  }?>
							  < ?php endforeach; ?>	
								
						  </select>
						  <span id="countryInfo"  class="text-danger"></span>
                    </div>
              </div>
               <div class="form-group col-xs-6">
                <!-- <div class=""> 
                    <label class="profile_label profile_field" for="txt_contactdetails">Contact Number</label>
                    <input type="text" class="profile_input profile_field" name="txt_contact_number" id="txt_contact_number" value="< ?php echo $contact_details; ?>"  placeholder="Contact Number">
             
              </div> 
          </div>
          -->
          <div class="row field_space restpassmain">
			  <div class="form-group col-xs-6">
                  <label class="profile_label profile_field" for="txt_contactdetails">Contact Number<span class="validationerror">*</span></label>
                  <input type="text" class="profile_input profile_field" name="txt_contact_number" maxlength="16" id="txt_contact_number" value="<?php echo $contact_details; ?>"  placeholder="Contact Number">
                  <span id="contactInfo"  class="text-danger"></span>
             </div>
             
             <div class="form-group col-xs-6">
                    <label class="profile_label profile_field" for="txt_first_name">Reset Password</label>
                    <input type="password" class="profile_input profile_field" name="txt_password" id="txt_password" value="" placeholder="Reset Password">
                   <span id="pwdInfo"  class="text-danger"></span>
              </div> 
         </div>
         
         
         </div>
          <div class="row main_profile_submit">
              <button type="submit" class="btn btn-default main_profile_btn" id="cmdSubmit">Submit</button> 
          </div>
     </form>
 </div>
  
 </div>
 </div>
 </div>
 </div>
 
 
</div>
<script src="<?php echo base_url();?>js/validate.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$("#message div").fadeOut(8000);
	$('#cmdSubmit').on('click', function (e) {
		$(".makeText").html("");
		flag=1;
		
		var pwd = $("#txt_password");
			var pwdInfo = $("#pwdInfo");  	
		var contact = $("#txt_contact_number");
			var contactInfo = $("#contactInfo");  	  			
		/*var country = $("#country");
			var countryInfo = $("#countryInfo");  				
		var flag=1;
		if(!validateEmpty(fName, fInfo, "first name")){
			flag = 0;
		} 	
		if(!validateEmpty(lName, lInfo, "last name")){
			flag = 0;
		}
		if(!checkCombo(country,countryInfo,"the country")){
			flag = 0;
		}*/
		if(contact.val()!=""){ 
			if(!RegExp(/^\+(?:[0-9] ?){6,14}[0-9]$/).test(contact.val())){
				contactInfo.html("Invalid phone number, the number should start with a plus sign, followed by the country code and national number");
				flag=0;
			}
			else{
				contactInfo.html("");
			}
		}
		
		
		/*if(!validateEmpty(pwd, pwdInfo, "the password")){
			flag = 0;
		}*/
		if(pwd.val()!="") {
			if(!CheckPasswordText(pwd, pwdInfo)){
				flag = 0;
			}
		}
		
		if(flag){
			$("#frmProfile").submit();
		}
		else{
			return false;
		}
		
	});
});
</script>
  <!--middle_section end here--> 
 
