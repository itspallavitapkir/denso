<!--container main_section end here--> 
<!--container main_section end here--> 
<!--middle_section Start here--> 
<div class="middle_section">
  <div class="container page_title">
 <div class="row">
 <div class="col-lg-12">
 <h2>Product Details</h2>
 <a href="<?php echo base_url(); ?>search" class="back_link">Back</a>
 </div> 
 </div>
 </div>
 
 <div class="container detail_page">
 <div class="row">
 <div class="col-lg-6">
 <?php   if($vehicledetails["v_image"]!="" && file_exists(realpath('vehicle_parts').'/'.$vehicledetails["v_image"])){?>
	<img alt="<?php echo $vehicledetails["v_image"];?>" title="<?php echo $vehicledetails["v_image"];?>" src="<?php echo base_url();?>vehicle_parts/<?php echo $vehicledetails["v_image"];?>">
 <?php }else{?>
	<img alt="No Image" src="<?php echo base_url();?>images/no-image.jpg">
 <?php } ?>
 <div class="car_disc">
	 <div class="car_name"><strong>Additional Info</strong></div>
	 <p class="discription_text"><?php echo $vehicledetails["vehicle_description"]?$vehicledetails["vehicle_description"]:'-';?></p>
 </div>
 </div>
  <div id="inform" class="col-lg-6">
    
  <div class="car_name"><?php echo ($vehicledetails["brand"] || $vehicledetails["v_model"])?$vehicledetails["brand"]." ".$vehicledetails["v_model"]:"";?>	</div>
  <p class="discription_text"></p>
  <div class="detail_info">
<ul class="car_detail_struc"> 	
<li>		 
<span>Vehicle Brand</span>
<p><?php echo $vehicledetails["brand"]?$vehicledetails["brand"]:"-";?></p>
</li>
<li>
<span>Vehicle Model</span>
<p><?php echo $vehicledetails["v_model"]?$vehicledetails["v_model"]:"-";?></p>
</li>
<li>
<span>Vehicle Year	</span>
<p><?php echo $vehicledetails["year"]?$vehicledetails["year"]:"-";?></p>
</li>
<li>
<span>Car Maker Part No.</span>
<p><?php echo $vehicledetails["car_maker_pn"]?$vehicledetails["car_maker_pn"]:"-";?></p>
</li>
<li>
<span>DENSO Part No.</span>
<p><?php echo $vehicledetails["denso_pn"]?$vehicledetails["denso_pn"]:"-";?></p>
</li>
<li>
<span>COOL GEAR Part No.</span>
<p><?php echo $vehicledetails["cool_gear_pn1"]?$vehicledetails["cool_gear_pn1"]:"-";?></p>
</li>
<li>
<span>Product Type</span>
<p><?php echo $vehicledetails["type"]?$vehicledetails["type"]:"-";?></p>
</li>

<li>
<span>Description</span>
<p><?php echo $vehicledetails["color"]?$vehicledetails["color"]:"-";?></p>
</li>
</ul>

  </div>
  
  
  </div>
  
 </div>
 </div>
 </div>
 
 <!-- Modal -->

  <!--middle_section end here--> 
