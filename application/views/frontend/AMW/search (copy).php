<?php
$CI =& get_instance();
$CI->load->model('administration_model');
?>

<!--container main_section end here--> 
<!--middle_section Start here--> 
 <div class="middle_section searchmiddle" ng-app="myApp">
 
  <div class="container page_title" id="searchsection">
	 <div class="row">
		 <div class="col-lg-12 searchtitle">
			<h2>Search By</h2>
		 </div>
	 </div>
  </div>
   
  <div class="container search_page_select search-selectionmain" >
 <div class="row" ng-controller="SearchCtrlByBrand">
	
	<form role="form" ng-controller="autocompleteController" id="frmSearchByBrand" name="frmSearchByBrand" class="formsearch-section">
    <div class="col-lg-3 col-xs-12"> 
	   <div class="form-group">
		<input type="hidden" id="selectedbrandid" value="{{selectedBrands.v_brand_id}}"/>   
		<input type="text" autocomplete="off" name="txtbrand" id="txtbrand" class="part_no" placeholder="Vehicle Brand" ng-model="selectedBrands" typeahead="b as b.brand for b in brands | filter:$viewValue">
		
		<i class="icon-search nav-search-icon"></i>
	  </div> 
    </div>
    <div class="col-lg-3 col-xs-3"> 
       <div class="form-group">
		<input type="hidden" id="selectedmodelid" value="{{selectedModels.v_model_id}}"/>   
		<input type="text" autocomplete="off" name="txtmodel" id="txtmodel" class="part_no" placeholder="Vehicle Model" ng-model="selectedModels" typeahead="b as b.v_model for b in models | filter:$viewValue">
	   </div> 
    </div>
    <div class="col-lg-2 col-xs-3"> 
		<div class="form-group">   
			<input type="hidden" id="selectedyear" value="{{selectedYears.year}}"/>
			<input type="text" autocomplete="off" name="selyear" id="selyear" class="part_no" placeholder="Select Year/Year Range" ng-model="selectedYears" typeahead="b as b.year for b in years | filter:$viewValue">
		</div>
   </div>
   <div class="col-lg-3 col-xs-12">
  <div class="pm"><div class="dataTables_length1">
						<div class="multiple-selection">
						<input type="hidden" name="txt_resource" id="txt_resource" />	
						<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):
								?>
								<li>
                                <input type="checkbox" name="dd_resources[]" value="<?php echo $cat['id']?>" placeholder="" id="dd_resources" class="events-category">
                               
								<?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?> 
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" value="<?php echo $subcat['id']?>" id="subcategory_<?php echo $subcat['id']?>" class="child events-child-category">
											<span><?php echo $subcat['sub_category'];?></span>
                                        </li>
									 
								 <?php endforeach;
								 ?>
								 </ul>
								 <?php
								    }
								 ?>
								    </li>
								    <?php
								 endforeach;
								 ?>
								 
								 <?php
									}
								?>
							</ul>
						</div>
						</div>
					</div>
   </div>
	<div class="col-lg-3 col-xs-12 srchmain-denso">
		<div class="form-group">
		 <button class="search_field" ng-click="SearchByBrand()">Search</button>
	</div>
   </div>
	</form>
 </div>
 </div>
 
  <div class="container page_title search2" id="searchsection">
	<div class="row">
		<div class="col-lg-12 searchtitle">
			<h2>Search By Car Maker Part No.</h2>
		</div>
	</div>
  </div>
  <div class="container search_page_select search-selectionmain">
 <div class="row" ng-controller="SearchCtrl">
   <div class="col-lg-3 searching-blcok25 " >
    <form role="form" ng-controller="autocompleteController2" id="frmcarpn" name="frmcarpn">
		<div class="form-group">
			<input type="text" autocomplete="off" name="car_maker_part_no" id="car_maker_part_no" class="part_no" placeholder="Vehicle Car Maker Part No." ng-model="selectedCarMaker" typeahead="v as v.car_maker_pn for v in vehicles | filter:$viewValue">
			<span class="validationerror" id="infoCarMaker"></span>
		</div> 
   </div>
  <div class="col-lg-3"> 
    <div class="form-group">
     <button type="submit" class="search_field" ng-click="search()">Search</button>
  </div> 
  </form>
</div>
   
  
 </div>
 </div>
 
 <div class="container resource_page" id="searchbrabd">
<div class="row">
<div class="col-lg-12">
 <div class="table-responsive" id="search_result">
 
  </div>
  
  
  </div>
  
 </div>
 </div>

</div>
  <!--middle_section end here--> 
  <style>
	  .pm {
position: relative;
}
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
background: url("../images/downArrow.gif") no-repeat scroll 95% center #FFFFFF !important;
}
.set_category {
    background-color: #fff;
    height: 150px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 31px;
    width: 266px;
    z-index: 99999 !important; border:1px solid #ccc; border-top:none;
}
.set_category li input#dd_resources {
    margin: 3px 8px 14px 0;float: left;
}
.event-children li {
    float: left;
    margin: 0 0 4px;
    width: 100%;
}
.set_category span {
    float: left;
    margin: 4px 0 0;
}
.event-children li input {
    float: left;
    margin: 3px 8px 6px 0;
}
.event-children span {
    float: left;
    margin: 0 !important;
}
#vehicle-categories > li {
    float: left;
    width: 100%;
}
</style>


<script type="text/javascript" src="<?php echo base_url();?>js/angular.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>js/ng-infinite-scroll.min.js"></script>
<script src="<?php echo base_url();?>js/ui-bootstrap-tpls-0.9.0.js"></script>
<script>
$(document).ready(function() {
		$("#vehicle-categories").hide();
		$('#select-category').click( function(){
			$("#vehicle-categories").toggle();
			
		});
	 
	 
	$('.events-category').change( function(){
    var c = this.checked; 
    if( c ){
        $(this).next('.event-children').css('display', 'block'); 
        
    }else{
        $(this).next('.event-children').css('display', 'none');
    }
	});
});
</script>
<script>
	//Define an angular module for our app
var app = angular.module('myApp', ['ui.bootstrap']);
//var appscrl = angular.module('scrollApp', ['infinite-scroll']);

app.controller('autocompleteController', function($scope, $http) {
  getVBrands(); // Load all which is matching brands 
  function getVBrands(){  
  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_brand").success(function(data){
        $scope.brands = data; 
       });
  };
  getVModels();
  function getVModels(){  
  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_model").success(function(data){
        $scope.models = data;
       });
  };
  getYear();
  function getYear(){  
  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_year").success(function(data){
        $scope.years = data;
       });
  };
});
app.controller('autocompleteController2', function($scope, $http) {
 
   getCarMakerPartNo();
  function getCarMakerPartNo(){
	  $http.post("<?php echo base_url();?>frontcontroller/autosuggetion_result_car_maker").success(function(data){ 
			$scope.vehicles = data;
       });
  }; 
});


/*function SearchCtrlByBrand($scope, $http, $templateCache) {
  var method = 'POST';
  var url = '<?php echo base_url();?>frontcontroller/search_result';
  $scope.codeStatus = "";
  var formData = new FormData($('#frmSearchByBrand')[0]); 
  
  $scope.SearchByBrand = function() {
	  
	var dd_category_val = new Array();
	$.each($("input[name='dd_resources[]']:checked"), function() {
	  dd_category_val.push($(this).val());
	  // or you can do something to the actual checked checkboxes by working directly with  'this'
	  // something like $(this).hide() (only something useful, probably) :P
	});  
	  
	var dd_sub_category_val = new Array();
	$.each($("input[name='dd_subresources[]']:checked"), function() {
	  dd_sub_category_val.push($(this).val());
	  // or you can do something to the actual checked checkboxes by working directly with  'this'
	  // something like $(this).hide() (only something useful, probably) :P
	}); 
	  
    var FormData = {
      'selectedbrandid' : document.frmSearchByBrand.selectedbrandid.value,
      'txtbrand' : document.frmSearchByBrand.txtbrand.value, 
      'selectedmodelid' : document.frmSearchByBrand.selectedmodelid.value, 
      'selyear' : document.frmSearchByBrand.selyear.value, 
      'dd_resources' : dd_category_val, 
      'dd_subresources' : dd_sub_category_val, 
    }; 
    $http({
      method: method,
      url: url,
      data: $.param({'data' : FormData}),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      cache: $templateCache
    }).
    success(function(response) {
       $("#search_result").html(response);
    }).
    error(function(response) {
         //$("#search_result").html("Request failed");
         alert("Sorry! Search could not be processes");
    });
    return false;
  };
}


function SearchCtrl($scope, $http, $templateCache) {
  var method = 'POST';
  var url = '<?php echo base_url();?>frontcontroller/search_result';
  $scope.codeStatus = "";
  $scope.search = function() {
    var FormData = {
      'car_maker_part_no' : document.frmcarpn.car_maker_part_no.value, 
    };
    $http({
      method: method,
      url: url,
      data: $.param({'data' : FormData}),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      cache: $templateCache
    }).
    success(function(response) {
       $("#search_result").html(response);
    }).
    error(function(response) {
         //$("#search_result").html("Request failed");
         alert("Sorry! Search could not be processes");
    });
    return false;
  };
}
*/

</script>  
