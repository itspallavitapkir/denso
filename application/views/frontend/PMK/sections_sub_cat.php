<?php 
$cat_id = (isset($_GET['cat_id'])?$_GET['cat_id']:'');
$sub_cat_id = (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:'');
$page_id = (isset($_GET['id'])?$_GET['id']:'');
?>
<script src="<?php echo base_url();?>js/jasny-bootstrap.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url();?>js/jquery_slide.js" type="text/javascript"></script>-->
<link href="<?php echo base_url();?>css_pmk/jasny-bootstrap.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url();?>css_pmk/navmenu-reveal.css" type="text/css" rel="stylesheet">
  <!--middle_section Start here--> 
 <section class="category_section"> 
 	<div class="container">
 		<div class="row">
              
        <div class="col-lg-12 menu_section" id="sub_inner_section">

<!-- left side menu -->
 <div class="col-lg-3 mobile_menu_show">
 <div style="" class="canvas">
      <div style="" class="navbar navbar-default navbar-fixed-top">
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
</div>
    <div class="navmenu navmenu-default navmenu-fixed-left">
       <?php echo $cat_html;?>
     
     </div>
     
      </div>


    <!-- left side menu -->

 <div class="col-lg-3 navigation_menu">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
          <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php echo $cat_html;?>
    </div>
  </div>
</nav>
    </div>
        
        <div class="right_site_contantarea col-lg-9">
         <?php if(isset($section_html) && $section_html != "no_rec"){?>
        <div class="col-lg-4 select_lang">
       	 	
       	 	<div class="bs-docs-example">
					<label> Select language :</label>
					<select class="selectpicker lang_section" data-style="btn-primary"  placeholder="Select language">
  			
				  </select>
			</div>
			
		</div>
		<?php } ?>
				<?php if(isset($cat_name) && $cat_name!=""){?>
					<div class="cat_heading"><h2><?php echo $cat_name;?>
						>><?php echo $sub_cat_name;?></h2>
					  </div>
				<?php } ?>											  
				
				<?php if(isset($section_html) && $section_html!="no_rec"){?>
				<ul class="nav nav-tabs col-lg-4" role="tablist">
					<?php echo $section_html; ?>
				</ul>
				<div class="right_hash"></div>
				<?php } 
				elseif(isset($section_html) && $section_html == "no_rec"){ ?>
					<div>No Records Available</div>
				<?php } ?>

  <!-- Tab panes -->
  <div class="tab-content col-lg-8">
 
   <div class="col-lg-12 main_content_area" id="examples">
   	<div class="content">
   <?php echo $page_description; ?>
   <?php echo $resources_html; ?>  
		</div>
        </div>
        </div>
        <!--Left side navigation close here-->
 	</div>
    </div>
    </div>
    <script>
		(function($){
			$(window).load(function(){
				
				$(".content").mCustomScrollbar({
					scrollButtons:{
						enable:true
					},
					callbacks:{
						onScrollStart:function(){ myCallback(this,"#onScrollStart") },
						onScroll:function(){ myCallback(this,"#onScroll") },
						onTotalScroll:function(){ myCallback(this,"#onTotalScroll") },
						onTotalScrollOffset:60,
						onTotalScrollBack:function(){ myCallback(this,"#onTotalScrollBack") },
						onTotalScrollBackOffset:50,
						whileScrolling:function(){ 
							myCallback(this,"#whileScrolling"); 
							$("#mcs-top").text(this.mcs.top);
							$("#mcs-dragger-top").text(this.mcs.draggerTop);
							$("#mcs-top-pct").text(this.mcs.topPct+"%");
							$("#mcs-direction").text(this.mcs.direction);
							$("#mcs-total-scroll-offset").text("60");
							$("#mcs-total-scroll-back-offset").text("50");
						},
						alwaysTriggerOffsets:false
					}
				});
				
				function myCallback(el,id){
					if($(id).css("opacity")<1){return;}
					var span=$(id).find("span");
					clearTimeout(timeout);
					span.addClass("on");
					var timeout=setTimeout(function(){span.removeClass("on")},350);
				}
				
				$(".callbacks a").click(function(e){
					e.preventDefault();
					$(this).parent().toggleClass("off");
					if($(e.target).parent().attr("id")==="alwaysTriggerOffsets"){
						var opts=$(".content").data("mCS").opt;
						if(opts.callbacks.alwaysTriggerOffsets){
							opts.callbacks.alwaysTriggerOffsets=false;
						}else{
							opts.callbacks.alwaysTriggerOffsets=true;
						}
					}
				});
				
			});
		})(jQuery);
	</script>
 </section> 
  <!--middle_section end here--> 
<script type="text/javascript">
	function ChangeUrl(page, url) {
		 
        if (typeof (history.pushState) != "undefined") {
            var obj = { Page: page, Url: url };
            history.pushState(obj, obj.Page, obj.Url);
        } else {
            //alert("Browser does not support HTML5.");
        }
    }
	$(document).ready(function(){  	
	$(".sections_li").click(function(){
		var cat_id =  '<?php echo (isset($_GET['cat_id'])?$_GET['cat_id']:''); ?>';
		var sub_cat_id =  '<?php echo (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:''); ?>';
		var section_name = $(this).find('a').attr('data-attr');
		var lang_id = 1;  
		var page_id =  '<?php echo (isset($_GET['id'])?$_GET['id']:''); ?>';
		if(section_name != "" && cat_id != ""){ 
		
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>frontcontroller/list_section",
				data: {"page_id":page_id,"cat_id":cat_id, "sub_cat_id" : sub_cat_id, "section_name" : section_name, "lang_id" : lang_id},
			}).success(function (json) {
				$(".lang_section").html(json.lang); 
				//$(descr_id).html(json.page_description);
				$("#Category_1_"+json.section).html(json.page_description+json.resources);
				$("#Category_1_"+json.section+" a" ).on('click',function( event ) {
				  //event.preventDefault();
				   var section_array = <?php echo json_encode($section_array);?>;
				  //var section_nm = 'Car Air Con section2';
				  var section_nm = $(this).attr("href").replace('#Category_1_','');
				  var section_nm = section_nm.split(/[_]+/).join(' ');
				  if($(this).attr("href").indexOf('#Category_1_')==0){
					  if($.inArray(section_nm,section_array)>-1){ 
						
						  if(!$($(this).attr("href")).hasClass("active")){ 
							$('a[href$="'+$(this).attr("href")+'"]').trigger("click");
						  }
					  }
					  else{
						  alert('Invalid linking or You don’t have access for it');
						  return false;
					  }
				  }
				});
				$(".sections_li a").bind( "mousedown", function(event) {
					
					 if( event.button == 2 ) { 
						  ChangeUrl('sections-category', $(this).attr("href"));
						   $(".right_hash").html($(this).attr("href"));
						  if(!$($(this).attr("href")).hasClass("active")){
							// alert($(this).attr("href"));
							 return false; 
						  }
					 }
				});
				//$('.selectpicker').selectpicker();
				//$(".lang_section").trigger('change'); 
			});
		}
		else{
			alert('Something went wrong');
			return false;
		} 
	});
	
	$(".lang_section").change(function(){
		var cat_id =  '<?php echo (isset($_GET['cat_id'])?$_GET['cat_id']:''); ?>';
		var sub_cat_id =  '<?php echo (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:''); ?>';
		var section_name = $(".sections_li.active a").attr('data-attr');
		var lang_id = $(this).val();
		var page_id =  '<?php echo (isset($_GET['id'])?$_GET['id']:''); ?>'; 
		if(section_name != "" && cat_id != ""){ 
		
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>frontcontroller/list_section",
				data: {"page_id":page_id, "cat_id":cat_id, "sub_cat_id":sub_cat_id, "section_name":section_name,"lang_id":lang_id},
			}).success(function (json) {
				//alert(json.resources);
				$("#Category_1_"+json.section).html(json.page_description+json.resources);
				$("#Category_1_"+json.section+" a" ).on('click',function( event ) {
				  //event.preventDefault();
				  var section_array = <?php echo json_encode($section_array);?>;
				  //var section_nm = 'Car Air Con section2';
				  if($(this).attr("href").indexOf('#Category_1_')==0){
					  var section_nm = $(this).attr("href").replace('#Category_1_','');
					  var section_nm = section_nm.split(/[_]+/).join(' ');
					  
					  if($.inArray(section_nm,section_array)>-1){ 
						
						  if(!$($(this).attr("href")).hasClass("active")){ 
							$('a[href$="'+$(this).attr("href")+'"]').trigger("click");
						  }
					  }
					  else{
						  alert('Invalid linking or You don’t have access for it');
						  return false;
					  }
					  }
				});
				$(".sections_li a").bind( "mousedown", function(event) {
					
					 if( event.button == 2 ) { 
						  ChangeUrl('sections-category', $(this).attr("href"));
						  if(!$($(this).attr("href")).hasClass("active")){
							// alert($(this).attr("href"));
							 return false; 
						  }
					 }
				});
			});
		}
		else{
			alert('Something went wrong');
			return false;
		} 
	});
	
	$(".sections_li.active a").trigger("click");
});

window.onload=function(){
      //$('.selectpicker').selectpicker();
      $('.rm-mustard').click(function() {
        $('.remove-example').find('[value=Mustard]').remove();
        $('.remove-example').selectpicker('refresh');
      });
      $('.rm-ketchup').click(function() {
        $('.remove-example').find('[value=Ketchup]').remove();
        $('.remove-example').selectpicker('refresh');
      });
      $('.rm-relish').click(function() {
        $('.remove-example').find('[value=Relish]').remove();
        $('.remove-example').selectpicker('refresh');
      });
      $('.ex-disable').click(function() {
          $('.disable-example').prop('disabled',true);
          $('.disable-example').selectpicker('refresh');
      });
      $('.ex-enable').click(function() {
          $('.disable-example').prop('disabled',false);
          $('.disable-example').selectpicker('refresh');
      });

      // scrollYou
      $('.scrollMe .dropdown-menu').scrollyou();

      prettyPrint();
      };
</script>
