<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title;?></title>
<meta name="description" content="<?php echo $meta_description;?>">
<meta name="keywords" content="<?php echo $meta_keyword;?>">
<link rel="icon" type="image/png" href="<?php echo base_url();?>favicon.png">
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

<!--Css file linking here-->
<link href="<?php echo base_url();?>css_pmk/demo.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css_pmk/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css_pmk/bootstrap-select.min.css">
<link href="<?php echo base_url();?>css_pmk/media.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url();?>css_pmk/jquery.mCustomScrollbar.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>css_pmk/bootstrap.min.css" type="text/css">
<!--js file linking here-->

<script src="<?php echo base_url();?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.mCustomScrollbar.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/bootstrap-select.js" type="text/javascript"></script>



</head>
<!--[if IE 6]>
<script type="text/javascript">
window.location = "<?php echo base_url();?>ie6-please-update-your-browser.html";
</script>
<![endif]-->
<!--[if IE 7]>
window.location = "<?php echo base_url();?>ie6-please-update-your-browser.html";
<![endif]-->
<body >

<div class="wrapper">
<div class="page_width">
 <!--container main_section end here--> 			
				<?php $this->load->view("frontend/PMK/incls/header");?>
				<?php $this->load->view($main_content);?>
				<?php $this->load->view("frontend/PMK/incls/footer");?>
			
			</div>
    <!-- main content end-->
  </div>
</div>


</body>
</html>			
