<?php
$resources =  ($this->uri->segment(1) == "viewresources") ? 'class="active"':'';
$userprofile =  ($this->uri->segment(1) == "profile") ? 'class="active"':'';
$news =  ($this->uri->segment(1) == "news" || $this->uri->segment(1) == "news_details") ? 'class="active"':'';
$search =  ($this->uri->segment(1) == "search" || $this->uri->segment(1) == "details") ? 'class="active"':'';
$userdata_header = $this->session->userdata('auth_densouser'); 
?> 

<header class="headsection "> 
<div class="container main_section">
<div class="row">
<div class="col-lg-8">
<div class="logo" id="loginpage_logo"><h1><a href="<?php echo base_url();?>PMK/index"><img src="<?php echo base_url();?>img/logo.png" border="0" title="DENSO"></a></h1></div>
</div>
<?php if(!empty($userdata_header)){
	?>
    <div class="welcome">Welcome, <?php echo $userdata_header['first_name']." ".$userdata_header['last_name'];?>
    <a href="<?php echo base_url();?>logoutuser"><span>&nbsp;</span> Logout </a>
    </div>
<?php } ?>    
 </div>  
 </div>
 
 </header>
  
  <!--container main_section end here--> 
