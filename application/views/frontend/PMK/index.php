<?php 
$cat_id = (isset($_GET['cat_id'])?$_GET['cat_id']:$cat_id);
$sub_cat_id = (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:'');
$section_name = (isset($sname)?$sname:'');
?>
<script src="<?php echo base_url();?>js/jasny-bootstrap.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url();?>js/jquery_slide.js" type="text/javascript"></script>-->
<link href="<?php echo base_url();?>css_pmk/jasny-bootstrap.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url();?>css_pmk/navmenu-reveal.css" type="text/css" rel="stylesheet">

<section class="category_section"> 
 	<div class="container">
 		<div class="row">


        <div class="col-lg-12 menu_section">

<!-- left side menu -->
 <div class="col-lg-3 mobile_menu_show">
 <div style="" class="canvas">
      <div style="" class="navbar navbar-default navbar-fixed-top">
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
</div>
    <div class="navmenu navmenu-default navmenu-fixed-left">
       <?php echo $cat_html;?>
     
     </div>
     
      </div>


    <!-- left side menu -->

 <div class="col-lg-3 navigation_menu">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
          <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php echo $cat_html;?>
    </div>
  </div>
</nav>
    </div>
        
        
        

  <!-- Tab panes -->
  <div class="tab-content col-lg-8">
 
   <div class="col-lg-12 main_content_area">
   <div role="tabpanel" class="tab-pane" id="Category_1">
  
     
      
    </div>
   
    <div class="home_text">Welcome to Products Market Kit</div>
      
    </div>

		</div>
        </div>
        
        <!--Left side navigation close here-->
 	</div>
    </div>
    </div>
 </section>
 
<input type="hidden" name="cat_id" id="cat_id" value="<?php ?>" />
<script type="text/javascript">
	
$(document).ready(function(){  	
	
	var cat_id =  '<?php echo (isset($_GET['cat_id'])?$_GET['cat_id']:''); ?>';
	var sub_cat_id =  '<?php echo (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:''); ?>';
	
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>frontcontroller/list_section",
			data: {"cat_id":cat_id,"sub_cat_id":sub_cat_id},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
				}
				else
				{
					$("#table").html(json.table);
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
		});
});
      window.onload=function(){
      $('.selectpicker').selectpicker();
      $('.rm-mustard').click(function() {
        $('.remove-example').find('[value=Mustard]').remove();
        $('.remove-example').selectpicker('refresh');
      });
      $('.rm-ketchup').click(function() {
        $('.remove-example').find('[value=Ketchup]').remove();
        $('.remove-example').selectpicker('refresh');
      });
      $('.rm-relish').click(function() {
        $('.remove-example').find('[value=Relish]').remove();
        $('.remove-example').selectpicker('refresh');
      });
      $('.ex-disable').click(function() {
          $('.disable-example').prop('disabled',true);
          $('.disable-example').selectpicker('refresh');
      });
      $('.ex-enable').click(function() {
          $('.disable-example').prop('disabled',false);
          $('.disable-example').selectpicker('refresh');
      });

      // scrollYou
      $('.scrollMe .dropdown-menu').scrollyou();

      prettyPrint();
      };
    </script>
