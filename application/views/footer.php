</div><!-- Header div close here -->
<div id="changepassword" class=" popup-basic admin-form mfp-with-anim mfp-hide resetvalues">
	<div class="panel">
		<div class="panel-heading p15">
			<span class="panel-title">Change Password</span>
		</div>
		<!-- end .panel-heading section -->
		<div id="passerror" class="text-danger messages"></div>
		<form action="#" id="resetpwd" method="post" name="resetpwd">
			<div class="panel-body p15">
				<div class="section row">
					<div class="col-md-12">
						<label for="oldpassword" class="field">
							<input type="password" name="oldpassword" id="oldpassword" class="gui-input" placeholder="Existing Password">
							<div id="oldPwdInfo" class="text-danger"></div>
						</label>
					</div>
					<!-- end section -->
				</div>
				
				<div class="section row ">
					<div class="col-md-12">
						<label for="newpwd" class="field">
							<input type="password" name="newpwd" id="newpwd" class="gui-input" placeholder="New Password">
							<div id="newPwdInfo" class="text-danger"></div>
						</label>
					</div>
					<!-- end section -->
				</div>
				<div class="section row mbn">
					<div class="col-md-12">
						<label for="confirmpwd" class="field">
							<input type="password" name="confirmpwd" id="confirmpwd" class="gui-input" placeholder="Confirm Password">
							<div id="ConPwdInfo" class="text-danger"></div>
						</label>
					</div>
					<!-- end section -->
				</div>
				<!-- end section row section -->
			</div>
			<!-- end .form-body section -->
			<div class="panel-footer">
				<button type="submit" class="button btn-info btn-xs" id="resetpwd1">Submit</button>
				<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
			</div>
			<!-- end .form-footer section -->
		</form>
	</div>
	<!-- end: .panel -->
</div>
 <!-- Page Model: End -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/v11.js"></script>
     <!-- Sparklines CDN -->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>

    <!-- Holder js  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/holder.min.js"></script>
  
    <!-- Datatables Bootstrap Modifications  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    
   <!-- Theme Javascript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/utility/utility.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/demo.js"></script>

    <!-- datepicker javascript-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap-datetimepicker.js"></script>
   
  
  	

    <!-- Page Javascript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/widgets.js"></script>
    <!-- Validation javascript -->
    <script src="<?php echo base_url();?>assets/js/validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core    
            Core.init();

            // Init Theme Core    
            Demo.init();

            // Init tray navigation smooth scroll
            $('.tray-nav a').smoothScroll({
                offset: -145
            });

            // Custom tray navigation animation
            setTimeout(function() {
                $('.custom-nav-animation li').each(function(i, e) {
                    var This = $(this);
                    var timer = setTimeout(function() {
                        This.addClass('animated zoomIn');
                    }, 100 * i);
                });
            }, 600);


         
           // Init Datatables with Tabletools Addon    
            $('#datatable').dataTable({
                "aoColumnDefs": [{
                   // 'bSortable': false,
                    'aTargets': [-1],
                }],
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "",
                        "sNext": ""
                    }
                },
                "bPaginate":false,
                "iDisplayLength": -1,
                "aLengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "sDom": 't<"dt-panelfooter clearfix"ip>',
                "oTableTools": {
                    "sSwfPath": "<?php echo base_url(); ?>assets/vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
	    });
    </script>
<script type="text/javascript">
var oldPwd = $("#oldpassword");
	var oldPwdInfo = $("#oldPwdInfo");
var newPwd = $("#newpwd");
	var newPwdInfo = $("#newPwdInfo");
var Conpassword = $("#confirmpwd");
	var newConPwdInfo = $("#ConPwdInfo");	
	$("#resetpwd").submit(function(){ 
		var flag=1;
		if(!validateEmpty(oldPwd, oldPwdInfo, "existing password")){
			flag = 0;
		} 
		if(oldPwd.val()!=""){
			if(CheckPasswordText(oldPwd, oldPwdInfo)){
					//flag = 1;
			}else
			{flag=0; }
		}
		if(!validateEmpty(newPwd, newPwdInfo, "new password")){
			flag = 0;
		}
		if(newPwd.val()!=""){
			if(CheckPasswordText(newPwd, newPwdInfo)){
				//flag = 1;
			}else
			{flag=0; }
		}
		if(!validateEmpty(Conpassword, newConPwdInfo, "confirm password")){
			flag = 0;
		}
		if(Conpassword.val()!=""){
				if(CheckPasswordText(Conpassword, newConPwdInfo)){
				//flag = 1;
			}else
			{flag=0; }
		}
		
		if(newPwd.val() != Conpassword.val()){
				newConPwdInfo.html('The password and confirm password does not match');
				flag = 0;
				$('#confirmpwd').val("");
				$('#newpwd').val("");
		}
		//return false;
		if(flag){  
			var oldpwd = oldPwd.val();
			var newpwd = newPwd.val();
			var confirmpassword = Conpassword.val();
				$.ajax({
                type: "POST",
                dataType: "json",
                url: "<?php echo base_url(); ?>login/save_password", 
                data: {currentPassword:oldpwd,newPassword:newpwd,confirmPassword:confirmpassword},
                success: function(data) 
                {
						if(data.msg){
							$('.messages').html(data.msg);
							$(".messages div").fadeOut(5000);
						}
						else if(data.msg1){
							$('.messages').html(data.msg1);
							$(".messages div").fadeOut(5000);
							setTimeout(function(){  $("#changepassword .mfp-close").trigger( "click" ); }, 3000);
						}
						$('#oldpassword').val("");
						$('#newpwd').val("");
						$('#confirmpwd').val("");
						
						
				}        
            });
			return false;
		}
		else{
			return false;
		}
	});
	$(document).on("click","#changepassword",function() {
		
		$("#changepassword .panel-title").html("Change Password");
	});
	$(document).on("click","#changepassword .mfp-close",function() {
	$('#oldpassword').val("");
	$('#newpwd').val("");
	$('#confirmpwd').val("");
	$(".nav .dropdown").removeClass("open");
	document.getElementById("resetpwd").reset();
});
	
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#dd_searchBy1').hide();
		$('#dd_searchBy').multiselect({
			buttonText: function(options, select) {
				return 'Search By';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text()); 
				});

				//console.log(labels);
				if($.inArray('Category',labels) >= 0)
				{
					$('#s_category').show();
				}else{
					$('#s_category').hide();
				}


				if($.inArray('Language',labels) >= 0)
				{
					$('#searchby_lang').show();
				}else{
					$('#searchby_lang').hide();
				}


				if($.inArray('Section',labels) >= 0)
				{
					$('#searchby_section').show();
				}else{
					$('#searchby_section').hide();
				}


				if($.inArray('Categories',labels) >= 0)
				{
					$('#searchby_category').show();
				}else{
					$('#searchby_category').hide();
				}
			
				if($.inArray('Language',labels) >= 0)
				{
					$('#lang_id').show();
				}else{
					$('#lang_id').hide();
				}
				if($.inArray('Categories',labels) >= 0)
				{
					$('#cat_id').show();
				}else{
					
					$('#cat_id').hide();
				}
				if($.inArray('Section',labels) >= 0)
				{
					$('#section_id').show();
				}else{
					$('#section_id').hide();
				}

				if($.inArray('First Name|| Last Name ||Email Address',labels) >= 0)
				{
					//console.log('ifff');
					$('#txt_search').show();
				}else{
					//console.log('elsee');
					$('#txt_search').hide();
				}

				if($.inArray('Start Date',labels) >= 0)
				{
					$('#start_date1').show();
				}else{
					$('#start_date1').hide();
				}
				if($.inArray('End Date',labels) >= 0)
				{
					$('#end_date1').show();
				}else{
					$('#end_date1').hide();
				}

				//console.log($.inArray('User Type',labels) >= 0);
				if($.inArray('First Name',labels) >= 0)
				{
					$('#txt_search').show();
				}else{
					//$('#txt_search').hide();
				}
				if($.inArray('Last Name',labels) >= 0)
				{
					$('#txt_search').show();
				}else{
					//$('#txt_search').hide();
				}
				if($.inArray('Email Address',labels) >= 0)
				{
					$('#txt_search').show();
				}else{
					//$('#txt_search').hide();
				}
 
				if($.inArray('User Type',labels)>=0){
					$('#dd_searchBy1').show();
				}
				else{
					$('#dd_searchBy1').hide();
				}
				  
				if($.inArray('Status',labels) >= 0){
					$('#searchby_status').show();
				}else{
					$('#searchby_status').hide();
				}

				if($.inArray('Country',labels) >= 0){ 
					$('#searchby_country').show();
				}else{
					$('#searchby_country').hide();
				}

				return labels.join(' - ');
			}
		});
	});
</script>
    <!-- END: PAGE SCRIPTS -->
</body>

</html>
