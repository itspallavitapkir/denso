<?php $this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Vehicle Parts</li>
										<li class="crumb-trail">Manage Models</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#vehicleModelModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="vehicleModelModel()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="brand">Vehicle Brand</option> 
													<option value="v_model">Vehicle Model</option> 
												</select>	
												</div>
											</div>
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" onClick="changePaginate(0,'v_model_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'v_model_id','DESC');" value="Refresh">
											</div>-->
                                              <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                    <input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,v_model_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="vehicleModelModel" class="popup-basic taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Vehicle Brand Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formVehicleModel" name="formVehicleModel">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="v_brand_id" class="col-lg-4 ptn mt5 control-label brandtitle">Vehicle Brand<span class="validationerror">*</span></label>
				  <label class="col-lg-8 brandtitleinner">
					<select class="select-sm arrow_new form-control" name="v_brand_id" id="v_brand_id">
						<option value="0">Select Brand</option>
						<?php
							if(is_array($brands)){
								foreach($brands as $brand):	?>
								<option value="<?php echo $brand['v_brand_id']; ?>"><?php echo $brand['brand'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
					<span id="brandInfo"  class="text-danger"></span>
				  </label>
				</div>
			  </div>
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="txt_vehicle_model" class="col-lg-4 ptn mt5 control-label">Vehicle Model<span class="validationerror">*</span></label>
				  <div class="col-lg-8">
					<input type="text" id="txt_vehicle_model" name="txt_vehicle_model" class="form-control input-sm" placeholder="Vehicle Model">
					<span id="catInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			   
			</div>   
			  <!-- end section --> 
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="vehicleModelId" id="vehicleModelId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 
<script>
	
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});		

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'v_model_id','DESC');
		}
		setTimeout(function(){  $("#vehicleModelModel .mfp-close").trigger( "click" ); }, 3000);
	}
}
	 
function changePaginate(start,column,order)
{
		$("#txt_paginate").val(start+","+column+","+order);
		getVehicleModelList();	
} 

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	if(flag){
		changePaginate(0,'v_model_id','DESC');
	}
});
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add Vehicle Model");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});
$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');
	//alert(id);
	//return false;
	$("#vehicleModelId").val(id);
	
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getVehicleModelById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_vehicle_model").val(json.v_model);
				$("#v_brand_id").val(json.v_brand_id);
				$("#add").trigger("click");
				$("#vehicleModelModel .panel-title").html("Edit Vehicle Model");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#vehicleModelModel .mfp-close",function() {
	$("#vehicleModelId").val("");
	$("#catInfo").html("");
	document.getElementById("formVehicleModel").reset();
	location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$("#dd_searchBy").val('');
	$(".multiselect-container li").removeClass("active");
	changePaginate(0,'v_model_id','DESC'); 
}

function loadmore(){
	getVehicleModelList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getVehicleModelList();

function getVehicleModelList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	//$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_vehicle_model",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html("");
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'v_model_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getVehicleModelList();
			}
	});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var catId = $("#vehicleModelId"); 
		var vcat = $("#txt_vehicle_model");
			var vcatInfo = $("#catInfo");  		
		var brand = $("#v_brand_id");
			var brandInfo = $("#brandInfo");  					
		var flag=1; 
		
		if(!checkCombo(brand, brandInfo, "vehicle brand")){
			flag = 0;
		}
		
		if(!validateEmpty(vcat, vcatInfo, "vehicle model")){
			flag = 0;
		} 
		 
		if(vcat.val()!=""){
			if(!CheckAlphanumericWithSomeChars(vcat, vcatInfo)){
				flag = 0;
			} 
		} 
		
		  
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json", 
				url: "<?php echo base_url(); ?>administration/save_vehicle_model",
				data: {"vehicleModelId": $("#vehicleModelId").val(),"txt_vehicle_model":$("#txt_vehicle_model").val(),"v_brand_id":$("#v_brand_id").val()},
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}

				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
			});
		} 
	});		
});

function vehicleModelModel()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected models?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_vehicle_model",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getVehicleModelList();
				alert((json.ids).length+" models has been deleted.");
				
			});
		}  
	}
	else {
	  alert('Please select at least one model.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
