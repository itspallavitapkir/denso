<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                <div id="message"></div>
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">

						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">

							<?php $message= $this->session->flashdata('Success'); if(!empty($message))?>

								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Questions</li>
											<li class="crumb-trail"> Manage Questions</li>
										</ol>
								</div>


								<span class="panel-controls Users">  
									<!-- <a id="add" class="model-open" href="#quetionnaireModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a>  -->
									<a class="" href="javascript:void(0)" onclick="deleteQuetionnaire()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<!-- <a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a> -->
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											

                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-3 col-sm-6 top-serchbar3">
													
													<a href="<?php echo base_url(); ?>administration/getQuestionnaire"><input type="button" id="" class="button table-submitbtn btn-info btn-xs" value="Back"></div></a>
													
												
                                                </div>

											</div>
                                            
											<!-- <div id="message"></div> -->
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>

        
       

       <!-- Modal to sub question -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title" id="myModalLabel">Edit Question</h4>

		            </div>
		            <div class="modal-body">
		             <div id="Questionmessage"></div>
		            <form class="form-horizontal" method="post" action="" id="formSubQuestion" name="formSubQuestion" >
		            	<div class="panel-body panel-body-custom"> 
						<div class="form-group col-md-12 nopad-right">
							<label for="question">Question </label>
							<input type="text" name="txt_question" id="txt_question" class="form-control ifCheckIsEmpty" value="" />
							<span id="questionInfo"  class="text-danger marg"></span>
						</div>

						<div class="col-md-3 nopad-left"><input name="txt_option1" id="txt_option1" class="q_op form-control options" type="text" placeholder="Option 1" /><span id="option1Info"  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="1" type="radio"> Correct Answer</label></div>
						
						<div class="col-md-3 "><input name="txt_option2" id="txt_option2" class="q_op form-control options" type="text" placeholder="Option 2" /><span id="option2Info"  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="2" type="radio"> Correct Answer</label></div>
						
						<div class="col-md-3 "><input name="txt_option3" id="txt_option3" class="form-control options" type="text" placeholder="Option 3" /><span  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="3" type="radio"> Correct Answer</label></div>
						
						<div class="col-md-3 "><input name="txt_option4" id="txt_option4" class="form-control options" type="text" placeholder="Option 4" /><span  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="4" type="radio"> Correct Answer</label></div>

						<div class="form-group col-md-12 nopad-right">
							<label for="question">Status</label>
							<select name="status" id="status" aria-controls="datatable2" class="form-control input-sm">
							<option value="">Status</option>
								<option value="Active">Active</option>
								<option value="Inactive">Inactive</option>
							</select>
							<span id="statusInfo"  class="text-danger marg"></span>
						</div>

						
						
					  </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <input type="hidden" name="o_id" id="o_id" value="">
		                <button type="button" class="btn btn-primary" id="edit_sub_questions">Submit </button>
		            </div>

		            </form>
		        </div>
		    </div>
		</div>

		 <!-- // Modal to sub question -->


		 <!-- Questions Listing -->

		 <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="mfp-close close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title" id="myModalLabel">Add New Question</h4>

		            </div>
		            <div class="modal-body">
		             <div id="Questionmessage"></div>
		            <form class="form-horizontal" method="post" action="" id="formSubQuestion" name="formSubQuestion" >
		            	<div class="panel-body"> 
						<div class="form-group">
							<label for="question">Question 1</label>
							<input type="text" name="txt_question" id="txt_question" class="form-control ifCheckIsEmpty" value="" />
							<span id="questionInfo"  class="text-danger marg"></span>
						</div>

						<div class="col-md-3 nopad-left"><input name="txt_option1" id="txt_option1" class="q_op form-control options" type="text" placeholder="Option 1" /><span id="option1Info"  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="1" type="radio"> Correct Answer</label></div>
						
						<div class="col-md-3 "><input name="txt_option2" id="txt_option2" class="q_op form-control options" type="text" placeholder="Option 2" /><span id="option2Info"  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="2" type="radio"> Correct Answer</label></div>
						
						<div class="col-md-3 "><input name="txt_option3" id="txt_option1" class="form-control options" type="text" placeholder="Option 3" /><span  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="3" type="radio"> Correct Answer</label></div>
						
						<div class="col-md-3 "><input name="txt_option4" id="txt_option4" class="form-control options" type="text" placeholder="Option 4" /><span  class="text-danger marg"></span><label class="correct_ans_top"><input name="answers_1" value="4" type="radio"> Correct Answer</label></div>

					<!-- 	<div class="form-group">
							<label for="question">Status</label>
							<select name="status" id="status" aria-controls="datatable2" class="form-control input-sm">
							<option value="">Status</option>
								<option value="Active">Active</option>
								<option value="Inactive">Inactive</option>
							</select>
							<span id="statusInfo"  class="text-danger marg"></span>
						</div> -->

						
						
					  </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <input type="hidden" name="question_id" id="question_id" value="">
		                <button type="button" class="btn btn-primary" id="save_sub_questions">Submit </button>
		            </div>

		            </form>
		        </div>
		    </div>
		</div>





		<div id="quetionnaireModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Quetionnaire Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"> </div>
			<!-- added by pallavi for usertype -->

			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Title<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new1">
					<input type="text" id="txt_title" name="txt_title" class="form-control input-sm" placeholder="Title">
					<span id="titleInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			   </div>
			</div>

			<div class="section row mbn">
			

			 
			</div>    

			
		


			

			<div class="section row mbn">
			<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category </label>
				  
					  <div class="col-lg-8 text_inpu_new categorymain">
					 	<div class="pm Vehicle-pm"><div class="dataTables_length1" >
						<div class="multiple-selection mr5">
							
							<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):	
										
								?>
								<li><input type="checkbox" name="dd_resources[]" id="dd_resources" value="<?php echo $cat['id']?>" placeholder=""  class="events-category"><?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?>
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" id="dd_subresources" value="<?php echo $subcat['id']?>"  class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
									
								
								 <?php endforeach;
								 ?>
								 </li>
								 </ul>
								 <?php
								    }
								 endforeach;
									}
								?>
							</ul>
							
						</div>
						<span id="resourcesInfo" class="text-danger"></span>
					</div>
					</div> 
				</div>
			  </div>
			</div>
			</div>

			
			  
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		 
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="questionId" id="questionId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
<style>
.bg_white
{
	background: #ffffff!important;
}
.nopad-right{padding-right:0px !important;}
.panel-body-custom{
	border:0px solid #fff;
}
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
* {
  .border-radius(0) !important;
}

#field {
    margin-bottom:20px;
}


</style>

<script src="<?php echo base_url(); ?>assets/js/jquery_validate.js"></script>

<script> 
		
$(document).ready(function(){

	 $('#start_date1').datetimepicker({
        });
	 $('#end_date1').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            //format:'Y-m-d'
        });

	 $('#datetimepicker6').datetimepicker({
        });

        $('#datetimepicker7').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            //format:'Y-m-d'
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
 });

jQuery("#txt_promo_amount").keypress(function (e) {  
	//console.log('ada'+e.which);
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && e.which != 46 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 
 

// $('#txt_search').keydown(function (e){
//     if(e.keyCode == 13){
//         $("#search_btn").trigger('click');
//     }
// });	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	



$(document).ready(function(){

	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {

	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(flag){
		changePaginate(0,'q_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}

function showMessagesDelete(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");
	$("#message div").fadeOut(1000);
	//refreshTable(0,'o_id','DESC');
	location.reload();

 
}

function showQuestionMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#Questionmessage").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'o_id','DESC');
		}
		setTimeout(function(){  $("#quetionnaireModel .mfp-close").trigger( "click" ); }, 500);
	}
}



function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'id','DESC');
		}
		setTimeout(function(){  $("#quetionnaireModel .mfp-close").trigger( "click" ); }, 500);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	//getSubQuetionList();
} 
  
$(document).on("click","#add",function() {
	
	$(".panel-title").html("Add New Questionnaire");
	
});



function changeStatus(o_id,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_questions_status",
			data: {"o_id":o_id,"status":status},
		}).success(function (json) { 
			// if(json.status == 1)
			// {		
			// 	if(json.action == "add")
			// 		{   $("#row_"+json.id).html(json.row);
			// 		}
			// }
			showMessagesStatus(json); 
			$("#message div").fadeOut(500);
			//refreshTable(0,'o_id','DESC');
			 location.reload();
		});
}





	// $(document).on("click",".remove_question",function() { 
	// 	var current_index = $('.remove_question').data('index')
	// 	//alert(current_index);
	// 	$.ajax({
	// 			type:"POST",
	// 			dataType:"json",
	// 			url:"<?php echo base_url(); ?>administration/deleteSubQuetionaryById",
	// 			data: {"o_id":current_index},
	// 		}).success(function (json) {
	// 			//console.log('json'+json);
	// 		});
	// });

$(document).on("click","#quetionnaireModel .mfp-close",function() {
	$("#questionId").val("");
	
	$(".pic1 img:last-child").remove();
	$("#titleInfo").html(""); 
	$("#countryInfo").html(""); 
	$("#userTypeInfo").html(""); 
	$("#promocodeInfo").html(""); 
	$("#promoamountInfo").html(""); 
	$("#startDateInfo").html(""); 
	$("#endDateInfo").html(""); 
	// $("#statusInfo").html(""); 

	//$("#ai_file_access").removeAttr("checked","checked");		

	$("input[name='dd_resources[]']").removeAttr("checked", true);
	$("input[name='dd_subresources[]']").removeAttr("checked", true);
	document.getElementById("formUser").reset();
	location.reload();
});

function refreshTable()
{
	//$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	// $("#searchby_status").val("");
	$("#start_date").val("");
	$("#end_date").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'id','DESC'); 	
}
function loadmore(){
	//getSubQuetionList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}

	$(document).ready(function(){     
	$('#edit_sub_questions').on('click', function (e) {
		e.preventDefault();
	
		//var o_id = $("#o_id").val(); 
		var question = $("#txt_question");
			var questionInfo = $("#questionInfo"); 
		var option1 = $("#txt_option1");
			var option1Info = $("#option1Info"); 
		var option2 = $("#txt_option2");
			var option2Info = $("#option2Info"); 
	    var status = $("#status");
			var statusInfo = $("#statusInfo");
 	
		var flag=1;
		if(!validateEmpty(question, questionInfo, "question")){
			flag = 0;
		} 	
		if(!validateEmpty(option1, option1Info, "option")){
				flag = 0;
		}
		if(!validateEmpty(option2, option2Info, "option")){
			flag = 0;
		} 

		if(!checkCombo(status, statusInfo, "status")){
			flag = 0;
		}
		
		var formData1 = new FormData($('#formSubQuestion')[0]); 
		
		if(flag)
		{
			$("#edit_sub_questions").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/edit_sub_questions",
				data: formData1
			}).success(function (json) {
				
				showQuestionMessages(json);
				//getQuetionList();

				//$("#btn_save").removeAttr('disabled','disabled');
				//$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});


	
	//var initial_qa_panel = $('.initial_q');
$(document).on("click","#edit",function() { 
	//$(".pic1 img:last-child").remove();
	$('#myModal').modal('show');
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#o_id").val(id);


	var o_id = $("#o_id").val(); 

		// var question = $("#txt_question");
		// 	var questionInfo = $("#questionInfo"); 

		// var option1 = $("#txt_option1");
		// 	var option1Info = $("#option1Info"); 

		// var option2 = $("#txt_option2");
		// 	var option2Info = $("#option2Info"); 

		
	 //    var status = $("#status");
		// 	var statusInfo = $("#statusInfo");


			 	
		// var flag=1;
		// if(!validateEmpty(question, questionInfo, "question")){
		// 	flag = 0;
		// } 	
		// if(!validateEmpty(option1, option1Info, "option")){
		// 		flag = 0;
		// }
		// if(!validateEmpty(option2, option2Info, "option")){
		// 	flag = 0;
		// } 

		// if(!checkCombo(status, statusInfo, "status")){
		// 	flag = 0;
		// }
		
		var formData1 = new FormData($('#formSubQuestion')[0]);
	if(o_id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getSubQuetionaryById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				//console.log(json);
				for (var i = 0; i < json.length; i++) {
					var eachObj = json[i];
					//console.log(eachObj.id);
					//$("input[name='optionslength[]']").attr('readonly','readonly');
					$("#txt_question").val(json[0].question);
					$("#txt_option1").val(json[0].option1);
					$("#txt_option2").val(json[0].option2);
					$("#txt_option3").val(json[0].option3);
					$("#txt_option4").val(json[0].option4);
					$("#status").val(json[0].status);

					if(json[0].correct_answer==1)
					{
						$("input[name=answers_1][value='1']").prop('checked', 'checked');
					}

					if(json[0].correct_answer==2)
					{
						$("input[name=answers_1][value='2']").prop('checked', 'checked');
					}
					if(json[0].correct_answer==3)
					{
						$("input[name=answers_1][value='3']").prop('checked', 'checked');
					}
					if(json[0].correct_answer==4)
					{
						$("input[name=answers_1][value='4']").prop('checked', 'checked');
					}
				
				}
				
				//console.log(json.length);
				
				$("#add").trigger("click"); 
				
				$("#quetionnaireModel .panel-title").html("Edit Questionnaire");
			}); 

	} 
	
});


  $(document).ready(function(){   
  	var url      = window.location.href; 
  	var o_id = url.substring(url.lastIndexOf('/') + 1);
  	//getSubQuetionList(o_id);
  		var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var user_type= $("#dd_searchBy1").val();  
	// var status= $("#searchby_status").val();
	var country= $("#searchby_country").val();
	var start_date= $("#start_date").val();
	var end_date= $("#end_date").val();
	var search = $("#txt_search").val();
	
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_sub_questions",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy,"o_id":o_id},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
  });

  
function deleteQuetionnaire()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected records?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/deleteSubQuetionaryById",
				data: {"o_id":chkId},
			}).success(function (json) {
				//alert(json.o_id[i]);
				// for (i = 0; i < (json.o_id).length; i++) { 
				// 	$("#row_"+json.o_id[i]).remove();
				// 	var paginate = $("#txt_paginate").val();
				// 	var result = paginate.split(",");
				// 	result[0] = result[0] - (json.o_id).length;
				// 	$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				// }
				//location.reload();
				showMessagesDelete(json);
				//alert((json.ids).length+" user(s) has been deleted.");
				
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
