<!DOCTYPE html>

<html>

<head>

<!-- Meta, title, CSS, favicons, etc. -->

<meta charset="utf-8">

<title>Denso | Login</title>

<meta name="keywords" content="Welcome to denso admin panel" />

<meta name="description" content="Welcome to denso admin panel">

<meta name="author" content="Welcome to denso admin panel">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Font CSS (Via CDN) -->

<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/theme.css">

<!-- Admin Forms CSS -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/admin-forms.css">

<!-- Favicon -->

<link rel="icon" href="<?php echo base_url(); ?>favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!--[if lt IE 9]>

   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

   <![endif]-->

  <style>

	.error{

		border-color:red !important;

	}

	.admin-form .panel-title{font-size:18px !important; font-weight:600;}

  </style>

</head>

<body class="external-page sb-l-c sb-r-c">

<!-- Start: Main -->

<div id="main" class="animated fadeIn">

  <!-- Start: Content-Wrapper -->

  <section id="content_wrapper">

    <!-- begin canvas animation bg -->

    <div id="canvas-wrapper">

      <canvas id="demo-canvas"></canvas>

    </div>

    <!-- Begin: Content -->

    <section id="content">

      <div class="admin-form theme-info" id="login1">

        <div class="row mb15 table-layout">

          <div class="col-xs-6 va-m pln"> <a href="<?php echo base_url(); ?>login" title="Return to Dashboard"> <img src="assets/img/logos/logo.png" title="Denso" class="img-responsive w250"> </a> </div>

          <div class="col-xs-6 text-right va-b pr5"> </div>

        </div>

        <div class="panel panel-info mt10 br-n">

          <div class="panel-heading heading-border bg-white"> <span class="panel-title"><i class="fa fa-sign-in"></i>Admin Login</span> </div>

          <!-- end .form-header section -->

          <form method="post" action="<?php echo base_url(); ?>login" id="contact">

			<div class="panel-body bg-light p20">

              <div class="row">

                <div class="col-sm-12 user_new">
					<div class="p5 mbn alert-dismissable text-danger pln">
					<?php echo $this->session->flashdata('msg');

							$postData = $this->session->flashdata('postData');

					?>
					</div>

                  <!-- end section -->

                  <div class="section">

                    <label for="username" class="field-label text-muted fs18 mb10">Email Address</label>

                    <label for="username" class="field prepend-icon">

                    <input type="text" name="txt_username" id="txt_username" class="gui-input" placeholder="Enter email address" value="<?php if($postData["txt_username"] != ""){ echo $postData["txt_username"]; } ?>">

                    <label for="username" class="field-icon"><i class="fa fa-user"></i></label>                   

                  </div>

                  <!-- end section -->

                  <div class="section mbn">

                    <label for="username" class="field-label text-muted fs18 mb10">Password</label>

                    <label for="password" class="field prepend-icon">

                    <input type="password" name="txt_password" id="txt_password" class="gui-input" placeholder="Enter password">

                    <label for="password" class="field-icon"><i class="fa fa-lock"></i> </label>

                  </div>
				
                  <!-- end section -->

                </div>

              </div>

            </div>

            <!-- end .form-body section -->

            <div class="panel-footer clearfix p10 ph15">

              <input type="submit" class="button btn-info btn-sm btn-block mr10 pull-left" name="btn_login" id="btn_login" value="Login">

            </div>

            <!-- end .form-footer section -->

          </form>

        </div>

      </div>

    </section>

    <!-- End: Content -->

  </section>

  <!-- End: Content-Wrapper -->

</div>

<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->

<script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Bootstrap -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

<!-- Page Plugins  -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/EasePack.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/rAF.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/TweenLite.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pages/login/login.js"></script>

<!-- Theme Javascript -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/utility/utility.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/demo.js"></script>

<!-- Page Javascript -->

<script type="text/javascript">

		$(document).ready(function() {

			<?php if($this->session->flashdata('msg')) { echo '$(".alert-dismissable").fadeOut(10000);'; } ?>

			

			$('.gui-input').bind('focus', function(){

			  $(this).removeClass('error');

			});

			

			function disableBack(id) { window.history.go(id); }

			disableBack(1);

			window.onpageshow = function(evt) { if (evt.persisted) disableBack() }

			

			function preventBack(){window.history.forward();}

				setTimeout("preventBack()", 0);

				window.onunload=function(){null};

			});

        jQuery(document).ready(function() {

			 "use strict";

			// Init Theme Core      

            Core.init();

            // Init Demo JS

            Demo.init();

            // Init CanvasBG and pass target starting location

            CanvasBG.init({

                Loc: {

                    x: window.innerWidth / 2,

                    y: window.innerHeight / 3.3

                },

            });

        });

    </script>

<!-- END: PAGE SCRIPTS -->

</body>

</html>

