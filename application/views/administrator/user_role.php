<?php $this->load->view("header"); ?>
<?php $permissionArr = get_user_permission(32); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb">
										<li class="crumb-link">Administration</li>
										<li class="crumb-link">Users</li>
										<li class="crumb-trail">User Roles</li>
									</ol>
								</div>
								<span class="panel-controls UserRoles">
									<?php if($permissionArr["permission_add"]){ ?>
									<a id="add" class="model-open" href="#userRole"></a>
									<a id="add_modal_btn" href="javascript:void(0)"><i class="fa fa-plus" data-toggle="modal" ></i></a>
									<?php } if($permissionArr["permission_update"]){ ?>
									<a href="javascript:void(0)" id="edit"><i class="fa fa-edit"></i></a>
									<?php } if($permissionArr["permission_delete"]){ ?>
									<a class="" href="javascript:void(0)" onclick="deleteUserRole()"><i class="fa fa-times-circle text-white"></i></a>
									<?php } ?>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onClick="getUserRoleList('role','ASC');" value="Search">
											</div>
										</div>
									</div>
								</div>
								<div id="table" class="table-responsive">
								</div>
								<input type="hidden" id="roleId" name="roleId"/>
							</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userRole" class="popup-basic admin-form mfp-with-anim resetvalues mfp-hide">
		</div>
<script> 
$(document).on("click","#add_modal_btn",function() {
	$.ajax({
			type: "POST",
			dataType: "html",
			url: "<?php echo base_url(); ?>administration/UserRoleForm",
			data: {},
		}).success(function (data) {
			$("#userRole").html(data)
			$("#add").trigger("click");
		});
});
$(document).on("click","#edit",function() {
	if($('.chk:checked').length == 1)
	{
		var id = "";
		var i = 0;
		$('.chk:checked').each(function () {
			id = $(this).val();
		});	
		if(id != "")
		{
			$("#roleId").val(id);
			$.ajax({
					type: "POST",
					dataType: "html",
					url: "<?php echo base_url(); ?>administration/UserRoleForm",
					data: {"id":id},
				}).success(function (data) {
					$("#userRole").html(data)
					$("#add").trigger("click");
				});
		}
	}
	else if($('.chk:checked').length > 1)
	{
		alert("Please select only one record");
	}
	else
	{
		alert("Please select atleast one record");
	}
});
$(document).on("click","#userRole .mfp-close",function() {
	$("#roleId").val("");
	document.getElementById("formUserRoles").reset();
});

function refreshTable()
{
	$("#txt_search").val("");
	getUserRoleList('role','ASC');
}

<?php 
	if($permissionArr["access"])
	{
?>
	getUserRoleList('role','ASC');
<?php } ?>
function getUserRoleList(column,order)
{
	var search = $("#txt_search").val();
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>inventory/list_user_roles",
			data: {"column":column,"order":order,"search":search},
		}).success(function (json) {
			$("#table").html(json.table);
			$("#table").append(json.links);
		});
}
function deleteUserRole()
{
	if($('.chk:checked').length) {
		chkId = new Array();
		var i = 0;
		$('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		});
	  
		if(confirm("Are you sure? you want to delete selected user role.!") == true)
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>inventory/delete_user_role",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
				}
				alert((json.ids).length+" records has been deleted.");
			});
		}
	}
	else {
	  alert('Please Select at Least One User Role.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
