<?php $this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">General</li>
										<li class="crumb-trail">Manage Sub-Categories</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#vehicleSubCatModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteVehicleSubCat()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="category">Vehicle Category</option> 
													<option value="sub_category">Vehicle Sub Category</option> 
												</select>
												</div>
											</div>
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" onClick="changePaginate(0,'v_sub_cat_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'v_sub_cat_id','DESC');" value="Refresh">
											</div>-->
                                              <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                   	    <input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'v_sub_cat_id','DESC');" value="Refresh">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,v_sub_cat_id,DESC"/>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="vehicleSubCatModel" class="popup-basic taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide vehsubcat">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Vehicle Category Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formVehicleSubCat" name="formVehicleSubCat">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 ptn mt5 control-label">Vehicle Category<span class="validationerror">*</span></label>
				  <div class="col-lg-8">
					<div class="dataTables_length1">
						<div class="multiple-selection fixed-height">
						<select name="cat_id" id="cat_id" aria-controls="datatable2" class="form-control input-sm">
							<?php
								if(is_array($category)){
									echo '<option value="">Select Category</option>';
									foreach($category as $cat):	?>
									<option value="<?php echo $cat['id']; ?>"><?php echo $cat['category'];?></option>
								 <?php endforeach;
								}
							?> 
							
						</select>
						
						</div>
						<span id="cInfo"  class="text-danger"></span>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 mt5 ptn control-label">Vehicle Sub Category<span class="validationerror">*</span></label>
				  <div class="col-lg-8">
					<input type="text" id="txt_vehicle_sub_cat" name="txt_vehicle_sub_cat" class="form-control input-sm" placeholder="Vehicle sub category">
					<span id="catInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div> 
			</div>
			<div class="section row mbn browse">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label">Upload Background Image(Max upload limit 5 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="file" id="pdf_file" name="pdf_file" class="">
					<div id="backg"></div>
					<input type="hidden" id="pdf_file_hidden" />
					<span id="pdfInfo"  class="text-danger"></span> 
					<div class="loading-progress"></div>
				  </div>
				</div>
			  </div> 
			</div>    
			  <!-- end section --> 
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="vehicleSubCatId" id="vehicleSubCatId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 
<script>
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'v_cat_id','DESC');
		}
		setTimeout(function(){  $("#vehicleSubCatModel .mfp-close").trigger( "click" ); }, 3000);
	}
}

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	if(flag){
		changePaginate(0,'v_sub_cat_id','DESC');
	}
});
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getVehicleSubCatList();
} 
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add Vehicle Sub-Category");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});

$("#pdf_file").change(function (){
	
   var pdfName = $('#pdf_file').val();
   $("#pdf_file_hidden").val(pdfName);
   
});

$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');
	//alert(id);
	//return false;
	$("#vehicleSubCatId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getVehicleSubCatById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_vehicle_sub_cat").val(json.cat['sub_category']);
				$("#cat_id").val(json.cat['v_cat_id']);
				
				$("#backg").html(json.delete_back);
				$("#add").trigger("click");
				$("#vehicleSubCatModel .panel-title").html("Edit Vehicle Sub-Category");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#vehicleSubCatModel .mfp-close",function() {
	$("#vehicleSubCatId").val("");
	$("#catInfo").html("");
	document.getElementById("formVehicleSubCat").reset();
	location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$("#dd_searchBy").val(""); 
	$(".multiselect-container li").removeClass("active");
	changePaginate(0,'v_sub_cat_id','DESC'); 
}
function loadmore(){
	getVehicleSubCatList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getVehicleSubCatList();

function getVehicleSubCatList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val(); 
	var search = $("#txt_search").val();
	//$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_vehicle_sub_category",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}
function changeStatus(cid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_subcategory_status",
			data: {"subcatId":cid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
		});
}
$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'v_sub_cat_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getVehicleSubCatList();
			}
	});*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var catId = $("#vehicleSubCatId"); 
		var ct = $("#cat_id");
			var cInfo = $("#cInfo");  		
		var vcat = $("#txt_vehicle_sub_cat");
			var vcatInfo = $("#catInfo");  		
		var pdf_file = $("#pdf_file");
			var pdfInfo = $("#pdfInfo"); 
		var flag=1; 
		
		if(!checkCombo(ct, cInfo, "the category")){
			flag = 0;
		}
		
		if(!validateEmpty(vcat, vcatInfo, "vehicle sub category")){
			flag = 0;
		}
		
		if(vcat.val()!=""){
			if(!CheckAlphanumericWithSomeChars(vcat, vcatInfo)){
				flag = 0;
			} 
		} 
		
		if($("#pdf_file_hidden").val() != ""){
			//$(".loading-progress").show();
			var fname = $("#pdf_file_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			
			if(lastVar != 'jpg' && lastVar != 'JPG' && lastVar != 'png' && lastVar != 'PNG'){ 
				pdfInfo.html("Please upload jpg/png file only.<br/>"+fname_e+" is not a jpg/png file.");
				flag = 0;
			}
		}
		
		var formData = new FormData($('#formVehicleSubCat')[0]);    
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false, 
				url: "<?php echo base_url(); ?>administration/save_vehicle_sub_category",
				//data: {"vehicleSubCatId": $("#vehicleSubCatId").val(),"cat_id":$("#cat_id").val(),"txt_vehicle_sub_cat":$("#txt_vehicle_sub_cat").val()},
				data:formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}

				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
			});
		} 
	});		
});

function deleteVehicleSubCat()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("The subcategories having product market kit pages will not get deleted from system.Are you sure want to delete selected sub categories?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_vehicle_sub_categories",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getVehicleSubCatList();
				alert((json.ids).length+" sub categories has been deleted.");
				
			});
		}  
	}
	else {
	  alert('Please select at least one sub category.');
	}
}

function deleteVehicleSubCatBackground(Id)
{
		
		if(confirm("Are you sure you want to continue?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_vehicle_subcategory_background",
				data: {"ids":Id},
			}).success(function (json) {
				getVehicleSubCatList();
				alert("Background removed successfully.");
				$("#backg").html("");
			});
		}  
}

</script>
<?php $this->load->view("footer"); ?>
