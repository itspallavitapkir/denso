<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">

							<?php $message= $this->session->flashdata('Success'); if(!empty($message))?>

								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Users</li>
											<li class="crumb-trail">Manage Users</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#userModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteUsers()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length set_querytypes">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="first_name">First Name</option>
													<option value="last_name">Last Name</option>
													<option value="email_address">Email Address</option>
													<option value="user_type">User Type</option>
												</select>
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<select name="dd_searchBy" id="dd_searchBy1" aria-controls="datatable2" class="form-control input-sm">
												<option value="0">User Type</option>
													<option value="1" selected="selected">Wholesaler</option>
													<option value="2">Dealers</option>
												</select>
												</div>
											</div>
											<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onClick="changePaginate(0,'user_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'user_id','DESC');" value="Refresh">
											</div>-->
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>

											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">User Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<!-- added by pallavi for usertype -->
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">User Type<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<!-- <input type="text" id="txt_first_name" name="txt_first_name" class="form-control input-sm" placeholder="First Name">
					<span id="fInfo"  class="text-danger marg"></span> -->
					<select id="user_type" name="user_type" class="form-control input-sm">
					     <option value="">Please select user type</option>
						  <option value="1">Wholesaler</option>
						  <option value="2">Dealers</option>
						</select> 
					<span id="userTypeInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			 
			</div> 
			<!-- end for user type -->
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">First Name<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_first_name" name="txt_first_name" class="form-control input-sm" placeholder="First Name">
					<span id="fInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Last Name<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_last_name" name="txt_last_name" class="form-control input-sm" placeholder="Last Name">
					<span id="lInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Email Address<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_email_address" name="txt_email_address" class="form-control input-sm" placeholder="Email Address">
					<span id="emailAddressInfo"  class="text-danger"></span>
				  </div>
				</div>
				
			  </div>
			  
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Country<span class="validationerror">*</span></label>
				  <label class="col-lg-8 text_inpu_new">
					<select class="select-sm arrow_new form-control"  name="country" id="country">
						<?php
							if(is_array($country)){
								echo '<option value="">Select</option>';
								foreach($country as $con):	?>
								<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
					<span id="countryInfo"  class="text-danger"></span>
				  </label>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Contact Number</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_contact_number" name="txt_contact_number" class="form-control input-sm" placeholder="+919999999999">
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">PDF File Access</label>
				  <div class="col-lg-8 chkboxpaddtop text_inpu_new">
					<input type="checkbox" id="pdf_file_access" name="pdf_file_access" value="1">  
				  </div>
				</div>
			  </div>
			  
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Company Name</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_company" name="txt_company" class="form-control input-sm" placeholder="Company Name">
				  </div>
				</div>
			  </div> 
			  <div class="col-sm-6 download_once">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">AI File Access</label>
				  <div class="col-lg-8 chkboxpaddtop text_inpu_new">
					<input type="checkbox" id="ai_file_access" name="ai_file_access" value="1">  
				  </div>
				</div>
			  </div>
			</div> 
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Super Admin Access</label>
				  <div class="col-lg-8 text_inpu_new chkboxpaddtop">
					<input type="checkbox" id="super_admin_access" name="super_admin_access" value="1">  
				  </div>
				</div>
			  </div>  
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first" >User Password<span class="validationerror" id="labelpassword"></span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="password" id="txt_password" name="txt_password" value="" class="form-control input-sm" placeholder="User Password">  
					<span id="pwdInfo"  class="text-danger"></span>
					<input type="hidden" id="password_status" name="password_status">  
					
				  </div>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn">
			<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category </label>
				  
					  <div class="col-lg-8 text_inpu_new categorymain">
					 	<div class="pm Vehicle-pm"><div class="dataTables_length1" >
						<div class="multiple-selection mr5">
							
							<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):	
										
								?>
								<li><input type="checkbox" name="dd_resources[]" id="dd_resources" value="<?php echo $cat['id']?>" placeholder=""  class="events-category"><?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?>
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" id="dd_subresources" value="<?php echo $subcat['id']?>"  class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
									
								
								 <?php endforeach;
								 ?>
								 </li>
								 </ul>
								 <?php
								    }
								 endforeach;
									}
								?>
							</ul>
							
						</div>
						<span id="resourcesInfo" class="text-danger"></span>
					</div>
					</div> 
				</div>
			  </div>
			</div>
			<div class="col-sm-6">
					<div class="form-group">
					  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Language Translator</label>
					  <div class="col-lg-8 text_inpu_new chkboxpaddtop">
						<input type="checkbox" id="lang_translator" name="lang_translator" value="1">  
					  </div>
					</div>
				</div> 
			  </div> 
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		 
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="userId" id="userId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
</style>

<script> 
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	

$(document).on("change","#user_type",function() {	
	if($(this).val()=='Supplier')
		$(".download_once").show();
	else{
		$(".download_once").hide();		
		$("#download_check").removeAttr("checked");
	}
});
	//console.log(liObj);

$(document).ready(function(){
  
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {

	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	// if($("#txt_search").val()==""){
	// 	alert("Please enter the search term");
	// 	flag=0;
	// 	return false;
	// } 
	

	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	// if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
	// 		//alert("Spaces are not allowed");
	// 		flag = 0;
	// 		return false;
	// }
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}


function changeStatus(uid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_user_status",
			data: {"userId":uid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
		});
}

function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getUserList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New User");
	$("#user_type").trigger("change");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#userId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getUserById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_first_name").val(json.first_name);
				$("#txt_last_name").val(json.last_name);
				$("#txt_first_name").attr('readonly','readonly');
				$("#txt_last_name").attr('readonly','readonly');
				$("#txt_email_address").val(json.email_address);
				$("#txt_contact_number").val(json.contact_details);
				$("#country").val(json.country);
				$("#user_type").val(json.user_type);
				if(json.v_cat_id){
					var valArr = json.v_cat_id; 
					var dataarray = valArr.split(","); 
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				}
				
				if(json.v_sub_cat_id){
					var valSubArr = json.v_sub_cat_id; 
					var datasubarray = valSubArr.split(","); 
					for (var count = 0; count < datasubarray.length; count++) { 
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				
				//console.log(json.country);
				$("#txt_company").val(json.company);
				if(json.ai_file_access == "1"){
					$("#ai_file_access").attr("checked","checked");
				}
				else{
					$("#ai_file_access").removeAttr("checked","checked");
				}
				if(json.pdf_file_access == "1"){
					$("#pdf_file_access").attr("checked","checked");
				}
				
				else{
					$("#pdf_file_access").removeAttr("checked","checked");
				}
				if(json.super_admin_access =="1"){
					$("#super_admin_access").attr("checked","checked");
				}
				else{
					$("#super_admin_access").removeAttr("checked","checked");
				}
				if(json.lang_translator =="1"){
					$("#lang_translator").attr("checked","checked");
				}
				else{
					$("#lang_translator").removeAttr("checked","checked");
				}
				$("#password_status").val(1);
				//$("#txt_city").val(json.city); 
				 
				$("#add").trigger("click"); 
				$("#labelpassword").html("");
				$("#userModel .panel-title").html("Edit User");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#userModel .mfp-close",function() {
	$("#userId").val("");
	$("#labelpassword").html("");
	$(".pic1 img:last-child").remove();
	$("#fInfo").html("");  	
	$("#lInfo").html("");  	
	$("#emailAddressInfo").html("");  	
	$("#countryInfo").html(""); 
	$("#userTypeInfo").html(""); 
	$("#txt_first_name").removeAttr('readonly','readonly');
	$("#txt_last_name").removeAttr('readonly','readonly');
	$("#super_admin_access").removeAttr("checked","checked");
	$("#pdf_file_access").removeAttr("checked","checked");
	$("#ai_file_access").removeAttr("checked","checked");			
	$("input[name='dd_resources[]']").removeAttr("checked", true);
	$("input[name='dd_subresources[]']").removeAttr("checked", true);
	document.getElementById("formUser").reset();
	location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'user_id','DESC'); 	
}
function loadmore(){
	getUserList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getUserList();

function getUserList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var user_type= $("#dd_searchBy1").val();
	var search = $("#txt_search").val();
	
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_users",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy,"user_type":user_type},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'user_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var userId = $("#userId"); 
		var fName = $("#txt_first_name");
			var fInfo = $("#fInfo");  	
		var lName = $("#txt_last_name");
			var lInfo = $("#lInfo");  	
		var emailAddress = $("#txt_email_address");
			var emailInfo = $("#emailAddressInfo"); 
			 			
		var upassword = $("#txt_password");
			var pwdInfo = $("#pwdInfo");
			
		var country = $("#country");
			var countryInfo = $("#countryInfo");  		
		
		var userType = $("#user_type");
			var userTypeInfo = $("#userTypeInfo");	
							
		var flag=1;
		if(!validateEmpty(fName, fInfo, "first name")){
			flag = 0;
		} 	
		if(!validateEmpty(lName, lInfo, "last name")){
			flag = 0;
		}
		if(!validateEmpty(emailAddress, emailInfo, "email address")){
			flag = 0;
		}
		if($("#password_status").val()==""){
			if(!validateEmpty(upassword, pwdInfo, "the password")){
				flag = 0;
			}
			else if(CheckPasswordText(upassword, pwdInfo)){
				
			} 
			else{
				flag=0;
			}
		}
		
		if($("#password_status").val()!=""){
			if(CheckPasswordText(upassword, pwdInfo)){
				
			} 
			else{
				flag=0;
			}
		}
		
		if(!validateEmpty(emailAddress, emailInfo, "email address")){
			flag = 0;
		}
		
		if(!checkCombo(country,countryInfo,"the country")){
			flag = 0;
		}
		
		if(emailAddress.val()!=""){
			if(!validateEmail(emailAddress, emailInfo)){
				flag = 0;
			}
		}
		 
		if(fName.val()!=""){	 
			if(!CheckAlphabates(fName, fInfo)){
				flag = 0;
			} 	 
		}
		
		if(lName.val()!=""){	 
			if(!CheckAlphabates(lName, lInfo)){
				flag = 0;
			} 	 
		} 
		
		if(!checkCombo(userType, userTypeInfo, "user type")){
			flag = 0;
		}
		
		
		var formData = new FormData($('#formUser')[0]); 
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_users",
				//data: {"userId": $("#userId").val(),"txt_first_name":$("#txt_first_name").val(),"txt_last_name":$("#txt_last_name").val(),"email_address":$("#txt_email_address").val(),"contact_number":$("#txt_contact_number").val(),"user_type":$("#user_type").val(),"country":$("#country").val(),"state":$("#txt_state").val(),"city":$("#txt_city").val()},
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected users?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_users",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				alert((json.ids).length+" user(s) has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
