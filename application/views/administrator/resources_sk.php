<?php $CI =& get_instance();

$CI->load->model('administration_model');
$this->load->view("header"); ?>

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Manage Resources PMK</li>
											<li class="crumb-trail">Resources PMK</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#resourceModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a id="view_details" class="model-open" href="#resourceSKModel" title="Add New Record" style="display:none;"></a> 
									<a class="" href="javascript:void(0)" onclick="deleteReource()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
									<a title="Edit" alt="Edit" id="edit" class="edit_once" data-option="" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="resources_sk_pdf">Resources PMK File</option>
												</select>
												</div>
											</div>
											
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,resource_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
	  <div id="resourceModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Resource PMK Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formResources" name="formResources" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5  control-label new_first">Sale Country <span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<div class="dataTables_length1">
						<div class="multiple-selection mr5 fixed-height salecountry datamaltiple-select">
						<select name="dd_country[]" id="dd_country" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
							
							<?php
								if(is_array($country)){
									//echo '<option value="-1">Select</option>';
									foreach($country as $con):	?>
									<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
								 <?php endforeach;
								}
							?> 
						</select>
						</div>
						<span id="countryInfo"  class="text-danger"></span>
					</div>
				  </div>
				</div>
			  </div>
			</div> 
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5  control-label new_first"> Search And Select Users<span class="validationerror">*</span></label>
				  <div class="col-lg-10 text_inpu_new">
					<div style="margin-bottom:5px;" class="resource_text_wrap"><input type="text" id="search_user" name="search_user" class="form-control input-sm" placeholder="Search User"></div>  
					<div class="dataTables_length1">
						
						<select multiple size="10" id="from_list" name="from_list" class="">
						  
						</select>
						<div class="controls"> 
							<a href="javascript:moveAll('from_list', 'to_list')" title="Move All Users">&gt;&gt;</a> 
							<a href="javascript:moveSelected('from_list', 'to_list')" title="Move Selected Users">&gt;</a> 
							<a href="javascript:moveSelected('to_list', 'from_list')"  title="Remove Selected Users">&lt;</a> 
							<a href="javascript:moveAll('to_list', 'from_list')" title="Remove All Users">&lt;&lt;</a> </div>
						<div class="multiple-selection mr5 fixed-height salecountry">
							<select multiple="multiple"  size="10" id="to_list" name="sel_users[]"  class="multiselect"></select>
						</div>
						<span id="userTypeInfo" style="float:left;"  class="text-danger"></span>
						</div>
					
				  </div>
				</div>
			  </div>
			</div>  
			  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select How to upload resources PMK file</label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="radio" name="ftype" value="browse" class="seloption" checked>Browse File
					<input type="radio" name="ftype" value="usetext" class="seloption">Use Text Box
					
				  </div>
				</div>
			  </div> 
			</div>  
			
			<div class="section row mbn browse">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload resource sales kit file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="file" id="pdf_file" name="pdf_file" class="">
					<input type="hidden" id="pdf_file_hidden" />
					<span id="pdfInfo"  class="text-danger"></span> 
					<div class="loading-progress"></div>
				  </div>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn textforfile">
			  <div class="col-sm-8">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-3 pn mt5 control-label new_first">Upload file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="text" id="text_pdf_file" name="text_pdf_file" class="form-control input-sm" placeholder="Enter already uploaded PDF file name">
					<span id="pdfTextInfo"  class="text-danger"></span> 
				  </div>
				</div>
			  </div> 
			</div>
			
			
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="resourceId" id="resourceId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
	  
  <!-- end: .panel -->
   	  <div id="resourceSKModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Resource PMK Details</span> </div>
		<!-- end .panel-heading section -->
		
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5  control-label new_first">Sale Country</label>
				  <div class="col-lg-10 text_inpu_new">
					<b><span id="country_details"></span></b>
				  </div>
				</div>
			  </div>
			</div>  
			<br/>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5  control-label new_first">Users</label>
				  <div class="col-lg-10 text_inpu_new">
					
					<div class="dataTables_length1">
						<b><span id="user_details"></span></b>
				  </div>
				</div>
			  </div>
			</div>  
			</div>
			<br/>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5  control-label new_first">Resource PMK File</label>
				  <div class="col-lg-10 text_inpu_new">
					
					<div class="dataTables_length1">
						<div id="file_details"></div>
				  </div>
				</div>
			  </div>
			</div>  
			</div>
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="resourceSKId" id="resourceSKId" class="gui-input"> 
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 
<!-- Summernote CSS  --> 
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
select {
        width: 200px;
        float: left;
}
.controls {
	width: 40px;
	float: left;
	margin: 10px;
}
.controls a {
	background-color: #eb181e;
	border-radius: 4px;
	border: 2px solid #eb181e;
	color: #ffffff;
	padding: 2px;
	font-size: 14px;
	text-decoration: none;
	display: inline-block;
	text-align: center;
	margin: 5px;
	width: 30px;
}
</style>
<script type="text/javascript">
	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$(".multiple-selection").click(function(e) {
	
	if(e.target.className =="multiselect dropdown-toggle btn btn-default" || e.target.className=="multiselect-selected-text"){
		
		$("#vehicle-categories").hide(); 
	}
});
	
	function moveAll(from, to) {
        $('#'+from+' option').remove().appendTo('#'+to); 
    }
    
    function moveSelected(from, to) {
        $('#'+from+' option:selected').remove().appendTo('#'+to); 
        ddUserList();
    }
    
    function selectAll_Val() {
        $("#to_list select option").attr("selected","selected");
    }
	
	
	function ddCountries(){
		
		$('#dd_country').multiselect({
			includeSelectAllOption: true,
			buttonText: function(options, select) {
				return 'Select Country';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text());
				});
				return labels.join(' - ');
			}
		});
	}
	
	/*function ddUserList(){
		$("#to_list").multiselect({
	
			autoOpen: true,
			close: function(event, ui) {
				$("#to_list").multiselect("open");
			}
		});
		
	}*/
	
	$(document).ready(function() {
		$(".textforfile").hide();
		$(document).on("click",".seloption",function() {
			var option = $('input[name=ftype]:radio:checked').val();
			if(option == "browse"){
				$(".textforfile").hide();
				$(".browse").show();
			}
			else if(option == "usetext"){
				$(".textforfile").show();
				$(".browse").hide();
			}
		});
		
		$(".textforaifile").hide();
		$(document).on("click",".seloptionai",function() {
			var option = $('input[name=ftypeai]:radio:checked').val();
			
			if(option == "browseai"){
				$(".textforaifile").hide();
				$(".browseai").show();
			}
			else if(option == "useaitext"){
				$(".textforaifile").show();
				$(".browseai").hide();
			}
		});
		//$(".chk").on("click", function() {
		
		
			ddCountries();
			//ddUserList();
	});
	
	
</script>
<script src="<?php echo base_url();?>js/jquery.progresstimer.js" type="text/javascript"></script>

<script>
	
$('html').keyup(function(e){
		if(e.keyCode == 8){
			
			$("#search_user").trigger("keyup");
		}
	})	
 $( document ).ready(function() {
	 
	
	$(".edit_once").hide();
	
	$(document).on("click","input:checkbox",function() {
		
        if($(this).is(":checked")) {
			
			$(".edit_once").show();
			$(".edit_once").attr('data-option',$(this).val());
		}
		else{
			
			var count = $(".chk:checked").length;
			if(count ==0){
				$(".edit_once").hide();
				$(".edit_once").attr('data-option','');
			}
		}
	}); 
	
	 
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(8000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'resource_id','DESC');
		}
		setTimeout(function(){  $("#resourceModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getResorcesList();
} 

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	if(flag){
		changePaginate(0,'resource_id','DESC');
	}
});

$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});
 
$(document).on("click","#add",function() {
	$(".panel-title").html("Add New Resource PMK");
	//ddUserList();
	
	//$("#userId").val("");
	//document.getElementById("formResources").reset();
});

/*$(document).on("click","#view",function() {
	$(".panel-title").html("View Resource SK Details"); 
});*/



$(document).on("keyup","#search_user",function() {
	//alert('rtset');
	var keyword = $(this).val();
	var  cid = "";
	$( "#dd_country option:selected" ).each(function() {
         cid = $("#dd_country").val() + ",";
    });
    //var to_list = $("#to_list").val();
    var arrUserList = new Array; 
		$("#to_list option").each  ( function() {
		   arrUserList.push ( $(this).val() );
		});
  
	$("#from_list").html('');
	//if(keyword!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getUsersList",
				data: {"keyword" : keyword,"to_list":arrUserList,"cid" : cid},
			}).success(function (json) {
				//console.log(json);
				$("#from_list").html(json.options);
			});
	/*}
	else{
		$("#from_list").html('');
	}*/
});

$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');  
	$("#resourceId").val(id);
	  
	if(id != ""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getResourcesSKById",
				data: {"id":id},
			}).success(function (json) {
				$("#dd_country").val(json.res.country); 
				$("#user_type").val(json.res.user_type); 
				$('#pdf_file_hidden').val(json.res.resources_sk_pdf);
				if(json.res.country){
					//$("#dd_country option").attr('selected', 'selected');
					var valArr = json.res.country;
					var dataarray=valArr.split(","); 
					$("#dd_country").val(dataarray);
					$("#dd_country").multiselect("refresh"); 
				}
				if(json.res.resource_cat!=0){
					$("#sel_category").val(json.res.resource_cat);
				}
				if(json.res.resource_subcat!=0){
					//$("#sel_sub_category").val(json.res.resource_subcat);  
				}
				//console.log(json.options_user); 
				$("#from_list").html(json.from_list);
				$("#to_list").html(json.options_user);
				//selectAll_Val();
				$("#to_list option").attr("selected","selected");
				//$("#dd_country").trigger('change');
				$("#add").trigger("click");
				
				$("#resourceModel .panel-title").html("Edit Resource PMK");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});

$(document).on("click","#view",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');  
	$("#resourceId").val(id);
	
	if(id != ""){
				$.ajax({
						type: "POST",
						dataType: "json",
						//fileElementId :'user_photo',
						url: "<?php echo base_url(); ?>administration/getResourcesSKDetailsById",
						data: {"id":id},
				}).success(function (json) {
						$("#user_details").html(json.users);
						$("#country_details").html(json.country);
						$("#file_details").html(json.file);
						
						$("#view_details").trigger("click");
						$("#resourceSKModel .panel-title").html("View Resource PMK Details");
						ajaxRunning = false;
				});
				
	} 
	else{
				$("#view_details").trigger("click");
	}
});

$(document).on("click","#resourceModel .mfp-close",function() {
	//$("#userId").val("");
	$("#pnInfo").html("");
	$("#countryInfo").html("");
	$("#resourceInfo").html("");			
	$("#pdfInfo").html("");
	$("#pdfTextInfo").html("");
	document.getElementById("formResources").reset();
	location.reload();
	
});
function loadmore(){
	getResorcesList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
} 
function refreshTable()
{
	$("#txt_search").val("");
	$(".edit_once").hide();
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	 $("#dd_searchBy").val(''); 
	changePaginate(0,'resource_id','DESC'); 
}

getResorcesList();

function getResorcesList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	//$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_resources_sk",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
$("#dd_country").change(function () {
		  	
          var cid = "";
          $("#dd_country option:selected").each(function () {
                cid += $(this).val() + ",";
          });
          
         // var to_list = $("#to_list").val();
		 var to_list = new Array; 
		$("#to_list option").each  ( function() {
		   to_list.push ( $(this).val() );
		});
          $("#from_list").html(''); 
			$.ajax({
					type: "POST",
					dataType: "json",
					url: "<?php echo base_url(); ?>administration/getUsersList",
					data: {"keyword" : '',"to_list":to_list,"cid" : cid},
				}).success(function (json) {
					
					$("#to_list").html(json.to_list_options);
					$("#to_list option").attr("selected","selected");
					$("#from_list").html(json.options);
				});
		 
          // do what you want with selections variable here
});
$("#pdf_file").change(function (){
	
   var pdfName = $('#pdf_file').val();
   $("#pdf_file_hidden").val(pdfName);
   
});
 
 
$(document).ready(function(){  
	$(".loading-progress").hide();
	$("#txt_search").val("");
	changePaginate(0,'resource_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getResorcesList();
			}
	});	
	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault(); 
		var resourceId = $("#resourceId"); 
		//var user_type = $("#user_type");
			var userTypeInfo = $("#userTypeInfo");     	
		var ddCountry = $("#dd_country");
			var countryInfo = $("#countryInfo");     	
		var to_list = $("#to_list");  			
		var pdf_file = $("#pdf_file");
			var pdfInfo = $("#pdfInfo");	
			var pdfTextInfo = $("#pdfTextInfo");
		var flag=1;
		
		
		chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		$("#to_list option").attr('selected','selected');
		
		
		var arrUserList = new Array; 
		$("#to_list option").each  ( function() {
		   arrUserList.push ( $(this).val() );
		});
  
		pdfInfo.html("");
		countryInfo.html("");
		userTypeInfo.html("");
		pdfInfo.html("");
		pdfTextInfo.html("");
		if(!checkCombo(ddCountry, countryInfo, "country")){
			flag = 0;
		} 
		
		if(arrUserList==""){
			userTypeInfo.html("Please select at list one user from above searched user list");
			flag = 0;
		}
		
		/*if(!$("#to_list option").attr("selected","selected")){
			
			userTypeInfo.html("Please select all values in list");
			flag = 0;
		}*/
		//return false;
		if($('#pdf_file').val() != ""){
			$(".loading-progress").show();
		} 
		if($("input[name=ftype]:checked").val() == 'browse'){
			if($('#pdf_file_hidden').val()==""){
				pdfInfo.html("Please upload file");
				flag = 0;
			}
			
			if($("#pdf_file_hidden").val() != ""){
					var fname = $("#pdf_file_hidden").val(); 
					var fname_e = fname.split('/').pop().split('\\').pop();  
					
					fname_ext = fname_e.split('.');  
					var lastVar = fname_ext.pop();
					
					if(lastVar != 'png' && lastVar != 'PNG' && lastVar != 'pdf' && lastVar != 'PDF' && lastVar != 'xls' && lastVar != 'XLS' && lastVar != 'xlsx' && lastVar != 'XLSX' && lastVar != 'ppt' && lastVar != 'PPT' && lastVar != 'doc' && lastVar != 'DOC' && lastVar != 'docx' && lastVar != 'DOX' && lastVar != 'zip' && lastVar != 'ZIP'){ 
						pdfInfo.html("Please upload png/xls/xlsx/ppt/doc/dox/zip file only.<br/>"+fname_e+" is a invalid file.");
						flag = 0;
					}
				}
		}
		else if($("input[name=ftype]:checked").val() == 'usetext'){
			 
			if($('#text_pdf_file').val() == ""){
				pdfTextInfo.html("Please upload file");
				flag = 0;
			}
		}
		
		if($("input[name=ftype]:checked").val() == 'browse'){
		if($("#pdf_file_hidden").val() != ""){
			//$(".loading-progress").show();
				var fname = $("#pdf_file_hidden").val(); 
				var fname_e = fname.split('/').pop().split('\\').pop();  
				
				fname_ext = fname_e.split('.');  
				var lastVar = fname_ext.pop();
				
				if(lastVar != 'pdf' && lastVar != 'PDF' && lastVar != 'doc' &&  lastVar != 'docx' && lastVar != 'xls' && lastVar != 'xlsx' && lastVar != 'ppt' && lastVar != 'zip'){ 
					pdfInfo.html("Please upload pdf/doc/docx/xls/ppt/zip file only.<br/>"+fname_e+" is not a pdf/doc/docx/xls/ppt/zip file.");
					flag = 0;
				}
			}
			
		}
		
		var formData = new FormData($('#formResources')[0]);  
		formData.append("chkId", chkId);
		formData.append("sel_users", arrUserList);
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$(".panel-footer").append('<img src="<?php echo base_url();?>img/gif-load.gif" />');
			if($("#pdf_file_hidden").val() != ""){
				var progress = $(".loading-progress").progressTimer({
					timeLimit: 7200,
					onFinish: function () {
						//alert('completed!');
					}
				});
			}
		 
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_resources_sk", 
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			}).error(function(){
				progress.progressTimer('error', {
					errorText:'ERROR!',
					onFinish:function(){
						//alert('There was an error processing your information!');
					}
				});
				
			}).done(function(){
				progress.progressTimer('complete');
			});
		} 
		else{$("#btn_save").show();	}
	});		
});
function deleteReource()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure? you want to delete selected resource sales kit.") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_resources_sk",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				} 
				alert((json.ids).length+" resource sales kit has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one resource. ');
	}
}
</script>

<?php $this->load->view("footer"); ?>
