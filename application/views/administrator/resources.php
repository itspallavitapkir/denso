<?php $CI =& get_instance();

$CI->load->model('administration_model');
$this->load->view("header"); ?>

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Resources</li>
										<li class="crumb-trail">Manage Resources</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#resourceModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteReource()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="product_name">Resource Name</option>
													<option value="pdf_name">PDF File</option> 
													<option value="ai_file_name">AI File</option>  
												</select>
												</div>
											</div>
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" onClick="changePaginate(0,'resource_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'resource_id','DESC');" value="Refresh">
											</div>-->
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,resource_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="resourceModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Resource Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formResources" name="formResources" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Resource Name <span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_product_name" name="txt_product_name" class="form-control input-sm" placeholder="Resource Name">
					<span id="pnInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5  control-label new_first">Sale Country <span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<div class="dataTables_length1">
						<div class="multiple-selection mr5 fixed-height salecountry datamaltiple-select">
						<input type="hidden" name="txt_country" id="txt_country" />
						<select name="dd_country[]" id="dd_country" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
							
							<?php
								if(is_array($country)){
									//echo '<option value="-1">Select</option>';
									foreach($country as $con):	?>
									<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
								 <?php endforeach;
								}
							?> 
						</select>
						</div>
						<span id="countryInfo"  class="text-danger"></span>
					</div>
				  </div>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label"><div class="new_first resource-category">Resource Category <span class="validationerror">*</span></div></label>
				   <div class="col-lg-8 text_inpu_new resourcecategory">
					 <div class="pm"><div class="dataTables_length1">
						<div class="multiple-selection">
						<input type="hidden" name="txt_resource" id="txt_resource" />	
						<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):	
										
								?>
								<li><input type="checkbox" name="dd_resources[]" name="dd_resources" value="<?php echo $cat['id']?>" placeholder="" id="category_<?php echo $cat['id']?>" class="events-category"><?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?>
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" value="<?php echo $subcat['id']?>" id="subcategory_<?php echo $subcat['id']?>" class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
									
								</li>
								 <?php endforeach;
								 ?>
								 </ul>
								 <?php
								    }
								 endforeach;
									}
								?>
							</ul>
						</div>
						
					</div></div>
					
				  </div>
				  <span id="resourceInfo" style="margin-left:170px;"  class="text-danger"></span>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Resource Description</label>
				  <div class="col-lg-12 text_inpu_new">
					<textarea rows="15" cols="80" id="txt_product_description" name="txt_product_description"></textarea>
					<span id="pdInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			</div>
			 
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select How to upload PDF file</label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="radio" name="ftype" value="browse" class="seloption" checked>Browse PDF File
					<input type="radio" name="ftype" value="usetext" class="seloption">Use Text Box
					
				  </div>
				</div>
			  </div> 
			</div>  
			
			<div class="section row mbn browse">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload PDF file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="file" id="pdf_file" name="pdf_file" class="">
					<input type="hidden" id="pdf_file_hidden" />
					<span id="pdfInfo"  class="text-danger"></span> 
					<div class="loading-progress"></div>
				  </div>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn textforfile">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload PDF file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="text" id="text_pdf_file" name="text_pdf_file" class="form-control input-sm" placeholder="Enter already uploaded file name">
				  </div>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select How to upload AI file</label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="radio" name="ftypeai" value="browseai" class="seloptionai" checked>Browse AI File
					<input type="radio" name="ftypeai" value="useaitext" class="seloptionai">Use Text Box
					
				  </div>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn browseai">
				<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload Adobe Illustrator file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="file" id="ai_file" name="ai_file" class="">
					<input type="hidden" id="ai_file_hidden" />
					<span id="aiInfo"  class="text-danger"></span>
					<div class="loading-progress2"></div>
				  </div>
				</div>
			    </div>
		    </div>
		    <div class="section row mbn textforaifile">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload AI file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="text" id="text_ai_file" name="text_ai_file" class="form-control input-sm" placeholder="Enter already uploaded AI/ZIP file name">
				  </div>
				</div>
			  </div> 
			</div>
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="resourceId" id="resourceId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 
<!-- Summernote CSS  --> 
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}

</style>
<script type="text/javascript">
	$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});
$(".multiple-selection").click(function(e) {
	
	if(e.target.className =="multiselect dropdown-toggle btn btn-default" || e.target.className=="multiselect-selected-text"){
		
		$("#vehicle-categories").hide(); 
	}
});
	/*function ddResources(){
		$('#dd_resources').multiselect({
			buttonText: function(options, select) {
				return 'Select Resources';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text());
				});
				return labels.join(' - ');
			}
		});
	}*/
	function ddCountries(){
		
		$('#dd_country').multiselect({
			includeSelectAllOption: true,
			buttonText: function(options, select) {
				return 'Select Country';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text());
				});
				return labels.join(' - ');
			}
		});
	}
	
	$(document).ready(function() {
		$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	$(".textforfile").hide();
	$(document).on("click",".seloption",function() {
		var option = $('input[name=ftype]:radio:checked').val();
		if(option == "browse"){
			$(".textforfile").hide();
			$(".browse").show();
		}
		else if(option == "usetext"){
			$(".textforfile").show();
			$(".browse").hide();
		}
	});
	
	$(".textforaifile").hide();
	$(document).on("click",".seloptionai",function() {
		var option = $('input[name=ftypeai]:radio:checked').val();
		
		if(option == "browseai"){
			$(".textforaifile").hide();
			$(".browseai").show();
		}
		else if(option == "useaitext"){
			$(".textforaifile").show();
			$(".browseai").hide();
		}
	});
	
	$('.events-category').click( function(){
		var c = this.checked; 
		
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	 
	$('.events-category').change( function(){
    var c = this.checked; 
    
    if( c ){
        $(this).next('.event-children').css('display', 'block'); 
    }else{
        $(this).next('.event-children').css('display', 'none');
    }
});

$('.events-child-category-all').change( function(){
    $(this).parent().siblings().find(':checkbox').attr('checked', this.checked);
});
		ddCountries();
	});
	
	
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>

<script src="<?php echo base_url();?>js/jquery.progresstimer.js" type="text/javascript"></script>

<script>
 $( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'resource_id','DESC');
		}
		setTimeout(function(){  $("#resourceModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getResorcesList();
} 
$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	if(flag){
		changePaginate(0,'resource_id','DESC');
	}
});
 
$(document).on("click","#add",function() {
	$(".panel-title").html("Add New Resource");
	loadeditor();
	//$("#userId").val("");
	//document.getElementById("formResources").reset();
});

$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');  
	$("#resourceId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getResourceById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_product_name").val(json.product_name);
				$("#txt_product_description").val(json.product_description); 
				$("#pdf_file_hidden").val(json.pdf_file); 
				$("#ai_file_hidden").val(json.ai_file_name); 
				if(json.pdf_name != ""){
					$("#text_pdf_file").val(json.pdf_name);
				}
				if(json.ai_file_name != ""){
					$("#text_ai_file").val(json.ai_file_name);
				}
				$("#dd_country").val(json.countries); 
				if(json.countries){
					//$("#dd_country option").attr('selected', 'selected');
					var valArr = json.countries;
					var dataarray=valArr.split(","); 
					$("#dd_country").val(dataarray);
					$("#dd_country").multiselect("refresh"); 
				}
				/*$("#dd_resources").val(json.resource_categories); 
				if(json.resource_categories){
					//$("#dd_country option").attr('selected', 'selected');
					var valArr = json.resource_categories;
					var dataarray=valArr.split(","); 
					$("#dd_resources").val(dataarray);
					$("#dd_resources").multiselect("refresh"); 
				}*/
				
				if(json.resource_categories){
					
					var valArr = json.resource_categories;
					var dataarray = valArr.split(",");
					//$('#dd_resources').attr('chekced', 'chekced');
					//$("#dd_resources").val(dataarray);
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				}
				if(json.resource_subcategories){
					//$("input[name='dd_subresources[]']").attr("checked", false);
					var valSubArr = json.resource_subcategories;
					var datasubarray = valSubArr.split(",");
					//alert(datasubarray);
					//$('#dd_resources').attr('chekced', 'chekced');
					//$("#dd_resources").val(dataarray);
					for (var count = 0; count < datasubarray.length; count++) {
						
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				
				$("#add").trigger("click");
				$("#resourceModel .panel-title").html("Edit Resource");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#resourceModel .mfp-close",function() {
	//$("#userId").val("");
	$("#pnInfo").html("");
	$("#countryInfo").html("");
	$("#resourceInfo").html("");			
	$("#pdfInfo").html("");
	$("#aiInfo").html("");
	document.getElementById("formResources").reset();
	location.reload();
	
});
function loadmore(){
	getResorcesList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
function loadeditor(){ 
		
    CKEDITOR.disableAutoInline = true; 
	CKEDITOR.replace('txt_product_description', {
				height: 210,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
				},
			}); 
}
function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	 $("#dd_searchBy").val(''); 
	changePaginate(0,'resource_id','DESC'); 
}

getResorcesList();

function getResorcesList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	//$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_resources",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}

$("#pdf_file").change(function (){
	
   var pdfName = $('#pdf_file').val();
   $("#pdf_file_hidden").val(pdfName);
   
});

$("#ai_file").change(function (){
	
   var aiName = $('#ai_file').val();
   $("#ai_file_hidden").val(aiName);
   
});

$(document).ready(function(){ 
	$(".loading-progress").hide();
	$(".loading-progress2").hide();
	$("#txt_search").val("");
	changePaginate(0,'resource_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getResorcesList();
			}
	});	
	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault(); 
		var resourceId = $("#resourceId"); 
		var pnName = $("#txt_product_name");
			var pnInfo = $("#pnInfo");     	
		var ddCountry = $("#dd_country");
			var countryInfo = $("#countryInfo");     	
		var ddResource = $("#dd_resources");
			var resourceInfo = $("#resourceInfo");    			
		var pdf_file = $("#pdf_file");
			var pdfInfo = $("#pdfInfo"); 
		var ai_file = $("#ai_file");
			var aiInfo = $("#aiInfo");	 			
		var flag=1;
		
		pdfInfo.html("");
		aiInfo.html("");
		if(!validateEmpty(pnName, pnInfo, "resource name")){
			flag = 0;
		} 
		
		if(pnName.val()!=""){
			if(!CheckAlphanumericWithSomeChars(pnName, pnInfo)){
				flag = 0;
			}
		}
		
		 
		if(!checkCombo(ddCountry, countryInfo, "country")){
			flag = 0;
		} 
		/*if(!checkCombo(ddResource, resourceInfo, "resource")){
			flag = 0;
		} */
		 
		
		if($("input[name='dd_resources[]']:checked").length ==0) {
			$("#resourceInfo").html("Please select the resource category");
			flag = 0;
		}
		else{
			$("#resourceInfo").html("");
		}
			
		
		if($('#pdf_file').val()!=""){
			$(".loading-progress").show();
		}
		
		if($('#ai_file').val()!=""){
			$(".loading-progress2").show();
		}
		
		if($("#pdf_file_hidden").val() != ""){
			//$(".loading-progress").show();
			var fname = $("#pdf_file_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = (fname_ext.pop()).trim();
			
			if(lastVar != 'pdf' && lastVar != 'PDF'){ 
				pdfInfo.html("Please upload PDF file only.<br/>"+fname_e+" is not a PDF file.");
				flag = 0;
			}
		}
		
		if($("#ai_file_hidden").val() != ""){
			//$(".loading-progress2").show();
			var fname = $("#ai_file_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = (fname_ext.pop()).trim();
			//alert(lastVar);
			if(lastVar != "ai" && lastVar != "AI" && lastVar != "zip" && lastVar != "ZIP"){
				aiInfo.html("Please upload Illustrator/zip file only.<br/>"+fname_e+" is not a Illistrator/zip file.");
				flag = 0;
			}
		}
		
		//var formData = new FormData($('#formResources')[0]); 
		//alert('test');
		var formData = $('#formResources input').serializeArray(); 
		var dd_country = $('#dd_country').serializeArray();
		//formData.append("txt_product_description", encodeURIComponent(CKEDITOR.instances['txt_product_description'].getData()))	;
		//alert('test');
		//alert(flag);
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$(".panel-footer").append('<img src="<?php echo base_url();?>img/gif-load.gif" />');
			if($("#pdf_file_hidden").val() != ""){
			var progress = $(".loading-progress").progressTimer({
				timeLimit: 7200,
				onFinish: function () {
					//alert('completed!');
				}
			});
		}
		if($("#ai_file_hidden").val() != ""){
			var progress2 = $(".loading-progress2").progressTimer({
				timeLimit: 7200,
				onFinish: function () {
					//alert('completed!');
				}
			});
		}
			
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				/*contentType: false,
                cache: false,
                processData: false,*/
				url: "<?php echo base_url(); ?>administration/save_resources", 
				data: {"dd_country":dd_country,"txt_product_description":encodeURIComponent(CKEDITOR.instances['txt_product_description'].getData()),"formData":formData}
				//data:formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			}).error(function(){
				progress.progressTimer('error', {
					errorText:'ERROR!',
					onFinish:function(){
						//alert('There was an error processing your information!');
					}
				});
				progress2.progressTimer('error', {
					errorText:'ERROR!',
					onFinish:function(){
						//alert('There was an error processing your information!');
					}
				});
			}).done(function(){
				progress.progressTimer('complete');
				progress2.progressTimer('complete');
			});
		} 
		else{$("#btn_save").show();	}
	});		
});

function deleteReource()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure? you want to delete selected resources.") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_resources",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				} 
				alert((json.ids).length+" resource(s) has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one resource.');
	}
}
</script>

<?php $this->load->view("footer"); ?>
