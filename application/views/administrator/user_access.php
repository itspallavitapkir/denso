<?php $this->load->view("header"); ?> 
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Users</li>
										<li class="crumb-trail">User Access</li>
									</ol>
								</div>
								<span class="panel-controls UserRoles"> 
									<a href="javascript:void(0)" id="save"><i class="fa fa-save"></i></a> 
								</span>
							</div>
							<div class="panel-body pn of-a">
							<div class="panel-heading">
								<span class="panel-title">Users Access</span>
							</div>  
                        <div class="rolepop-mainrow"> 
							
							<div id="messages"></div>
							<form method="post" action="" id="formUserAccess" name="formUserAccess" class="dataUserAccess">
								<?php echo $userAccess;?>
							</form>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userRole" class="popup-basic admin-form mfp-with-anim resetvalues mfp-hide">
		</div>
<script>  
/*$("input[type=checkbox]").change(function() {
	   if($(this).is(':checked')){
		   $(this).val(1);
	   }
	   else{
		   $(this).val(0);
	   }
});*/
$(document).on("click","#save",function() {
	var frm_val = $("#formUserAccess").serialize();
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/SaveUserAccessSettings",
			data: frm_val,
		}).success(function (data) {   
			if(data['msg']){
					
					$("#messages").html(data['msg']);
					$("#messages div").fadeOut(3000);
			}
			$(".dataUserAccess").html(data['userAccess']);
		});
}); 
</script>
<?php $this->load->view("footer"); ?>
