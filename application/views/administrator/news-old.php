<?php $this->load->view("header"); ?>
<section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
<script type="text/javascript" src="<?php echo base_url(); ?>jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "openmanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,autosave,visualblocks",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline|justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "undo,redo,link,preview, emotions",
		theme_advanced_resizing : false,
		
		//Open Manager Options
		file_browser_callback: "openmanager",
		//open_manager_upload_path: '../../../../uploads/',

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		
		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],
	});
</script>
<textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 80%"></textarea>
</div>
</div>
</div>
</section>
</section>
<section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">News</li>
										<li class="crumb-trail">News Details</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#newsModel"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteNews()"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<!--<option value="news_cat_id">News Category</option>-->
													<option value="news_heading">News Heading</option>
												</select>
												</div>
											</div>
											<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onClick="changePaginate(0,'news_id','ASC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'news_id','ASC');" value="Refresh">
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,news_id,ASC"/>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
        
		<div id="newsModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">News Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6"> 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Category</label>
				  <label class="field select drop_new">
					<select class="select-sm arrow_new" name="news_cat_id" id="news_cat_id">
						<?php
							if(is_array($newscategory)){
								echo '<option value="">Select</option>';
								foreach($newscategory as $cat):	?>
								<option value="<?php echo $cat['news_cat_id']; ?>"><?php echo $cat['news_category'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
				  </label>
				 </div>
				</div>
			  <div class="col-sm-6">
				  <div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Country</label>
				  <label class="field select drop_new">
					<select class="select-sm arrow_new" name="country" id="country">
						<?php
							if(is_array($country)){
								echo '<option value="">Select</option>';
								foreach($country as $con):	?>
								<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
				  </label>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Heading</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_news_heading" name="txt_news_heading" class="form-control input-sm" placeholder="News Heading">
					<span id="emailAddressInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			  
			 </div>  
			 <div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Description</label>
				</div>
			  </div>
			  <div class="col-lg-8 text_inpu_new">
				<textarea cols="50" rows="8" type="text" id="txt_description" name="txt_description" class="form-control input-sm" placeholder="News Description"></textarea>
			  </div>
			 </div> 
			  <!-- end section --> 
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="newsId" id="newsId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Ok</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div>
<?php $this->load->view("footer"); ?>
