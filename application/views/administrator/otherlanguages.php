<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                <div id="message"></div>
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">

						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">

							<?php $message= $this->session->flashdata('Success'); if(!empty($message))?>

								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Other Languages</li>
											<li class="crumb-trail">Manage Other Languages</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#otherLangModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteOtherLanguage()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length set_querytypes">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="lang_id">Language</option>
													<option value="cat_id">Categories</option>
													<!-- <option value="section_id">Section</option> -->
													
												</select>
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="searchby_lang" id="searchby_lang" aria-controls="datatable2" class="form-control input-sm">
													<?php
														if(is_array($languages)){
															echo '<option value="">Select</option>';
															foreach($languages as $lan):	?>
															<option value="<?php echo $lan['id'];?>"><?php echo $lan['language'];?></option>
														 <?php endforeach;
														}
													?>
												</select>
												</div>
											</div>
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="cat_id" id="cat_id" aria-controls="datatable2" class="form-control input-sm">
													<?php
														if(is_array($categories)){
															echo '<option value="">Select</option>';
															foreach($categories as $con):	?>
															<option value="<?php echo $con['id']; ?>"><?php echo $con['category'];?></option>
														 <?php endforeach;
														}
													?>
												</select>
												</div> 
											</div>
											
											
											
                                            <div class="dataTables_filter pull-left otherList" style="min-width: 280px;">
												<div class="row">
													
													<div class="col-xs-3 col-sm-3 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-3 col-sm-3 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>

													<div class="col-xs-3 col-sm-3 top-serchbar4 button-submit col-sm-3"><a href="<?php echo base_url();?>administration/getPagesListSK"><input type="button" class="button table-submitbtn btn-info btn-xs" value="Back"></a></div>
                                                </div>

											</div>
                                            
											<!-- <div id="message"></div> -->
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="otherLangModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Other Language Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<!-- added by pallavi for usertype -->
			<div class="section row mbn">


				<div class="form-group" id="lng_name">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Language</label><span class="validationerror">*</span></label>
				  <div class="col-lg-4"> 
				
					<input type="text" name="language" id="language" class="form-control input-sm seccat" value="">
						<!-- <span id="setLanguageInfo" class="text-danger marg"></span> -->
				  </div>
				</div>
				<br>

			  <div class="form-group" id="selectedlang">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Language</label><span class="validationerror">*</span></label>
				  <div class="col-lg-4"> 
				
					<select name="select_language" id="select_language" aria-controls="datatable2" class="form-control input-sm">
							<?php
								if(is_array($languages)){
									echo '<option value="0">Select</option>';
									foreach($languages as $lan):	?>
									<option value="<?php echo $lan['id']; ?>"><?php echo $lan['language'];?></option>
								 <?php endforeach;
								}
							?>
						</select>
						<span id="languageInfo" class="text-danger marg"></span>
				  </div>
				</div><br>

				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Section</label><span class="validationerror">*</span></label>
				  <div class="col-lg-4"> 
				
					<select name="select_section" id="select_section" aria-controls="datatable2" class="form-control input-sm">
							<?php
								if(is_array($sections)){
									echo '<option value="">Select</option>';
									foreach($sections as $section):	?>
									<option value="<?php echo $section['id']; ?>"><?php echo $section['name'];?></option>
								 <?php endforeach;
								}
							?>
						</select>
						<span id="sectionInfo" class="text-danger marg"></span>
				  </div>
				</div><br>

				<div class="col-sm-12"> 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Public Access</label>
				   <div class="col-lg-6 dir">
				<input type="checkbox" id="chk_public" name="chk_public" class="" value="1">
					<label>Sections with public access is directly displayed for non registered users</label>
				   </div>
				</div>
			  </div><br><br><br><br><br>
			   <div class="col-sm-12"> 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Offline Reading</label>
				   <div class="col-lg-6 dir">
					<input type="checkbox" id="chk_offline" name="chk_offline" class=""  value="1">
					<label>Section with offline reading the user can read section offline</label>
				   </div>
				</div>
			  </div> <br><br><br>

				<div class="form-group">
				 <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>English Version</label></label>	
				  <div class="col-lg-6"> 
					 <!-- <label id="set_english_version" for="inputStandard" class="col-lg-6 pn mt5 control-label"><?php //echo $res[0]['page_data']; ?></label> --> 
					  <textarea  rows="10" cols="70" id="txt_description" name="txt_description"></textarea>
				 </div>
				</div><br>

				<div class="form-group">
				 <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Page Description</label></label>	
				  <div class="col-lg-6"> 
			 <textarea  rows="10" cols="70" id="txt_description1" name="txt_description1"></textarea>
				 </div>
				</div><br>

				

			  
			 
			</div> 
			<!-- end for user type -->
			
			
			 
			   
		 
			
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		 
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="lang_id" id="lang_id" value="" class="gui-input">
			<input type="hidden" name="pageId" id="pageId" class="gui-input">
			<input type="hidden" name="section_id" id="section_id" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
<style>
.height35{
	height: 35px;
}
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
</style>

<script> 

function loadeditor(){ 
	 
 //    CKEDITOR.disableAutoInline = true;
	// CKEDITOR.replace('txt_description11', {
	// 			height: 210,
	// 			on: {
	// 				instanceReady: function(evt) {
	// 					$('.cke').addClass('admin-skin cke-hide-bottom');
	// 				}
	// 			},
	// 		}); 
}
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	

$(document).on("change","#user_type",function() {	
	if($(this).val()=='Supplier')
		$(".download_once").show();
	else{
		$(".download_once").hide();		
		$("#download_check").removeAttr("checked");
	}
});
	//console.log(liObj);

$(document).ready(function(){
  
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {

	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	// if($("#txt_search").val()==""){
	// 	alert("Please enter the search term");
	// 	flag=0;
	// 	return false;
	// } 
	

	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	// if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
	// 		//alert("Spaces are not allowed");
	// 		flag = 0;
	// 		return false;
	// }
	
	if(flag){
		changePaginate(0,'page_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}

function showMessagesDelete(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}



function changeStatus(uid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_user_status",
			data: {"pageId":uid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
		});
}

function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'page_id','DESC');
		}
		setTimeout(function(){  $("#otherLangModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getUserList();
} 
  
$(document).on("click","#add",function() {
	//loadeditor();
	var pg_id = $("#pageId").val();

	if(pg_id=="")
	{
		$("#lng_name").hide();
	}else{
		$("#lng_name").show();
	}
	

	var url      = window.location.href; 
	var string = url.split('#')[0];
  	var s_id = string.substring(url.lastIndexOf('/') + 1);

  	$("#section_id").val(s_id);

  	$("#select_section option").attr('disabled','disabled');
	$("#select_section option").attr('readonly','readonly');
	$('#select_section').prop('disabled', true);


  	if(s_id!=""){
		$("#loading").show();
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getEnglishDescription",
				data: {"section_id":s_id,"lang_id":"1"},
			}).success(function (json) {
				$("#loading").hide();
				//console.log(json);
				$("#select_section").val(json.id);
				$("#select_section option").attr('disabled','disabled');
				$("#select_section option").attr('readonly','readonly');
				$("#txt_description").attr('readonly','readonly');
				$("#txt_description").val($(json.page_data).text());
				//$("#txt_description1").val($(json.page_description).text());

			});
	}

	$("#select_language option[value='1']").remove();

	$("#labelpassword").html("*");
	$(".panel-title").html("Add Language Translator");
	$("#user_type").trigger("change");
	//$("#pageId").val("");
	//document.getElementById("formUser").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	
	$("#pageId").val(id);
	if(id!=""){
		$('#selectedlang').hide();
		$("#loading").show();
		$('#lng_name').show();
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getOtherLangById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#select_section").val(json.s_id);
				$("#select_section").attr('readonly','readonly');
				$('#select_section').prop('disabled', true);
				$("#section_id").val(json.s_id);
				$("#lang_id").val(json.lang_id);
				//alert(json.lang_id);

				$("#language").val(json.language);
				$("#language").attr("readonly","readonly");
				//$('#selectedlang').hide();
				
				
				if(json.public_access == "1"){
					$("#chk_public").attr("checked","checked");
				}
				else{
					$("#chk_public").removeAttr("checked","checked");
				}
				if(json.offline_reading == "1"){
					$("#chk_offline").attr("checked","checked");
				}
				else{
					$("#chk_offline").removeAttr("checked","checked");
				}
				
				
				$("#txt_description").val(json.page_data);
				if(json.page_description){
					$('#txt_description1').val(json.page_description);
					//CKEDITOR.instances['txt_description1'].setData(json.page_description);		
				}
				
				$("#add").trigger("click"); 
				$("#labelpassword").html("");
				$("#loading").hide();
				$("#otherLangModel .panel-title").html("Edit Language Translator");
			}); 
	} 
	else{
		$('#lng_name').hide();
		$("#add").trigger("click");
	}
});
$(document).on("click","#otherLangModel .mfp-close",function() {
	$("#pageId").val("");
	$("#labelpassword").html("");
	$(".pic1 img:last-child").remove();
	$("#fInfo").html("");  	
	$("#lInfo").html("");  	
	$("#emailAddressInfo").html("");  	
	$("#countryInfo").html(""); 
	$("#userTypeInfo").html(""); 
	$("#contactInfo").html(""); 

	$("#txt_password").removeAttr('readonly','readonly');
	$("#txt_first_name").removeAttr('readonly','readonly');
	$("#txt_last_name").removeAttr('readonly','readonly');
	$("#super_admin_access").removeAttr("checked","checked");
	$("#pdf_file_access").removeAttr("checked","checked");
	$("#ai_file_access").removeAttr("checked","checked");			
	$("input[name='dd_resources[]']").removeAttr("checked", true);
	$("input[name='dd_subresources[]']").removeAttr("checked", true);
	document.getElementById("formUser").reset();
	location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	$("#searchby_lang").hide(); 
	$("#cat_id").hide(); 

	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'page_id','DESC'); 	
}
function loadmore(){
	getUserList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getUserList();

function getUserList()
{
	// var url      = window.location.href; 
 //  	var s_id = url.substring(url.lastIndexOf('/') + 1);
  	var url      = window.location.href; 
	var string = url.split('#')[0];
  	var s_id = string.substring(url.lastIndexOf('/') + 1);

	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");

	var searchBy = $("#dd_searchBy").val();
	var cat_id= $("#cat_id").val();
	var lang_id= $("#lang_id").val();
	var section_id = $("#section_id").val();
	var search = $("#txt_search").val();
	var langid = $("#searchby_lang").val();


	$("#searchby_lang option[value='1']").remove();

	
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_otherslanguages",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy,"section_id":section_id,"cat_id":cat_id,"lang_id":langid,"s_id":s_id},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
			//location.reload()
		});
		//location.reload();
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'page_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var pageId = $("#pageId").val(); 
			//alert(pageId);
		var select_language = $("#select_language");
			var languageInfo = $("#languageInfo");  	

		var select_section = $("#select_section");
			var sectionInfo = $("#sectionInfo"); 

		var txt_description = $("#txt_description");
		var txt_description1 = $("#txt_description1");

		var chk_offline = $("#chk_offline");
		var chk_public = $("#chk_public");

			
		var flag=1;	

		 if(pageId==''){
		 	var slng = $("#select_language").val();
			if(slng==0){

				languageInfo.html("Please select language");
			 	flag=0;
			}else{
				
				languageInfo.html(""); 
			}	 
		}
		
		var formData = new FormData($('#formUser')[0]); 
		//formData.append("txt_description1", CKEDITOR.instances['txt_description1'].getData());
		if(flag)
		{
			$("#loading").show();
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_otherLanguages",
				//data: {"pageId": $("#pageId").val(),"txt_first_name":$("#txt_first_name").val(),"txt_last_name":$("#txt_last_name").val(),"email_address":$("#txt_email_address").val(),"contact_number":$("#txt_contact_number").val(),"user_type":$("#user_type").val(),"country":$("#country").val(),"state":$("#txt_state").val(),"city":$("#txt_city").val()},
				data: formData
				
			}).success(function (json) {
				//console.log(json);
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				//getUserList();
				showMessages(json);
				$("#loading").hide();
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteOtherLanguage()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected record?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_otherLanguage",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				
				showMessagesDelete(json);
				location.reload();
				//alert((json.ids).length+" user(s) has been deleted.");
				
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>
<?php $this->load->view("footer"); ?>
