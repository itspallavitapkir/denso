<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>
<div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">News Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6"> 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Category</label>
				  <label class="field select drop_new">
					<select class="select-sm arrow_new" name="news_cat_id" id="news_cat_id">
						<?php
							if(is_array($newscategory)){
								echo '<option value="">Select</option>';
								foreach($newscategory as $cat):	?>
								<option value="<?php echo $cat['news_cat_id']; ?>"><?php echo $cat['news_category'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
				  </label>
				 </div>
				</div>
			  <div class="col-sm-6">
				  <div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Country</label>
				  <label class="field select drop_new">
					<select class="select-sm arrow_new" name="country" id="country">
						<?php
							if(is_array($country)){
								echo '<option value="">Select</option>';
								foreach($country as $con):	?>
								<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
				  </label>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Heading</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_news_heading" name="txt_news_heading" class="form-control input-sm" placeholder="News Heading">
					<span id="emailAddressInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			  
			 </div>
			 <div class="section row mbn">
			 <div class="col-sm-12">
				 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Description</label>
				  <div class="col-lg-8 text_inpu_new">
					<textarea id="txt_description" name="txt_description"></textarea>
				  </div>
				</div>
			  </div>
			  </div>  
			  
			  
			  <!-- end section --> 
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="newsId" id="newsId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Ok</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
<script type="text/javascript">
function loadeditor(){ 
	
	var editor = CKEDITOR.instances['txt_description']; 
    if (editor) {  
        delete CKEDITOR.instances['txt_description'];  
        alert("destroy");
    }
    CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('txt_description', {
				height: 210,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
				},
			}); 
}
</script>
