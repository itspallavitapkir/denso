<?php
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header");?>
<section id="content_wrapper">
	<!-- Begin: Content -->
	<section id="content" class="p15 pbn">
		<div class="row">
				<!-- Three panes -->
				
			<div class="col-md-12 admin-grid" id="animation-switcher">
				<div class="panel panel-info sort-disable" id="p0">
					<div class="panel-heading">
						<div class="topbar-left pull-left">
								<ol class="breadcrumb"> 
								<li class="crumb-link">Vehicle Parts</li>
								<li class="crumb-trail">Import Parts via CSV/EXCEL</li>
							</ol>
						</div>
						 
					</div> 
					<div class="panel-body mnw700 pn of-a" style="display: block !important;height: 385px;">
						<div class="row mn">
							<div class="col-md-12 pn">
								<div class="dt-panelmenu clearfix" style="background:#ffffff !important; border:none !important;">
									<?php if(!empty($validation_message)){?> 
									<div class="form-group col-md-12">
										<div class="alert alert-dismissable alert-danger" role="alert"><?php echo $validation_message; ?></div>
									</div>
									<?php }?>
									<?php if(!empty($message)) { ?>
									<div class="form-group col-md-12">
										<div class="alert alert-dismissable alert-success" role="alert"><?php echo $message; ?></div>
									</div>
									<?php } ?>
									
									<div class="form-group col-md-12">
										<div class="bg-primary importcv-bgprimary"><b>Note</b> : 1. Maximum allowed size for CSV/EXCEL file is 100 mb<br/>
										2. CSV/EXCEL format column sequence should be like Product category, Product Sub-Category ,COOL GEAR PN,DENSO PN, Car Maker PN, Vehicle, Model, Year, Type, Description, Additional information<br/>
										3. Product category is required field in CSV/EXCEL.<br/>
										4. For part images - The format will be 447260-5560.jpg i.e 'COOL GEAR Part No' OR 'DENSO Part No'.<br/>
										
										</div>
									</div>
									
									<div class="dataTables_filter pull-left">
										<form class="form-horizontal" method="post" action="<?php echo base_url();?>administration/importcsvproduct" id="formImport" name="formImport" enctype="multipart/form-data">
										<!---- Select Category --- -->
                                  <div class="section row mbn">
									<div class="col-sm-12">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category </label>
										  
											  <div class="col-lg-8 text_inpu_new categorymain">
												<div class="pm Vehicle-pm"><div class="dataTables_length1" >
												<div class="multiple-selection mr5">
													
												<div id="select-category" style="text-align:left;">Click to Select Category</div>
													
													<ul name="categories" id="vehicle-categories" class="set_category">
														<?php
															if(is_array($categories)){
																//echo '<option value="">Select</option>';
																foreach($categories as $cat):	
																
														?>
														<li><input type="checkbox" name="dd_resources[]" id="dd_resources" value="<?php echo $cat['id']?>" placeholder=""  class="events-category"><?php echo $cat['category'];?>
															<?php 
															$cond = array("v_cat_id"=>$cat['id']);
															$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
															if(count($res_subcat)>0){
																?>
																<ul class="event-children">
																<?php
																foreach($res_subcat as $subcat):
															?>
															
																<li><input type="checkbox" name="dd_subresources[]" id="dd_subresources" value="<?php echo $subcat['id']?>"  class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
															
														
														 <?php endforeach;
														 ?>
														 </li>
														 </ul>
														 <?php
															}
														 endforeach;
															}
														?>
													</ul>
													
												</div>
													<span id="resourcesInfo" class="text-danger"></span>
											</div>
											</div> 
										</div>
									  </div>
									</div>					
								</div>								 

                                      <!------close category------>


										<div class="section row mbn">
										  <div class="col-sm-12">
											<div class="form-group">
											  <label for="inputStandard" class="col-sm-3"><label>Upload CSV/EXCEL File:</label></label>
											  <div class="col-sm-9"> 
												<input type="file" id="csv" name="csv" >
												<input type="hidden" id="csv_hidden"/>
											  </div>
											</div>
											<span id="csvInfo" style="margin-right:70px;"  class="text-danger"></span>
										  </div> 
										</div>
										<div class="col-lg-7"> 
											<input type="button" class="button table-submitbtn btn-info btn-xs" id="btn_save" value="Submit">
										</div>
										</form>
										</div>
										
										  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div> 
	</section>
</section> 
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>  
<script>
$( document ).ready(function() {
	
	$('#dd_country').multiselect({
			buttonText: function(options, select) {
				return 'Select Country';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text());
				});
				return labels.join(' - ');
			}
		});
	
	 
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'news_id','ASC');
		}
		setTimeout(function(){  $("#importCSVModel .mfp-close").trigger( "click" ); }, 3000);
	}
} 
   
  
$(document).on("click","#add",function() { 
	$(".panel-title").html("Import CSV Product"); 
}); 
$("#csv").change(function (){
	
   var csv = $('#csv').val();
   $("#csv_hidden").val(csv);
   
}); 


$(document).ready(function(){     
	$("#txt_search").val(""); 
	$('#btn_save').on('click', function (e) {
		e.preventDefault(); 
		var csv = $("#csv");
			var csvInfo = $("#csvInfo");  	
		var flag=1;
		
		if(!checkCombo(csv, csvInfo, "the csv/excel file to upload")){
			flag = 0;
		}
		
		if($("#csv_hidden").val() != ""){
			var fname = $("#csv_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
				
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			lastVar   = lastVar.toLowerCase();
			if(lastVar != "csv" && lastVar != "xls"){
				csvInfo.html("Please upload CSV/EXCEL file only.");
				flag = 0;
			}
		}
		
		 
		if(flag)
		{
			$("#formImport").submit();
		} 
	});		
});
</script>

<script> 
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {
	//var height = $( window ).height() - 230;
	//$("#animation-switcher .table-responsive").css('height',height);
}); 	
 
 

$( document ).ready(function(){
	 
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}


function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}

</script>
<?php $this->load->view("footer");?>
