<?php $this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Statistics</li>
										<li class="crumb-trail">Download Statistics of <?php echo ucwords($user_name);?></li>
									</ol>
								</div>
								 <span class="panel-controls Users">  
									<a  href="<?php echo base_url();?>administration/getDownloadStatistics">Back</a>  
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length" style="width:100%;">
												<div >
												<form action="<?php echo base_url();?>administration/viewdownloadstatistics/" name="frmstat" id="frmstat" method="get">
													<input type="hidden" name="uid" id="uid" value="<?php echo $_GET['uid'];?>" />
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>From Date:</label></div>
													<div class="col-xs-4 col-sm-4 top-serchbar2 pn">
                                                    	<input type="text" class="form-control input-sm" name="fromdate" id="fromdate" value=""/>
                                                    	<span id="fromdateInfo"  class="text-danger"></span>
                                                    </div>
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>To Date:</label></div>
													<div class="col-xs-4 col-sm-4 top-serchbar2 pn">
                                                    	<input type="text" class="form-control input-sm" name="todate" id="todate" value=""/>
                                                    	<span id="todateInfo"  class="text-danger"></span>
                                                    </div>
													<div class="col-xs-4 col-sm-2 top-serchbar3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" onClick="SearchDownload();" value="Search">
                                                    </div>
												</form>
												</div>
											</div>  
										</div>
									</div>
								</div>
								
								
								<!--<div class="col-md-6">
									<div class="tab-block mb25">
										<ul class="nav tabs-left">
											<li class="active">
												<a href="#tab12_1" data-toggle="tab">Tab One</a>
											</li>
											<li>
												<a href="#tab12_2" data-toggle="tab"><i class="fa fa-pencil text-purple pr5"></i> Tab Two</a>
											</li>
											<li class="dropdown">
												<a class="dropdown-toggle" href="#" role="menu" data-toggle="dropdown"> Dropdowns <i class="fa fa-caret-down pl5"></i>
												</a>
												<ul class="dropdown-menu">
													<li>
														<a href="#tab12_3" tabindex="-1" data-toggle="tab">Tab Three</a>
													</li>
													<li>
														<a href="#tab12_4" tabindex="-1" data-toggle="tab">Tab Four</a>
													</li>
												</ul>
											</li>
										</ul>
										<div class="tab-content" style="margin-top:10px;">
											<div id="tab12_1" class="tab-pane active">
												<p><b>TAB ONE - </b> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
											</div>
											<div id="tab12_2" class="tab-pane">
												<p><b>TAB TWO - </b> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
											</div>
											<div id="tab12_3" class="tab-pane">
												<p><b>TAB THREE - </b>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
											</div>
											<div id="tab12_4" class="tab-pane">
												<p><b>TAB FOUR - </b>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
											</div>
										</div>
									</div>
								</div>-->
								
								 
								<div id="table" class="table-responsive">
								</div> 
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	
	
  
</div> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/moment.js"></script> 	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script> 	
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css"> 
	
<script>  	

$('#fromdate,#todate').datetimepicker( {pickTime: false,format: 'YYYY-MM-DD'}); 
$('#fromdate').val('<?php echo date('Y-m-01');?>');
$('#todate').val('<?php echo date('Y-m-t');?>');
$( document ).ready(function() {
	SearchDownload();
	var height = $( window ).height();
	$("#animation-switcher .table-responsive").css('height',height);
});

function SearchDownload(){
	var fromdate = $("#fromdate");
		var fromdateInfo = $("#fromdateInfo");
	var todate = $("#todate");
		var todateInfo = $("#todateInfo");	
	var uid = $("#uid");
	flag=1;	
	
	if(!checkCombo(fromdate, fromdateInfo, "From Date")){
			flag = 0;
	} 
	if(!checkCombo(todate, todateInfo, "To Date")){
			flag = 0;
	}
	
	if(fromdate.val()>todate.val()){
		fromdateInfo.html('The From date should not be greater than To date');
		flag=0;
	}
	
	if(flag){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getUserDownloadStatistics",
				data: {"uid":uid.val(),"fromdate":fromdate.val(),"todate":todate.val()},
			}).success(function (json) {
				$("#table").html(json.table);
			}); 
	}
}

  
</script>
<?php $this->load->view("footer"); ?>
