<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
             <div id="message"></div>
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
						<!-- flash message -->
						

               <!-- flash message -->

							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Banners</li>
											<li class="crumb-trail">Manage Banners</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#bannerModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteBanners()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length set_querytypes">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="user_type">User Type</option>
													<option value="status">Status</option>
													<option value="start_date">Start Date</option>
													<option value="end_date">End Date</option>
													<option value="country">Country</option>
												</select>
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<select name="dd_searchBy1" id="dd_searchBy1" aria-controls="datatable2" class="form-control input-sm">
												<option value="0">User Type</option>
													<option value="1" selected="selected">Wholesaler</option>
													<option value="2">Dealers</option>
												</select>
												</div>
											</div>

											 <div class="dataTables_length">
												<div class="multiple-selection mr5">
												
												<select name="searchby_status" id="searchby_status" aria-controls="datatable2" class="form-control input-sm">
												<option value="">Status</option>
													<option value="Active" selected="selected">Active</option>
													<option value="Inactive">Inactive</option>
												</select>
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<div class='input-group date' id='start_date1'>
									                <input type='text' placeholder="Start Date" class="form-control" name="start_date" id="start_date" />
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
												
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<div class='input-group date' id='end_date1'>
									                <input type='text' placeholder="End Date" class="form-control" name="end_date" id="end_date" />
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
												
												</div>
											</div>


											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<select name="searchby_country" id="searchby_country" aria-controls="datatable2" class="form-control input-sm">
													<?php
														if(is_array($country)){
															echo '<option value="">Select</option>';
															foreach($country as $con):	?>
															<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
														 <?php endforeach;
														}
													?>
												</select>
												</div>
											</div>

                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-6 col-sm-6 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
													</div>
                                                </div>
											</div>
											<!-- <div id="message"></div> -->
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,b_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
	<div id="bannerModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Banner Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<!-- added by pallavi for usertype -->
			
			<div class="section row mbn">
				<div class="col-sm-6">
					<div class="form-group">
					  <!-- <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">User Type<span class="validationerror">*</span></label> -->
					  <div class="col-lg-8 text_inpu_new">
						<img id="blah" src="<?php echo base_url();?>images/no-image.jpg" height="200" width="400"  />
						<span id="bannerImageInfo" class="text-danger marg"></span>
					  </div>
					</div>
				</div>
			</div>
			<div class="section row mbn">
				<div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
								<input type="file" name="banner_image" id="banner_image">
								<input type="hidden" name="imgflag" id="imgflag" class="imgflag">
								<input type="hidden" name="upload_flag" id="upload_flag" class="upload_flag">
								<span id="bannerimageInfo" class="text-danger marg"></span>

								<span class="banner_note">Note : Please upload the image size width = 1500px height = 514px</span>
						    
								
				  			</div>
					</div>
			 	 </div>
			</div> 

			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">User Type<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<select id="user_type" name="user_type" class="form-control input-sm">
					     <option value="">Please select user type</option>
						  <option value="1">Wholesaler</option>
						  <option value="2">Dealers</option>
						</select> 
					<span id="userTypeInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>

			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Country<span class="validationerror">*</span></label>
				  <label class="col-lg-8 text_inpu_new">
					<select class="select-sm arrow_new form-control"  name="country" id="country">
						<?php
							if(is_array($country)){
								echo '<option value="">Select</option>';
								foreach($country as $con):	?>
								<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
					<span id="countryInfo"  class="text-danger"></span>
				  </label>
				</div>
			  </div>
			</div> 
			
			<div class="section row mbn">
			 	<div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Start Date<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
								<div class="form-group">
						            <div class='input-group date' id='datetimepicker6'>
						                <input type='text' class="form-control" name="start_date" id="start_date" />
						                <span class="input-group-addon">
						                    <span class="glyphicon glyphicon-calendar"></span>
						                </span>
						            </div>
						        </div>
								<span id="startDateInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>

			 	 <div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">End date<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
								<div class="form-group">
						            <div class='input-group date' id='datetimepicker7'>
						                <input type='text' class="form-control" name="end_date" id="end_date" />
						                <span class="input-group-addon">
						                    <span class="glyphicon glyphicon-calendar"></span>
						                </span>
						            </div>
						        </div>
								<span id="endDateInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>	  
			</div>  
			
			<div class="section row mbn">
				<div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Status<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
								<select id="status" name="status" class="form-control input-sm">
							     <option value="">Select</option>
								  <option value="Active">Active</option>
								  <option value="Inactive">Inactive</option>
								</select> 
								<span id="statusInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>

			 	 <!-- category -->
			 	<div class="col-sm-6">
					<div class="form-group">
					  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category </label>
					  
						  <div class="col-lg-8 text_inpu_new categorymain">
						 	<div class="pm Vehicle-pm"><div class="dataTables_length1" >
							<div class="multiple-selection mr5">
								
								<div id="select-category">Click to Select Category</div>
								
								<ul name="categories" id="vehicle-categories" class="set_category">
									<?php
										if(is_array($categories)){
											//echo '<option value="">Select</option>';
											foreach($categories as $cat):	
											
									?>
									<li><input type="checkbox" name="dd_resources[]" id="dd_resources" value="<?php echo $cat['id']?>" placeholder=""  class="events-category"><?php echo $cat['category'];?>
										<?php 
										$cond = array("v_cat_id"=>$cat['id']);
										$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
										if(count($res_subcat)>0){
											?>
											<ul class="event-children">
											<?php
											foreach($res_subcat as $subcat):
										?>
										
											<li><input type="checkbox" name="dd_subresources[]" id="dd_subresources" value="<?php echo $subcat['id']?>"  class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
										
									
									 <?php endforeach;
									 ?>
									 </li>
									 </ul>
									 <?php
									    }
									 endforeach;
										}
									?>
								</ul>
								
							</div>
							<span id="resourcesInfo" class="text-danger"></span>
						</div>
						</div> 
					</div>
				  </div>
				</div>
			 	 <!-- //end category -->
			</div> 

			

			  <!-- end section -->
			</div>
			<!-- end section row section -->
		 
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="bannerId" id="bannerId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
.date #start_date{display: inline-flex;}
.date #end_date{display: inline-flex;}
</style>

<script> 


//banner preview
	function readURL(input) {
		var fieldName='';
		var filename =  $(":file").val();
		var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png|\.bmp|\.GIF|\.GPEG|\.PNG|\.BMP)$/i;   
		if(valid_extensions.test(filename))
		{ 
		  	if (input.files && input.files[0]) {
        		 var reader = new FileReader();
            
		            reader.onload = function (e) {
		                $('#blah').attr('src', e.target.result);
		            }
		         reader.readAsDataURL(input.files[0]);
		   	    
        	}
		}       
    }
     // $('#blah').attr('src','."'.base_url().'"');
    
    $("#banner_image").change(function(){
          readURL(this);
        var imageName = $('#banner_image').val();
	   		$("#imgflag").val(imageName);
	});


$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {

        $('#datetimepicker6').datetimepicker({
        	//minDate:new Date()

        });

        $('#datetimepicker7').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            //format:'Y-m-d'
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // searching date picker
        $('#start_date1').datetimepicker();
        $('#end_date1').datetimepicker({
            useCurrent: false //Important! See issue #1075
           // format:'y-m-d'
        });
        $("#start_date1").on("dp.change", function (e) {
            $('#end_date1').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date1").on("dp.change", function (e) {
            $('#start_date1').data("DateTimePicker").maxDate(e.date);
        });
        // end searching date picker

	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
	jQuery("#txt_contact_number").keypress(function (e) {  
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
	}); 	

$(document).on("change","#user_type",function() {	
	if($(this).val()=='Supplier')
		$(".download_once").show();
	else{
		$(".download_once").hide();		
		$("#download_check").removeAttr("checked");
	}
});
	//console.log(liObj);

$(document).ready(function(){
  
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {

	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	// if($("#txt_search").val()==""){
	// 	alert("Please enter the search term");
	// 	flag=0;
	// 	return false;
	// } 
	

	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	// if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
	// 		//alert("Spaces are not allowed");
	// 		flag = 0;
	// 		return false;
	// }
	
	if(flag){
		changePaginate(0,'b_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesDelete(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}


function changeStatus(bid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_banner_status",
			data: {"bannerId":bid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
			$("#message div").fadeOut(5000);
			refreshTable(0,'b_id','DESC');
		});
}

function showMessages(json)
{
	//alert(json.msg);
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'b_id','DESC');
		}
		setTimeout(function(){  $("#banerModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getBannerList();
} 
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add New Banner");
	$("#user_type").trigger("change");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});

//function to edit banner

$(document).on("click","#edit",function() { 
	//$(".pic1 img:last-child").remove();
	//console.log($(".pic1 img:last-child"));

	$('#datetimepicker6').prop('disabled', false);
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#bannerimageInfo").html(""); 
	$("#banner_image").change(function(){
        readURL(this);
        $("#upload_flag").val('1');
    });
	
	$("#bannerId").val(id);
	//$("#imgflag").val(1);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getBannerById",
				data: {"id":id},
			}).success(function (json) {
				$('#datetimepicker6').data("DateTimePicker").date(json.start_date);
				$('#datetimepicker7').data("DateTimePicker").date(json.end_date);
				
				$("#end_date").val(json.end_date);
				$("#status").val(json.status);
				$("#country").val(json.country);
				$("#user_type").val(json.user_type);
				$("#imgflag").val(json.banner_image);
				$("#blah").attr("src",'<?php echo base_url();?>banners_img/'+json.banner_image);

				if(json.category_id){
					var valArr = json.category_id; 
					var dataarray = valArr.split(","); 
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				}
				
				if(json.sub_category_id){
					var valSubArr = json.sub_category_id; 
					var datasubarray = valSubArr.split(","); 
					for (var count = 0; count < datasubarray.length; count++) { 
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				
				$("#add").trigger("click"); 
				$("#bannerModel .panel-title").html("Edit Banner");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#bannerModel .mfp-close",function() {
	$("#bannerId").val("");
	
	//$(".pic1 img:last-child").remove();
		
	$("#countryInfo").html(""); 
	$("#userTypeInfo").html(""); 
	$("#startDateInfo").html(""); 
	$("#endDateInfo").html(""); 
	$("#statusInfo").html(""); 
			
	$("input[name='dd_resources[]']").removeAttr("checked", true);
	$("input[name='dd_subresources[]']").removeAttr("checked", true);
	document.getElementById("formUser").reset();
	location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	$("#searchby_status").val(''); 
	$("#searchby_country").val(''); 
	changePaginate(0,'b_id','DESC'); 	
}
function loadmore(){
	getBannerList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getBannerList();

function getBannerList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var user_type= $("#dd_searchBy1").val();
	var status= $("#searchby_status").val();
	var country= $("#searchby_country").val();
	var start_date= $("#start_date").val();
	var end_date= $("#end_date").val();
	var search = $("#txt_search").val();
	//console.log(start_date);
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_banners",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy,"user_type":user_type,"status":status,"country":country,"start_date":start_date,"end_date":end_date},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}

$(document).ready(function(){  
	var url      = window.location.href; 
  	var id = url.substring(url.lastIndexOf('/') + 1);
  
  	$("#bannerId").val(id);
	//$("#imgflag").val(1);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getBannerById",
				data: {"id":id},
			}).success(function (json) {
				$('#datetimepicker6').data("DateTimePicker").date(json.start_date);
				$('#datetimepicker7').data("DateTimePicker").date(json.end_date);
				
				$("#end_date").val(json.end_date);
				$("#status").val(json.status);
				$("#country").val(json.country);
				$("#user_type").val(json.user_type);
				$("#imgflag").val(json.banner_image);
				$("#blah").attr("src",'<?php echo base_url();?>banners_img/'+json.banner_image);

				if(json.category_id){
					var valArr = json.category_id; 
					var dataarray = valArr.split(","); 
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				}
				
				if(json.sub_category_id){
					var valSubArr = json.sub_category_id; 
					var datasubarray = valSubArr.split(","); 
					for (var count = 0; count < datasubarray.length; count++) { 
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				
				$("#add").trigger("click"); 
				$("#bannerModel .panel-title").html("Edit Banner");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}

});

$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'b_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getBannerList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var bannerId = $("#bannerId"); 


		var bannerImage = $("#banner_image");
			var bannerimageInfo=$("#bannerimageInfo");

		var country = $("#country");
			var countryInfo = $("#countryInfo");  		
		
		var userType = $("#user_type");
			var userTypeInfo = $("#userTypeInfo");	

		var status = $("#status");
			var statusInfo = $("#statusInfo");

		var startDate = $("#start_date");
			var startDateInfo = $("#startDateInfo");

		var endDate = $("#end_date");
			var endDateInfo = $("#endDateInfo");

		
		var flag=1;
		bannerimageInfo.html("");

		if($("#imgflag").val() != "")
		{
			//$(".loading-progress").show();
			var fname = $("#imgflag").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  //image name
			
			fname_ext = fname_e.split('.');  
			var lastIndex = fname_ext.length - 1;
			var lastVar = fname_ext[lastIndex];
			//var lastVar = (fname_ext.pop()).trim();
			if((lastVar == 'png') || (lastVar == 'PNG') || (lastVar == 'jpg') || (lastVar == 'JPG') || (lastVar == 'jpeg') || (lastVar == 'JPEG')){ 	
			}
			else
			{
				bannerimageInfo.html("Please select Image with extension gif|png|jpg|jpeg|GIF|PNG|JPG|JPEG");
				flag = 0;
			}
		}
		else
		{
			bannerimageInfo.html("Please select banner Image");
				flag = 0;
		}


		if(!checkCombo(startDate, startDateInfo, "start sate")){
			flag = 0;
		} 
		if(!checkCombo(endDate, endDateInfo, "end date")){
			flag = 0;
		} 

		
		if(!checkCombo(country,countryInfo,"the country")){
			flag = 0;
		}
		
		if(!checkCombo(userType, userTypeInfo, "user type")){
			flag = 0;
		}

		if(!checkCombo(status, statusInfo, "status")){
			flag = 0;
		}

		var file_data = $('#banner_image').prop('files')[0];
		var formData = new FormData($('#formUser')[0]); 
		    formData.append('file', file_data);
		

		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                async : false,
                fileElementId:'banner_image',
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_banners",
				//data: {"userId": $("#userId").val(),"txt_first_name":$("#txt_first_name").val(),"txt_last_name":$("#txt_last_name").val(),"email_address":$("#txt_email_address").val(),"contact_number":$("#txt_contact_number").val(),"user_type":$("#user_type").val(),"country":$("#country").val(),"state":$("#txt_state").val(),"city":$("#txt_city").val()},
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}

				showMessages(json);
				$("#message div").fadeOut(10000);

				//$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteBanners()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected banner?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_banners",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				showMessagesDelete(json);
				//alert((json.ids).length+" Banners(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one banner.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
