<?php $this->load->view("header");?>
<section id="content_wrapper">
	<!-- Begin: Content -->
	<section id="content" class="p15 pbn">
		<div class="row">
				<!-- Three panes -->
			<div class="col-md-12 admin-grid" id="animation-switcher">
				<div class="panel panel-info sort-disable" id="p0">
					<div class="panel-heading">
						<div class="topbar-left pull-left">
								<ol class="breadcrumb"> 
								<li class="crumb-link">Vehicle Parts</li>
								<li class="crumb-trail">Import Parts via CSV/EXCEL</li>
							</ol>
						</div>
						 
					</div> 
					<div class="panel-body mnw700 pn of-a">
						<div class="row mn">
							<div class="col-md-12 pn">
								<div class="dt-panelmenu clearfix">
									<?php if(!empty($validation_message)){?> 
									<div class="form-group col-md-12">
										<div class="alert alert-dismissable alert-danger" role="alert"><?php echo $validation_message; ?></div>
									</div>
									<?php }?>
									<?php if(!empty($message)) { ?>
									<div class="form-group col-md-12">
										<div class="alert alert-dismissable alert-success" role="alert"><?php echo $message; ?></div>
									</div>
									<?php } ?>
									
									<div class="form-group col-md-12">
										<div class="bg-primary importcv-bgprimary"><b>Note</b> : 1. Maximum allowed size for CSV/EXCEL file is 100 mb<br/>
										2. CSV/EXCEL format column sequence should be like Product category, Product Sub-Category ,COOL GEAR PN,DENSO PN, Car Maker PN, Vehicle, Model, Year, Type, Description, Additional information<br/>
										3. Product category is required field in CSV/EXCEL.<br/>
										4. For part images - The format will be 447260-5560.jpg i.e 'COOL GEAR Part No' OR 'DENSO Part No'.<br/>
										
										</div>
									</div>
									
									<div class="dataTables_filter pull-left">
										<form class="form-horizontal" method="post" action="<?php echo base_url();?>administration/importcsvproduct" id="formImport" name="formImport" enctype="multipart/form-data">
										
										<div class="section row mbn">
										  <div class="col-sm-12">
											<div class="form-group">
											  <label for="inputStandard" class="col-sm-3"><label>Upload CSV/EXCEL File:</label></label>
											  <div class="col-sm-9"> 
												<input type="file" id="csv" name="csv" >
												<input type="hidden" id="csv_hidden"/>
											  </div>
											</div>
											<span id="csvInfo" style="margin-right:70px;"  class="text-danger"></span>
										  </div> 
										</div>
										<div class="col-lg-7"> 
											<input type="button" class="button table-submitbtn btn-info btn-xs" id="btn_save" value="Submit">
										</div>
										</form>
										</div>
										
										  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div> 
	</section>
</section> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>  
<script>
$( document ).ready(function() {
	
	$('#dd_country').multiselect({
			buttonText: function(options, select) {
				return 'Select Country';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text());
				});
				return labels.join(' - ');
			}
		});
	
	var height = $( window ).height() - 180;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'news_id','ASC');
		}
		setTimeout(function(){  $("#importCSVModel .mfp-close").trigger( "click" ); }, 3000);
	}
} 
   
  
$(document).on("click","#add",function() { 
	$(".panel-title").html("Import CSV Product"); 
}); 
$("#csv").change(function (){
	
   var csv = $('#csv').val();
   $("#csv_hidden").val(csv);
   
}); 


$(document).ready(function(){     
	$("#txt_search").val(""); 
	$('#btn_save').on('click', function (e) {
		e.preventDefault(); 
		var csv = $("#csv");
			var csvInfo = $("#csvInfo");  	
		var flag=1;
		
		if(!checkCombo(csv, csvInfo, "the csv/excel file to upload")){
			flag = 0;
		}
		
		if($("#csv_hidden").val() != ""){
			var fname = $("#csv_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
				
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			lastVar   = lastVar.toLowerCase();
			if(lastVar != "csv" && lastVar != "xls"){
				csvInfo.html("Please upload CSV/EXCEL file only.");
				flag = 0;
			}
		}
		
		 
		if(flag)
		{
			$("#formImport").submit();
		} 
	});		
});
function deleteNews()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure? you want to delete selected news.") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_news",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				
				alert((json.ids).length+" news has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one news.');
	}
}
</script>
<?php $this->load->view("footer");?>
