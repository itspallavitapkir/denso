<script>
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(3000);
	if(json.status == 1)
	{
		setTimeout(function(){  $("#userRole .mfp-close").trigger( "click" ); }, 3000);
		if(json.action == "add")
		{
			refreshTable();
		}
	}
}
$('#btn_save').on('click', function (e) {
	e.preventDefault();
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>inventory/save_user_roles",
			data: {"id":$( "#formUserRoles :checkbox" ).serializeArray(),"roleName":$("#txt_role_name").val(),"role":$("#txt_role").val(),},
		}).success(function (json) {
			if(json.action == "modify")
			{
				$("#row_"+json.id).html(json.row);
			}
			showMessages(json);
		});
	
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/admin-forms.css">
<?php  $data = '<div class="panel">
				<div class="panel-heading p15">
					<span class="panel-title">User Role</span>
				</div>
				<div id="message"></div>
				<form method="post" action="" id="formUserRoles" name="formUserRoles">
					<input type="hidden" name="txt_role" id="txt_role" value="'.$roleId.'"/>
					<div class="panel-body p15">
						<div class="section row mbn">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="inputStandard1" class="col-xs-2 pn mt5 control-label">Enter Role</label>
									<div class="col-xs-10">
									<input type="text" id="txt_role_name" name="txt_role_name" class="form-control input-sm" value="'.$roleName.'" placeholder="Enter Role">
									</div>
								 </div>
							</div>
							  <div class="col-sm-6"></div>
						</div>
						<div class="userrole-tablemain mt10">
                        <div class="userrleheader-tle table-bordered" style="overflow:hidden;">
							<div class="pull-left roletitle roletitle-first">Module</div>
                            <div class="pull-left roletitle">View</div>    
                            <div class="pull-left roletitle">Add</div> 
                            <div class="pull-left roletitle">Modify</div>                                            	
                            <div class="pull-left roletitle">Delete</div>                                                                                                                                                
                        </div>
                        <div class="rolepop-mainrow">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
							$parent = "";
							$i = 0;
						for($j = 0; $j < count($parentModules); $j++){
							$parent = $parentModules[$j];
							$data .= '<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$j.'" aria-expanded="true" class="arrowup-down collapsed"> <span><i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></span>'.$parent["parent"].'</a>
										</h4>
									</div>
									<div id="collapse'.$j.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">';
										$childModule = getChildModules($parent["parent"]);
										for($c = 0; $c < count($childModule); $c++){
											$arrAccess = getAccessPermission($childModule[$c]["id"],$roleId);
											$i = $childModule[$c]["id"];
											if($arrAccess["access"]){ $access_chk = 'checked="checked"';}else{$access_chk = '';}
											if($arrAccess["permission_add"]){ $add_chk = 'checked="checked"';}else{$add_chk = '';}
											if($arrAccess["permission_update"]){ $update_chk = 'checked="checked"';}else{$update_chk = '';}
											if($arrAccess["permission_delete"]){ $delete_chk = 'checked="checked"';}else{$delete_chk = '';}
											
									$data .='<div class="userrleheader-tle userrleheader-inner table-bordered">
												<div class="pull-left roletitle roletitle-first">'.$childModule[$c]["name"].'</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input value="1" type="checkbox" '.$access_chk.' id="view_'.$i.'" name="view_'.$i.'">
														<label for="view_'.$i.'" class="pln">&nbsp;</label>
													</div>
												</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input value="1" type="checkbox" '.$add_chk.' id="add_'.$i.'" name="add_'.$i.'">
														<label for="add_'.$i.'" class="pln">&nbsp;</label>
													</div>
												</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input value="1" type="checkbox" '.$update_chk.' id="edit_'.$i.'" name="edit_'.$i.'">
														<label for="edit_'.$i.'" class="pln">&nbsp;</label>
													</div>
												</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input value="1" type="checkbox" '.$delete_chk.' id="delete_'.$i.'" name="delete_'.$i.'">
														<label for="delete_'.$i.'" class="pln">&nbsp;</label>
													</div>
												</div> 
											</div>';
											//$i++;
										}
								$data .='</div>
									</div>
								</div>';
							}	
							$data .='</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="button btn-info btn-xs" name="btn_save" id="btn_save" type="submit">Submit</button>
						<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
					</div>
				</form>
				</div>
				<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
echo $data;
exit;
?>


<?php $data = '
<div class="panel">
	<div class="panel-heading p15">
		<span class="panel-title">User Role</span>
	</div>
	<form>
		<div class="panel-body p15">
			<div class="section row mbn">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="inputStandard1" class="col-xs-2 pn mt5 control-label">Enter Role</label>
						<div class="col-xs-10">
						<input type="text" id="inputStandard1" class="form-control input-sm" placeholder="Enter Role">
						</div>
					 </div>
				</div>
				  <div class="col-sm-6"></div>
			</div>
			<div class="section row mbn">
				<div class="table-responsive">
				<table class="table table-striped mt10 table-hover table-squebrder">
					<tbody>
						<tr class="tbodymain-userrole">
						  <th class="firstmodule">Module</th>
						  <th>View</th>
						  <th>Add</th>
						  <th>Modify</th>
						  <th>Delete</th>
						</tr>';
						$parent = "";
						$i = 0;
						for($j = 0; $j < count($modules); $j++){
							$module = $modules[$j];
							$k = 0;
						if($module["parent"] != $modules[$j-1]["parent"])
						{
							$k = 1;
							$parent = $module["parent"];
							$data .='<tr>
							<td colspan="5" class="pn action-items"><a data-toggle="collapse" href="#datacollapse'.$i.'" aria-expanded="false" aria-controls="datacollapse'.$i.'" class="titleinventory collapsed mn p10 btn-block link-unstyled"><b>'.$module["parent"].'</b></a>
								<div id="datacollapse'.$i.'" class="collapse" style="height: 0px;">
									<table class="table table-bordered usermodle-role nomargin">
										<tbody>';
							$i++;
						}
										$data .='<tr>
												<td class="firstaction-item">'.$module["name"].'</td>
												<td>
													<div class="checkbox-custom mbn mln form-group">
														<input type="checkbox" checked="" id="userrole-ckbox1">
														<label for="userrole-ckbox1" class="pln">&nbsp;</label>
													</div>
												</td>
												<td>
													<div class="checkbox-custom mbn mln form-group">
														<input type="checkbox" checked="" id="userrole-ckbox2">
														<label for="userrole-ckbox2" class="pln">&nbsp;</label>
													</div>
												</td>
												<td>
													<div class="checkbox-custom mbn mln form-group">
														<input type="checkbox" checked="" id="userrole-ckbox3">
														<label for="userrole-ckbox3" class="pln">&nbsp;</label>
													</div>
												</td>
												<td>
													<div class="checkbox-custom mbn mln form-group">
														<input type="checkbox" checked="" id="userrole-ckbox4">
														<label for="userrole-ckbox4" class="pln">&nbsp;</label>
													</div>
												</td>
											</tr>';
							if($modules[$j-1]["parent"] != $parent)
							{
										$data .='</tbody>
										</table>
									</div>
								</td>
							</tr>';
							} 
						}
				$data .='</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<button type="submit" class="button btn-info btn-xs">Ok</button>
		<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
	</div>
</form>
</div>
<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
echo $data;
?>
