<?php $this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Users</li>
										<li class="crumb-trail">User Details</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#userModel"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteUsers()"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="first_name">First Name</option>
													<option value="last_name">Last Name</option>
													<option value="email_address">Email Address</option>
												</select>
												</div>
											</div>
											<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onClick="changePaginate(0,'user_id','ASC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'user_id','ASC');" value="Refresh">
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,ASC"/>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">User Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">First Name</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_first_name" name="txt_first_name" class="form-control input-sm" placeholder="First Name">
					<span id="fInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Last Name</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_last_name" name="txt_last_name" class="form-control input-sm" placeholder="Last Name">
					<span id="lInfo" class="text-danger"></span>
				  </div>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Email Address</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_email_address" name="txt_email_address" class="form-control input-sm" placeholder="Email Address">
					<span id="emailAddressInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">User Type</label>
				  <label class="field select drop_new">
					<select class="select-sm arrow_new" name="user_type" id="user_type">
						<option value="Internal">Internal</option>
						<option value="External">External</option>
						<option value="Supplier">Supplier</option> 
					</select>
				  </label>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Contact Number</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_contact_number" name="txt_contact_number" class="form-control input-sm" placeholder="Contact Number">
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Country</label>
				  <label class="field select drop_new">
					<select class="select-sm arrow_new" name="country" id="country">
						<?php
							if(is_array($country)){
								echo '<option value="">Select</option>';
								foreach($country as $con):	?>
								<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
				  </label>
				</div>
			  </div>
			  
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">State</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_state" name="txt_state" class="form-control input-sm" placeholder="State">
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">City</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_city" name="txt_city" class="form-control input-sm" placeholder="City">
				  </div>
				</div>
			  </div>
			</div> 
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="userId" id="userId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Ok</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div>
		
<script>
	
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','ASC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 3000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getUserList();
} 
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add New User");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});
$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');
	//alert(id);
	//return false;
	$("#userId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getUserById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_first_name").val(json.first_name);
				$("#txt_last_name").val(json.last_name);
				$("#txt_email_address").val(json.email_address);
				$("#user_type").val(json.user_type);
				$("#txt_contact_number").val(json.contact_details);
				//$("#profile_pic").val(json.profile_pic);
				$("#country").val(json.country);
				//console.log(json.country);
				$("#txt_state").val(json.state);
				$("#txt_city").val(json.city); 
				$("#add").trigger("click");
				$("#userModel .panel-title").html("Edit User");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#userModel .mfp-close",function() {
	$("#userId").val("");
	document.getElementById("formUser").reset();
});

function refreshTable()
{
	$("#txt_search").val("");
	getUserList();
}

getUserList();

function getUserList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_users",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			$("#table").html(json.table);
			$("#table").append(json.links);
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'user_id','ASC'); 
	$(window).scroll(function(){
			if  ($(window).scrollTop() == $(document).height() - $(window).height()){
				getUserList();
			}
		});
	$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var userId = $("#userId"); 
		var fName = $("#txt_first_name");
			var fInfo = $("#fInfo");  	
		var lName = $("#txt_last_name");
			var lInfo = $("#lInfo");  	
		var emailAddress = $("#txt_email_address");
			var emailInfo = $("#emailAddressInfo");  			
		var flag=1;
		if(!validateEmpty(fName, fInfo, "first name")){
			flag = 0;
		} 	
		if(!validateEmpty(lName, lInfo, "last name")){
			flag = 0;
		}
		if(!validateEmpty(emailAddress, emailInfo, "email address")){
			flag = 0;
		}
		if(!validateEmail(emailAddress, emailInfo)){
			flag = 0;
		}
		//return false;
		if(flag)
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/save_users",
				data: {"userId": $("#userId").val(),"txt_first_name":$("#txt_first_name").val(),"txt_last_name":$("#txt_last_name").val(),"email_address":$("#txt_email_address").val(),"contact_number":$("#txt_contact_number").val(),"user_type":$("#user_type").val(),"country":$("#country").val(),"state":$("#txt_state").val(),"city":$("#txt_city").val()},
			}).success(function (json) {
				showMessages(json);
			});
		} 
	});		
});
function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure? you want to delete selected users.") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_users",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				alert((json.ids).length+" user(s) has been deleted.");
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
