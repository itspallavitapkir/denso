<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
             <div id="message"></div>
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
						<!-- flash message -->
						

               <!-- flash message -->

							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Language</li>
											<li class="crumb-trail">Manage Languages</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#sectionModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteSection()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<!-- <div class="dataTables_length set_querytypes">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="user_type">User Type</option>
													<option value="country">Country</option>
												</select>
												</div>
											</div> -->

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<!-- <select name="dd_searchBy1" id="dd_searchBy1" aria-controls="datatable2" class="form-control input-sm">
												<option value="0">User Type</option>
													<option value="1" selected="selected">Wholesaler</option>
													<option value="2">Dealers</option>
												</select> -->
												</div>
											</div>



											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												
												</div>
											</div>



                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<!-- <div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div> -->
													<!-- <div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div> -->
													<!-- <div class="col-xs-6 col-sm-6 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div> -->
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">

													</div>
                                                </div>
											</div>
                                            
											<!-- <div id="message"></div> -->
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
	<div id="sectionModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">language Management Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<!-- added by pallavi for usertype -->
			
			<div class="section row mbn">
				<div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Language Name<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
				  			<input type="text" name="language" id="language" value="" class="form-control input-sm">
								<span id="languageInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>
			 	 <div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Language Code<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
				  			<input type="text" name="lang_code" id="lang_code" value="" class="form-control input-sm">
								<span id="langcodeInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>
			</div>

			

			  <!-- end section -->
			</div>
			<!-- end section row section -->
		 
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="languageId" value="" id="languageId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
.date #start_date{display: inline-flex;}
.date #end_date{display: inline-flex;}
</style>

<script> 

$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {

	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
	jQuery("#txt_contact_number").keypress(function (e) {  
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
	}); 	

// $(document).on("change","#user_type",function() {	
// 	if($(this).val()=='Supplier')
// 		$(".download_once").show();
// 	else{
// 		$(".download_once").hide();		
// 		$("#download_check").removeAttr("checked");
// 	}
// });
	//console.log(liObj);

$(document).ready(function(){
  
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {

	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	// if($("#txt_search").val()==""){
	// 	alert("Please enter the search term");
	// 	flag=0;
	// 	return false;
	// } 
	

	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	// if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
	// 		//alert("Spaces are not allowed");
	// 		flag = 0;
	// 		return false;
	// }
	
	if(flag){
		changePaginate(0,'id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesDelete(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}

// function showMessagesStatus(json)
// {

// $("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

// //$("#message div").fadeOut(10000);
// }



function showMessages(json)
{
	//alert(json.msg);
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'id','DESC');
		}
		setTimeout(function(){  $("#sectionModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getLanguageList();
} 
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Language Management");
	//$("#user_type").trigger("change");
	//$("#userid").val("");
	//document.getElementByid("formUser").reset();
});

//function to edit banner


$(document).on("click","#sectionModel .mfp-close",function() {
	$("#languageId").val("");
	
	//$(".pic1 img:last-child").remove();
		
//	$("#countryInfo").html(""); 
//	$("#userTypeInfo").html("");
	$("input[name='dd_resources[]']").removeAttr("checked", true);
	$("input[name='dd_subresources[]']").removeAttr("checked", true);
	//document.getElementByid("formUser").reset();
	location.reload();
});

function refreshTable()
{
	//$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	
	changePaginate(0,'id','DESC'); 	
}
function loadmore(){
	getLanguageList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getLanguageList();

function getLanguageList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var user_type= $("#dd_searchBy1").val();
	
	//var country= $("#searchby_country").val();
	
	var search = $("#txt_search").val();
	//console.log(start_date);
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_language",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getLanguageList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var languageId = $("#languageId"); 
		var language = $("#language");
			var languageInfo = $("#languageInfo");	

		var lang_code = $("#lang_code");
			var langcodeInfo = $("#langcodeInfo");

		var flag=1;
		if(!validateEmpty(language, languageInfo, "Language")){
			flag = 0;
		} 
		if(!validateEmpty(lang_code, langcodeInfo, "Language Code")){
			flag = 0;
		} 

		if(language.val()!=""){	 
			if(!CheckAlphabates(language, languageInfo)){
				flag = 0;
			} 	 
		}
		if(lang_code.val()!=""){	 
			if(!CheckAlphabates(lang_code, langcodeInfo)){
				flag = 0;
			} 	 
		}

		var formData = new FormData($('#formUser')[0]); 

		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                async : false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_language",
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.s_id).html(json.row);
				}else{  
					changePaginate('','add');
				}
				showMessages(json);
				$("#message div").fadeOut(10000);

				//$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteSection()
{
	if($('.chk:checked').length) {
		  chkid = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkid[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected record?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_language",
				data: {"ids":chkid},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				showMessagesDelete(json);
				//alert((json.ids).length+" Banners(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one banner.');
	}
}

$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#languageId").val(id);
	if(id!=""){
		$("#loading").show();
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getLanguageById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#language").val(json.language);
				$("#lang_code").val(json.lang_code);
		
				
				$("#add").trigger("click"); 
				$("#labelpassword").html("");
				$("#loading").hide();
				$("#userModel .panel-title").html("Edit Language Management");
			}); 
	} 
	else{
		$("#add").trigger("click");
	}
});
</script>
<?php $this->load->view("footer"); ?>
