<?php $this->load->view("header");?>
<?php

$lang_id = (isset($res[0]['lang_id'])?$res[0]['lang_id']:'');
$section_name = (isset($res[0]['section_name'])?$res[0]['section_name']:'');
$description = (isset($res[0]['page_data'])?$res[0]['page_data']:'');
$cat_id = (isset($res[0]['category'])?$res[0]['category']:'');
$subcat_id = (isset($res[0]['sub_category'])?$res[0]['sub_category']:'');
$public_access = (isset($res[0]['public_access'])?$res[0]['public_access']:'0');
$offline_reading = (isset($res[0]['offline_reading'])?$res[0]['offline_reading']:'0');
$section_id = (isset($res[0]['s_id'])?$res[0]['s_id']:'');
$sec_name = (isset($res[0]['name'])?$res[0]['name']:'');
if(isset($page) && $page!=""){
	$lang_id = (isset($page['lang_id'])?$page['lang_id']:'');
	$section_name = (isset($page['section_name'])?$page['section_name']:'');
	$description = (isset($page['page_data'])?$page['page_data']:'');
	$cat_id = (isset($page['category'])?$page['category']:'');
	$subcat_id = (isset($page['sub_category'])?$page['sub_category']:'');
	$public_access = (isset($page['public_access'])?$page['public_access']:'0');
	$offline_reading = (isset($page['offline_reading'])?$page['offline_reading']:'0');
	$resources_id_sel = (isset($page['resources_id'])?$page['resources_id']:'0');
	$message_error = (isset($page['message_error'])?$page['message_error']:'');

	
}

?>



<section id="content_wrapper">
	<!-- Begin: Content -->
	<section id="content" class="p15 pbn">
		<div class="row">
				<!-- Three panes -->
			<div class="col-md-12 admin-grid" id="animation-switcher">
				<div class="panel panel-info sort-disable" id="p0">
					<div class="panel-heading">
						<div class="topbar-left pull-left">
								<ol class="breadcrumb"> 
									<li class="crumb-link">Manage Pages</li>
									<?php if(isset($_GET['pid']) && $_GET['pid']!=""){?>
										<li class="crumb-trail">Edit Pages Data</li>
									<?php }else{ ?>
										<li class="crumb-trail">Add Pages Data</li>
									<?php } ?>
								</ol>
						</div>
						 
					</div> 
					<div class="panel-body mnw700 pn of-a">
						<div class="row mn">
							<div class="col-md-12 pn">
								<div class="dt-panelmenu clearfix"  id="resourc_sk">
									
									<?php if(!empty($message)) { ?>
									<div class="form-group col-md-12" id="message">
										<?php echo '<div class="alert alert-success">'.$message.'</div>'; 
										$url = base_url().'administration/getPagesListSK';
										?>
										<?php header( "refresh:1;url=$url" ); ?>
									</div>
									<?php } ?>
									<?php if(!empty($message_error)) { ?>
										<div class="form-group col-md-12" id="message">
											<?php echo '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$message_error.'</div>';
											?>
										</div>
									<?php } ?>
									
									<div class="dataTables_filter pull-left">
										<form class="form-horizontal" method="post" action="<?php echo base_url();?>administration/save_pages_data_sk" id="formcms" name="formcms" enctype="multipart/form-data">
										
										

										  <div class="section row mbn">
										  
										  
									   <div class="col-sm-12">

										<?php if(isset($_GET['pid']) && $_GET['pid']!=""){?>
										<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Sections Name</label>
											   <div class="col-lg-2 dir">
												<input type="text" id="sec_name" name="sec_name" class="form-control" value="<?php echo $sec_name; ?>">
										<input type="hidden" name="section_id" id="section_id" value="<?php echo $section_id; ?>">
											   </div>
											</div>
									<?php }else{ ?>
										<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Sections</label></label>
											  <div class="col-lg-4"> 
												<select name="sel_section" id="sel_section" class="select-sm arrow_new form-control">
												

					      
									         <?php if(is_array($sections) && !(empty($sections))){
																		echo '<option value="0">Select Section</option>';
																		
																		foreach($sections as $section):
																			$selected = '';
																			
																				if($section_id == $section['id']){
																					$selected = 'selected="selected"';
																				}
																				?>
									<option <?php echo $selected;?> value="<?php echo $section['id']?>" ><?php echo $section['name'];?></option>
																				<?php
																		endforeach;
																}?>
									      </select>
												<span id="selSectionInfo" class="text-danger"></span>
											  </div>
											  
											</div>
											
										  </div> 
									<?php } ?>
										  
										  </div>

										  <div class="col-sm-12 sec_cat" > 
											<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Category</label>
											   <div class="col-lg-2 dir">
<!-- 												<label id="sec_cat">Category</label> -->
										<input type="text" id="sec_cat" name="sec_cat" class="form-control input-sm seccat" value="">
												
											   </div>
											</div>
										  </div>

										  <div class="col-sm-12 sec_subcat"> 
											<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Sub Category</label>
											   <div class="col-lg-2 dir">
<!-- 												<label id="sec_subcat">Category</label> -->
												<input type="text" id="sec_subcat" name="sec_subcat" class="form-control input-sm seccat" value="">
												
											   </div>
											</div>
										  </div>

										  <div class="col-sm-12"> 
											<div class="form-group">
											  <label for="inputStandard" class="nopad-left col-lg-2 pn mt5 control-label new_first">Public Access</label>
											   <div class="col-lg-6 dir">
												<input type="checkbox" id="chk_public" name="chk_public" class="" <?php echo ($public_access=='1'?'checked':''); ?> value="<?php echo $public_access; ?>">
												<label>Sections with public access is directly displayed for non registered users</label>
											   </div>
											</div>
										  </div>
										   <div class="col-sm-12"> 
											<div class="form-group">
											  <label for="inputStandard" class="nopad-left col-lg-2 pn mt5     control-label new_first">Offline Reading</label>
											   <div class="col-lg-6 dir">
												<input type="checkbox" id="chk_offline" name="chk_offline" class="" <?php echo ($offline_reading=='1'?'checked':''); ?> value="<?php echo $offline_reading; ?>">
												<label>Section with offline reading the user can read section offline</label>
											   </div>
											</div>
										  </div>
										  <div class="col-sm-12"> 
											<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first res_pon">English version data <br/> to see at page creation</label>
											  <div class="col-lg-8 text_inpu_new">
												<textarea id="txt_description" name="txt_description"><?php echo $description;?></textarea>
												<textarea style="display:none;" id="txt_description1" name="txt_description1"></textarea>
												<span id="descriptionInfo"  class="text-danger"></span>
											  </div>
											</div>
										  </div>
										  
										<!-- <div class="col-sm-12 attached_kit "> 
											<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first choose_resources">Choose Resources Sales Kit Attached</label>
											  <div class="col-lg-4 chos_kit">
												<a href="#" id="choose_res_sk">Choose Resources</a>
												
											  </div>
											  <div class="col-lg-4 chos_kit"><span id="resourcesInfo"  class="text-danger"></span></div>
											</div>
										  </div> -->
										  
										   <div class="col-sm-12" id="sales_kit" style="height:0px;min-height:0px;overflow:auto;"> 
											<div class="form-group" >
											  <div class="col-lg-12">
                                             
                                               <div class="col-lg-12">
												 <?php if(is_array($res_sk)){
													 $i=0;
													 $arr_res =array();
													// echo $res[0]['resources_id'];
													 if($res[0]['resources_id'] != ""){   
														$path = explode(',',$res[0]['resources_id']);														
														$arr_res = array_merge($arr_res, $path);
													 } 
													 elseif($resources_id_sel){ 
														$path = explode(',',$resources_id_sel);														
														$arr_res = array_merge($arr_res, $path); 
													 }  
													 foreach($res_sk as $row):
													 $checked = "";
													// print_r($arr_res);
													 //echo $res[0]['resources_id'];
													 if(in_array($row['id'], $arr_res)) { $checked = 'checked="checked"'; }
													 
												 ?>
													<label class="checkbox-inline"><input <?php echo $checked;?> name="resources_sk_files[]" type="checkbox" value="<?php echo $row['id']; ?>"><?php echo $row['resources_sk_pdf']; ?></label>
												
												<?php 
												$i++;
												endforeach;
												}
												?>
                                                </div>
											  </div>
											</div>
										  </div>  
										</div>
										<div id="set_text"></div>
										<div class="col-lg-12 botm_save_cancel"> 
                                        <div class="col-lg-2">&nbsp;</div>
											<div class="col-lg-10"><input type="submit" class="button table-submitbtn btn-info btn-xs sals_kit pull-left" id="btn_save" value="Submit">
											<a href="<?php echo base_url();?>administration/getPagesListSK" class="button btn-info sals_kit button table-submitbtn pull-left ">Cancel</a>
                                            </div>
										</div>
										<input type="hidden" name="pageId" id="pageId" value="<?php echo (isset($_GET['pid'])?$_GET['pid']:'');?>" class="gui-input">
										</form>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div> 
	</section>
</section> 

<style type="text/css">
	.seccat{margin-left:0px !important;}
	.nopad-left{padding-left:0px !important;}
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor2/ckeditor.js"></script>
<script> 

$( window ).load(function() {
	$("#sel_cat").trigger("change",[{sub_cat_id:"<?php echo $subcat_id;?>"}]);
//	$("#sel_page").trigger('change');
	$("#message div").fadeOut(5000);
}); 
loadeditor();

$(document).ready(function() { 
$(".sec_subcat").hide();
$(".sec_cat").hide();

var id= $("#pageId").val();
if(id!=""){
	$("#sec_name").attr('readonly','readonly');
}

$("#chk_public").change(function() {
	if($(this).is(':checked')){
		   $(this).val(1);
	}
	else{
	   $(this).val(0);
	}
});	

$("#chk_offline").change(function() {
	if($(this).is(':checked')){
		   $(this).val(1);
	}
	else{
	   $(this).val(0);
	}
});	

	$(document).on("change","#sel_section",function(e,data) {
		var sec_id =  $(this).val(); 
		
		$.ajax({
		type: "POST",
		dataType: "json",		
		url: "<?php echo base_url(); ?>administration/getCategoryBySecId", 
		//url: "<?php //echo base_url(); ?>administration/getSubCategoryByCatId", 
		data: {"sec_id": sec_id}
		}).success(function (json) {
			if(json.sub_category){
				$(".sec_subcat").show();
			}else{
				$(".sec_subcat").hide();
			}
			//$(".sec_subcat").show();
			$(".sec_cat").show();
			$("#sec_cat").attr('readonly','readonly');
			$("#sec_subcat").attr('readonly','readonly');
			$("#sec_cat").val(json.category);
			$("#sec_subcat").val(json.sub_category);	
		});
		
	});


$(document).on("change","#sel_cat",function(e,data) {
    //alert(sub_cat_id);
	var catid =  $(this).val(); 
	//$("html").attr('lang',lang);
	if(data == undefined){
		sub_cat_id ="";
	}
	else{
		sub_cat_id = data.sub_cat_id;
	}
	$.ajax({
		type: "POST",
		dataType: "json",		
		url: "<?php echo base_url(); ?>administration/getSubCategoryByCatId", 
		data: {"catid": catid,"sub_cat_id":sub_cat_id}
	}).success(function (json) {
		$("#set_subcategory").html(json.res);
		
	});
});

});

function loadeditor(){ 
    CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('txt_description', {
				width: 750,
				height: 450,
				
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
					
				}
			}); 
}  
 
$(document).ready(function() {
$("#sales_kit").show();
$(document).on("click","#choose_res_sk",function() {
	$("#sales_kit").toggle('slow');
});	

$('#btn_save').on('click', function (e) {
	e.preventDefault();  
	
	flag=1;
	var textbox_data = CKEDITOR.instances.txt_description.getData();	
	$('#txt_description1').text(encodeURIComponent(textbox_data));
	$("#txt_description").text("");
	
	//console.log(textbox_data);return false;
	// var txt_section = $("#txt_section");
	// 	var sectionInfo = $("#sectionInfo");	
	
	var txt_description = $("#txt_description");
		var descriptionInfo = $("#descriptionInfo");
			
	var sel_section = $("#sel_section");
		var selCatInfo = $("#selSectionInfo");
	
	var data=$("#pageId").val();

	if(data==""){
		var slng = $("#sel_section").val();
		//alert(slng);
		if(slng==0){
			selCatInfo.html("Please select section");
			flag = 0;
		}else{
			selCatInfo.html("");
		}  
	}
	else{
			//alert('Please add sections');
		}
	// if(!checkCombo(sel_section, selCatInfo, "section")){
	// 		flag = 0;
	// } 


	//console.log(textbox_data);return false;
	if(textbox_data == ""){
			descriptionInfo.html("Please enter the page description");
			flag = 0;
	}
	else{
		descriptionInfo.html("");
	}
	
	$("#btn_save").attr('disabled','disabled');
	
	if(flag){ 
		$("#formcms").submit();
	}
	else{
		$("#btn_save").removeAttr('disabled','disabled');
		return false;
	}
});


});


</script>
<?php $this->load->view("footer");?>
