<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                <div id="message"></div>
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">

						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">

							<?php $message= $this->session->flashdata('Success'); if(!empty($message))?>

								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Questionnaire</li>
											<li class="crumb-trail">Manage Questionnaire</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#quetionnaireModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteQuetionnaire()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length set_querytypes">
												<div class="multiple-selection mr5">
													
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="start_date">Start Date</option>
													<option value="end_date">End Date</option>
													<option value="user_type">User Type</option>
												<!-- 	<option value="status">Status</option> -->
													<option value="country">Country</option>
												</select>
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy1" aria-controls="datatable2" class="form-control input-sm">
												<option value="0">User Type</option>
													<option value="1" selected="selected">Wholesaler</option>
													<option value="2">Dealers</option>
												</select>
												</div>
											</div>
											<!-- <div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="searchby_status" id="searchby_status" aria-controls="datatable2" class="form-control input-sm">
												<option value="0">User Status</option>
													<option value="Active" selected="selected">Active</option>
													<option value="Inactive">Inactive</option>
												</select>
												</div> 
											</div> -->

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<div class='input-group date' id='start_date1'>
									                <input type='text' placeholder="Start Date" class="form-control" name="start_date" id="start_date" />
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<div class='input-group date' id='end_date1'>
									                <input type='text' placeholder="End Date" class="form-control" name="end_date" id="end_date" />
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
												
												</div>
											</div>

											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<!-- <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"> -->
												<select name="searchby_country" id="searchby_country" aria-controls="datatable2" class="form-control input-sm">
													<?php
														if(is_array($country)){
															echo '<option value="">Select</option>';
															foreach($country as $con):	?>
															<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
														 <?php endforeach;
														}
													?>
												</select>
												</div>
											</div>
											
                                            <div class="dataTables_filter pull-left">
												<div class="row">
												
													<!-- <div class="col-xs-6 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div> -->
													<div class="col-xs-3 col-sm-6 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-3 col-sm-3 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>

											</div>
                                            
											<!-- <div id="message"></div> -->
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="quetionnaireModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Quetionnaire Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"> </div>
			<!-- added by pallavi for usertype -->

			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Title<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_title" name="txt_title" class="form-control input-sm" placeholder="Title">
					<span id="titleInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			   </div>
			</div>

			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">User Type<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<!-- <input type="text" id="txt_title" name="txt_title" class="form-control input-sm" placeholder="First Name">
					<span id="titleInfo"  class="text-danger marg"></span> -->
					<select id="user_type" name="user_type" class="form-control input-sm">
					     <option value="">Please select user type</option>
						  <option value="1">Wholesaler</option>
						  <option value="2">Dealers</option>
						</select> 
					<span id="userTypeInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>

			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Country<span class="validationerror">*</span></label>
				  <label class="col-lg-8 text_inpu_new">
					<select class="select-sm arrow_new form-control"  name="country" id="country">
						<?php
							if(is_array($country)){
								echo '<option value="">Select</option>';
								foreach($country as $con):	?>
								<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
					<span id="countryInfo"  class="text-danger"></span>
				  </label>
				</div>
			  </div>
			</div>    

			
			<!-- end for user type -->
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Promo Code<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_promocode" readonly="readonly" name="txt_promocode" class="form-control input-sm" placeholder="Promo Code">
					<span id="promocodeInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Promo Amount<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_promo_amount" name="txt_promo_amount" class="form-control input-sm" placeholder="Promo Amount">
					<span id="promoamountInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>  


			<div class="section row mbn">
			 	<div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Start Date<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
								<div class="form-group">
						            <div class='input-group date' id='datetimepicker6'>
						                <input type='text' class="form-control" name="start_date" id="start_date" />
						                <span class="input-group-addon">
						                    <span class="glyphicon glyphicon-calendar"></span>
						                </span>
						            </div>
						        </div>
								<span id="startDateInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>

			 	 <div class="col-sm-6">
					<div class="form-group">
				  		<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">End date<span class="validationerror">*</span></label>
				  			<div class="col-lg-8 text_inpu_new">
								<div class="form-group">
						            <div class='input-group date' id='datetimepicker7'>
						                <input type='text' class="form-control" name="end_date" id="end_date" />
						                <span class="input-group-addon">
						                    <span class="glyphicon glyphicon-calendar"></span>
						                </span>
						            </div>
						        </div>
								<span id="endDateInfo" class="text-danger marg"></span>
				  			</div>
					</div>
			 	 </div>	  
			</div> 

			<!-- <div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Status<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_promocode" name="txt_promocode" class="form-control input-sm" placeholder="Promo Code">
					<select name="status" id="status" aria-controls="datatable2" class="form-control input-sm">
						<option value="0">Status</option>
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
				   </select>
					<span id="statusInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			 </div> -->

			

			<div class="section row mbn">
			<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category </label>
				  
					  <div class="col-lg-8 text_inpu_new categorymain">
					 	<div class="pm Vehicle-pm"><div class="dataTables_length1" >
						<div class="multiple-selection mr5">
							
							<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):	
										
								?>
								<li><input type="checkbox" name="dd_resources[]" id="dd_resources" value="<?php echo $cat['id']?>" placeholder=""  class="events-category"><?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?>
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" id="dd_subresources" value="<?php echo $subcat['id']?>"  class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
									
								
								 <?php endforeach;
								 ?>
								 </li>
								 </ul>
								 <?php
								    }
								 endforeach;
									}
								?>
							</ul>
							
						</div>
						<span id="resourcesInfo" class="text-danger"></span>
					</div>
					</div> 
				</div>
			  </div>
			</div>
			</div>
			<div class="section row mbn quetion initial_q" id="q_0">
				
			 	<div class="col-md-12">
					<div class="panel panel-default qa_panel" data-index="1">
					  <div class="panel-heading"><label for="question">Question 1</label><button data-index="1" type="button" class="add_question btn pull-right">+</button><button data-index="1" type="button" class="remove_question btn pull-right hide">X</button></div>
					  <div class="panel-body"> 
						<div class="form-group">
							<label for="question">Question 1</label>
							<input type="text" name="questions[]" class="form-control quetion" value="" />
						</div>
						<div class="form-group">
							<label for="question">No.of Options</label>
							<input type="number" name="optionslength[]" min="2" max="4" class="form-control options_length" value="" /> 
						</div> 
						<div class="form-group answers_field" id="q"></div> 
					  </div>
					</div>
				</div>
			 </div>
			  
			    
			 
			 
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		 
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="questionId" id="questionId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
* {
  .border-radius(0) !important;
}

#field {
    margin-bottom:20px;
}


</style>

<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>

<script> 
		$(document).ready(function(){
			var no_of_options_obj = $('.options_length');
			var add_question_btn = $('.add_question');
			
			var option_html_block = '<div class="col-md-3"><input name="option_{{question_index}}_{{index}}" class="form-control" type="text" placeholder="Option {{index}}" /><label><input name="answers_{{question_index}}" value="{{index}}" type="radio"> Correct Answer</label></div>';
			
			var question_html_block = '<div class="panel panel-default qa_panel" data-index="{{index}}"><div class=panel-heading><label for=question>Question {{index}}</label><button class="btn pull-right add_question" data-index="{{index}}" type=button>+</button><button data-index="{{index}}" class="remove_question btn pull-right hide" type="button">X</button></div><div class=panel-body><div class=form-group><label for=question>Question {{index}}</label><input class="form-control" name=questions[] /></div><div class=form-group><label for=question>No.of Options</label><input class="form-control options_length"name=optionslength[] max=4 min=2 type=number></div><div class="form-group answers_field"id=q></div></div></div>';
			
			$(document).on('click', '.options_length', function(e){ 
				e.preventDefault(); 
				var get_parent_obj = $(this).parent().parent().parent('.qa_panel');
				var option_fields_append_to_obj = get_parent_obj.find('.answers_field');
				var no_of_options = $(this).val();
				var question_index = get_parent_obj.data('index');
				option_fields_append_to_obj.html('');
				for(var i=1; i<=no_of_options; i++){ 
					var optionObj = $(option_html_block.replace(new RegExp('{{index}}', 'g'), i).replace(new RegExp('{{question_index}}', 'g'), question_index));  
					optionObj.appendTo(option_fields_append_to_obj);
				}
			});
			
			$(document).on('click', '.add_question', function(e){
				e.preventDefault(); 
				var get_parent_obj = $(this).parent().parent('.qa_panel');
				var option_fields_append_to_obj = get_parent_obj.find('.answers_field'); 
				var this_remove_question_btn = get_parent_obj.find('.remove_question');
				var this_add_question_btn = get_parent_obj.find('.add_question');
				var index = $(this).data('index');   
				if(index > 0)
				{ 
					var questionObj = $(question_html_block.replace(new RegExp('{{index}}', 'g'), index + 1));
					questionObj.insertAfter(get_parent_obj);
					this_remove_question_btn.removeClass('hide');
					this_add_question_btn.addClass('hide');
				}
			});
			
			$(document).on('click', '.remove_question', function(){
				var get_parent_obj = $(this).parent().parent('.qa_panel'); 
				var index = $(this).data('index');   
				if(index > 0) {
					get_parent_obj.remove();
				}
			});
		});

	


//end validation

$(document).ready(function(){

	 $('#datetimepicker6').datetimepicker({
        });

        $('#datetimepicker7').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            //format:'Y-m-d'
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
 });

jQuery("#txt_promo_amount").keypress(function (e) {  
	//console.log('ada'+e.which);
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && e.which != 46 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 
 

// $('#txt_search').keydown(function (e){
//     if(e.keyCode == 13){
//         $("#search_btn").trigger('click');
//     }
// });	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	

// $(document).on("change","#user_type",function() {	
// 	if($(this).val()=='Supplier')
// 		$(".download_once").show();
// 	else{
// 		$(".download_once").hide();		
// 		$("#download_check").removeAttr("checked");
// 	}
// });
	//console.log(liObj);

$(document).ready(function(){

	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	
	 
	$('.events-category').change( function(){
		var c = this.checked; 
		if( c ){
			$(this).next('.event-children').css('display', 'block'); 
			
		}else{
			$(this).next('.event-children').css('display', 'none');
		}
	});
});

$(document).on("click","#search_btn",function() {

	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	// if($("#txt_search").val()==""){
	// 	alert("Please enter the search term");
	// 	flag=0;
	// 	return false;
	// } 
	

	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	// if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
	// 		//alert("Spaces are not allowed");
	// 		flag = 0;
	// 		return false;
	// }
	
	if(flag){
		changePaginate(0,'q_id','DESC');
	}
	else{
		return false;
	}
});

// function showMessagesStatus(json)
// {

// $("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

// //$("#message div").fadeOut(10000);
// }

function showMessagesDelete(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}



// function changeStatus(uid,status)
// {
// 	$.ajax({
// 			type: "POST",
// 			dataType: "json",
// 			url: "<?php echo base_url(); ?>administration/change_user_status",
// 			data: {"questionId":uid,"status":status},
// 		}).success(function (json) { 
// 			if(json.status == 1)
// 			{		
// 				if(json.action == "add")
// 					{   $("#row_"+json.id).html(json.row);
// 					}
// 			}
// 			showMessagesStatus(json); 
// 		});
// }

function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'id','DESC');
		}
		setTimeout(function(){  $("#quetionnaireModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getQuetionList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New Questionnaire");
	//$("#user_type").trigger("change");
	//$("#questionId").val("");
	//document.getElementById("formUser").reset();
});



var option_html_block = '<div class="col-md-3"><input name="option_{{question_index}}_{{index}}" class="form-control" type="text" placeholder="Option {{index}}" /><label><input name="answers_{{question_index}}" value="{{index}}" type="radio"> Correct Answer</label></div>';

var question_html_block = '<div class="panel panel-default qa_panel" data-index="{{index}}"><div class=panel-heading><label for=question>Question {{index}}</label><button class="btn pull-right add_question" data-index="{{index}}" type=button>+</button><button data-index="{{index}}" class="remove_question btn pull-right hide" type="button">X</button></div><div class=panel-body><div class=form-group><label for=question>Question {{index}}</label><input class="form-control" name=questions[] value="{{question}}" /></div><div class=form-group><label for=question>No.of Options</label><input class="form-control options_length"name=optionslength[] value="{{options_length}}" max=4 min=2 type=number></div><div class="form-group answers_field"id=q></div></div></div>';
var initial_qa_panel = $('.initial_q');
$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#questionId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getQuetionaryById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				//console.log(json);
				for (var i = 0; i < json.length; i++) {
					var eachObj = json[i];
					//console.log(eachObj.id);
					$("#txt_title").val(json[0].title);
					$("#txt_promocode").val(json[0].promo_code);
					$("#txt_promo_amount").val(json[0].promo_amt);
				     $('#datetimepicker6').data("DateTimePicker").date(json[0].start_date);
				     $('#datetimepicker7').data("DateTimePicker").date(json[0].end_date);
					$("#country").val(json[0].country);
					$("#user_type").val(json[0].user_type);
				}
				if(json[0].category_id){
					var valArr = json[0].category_id; 
					var dataarray = valArr.split(","); 
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				} 
				 
				if(json[0].sub){
					var valSubArr = json[0].sub_category_id; 
					var datasubarray = valSubArr.split(","); 
					for (var count = 0; count < datasubarray.length; count++) { 
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				console.log(json.length);

				for (var i =0;i < json.length; i++) {
					console.log(json[i].option1);  
					console.log(json[i].option2);  
					console.log(json[i].option3);  
					console.log(json[i].option4);  
					//console.log(json[i].);  

					var questionObj = $(question_html_block.replace(new RegExp('{{index}}', 'g'), i+1).replace(new RegExp('{{question}}', 'g'),json[i].quetion)); 
						
						questionObj.insertBefore(initial_qa_panel);



				}


				
				//console.log(json.country);
				
				 
				$("#add").trigger("click"); 
				
				$("#quetionnaireModel .panel-title").html("Edit Questionnaire");
			}); 
	} 
	else{
		$("#add").trigger("click");
	}
});
$(document).on("click","#quetionnaireModel .mfp-close",function() {
	$("#questionId").val("");
	
	$(".pic1 img:last-child").remove();
	$("#titleInfo").html(""); 
	$("#countryInfo").html(""); 
	$("#userTypeInfo").html(""); 
	$("#promocodeInfo").html(""); 
	$("#promoamountInfo").html(""); 
	$("#startDateInfo").html(""); 
	$("#endDateInfo").html(""); 
	// $("#statusInfo").html(""); 

	//$("#ai_file_access").removeAttr("checked","checked");		

	$("input[name='dd_resources[]']").removeAttr("checked", true);
	$("input[name='dd_subresources[]']").removeAttr("checked", true);
	document.getElementById("formUser").reset();
	location.reload();
});

function refreshTable()
{
	//$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	// $("#searchby_status").val("");
	$("#start_date").val("");
	$("#end_date").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'id','DESC'); 	
}
function loadmore(){
	getQuetionList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getQuetionList();

function getQuetionList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var user_type= $("#dd_searchBy1").val();
	// var status= $("#searchby_status").val();
	var country= $("#searchby_country").val();
	var start_date= $("#start_date").val();
	var end_date= $("#end_date").val();
	var search = $("#txt_search").val();
	
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_questionary",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy,"user_type":user_type,"country":country,"start_date":start_date,"end_date":end_date},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getQuetionList();
			}
		});	*/

		function randomString(length, chars) {
		    var result = '';
		    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
		    return result;
		}
		var rString = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		//console.log(rString);
		$("#txt_promocode").val(rString);
	

	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var questionId = $("#questionId"); 
		var title = $("#txt_title");
			var titleInfo = $("#titleInfo"); 
		var country = $("#country");
			var countryInfo = $("#countryInfo"); 
		var userType = $("#user_type");
			var userTypeInfo = $("#userTypeInfo");
		var promocode = $("#txt_promocode");
			var promocodeInfo = $("#promocodeInfo");  	
		var promo_amount = $("#txt_promo_amount");
			var promoamountInfo = $("#promoamountInfo");
		var startDate = $("#start_date");
			var startDateInfo = $("#startDateInfo");
		var endDate = $("#end_date");
			var endDateInfo = $("#endDateInfo");
		// var status = $("#status");
		// 	var statusInfo = $("#statusInfo");
			 	
		var flag=1;
		if(!validateEmpty(title, titleInfo, "title")){
			flag = 0;
		} 	
		if(!validateEmpty(promocode, promocodeInfo, "Promo")){
				flag = 0;
		}
		if(!validateEmpty(promo_amount, promoamountInfo, "Promo")){
			flag = 0;
		} 
		if(!checkCombo(startDate, startDateInfo, "start sate")){
			flag = 0;
		} 
		if(!checkCombo(endDate, endDateInfo, "end date")){
			flag = 0;
		} 
		if(!checkCombo(country,countryInfo,"the country")){
			flag = 0;
		}
		if(!checkCombo(userType, userTypeInfo, "user type")){
			flag = 0;
		}

		// $("#formUser").validate({
		//     rules: {
		//         "questions[0]": {
		//             required: true
		//         }
		//     },
		//      messages: {

		//        "questions[0]": {
		//       		required: "Required"
		//    		 }
		// 	}

		// });

		// $('.quetion').each(function () {
		//         $(this).rules("add", {
		//             required: true
		//         });
		//     });

	
		var formData = new FormData($('#formUser')[0]); 
		//console.log(formData);

		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/submit_questions",
				data: formData
			}).success(function (json) {
				//console.log(json);
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
			//	$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteQuetionnaire()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected records?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_quetionary",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				//location.reload();
				showMessagesDelete(json);
				//alert((json.ids).length+" user(s) has been deleted.");
				
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>
<?php $this->load->view("footer"); ?>
