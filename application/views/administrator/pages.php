<?php $this->load->view("header");?>
<?php
$lang_id = (isset($res[0]['lang_id'])?$res[0]['lang_id']:'');
$cat_id = (isset($res[0]['category'])?$res[0]['category']:'');
$subcat_id = (isset($res[0]['sub_category']) && $res[0]['sub_category']!="NULL" ?$res[0]['sub_category']:'');
$section_id = (isset($res[0]['section_id'])?$res[0]['section_id']:'');
$description = (isset($res[0]['page_description'])?$res[0]['page_description']:'');

if(isset($page) && $page!=""){
	$lang_id = (isset($page['lang_id'])?$page['lang_id']:'');
	$description = (isset($page['page_description'])?$page['page_description']:'');
}

?>

<section id="content_wrapper">
	<!-- Begin: Content -->
	<section id="content" class="p15 pbn">
		<div class="row">
				<!-- Three panes -->
			<div class="col-md-12 admin-grid" id="animation-switcher">
				<div class="panel panel-info sort-disable" id="p0">
					<div class="panel-heading">
						<div class="topbar-left pull-left">
								<ol class="breadcrumb"> 
									<li class="crumb-link">Manage Language Translation</li>
									<?php if(isset($_GET['pid']) && $_GET['pid']!=""){?>
										<li class="crumb-trail">Edit Pages</li>
									<?php }else{?>
										<li class="crumb-trail">Add Pages</li>
									<?php } ?>
									
								</ol>
						</div>
						 
					</div> 
					<div class="panel-body mnw700 pn of-a">
						<div class="row mn">
							<div class="col-md-12 pn">
								<div class="dt-panelmenu clearfix"  id="resourc_sk">
									
									<?php if(!empty($message)) { ?>
									<div class="form-group col-md-12" id="message">
										<div class="alert alert-success"><?php echo $message;?></div>	
										<?php										
										$url = base_url().'administration/getPagesListSK';
										?>
										<?php header( "refresh:5;url=$url" ); ?>
									</div>
									<?php } ?>
									<?php if(!empty($message_error)) { ?>
									<div class="form-group col-md-12" id="message">
										<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button><?php echo $message_error; 
										?>
										</div>
									</div>
									<?php } ?>
									
									<div class="dataTables_filter pull-left" id="select_engin">
										<form class="form-horizontal" method="post" action="<?php echo base_url();?>administration/save_pages_sk" id="formcms" name="formcms" enctype="multipart/form-data">
										
										<div class="section row mbn">
										  <div class="col-sm-12">
											<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Language</label><span class="validationerror">*</span></label>
											  <div class="col-lg-4"> 
												<select name="sel_lang" id="sel_lang" class="select-sm arrow_new form-control">
													<?php if(is_array($langauages) && !(empty($langauages))){
																echo '<option value="">Select Language</option>';
																foreach($langauages as $lang):
																		$selected = '';
																		if($lang_id == $lang['id']){
																			$selected = 'selected="selected"';
																		}
																		?>
																		<option <?php echo $selected;?> value="<?php echo $lang['id']?>" data-lang-id="<?php echo $lang['lang_code'];?>"><?php echo $lang['language'];?></option>
																		<?php
																endforeach;
														}
													?>
												</select>
												<span id="selLangInfo" class="text-danger"></span>
											  </div>
											</div>
											
											<div class="form-group">
											  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Category</label><span class="validationerror">*</span></label>
											  <div class="col-lg-4"> 
												<label for="inputStandard" class="col-lg-2 pn mt5 control-label"><?php echo $categories[0]['category']; ?></label>
												<input type="hidden" id="sel_cat" name="sel_cat" value="<?php echo $cat_id;?>">
												 
											  </div>
											</div>
											
											<div class="form-group"> 
											 <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Sub Category</label></label>
											 <div class="col-lg-4"> 
												<label for="inputStandard" class="col-lg-1 pn mt5 control-label"><?php echo ($sub_categories[0]['sub_category']?$sub_categories[0]['sub_category']:'-'); ?></label>
												<input type="hidden" id="sel_subcat" name="sel_subcat" value="<?php echo $subcat_id;?>">
											  </div>
											</div>
											
											<div class="form-group">
											 <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Section Name</label></label>	
											  <div class="col-lg-4"> 
												 <label for="inputStandard" class="col-lg-2 pn mt5 control-label"><?php echo $section_name; ?></label>
												 <input type="hidden" id="sel_section" name="sel_section" value="<?php echo $section_name;?>">
											 </div>
											</div>
											
											
											<div class="form-group">
											 <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>English Version</label></label>	
											  <div class="col-lg-8"> 
												 <label id="set_english_version" for="inputStandard" class="col-lg-6 pn mt5 control-label"><?php echo $res[0]['page_data']; ?></label> 
											 </div>
											</div>
											
											<div class="form-group">
											 <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Page Description<span class="validationerror">*</span></label></label>	
											  <div class="col-lg-8"> 
												 <textarea id="txt_description" name="txt_description"><?php echo $description;?></textarea>
												 <textarea style="display:none;" id="txt_description1" name="txt_description1"></textarea>
													<span id="descriptionInfo"  class="text-danger"></span> 
											 </div>
											</div>
												 
											
										  </div>  
										  
										   
										</div>
										<div id="set_text"></div>
										<div class="col-lg-12 botm_save_cancel"> 
                                        <div class="col-lg-4">&nbsp;</div>
											<div class="col-lg-6"><input type="submit" class="button table-submitbtn btn-info btn-xs sals_kit" id="btn_save" value="Submit">
											<a href="<?php echo base_url();?>administration/getPagesListSK" class="button btn-info sals_kit">Cancel</a>
                                            </div>
										</div>
										<input type="hidden" name="pageId" id="pageId" value="<?php echo (isset($_GET['pid'])?$_GET['pid']:'');?>" class="gui-input">
										<input type="hidden" name="pgId" id="pgId" value="<?php echo (isset($_GET['pgid'])?$_GET['pgid']:'');?>" class="gui-input">
										</form>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div> 
	</section>
</section> 


<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor2/ckeditor.js"></script>
<script> 

jQuery("#txt_cphone").keypress(function (e) {  
	if (e.which != 8 && e.which != 40 && e.which != 41 && e.which != 45 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	
jQuery("#txt_cfax").keypress(function (e) {  
	if (e.which != 8 && e.which != 40 && e.which != 41 && e.which != 45 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	
$("#contact_div").hide();
loadeditor();


function loadeditor(){ 
	
    CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('txt_description', {
				width: 700,
				height: 400,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
					
				}
			}); 
}  

$( window ).load(function() {
	$("#sel_cat,#sel_subcat").trigger("change",[{sub_cat_id:"<?php echo $subcat_id;?>",section_name:"<?php echo $section_name;?>"}]);
//	$("#sel_page").trigger('change');
	$("#message div").fadeOut(5000);
}); 

$(document).ready(function() {
$("#sales_kit").show();
$(document).on("click","#choose_res_sk",function() {
	$("#sales_kit").toggle('slow');
});	
$(document).on("keypress","#msg_welcome",function() {

	var text =  $(this).val();
	//$("html").attr('lang',lang);
	$.ajax({
				type: "POST",
				dataType: "json",		
				url: "<?php echo base_url(); ?>administration/getTranslatedText", 
				data: {"text": text}
			}).success(function (json) {
				$("#set_text").html(json.result);
			});
});

$(document).on("change","#sel_cat",function(e,data) {

	var catid =  $(this).val(); 
	var sub_cat_id =  $("#sel_subcat").val(); 
	
	if(data == undefined){
		sub_cat_id ="";
		section_name ="";
	}
	else{
		sub_cat_id = data.sub_cat_id;
		section_name = data.section_name;
	}
	
	$.ajax({
		type: "POST",
		dataType: "json",		
		url: "<?php echo base_url(); ?>administration/getSectionsDataByCatId", 
		data: {"catid": catid,"sub_cat_id":sub_cat_id,"section_name":section_name}
	}).success(function (json) {
		$("#set_subcategory").html(json.res);
		$("#set_sections").html(json.res_section);
		$("#sel_section").trigger('change');
		
	});
});

$(document).on("change","#sel_subcat",function(e,data) {

	var sub_cat_id =  $(this).val();  
	
	$.ajax({
		type: "POST",
		dataType: "json",		
		url: "<?php echo base_url(); ?>administration/getSectionsDataBySubCatId", 
		data: {"sub_cat_id":sub_cat_id,"section_name":section_name}
	}).success(function (json) {
		//$("#set_subcategory").html(json.res);
		$("#set_sections").html(json.res_section);
		//$("#sel_section").trigger('change');
		
	});
});



$(document).on("change","#sel_section",function(e,data) {

	var sectionid =  $(this).val(); 
	//$("html").attr('lang',lang);
	
	var sel_cat = $("#sel_cat").val();
	
	var sel_subcat = $("#sel_subcat").val();
	
	$.ajax({
		type: "POST",
		dataType: "json",		
		url: "<?php echo base_url(); ?>administration/getSectionsPageDataBySectionId", 
		data: {"sectionid": sectionid, "sel_cat":sel_cat, "sel_subcat":sel_subcat}
	}).success(function (json) {
		$("#set_english_version").html(json.res_data); 
		
	});
});



$('#btn_save').on('click', function (e) {
	e.preventDefault();  
	 

	flag=1;
	//var textbox_data = CKEDITOR.instances.txt_description.getData();
	
	
	var sel_lang = $("#sel_lang");
		var selLangInfo = $("#selLangInfo"); 
	
	var sel_cat = $("#sel_cat");
		var selCatInfo = $("#selCatInfo");
		
	var txt_section = $("#sel_section");
		var sectionInfo = $("#selSectionInfo");	
	
	var txt_description = $("#txt_description");
		var descriptionInfo = $("#descriptionInfo");
			
	if(!checkCombo(sel_lang, selLangInfo, "the language")){
		
			flag = 0;
	} 
	
	/*if(!checkCombo(sel_cat, selCatInfo, "the category")){
			flag = 0;
	} 
	
	if(!validateEmpty(txt_section, sectionInfo, "section name.")){
			flag = 0;
	}*/
	
	var textbox_data = CKEDITOR.instances.txt_description.getData(); 
	$('#txt_description1').text(encodeURIComponent(textbox_data));
	$("#txt_description").text("");
	//alert($('#txt_description1').text());
	if(textbox_data == ""){
			descriptionInfo.html("Please enter the page description");
			flag = 0;
	}
	else{
		descriptionInfo.html("");
	}
	
	$("#btn_save").attr('disabled','disabled');
	
	
	//alert(flag);
	if(flag){ 
		
		$("#formcms").submit();
	}
	else{
		$("#btn_save").removeAttr('disabled','disabled');
		return false;
	}
});


});


</script>
<?php $this->load->view("footer");?>
