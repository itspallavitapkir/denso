<?php $this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row" >
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Statistics</li>
										<li class="crumb-trail">Download Statistics</li>
									</ol>
								</div>
								 
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn" style="height:30px;">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="first_name">First Name</option>
													<option value="last_name">Last Name</option>
													<option value="email_address">Email Address</option>
												</select>
												</div>
											</div> 
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                    	<input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" onClick="changePaginate(0,'user_id','DESC');" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'user_id','DESC');" value="Refresh">
                                                    </div>
                                                </div>
                                                
											</div>
											<!--<div class="total_user_div" style="text-align:right"><span class="label label-primary">Total Login Users</span> <span class="label label-rounded label-dark"><?php //echo $usercount["0"]["cnt"];?></span></div>-->
											<div id="message"></div>
										</div>
										<div class="dt-panelmenu clearfix">
											 
                                            <div class="dataTables_filter pull-left" style="width:100%">
												<div class="row">
													<div class="col-md-12">
														<div class="col-xs-4 col-sm-2 top-serchbar2 pn dn_top_resources"><span class="label label-primary">Top 5 Resources Downloaded</span></div>
														<div class="col-xs-12 col-sm-6 top-serchbar2 dn_top_resources">
															<div class="col-md-6"><span><strong>PDF File</strong></span>&nbsp;<?php echo ($downloadpdf?$downloadpdf:'-');?></div>
															<div class="col-md-6"><span><strong>AI File</strong></span>&nbsp;<?php echo ($downloadai?$downloadai:'-');?></div>
														</div>
                                                    </div>
                                                    <div class="col-md-12">
														<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5 dn_top_resources">
															<span class="label label-primary">Top Pages Visited</span>
														</div>
														<div class="col-xs-12 col-sm-6 top-serchtitle1 pt5 dn_top_resources">	
														<div class="col-md-10">
															<?php echo ($topvisited?$topvisited:'-');?>
														</div>
														</div>
                                                    </div>
                                                </div>
											</div>
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	
	
  
</div> 	
<script>  	
 
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$( document ).ready(function() {
	var height = $( window ).height() - 210;
	$("#animation-switcher .table-responsive").css('height',height);
	
});
   
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getDownloadStatistics();
} 
 
$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	if(!CheckPasswordText($("#txt_search"),$("#searchInfo"))){
			alert("Spaces are not allowed");
			flag = 0;
	}
	if(flag){
		changePaginate(0,'name','DESC');
	}
}); 
function loadmore(){
	getDownloadStatistics();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	//getUserList();
	changePaginate(0,'user_id','DESC'); 	
}


getDownloadStatistics();
function getDownloadStatistics()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/download_statistics",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) { 
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
 

</script>
<?php $this->load->view("footer"); ?>
