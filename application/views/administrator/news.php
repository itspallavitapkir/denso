<?php
$CI =& get_instance();
$CI->load->model('administration_model');
 $this->load->view("header"); ?>

        <!-- Start: Content-Wrapper -->
        
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">News And Updates</li>
										<li class="crumb-trail">Manage News</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#newsModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteNews()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="news_heading">News Heading</option>
													<option value="news_category">News Category</option>
												</select>
												</div>
											</div>
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" onClick="changePaginate(0,'news_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'news_id','DESC');" value="Refresh">
											</div>-->
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
                                                    </div>
                                                </div>
											</div>
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,news_id,DESC"/>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
	<div id="newsModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">News Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6"> 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Category<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new "><label class="field select drop_new newscategroy mln">
					<select class="select-sm arrow_new" name="news_cat_id" id="news_cat_id">
						<?php
							if(is_array($newscategory)){
								echo '<option value="">Select Category</option>';
								foreach($newscategory as $cat):	?>
								<option value="<?php echo $cat['news_cat_id']; ?>"><?php echo $cat['news_category'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
					<span id="catInfo"  class="text-danger"></span>
				  </label></div>
				 </div>
				</div>
			 <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label">Sale Country <span class="validationerror">*</span></label>
				  <div class="col-lg-8 salecountry">
					<div class="dataTables_length1">
						<div class="multiple-selection fixed-height datamaltiple-select">
						<select name="dd_country[]" id="dd_country" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
							<?php
								if(is_array($country)){
									//echo '<option value="">Select</option>';
									foreach($country as $con):	?>
									<option value="<?php echo $con['country_id']; ?>"><?php echo $con['name'];?></option>
								 <?php endforeach;
								}
							?> 
						</select>
						
						
						</div>
						<span id="countryInfo"  class="text-danger"></span>
					</div>
				  </div>
				</div>
			  </div>
              </div>
			<div class="section row mbn">
			<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category</label>
				  
					  <div class="col-lg-8 text_inpu_new categorymain">
					 	<div class="pm Vehicle-pm"><div class="dataTables_length1" >
						<div class="multiple-selection mr5">
							
							<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):	
										
								?>
								<li><input type="checkbox" name="dd_resources[]" id="dd_resources" value="<?php echo $cat['id']?>" placeholder="" id="category_<?php echo $cat['id']?>" class="events-category"><?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?>
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" id="dd_subresources" value="<?php echo $subcat['id']?>" id="subcategory_<?php echo $subcat['id']?>" class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
									
								</li>
								 <?php endforeach;
								 ?>
								 </ul>
								 <?php
								    }
								 endforeach;
									}
								?>
							</ul>
							
						</div>
						<span id="resourcesInfo" class="text-danger"></span>
					</div></div>
					</div> 
				</div>
			  </div> 
              </div>
             <div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">News Heading<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_news_heading" name="txt_news_heading" class="form-control input-sm" placeholder="News Heading">
					<span id="headingInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div> 
			 </div> 
			 <div class="section row mbn">
			 <div class="col-sm-12">
				 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">News Description</label>
				  <div class="col-lg-8 text_inpu_new">
					<textarea id="txt_description" name="txt_description"></textarea>
				  </div>
				</div>
			  </div>
			  
			  <div class="section row mbn">
				  <div class="col-sm-6">
					<div class="form-group">
					  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select How to upload News file</label>
					  <div class="col-lg-8 text_inpu_new"> 
						<input type="radio" name="ftype" value="browse" class="seloption" checked>Browse News File
						<input type="radio" name="ftype" value="usetext" class="seloption">Use Text Box
						
					  </div>
					</div>
				  </div> 
			  </div>  
			  
			  <div class="section row mbn browse">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Browser and upload PDF/DOC file(Max upload limit 500 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="file" id="pdf_file" name="pdf_file" class="">
					<input type="hidden" id="pdf_file_hidden" />
					<span id="pdfInfo"  class="text-danger"></span>
					<div class="loading-progress"></div>
				  </div>
				</div>
			  </div> 
			</div>
			
			<div class="section row mbn textforfile">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Browser and upload PDF/DOC file(Max upload limit 500 mb)</label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="text" id="text_pdf_file" name="text_pdf_file" class="form-control input-sm" placeholder="Enter already uploaded file name">
				  </div>
				</div>
			  </div> 
			</div> 
			 
			  </div>   
			</div>  
			
			 
			  <!-- end section --> 
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="newsId" id="newsId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  </div>
<!-- end: .panel -->
  <div id="newsComment" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">News Comment</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formComment" name="formComment">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn"> 
			 <div class="col-sm-12">
				 
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Comments</label>
				  <div class="col-lg-8 text_inpu_new">
					
					<textarea id="comments" name="comments" cols="90" rows="5"></textarea>
					<span id="commentInfo" class="text-danger"></span> 
				  </div>
				</div>
			  </div>
			  </div>   
			</div>   
			  <!-- end section --> 
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="commentId" id="commentId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save_comment" id="btn_save_comment">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  </div>
  <a href="#newsComment" id="openMe" class="model-open"></a>
  
<script src="<?php echo base_url(); ?>js/bootstrap-tooltip.js"></script>
<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>  
<script src="<?php echo base_url();?>js/jquery.progresstimer.js" type="text/javascript"></script>
<script>
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});		
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});	
$(".multiple-selection, .arrow_new, .multiselect").click(function(e) {
	if(e.target.className =="multiselect dropdown-toggle btn btn-default" ||  e.target.className=="multiselect-selected-text"){
		$("#vehicle-categories").hide(); 
	}
});

$("#news_cat_id").click(function(e) {

	$("#vehicle-categories").hide(); 
	$(".multiselect dropdown-toggle btn btn-default").hide(); 
	
});

$(".textforfile").hide();
	$(document).on("click",".seloption",function() {
		var option = $('input[name=ftype]:radio:checked').val();
		if(option == "browse"){
			$(".textforfile").hide();
			$(".browse").show();
		}
		else if(option == "usetext"){
			$(".textforfile").show();
			$(".browse").hide();
		}
	});

function showmore(id){
	$("#report tr.thisshow").hide();
	$("#"+id).next("tr").toggle();
} 

$( document ).ready(function() {
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
	});
	 
	$('.events-category').click( function(){ 
		if(this.checked==true){ 
			$(this).next('.event-children').find(':checkbox').prop('checked', true); 
		}
		else{
			$(this).next('.event-children').find(':checkbox').prop('checked', false); 
		}
	});
	 
	$('.events-category').change( function(){
    var c = this.checked; 
    if( c ){
        $(this).next('.event-children').css('display', 'block'); 
        
    }else{
        $(this).next('.event-children').css('display', 'none');
    }
});


	$('#dd_country').multiselect({
		    includeSelectAllOption: true,
			buttonText: function(options, select) {
				return 'Select Country';
			},
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).text());
				});
				return labels.join(' - ');
			}
		});
	
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>Ã—</button>"+json.msg+"</div>");

$("#message div").fadeOut(5000);
}
function changeStatus(nid,cid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_comment_status",
			data: {"newsId":nid,"commentId":cid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#rowc_"+json.id).html(json.row);
					} 
				if(json.comcount!=""){ 
					//$("#count_"+json.nid).html('<strong>'+json.comcount+'</strong>');
				}
			}
			showMessagesStatus(json); 
		});
}
 
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'news_id','DESC');
		}
		setTimeout(function(){  $("#newsModel .mfp-close").trigger( "click" ); }, 3000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getNewsList();
} 
 
function loadeditor(){ 
	
	
    CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('txt_description', {
				height: 210,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
				},
			}); 
}

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'news_id','DESC');
	}
});

$(document).on("click","#edit",function() {
	 $(".thisshow").hide();
});
  
$(document).on("click","#add",function() { 
	$(".panel-title").html("Add News");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
	loadeditor();
});

$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');
	//alert(id);
	//return false;
	$("#newsId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json", 
				url: "<?php echo base_url(); ?>administration/getNewsById",
				data: {"id":id},
			}).success(function (json) {
				console.log(json);
				
				$("#news_cat_id").val(json.news_cat_id);
				$("#txt_news_heading").val(json.news_heading);
				$("#txt_description").val(json.news_description);
				$("#dd_country").val(json.country);
				
				if(json.country){
					//$("#dd_country option").attr('selected', 'selected');
					var valArr = json.country;
					var dataarray=valArr.split(","); 
					$("#dd_country").val(dataarray);
					$("#dd_country").multiselect("refresh"); 
				}
				if(json.v_cat_id){
					
					var valArr = json.v_cat_id;
					var dataarray = valArr.split(",");
					//$('#dd_resources').attr('chekced', 'chekced');
					//$("#dd_resources").val(dataarray);
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				}
				if(json.v_sub_cat_id){
					//$("input[name='dd_subresources[]']").attr("checked", false);
					var valSubArr = json.v_sub_cat_id;
					var datasubarray = valSubArr.split(",");
					//alert(datasubarray);
					//$('#dd_resources').attr('chekced', 'chekced');
					//$("#dd_resources").val(dataarray);
					for (var count = 0; count < datasubarray.length; count++) {
						
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				
				
				$("#add").trigger("click");
				$("#newsModel .panel-title").html("Edit News");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});


$(document).on("click","#editComment",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');
	
	//return false;
	$("#commentId").val(id);
	$(".panel-title").html("Edit News Comment");
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json", 
				url: "<?php echo base_url(); ?>administration/getNewsCommentById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json); 
				$("#comments").val(json.comments);
				$('#openMe').trigger('click');
			}); 
	} 
});
$("#pdf_file").change(function (){
	
   var pdfName = $('#pdf_file').val();
   $("#pdf_file_hidden").val(pdfName);
   
});

$(document).on("click","#newsModel .mfp-close",function() {
	//$("#userId").val("");
	document.getElementById("formUser").reset(); 
	
	/*var editor = CKEDITOR.instances['txt_description']; 
    if (editor) {  
        delete CKEDITOR.instances['txt_description'];   
    }*/
    $("#newsHeadingInfo").html("");
    $("#commentInfo").html("");
	location.reload();
});

$(document).on("click","#newsComment .mfp-close",function() {   
    
    $("#commentInfo").html("");
});


function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$("#dd_searchBy").val(''); 
	$(".multiselect-container li").removeClass("active");
	changePaginate(0,'news_id','DESC'); 
}

getNewsList();

function getNewsList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_news",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
				}
				else
				{
					$("#table").html(json.table);
				} 
				
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'news_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getNewsList();
			}
	});	
	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var newsId = $("#newsId"); 
		var nhName = $("#txt_news_heading");
			var nhInfo = $("#headingInfo");  	
		var newscatid = $("#news_cat_id");
			var catInfo = $("#catInfo");  	
		var dd_country = $("#dd_country");
			var countryInfo = $("#countryInfo");  		
		var pdf_file = $("#pdf_file");
			var pdfInfo = $("#pdfInfo"); 	
		var flag=1;
		
		if(!validateEmpty(nhName, nhInfo, "the news heading")){
			flag = 0;
		}  
		if(!checkCombo(newscatid, catInfo, "the news category")){
			flag = 0;
		}
		if(!checkCombo(dd_country, countryInfo, "the country")){
			flag = 0;
		}
		if($("#pdf_file_hidden").val() != ""){
			var fname = $("#pdf_file_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			
			if(lastVar != 'pdf' && lastVar != 'PDF' && lastVar != 'doc' && lastVar != 'DOC' && lastVar != 'docx' && lastVar != 'DOCX'){ 
				pdfInfo.html("Please upload PDF/DOC file only.<br/>"+fname_e+" is not a PDF/DOC file.");
				flag = 0;
			}
		}
		var formData = new FormData($('#formUser')[0]); 
		formData.append("txt_description", CKEDITOR.instances['txt_description'].getData());
		var progress = $(".loading-progress").progressTimer({
				timeLimit: 7200,
				onFinish: function () {
					//alert('completed!');
				}
		}); 
		if(flag)
		{
			$.ajax({
				type: "POST",
				dataType: "json",	
				contentType: false,
                cache: false,
                processData: false,			
				url: "<?php echo base_url(); ?>administration/save_news",
				data:  formData,
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
			}).error(function(){
				progress.progressTimer('error', {
					errorText:'ERROR!',
					onFinish:function(){
						//alert('There was an error processing your information!');
					}
				}); 
			}).done(function(){
				progress.progressTimer('complete'); 
			});
		} 
	});	
	
	$('#btn_save_comment').on('click', function (e) {
		e.preventDefault();
		var commentId = $("#commentId"); 
		var comment = $("#comments");
			var commentInfo = $("#commentInfo");  	
		var flag=1;
		
		if(!validateEmpty(comment, commentInfo, "the comment")){
			flag = 0;
		}
		
		
		
		var formData = new FormData($('#formComment')[0]);  
		if(flag)
		{
			$.ajax({
				type: "POST",
				dataType: "json",	
				contentType: false,
                cache: false,
                processData: false,			
				url: "<?php echo base_url(); ?>administration/save_news_comment",
				data:  formData,
			}).success(function (json) {
				if(json.action == "modify")
				{   
					$("#rowc_"+json.id).html(json.row);
				}  
				//showMessages(json);
				location.reload();
			});
		} 
	});	
	
});
function deleteNews()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure you want to continue?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_news",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				
				alert((json.ids).length+" news has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one news.');
	}
}
function deleteNewsComment(chkId)
{ 
	if(confirm("Are you sure you want to continue?") == true)
	{
	  $.ajax({
			type: "POST",
			dataType: "json",
			cache:false,
			url: "<?php echo base_url(); ?>administration/delete_news_cooment",
			data: {"ids":chkId},
		}).success(function (json) {
			alert(json.msg);
			location.reload();
		});
	}  
	 
}
</script>
<?php $this->load->view("footer"); ?>
