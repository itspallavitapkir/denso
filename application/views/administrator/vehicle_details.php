<?php
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Vehicle Parts</li>
										<li class="crumb-trail">Manage Vehicles Parts</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#vehicleModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteVehicle()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="vehicle_name">Vehicle Name</option>
													<option value="brand">Vehicle Brand</option>
													<option value="v_model">Vehicle Model</option> 
												</select>
												</div>
											</div>
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" onClick="changePaginate(0,'vehicle_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'vehicle_id','DESC');" value="Refresh">
											</div>-->
                                              <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" id="search_btn" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                  		<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,vehicle_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="vehicleModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Vehicle Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formVehicle" name="formVehicle" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Part Name<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_vehicle_name" name="txt_vehicle_name" class="form-control input-sm" placeholder="Vehicle Part Name">
					<span id="vInfo"  class="text-danger"></span>
				 </div>
				  
				</div>
			  </div>
			  
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Category </label>
				  
					  <div class="col-lg-12 text_inpu_new veccategorybk">
						<div class="pm"> <div class="dataTables_length1" style="width:100%;float:left;">
						<div class="multiple-selection1 mr5">
							
							<div id="select-category">Click to Select Category</div>
							
							<ul name="categories" id="vehicle-categories" class="set_category">
								<?php
									if(is_array($categories)){
										//echo '<option value="">Select</option>';
										foreach($categories as $cat):	
										
								?>
								<li><input type="checkbox" name="dd_resources[]" value="<?php echo $cat['id']?>" placeholder="" id="category_<?php echo $cat['id']?>" class="events-category"><?php echo $cat['category'];?>
									<?php 
									$cond = array("v_cat_id"=>$cat['id']);
									$res_subcat = $CI->administration_model->getAllVehicleSubCategory($cond);
									if(count($res_subcat)>0){
										?>
										<ul class="event-children">
										<?php
										foreach($res_subcat as $subcat):
									?>
									
										<li><input type="checkbox" name="dd_subresources[]" value="<?php echo $subcat['id']?>" id="subcategory_<?php echo $subcat['id']?>" class="child events-child-category"><?php echo $subcat['sub_category'];?></li>
									
								</li>
								 <?php endforeach;
								 ?>
								 </ul>
								 <?php
								    }
								 endforeach;
									}
								?>
							</ul>
							
						</div>
						<span id="resourcesInfo" class="text-danger"></span>
					</div></div>
					</div> 
				</div>
			  </div> 
			</div>
             
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Brands</label>
				  <label class="drop_new select col-lg-8 text_inpu_new mln vechilebrand-inner">
					<select class="select-sm arrow_new" name="brands" id="brands">
						<?php
							if(is_array($brands)){
								echo '<option value="0">Select Brand</option>';
								foreach($brands as $brand):	?>
								<option value="<?php echo $brand['v_brand_id']; ?>"><?php echo $brand['brand'];?></option>
							 <?php endforeach;
							}
						?>
					</select>
				  </label>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Model</label>
				  <label class="drop_new select col-lg-8 text_inpu_new mln vechilebrand-inner" id="vehi_model"> 
				  </label>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Car Maker Part Number</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_car_maker_pn" name="txt_car_maker_pn" class="form-control input-sm" placeholder="Car Maker Part Number">
					<span id="vcarmakerpnInfo"  class="text-danger"></span>
				  </div>
				  
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Denso Part Number </label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_denso_pn" name="txt_denso_pn" class="form-control input-sm" placeholder="Denso Part Number">
					<span id="vdensopnInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div> 
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">COOL GEAR Part Number</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_cool_gear_pn1" name="txt_cool_gear_pn1" class="form-control input-sm" placeholder="COOL GEAR Part Number">
					<span id="coolgearpn1Info"  class="text-danger"></span>
				  </div>
				</div>
			  </div> 
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Description</label>
				  <div class="col-lg-8 text_inpu_new">
					<textarea id="txt_color" name="txt_color" class="form-control input-sm" placeholder="Description"></textarea>
				  </div>
				</div>
			  </div> 
			</div> 
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Year (Example:2000 or 2000-2008)</label>
				 <!-- <label class="field select drop_new year">-->
					<label class="field col-lg-8 text_inpu_new">
					<input type="text" name="year" id="year" class="form-control input-sm" placeholder="Enter Year/Year Range" maxlength="9">
					<span id="yearInfo"  class="text-danger"></span>
				  </label>
				</div>
			  </div>
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Vehicle Part Image</label>
				  <label class="field select drop_new year"> 
					<input type="hidden" name="v_image_val" id="v_image_val"> 
					<input type="file" name="v_image" id="v_image">
					<span id="show_image"></span>
				  </label>
				</div>
			  </div>
			</div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Type</label>
				  <!--<label class="field select drop_new year">-->
                  <label class="col-lg-8 text_inpu_new">
					<input type="text" name="type" id="type" class="form-control input-sm" placeholder="Enter Type">
					
				  </label>
				</div>
			  </div>
			 
			</div> 
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Additional Info</label>
				 <div class="col-lg-8 text_inpu_new">
					<textarea  id="txt_vehicle_description" name="txt_vehicle_description"></textarea>
					<span id="vehicleInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div> 
			</div>  
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="vehicleId" id="vehicleId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	
 
    
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.numeric.js"></script>

<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 150px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 266px;
    z-index: 99999 !important;
}

</style>
<script>  
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});
$(".numeric").numeric({decimal:'.',negative:false});	
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
});  
function loadeditor(){ 
	 
    CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('txt_vehicle_description', {
				height: 210,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
				},
			}); 
}
$( document ).ready(function() {
	$("#vehicle-categories").hide();
	$('#select-category').click( function(){
		$("#vehicle-categories").toggle();
		
	});
	 
	$('.events-category').click( function(){ 
		$(this).next('.event-children').find(':checkbox').attr('checked', 'checked'); 
	});
	 
	$('.events-category').change( function(){
    var c = this.checked; 
    if( c ){
        $(this).next('.event-children').css('display', 'block'); 
        
    }else{
        $(this).next('.event-children').css('display', 'none');
    }
});

$('.events-child-category-all').change( function(){
    $(this).parent().siblings().find(':checkbox').attr('checked', this.checked);
});

	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'vehicle_id','DESC');
		}
		setTimeout(function(){  $("#vehicleModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getVehicleList();
} 

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	if(flag){
		changePaginate(0,'vehicle_id','DESC');
	}
});
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add New Vehicle"); 
	loadeditor();
	//$("#vehicleId").val("");
	//document.getElementById("formVehicle").reset();
});
$(document).on("change","#brands",function() {
	var id = $(this).val();
	
	$.ajax({
				type: "POST",
				dataType: "json", 
				url: "<?php echo base_url(); ?>administration/getModelById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#vehi_model").html(json);
			});
	
	
});
$("#brands").trigger("change");

$(document).on("click","#edit",function() {
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#vehicleId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>administration/getVehicleById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_vehicle_name").val(json.vehicle_name);
				$("#txt_number").val(json.p_no);
				$("#brands").val(json.v_brand_id);
				$("#brands").trigger("change"); 
				$("#dd_resources").val(json.v_cat_id); 
				if(json.v_cat_id){
					
					var valArr = json.v_cat_id;
					var dataarray = valArr.split(",");
					//$('#dd_resources').attr('chekced', 'chekced');
					//$("#dd_resources").val(dataarray);
					for (var count = 0; count < dataarray.length; count++) {
						$("input[name='dd_resources[]'][value='" + dataarray[count] + "']").attr("checked", true);
					} 
					$('.events-category').trigger('change');
				}
				if(json.v_sub_cat_id){
					//$("input[name='dd_subresources[]']").attr("checked", false);
					var valSubArr = json.v_sub_cat_id;
					var datasubarray = valSubArr.split(",");
					//alert(datasubarray);
					//$('#dd_resources').attr('chekced', 'chekced');
					//$("#dd_resources").val(dataarray);
					for (var count = 0; count < datasubarray.length; count++) {
						
						$("input[name='dd_subresources[]'][value='" + datasubarray[count] + "']").attr("checked", true);
					}  
					$('.events-category').trigger('change');
				}
				 
				setTimeout(function(){$("#vmodel").val(json.v_model_id);}, 1000);
				 
				$("#txt_car_maker_pn").val(json.car_maker_pn);
				$("#txt_denso_pn").val(json.denso_pn); 
				$("#txt_cool_gear_pn1").val(json.cool_gear_pn1); 
				$("#year").val(json.year);
				$("#txt_car_maker_pn").val(json.car_maker_pn);
				$("#txt_color").val(json.color);
				$("#type").val(json.type);
				$("#txt_vehicle_description").val(json.vehicle_description); 
				
			    $("#v_image_val").val(json.v_image);
			    
				if($("#v_image_val").val()!="" && json.v_image!=""){
					if(file_exists('<?php echo base_url();?>vehicle_parts/'+json.v_image)){
						$("#show_image").html('<a target="_blank" href="<?php echo base_url();?>vehicle_parts/'+json.v_image+'">View Image</a>');
					}
					else{
						$("#show_image").html('<a target="_blank" href="<?php echo base_url();?>images/no-image.jpg">View Image</a>');
					}
				}
				else{
					$("#show_image").html("");
				}
				
				$("#add").trigger("click");
				$("#vehicleModel .panel-title").html("Edit Vehicle");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
function file_exists (url) { 
  var req = this.window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
  if (!req) {
    throw new Error('XMLHttpRequest not supported');
  }

  // HEAD Results are usually shorter (faster) than GET
  req.open('HEAD', url, false);
  req.send(null);
  if (req.status == 200) {
    return true;
  }

  return false;
}

$(document).on("click","#vehicleModel .mfp-close",function() {
	$("#vehicleId").val("");
	$(".pic1 img:last-child").remove();
	document.getElementById("formVehicle").reset();
	location.reload(); 
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$("#dd_searchBy").val(''); 
	$(".multiselect-container li").removeClass("active");
	changePaginate(0,'vehicle_id','DESC'); 
}

getVehicleList();

 
function getVehicleList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	//$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_vehicle",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
					
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="5" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
					
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html("");
		});
}

function loadmore(){
	getVehicleList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}

$(document).ready(function(){ 
	
	$("#txt_search").val("");
	changePaginate(0,'vehicle_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getVehicleList();
			}
		});	
		*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var vehicleId = $("#vehicleId"); 
		var vName = $("#txt_vehicle_name");
			var vInfo = $("#vInfo");  	
		var resources = $("#dd_resources");
			var resourcesInfo = $("#resourcesInfo");  		
			
		var vcarmakerpnName = $("#txt_car_maker_pn");
			var vcarmakerpnInfo = $("#vcarmakerpnInfo"); 
		var year = $("#year");
			var yearInfo = $("#yearInfo"); 	
		var vdensopnName = $("#txt_denso_pn");
			var vdensopnInfo = $("#vdensopnInfo"); 
		var coolgearpn1Name = $("#txt_cool_gear_pn1");
			var coolgearpn1Info = $("#coolgearpn1Info"); 	 	
		var coolgearpn2Name = $("#txt_cool_gear_pn2");
			var coolgearpn2Info = $("#coolgearpn2Info"); 	 	
		var flag=1;
		if(!validateEmpty(vName, vInfo, "vehicle name")){
			flag = 0;
		} 
		
		/*if(!checkCombo(resources, resourcesInfo, "category")){
			flag = 0;
		} */
		/*if(year.val()!=""){ 
			
			if(!RegExp(/^([0-9]{4})|[0-9]{4}-|([0-9]{4}-[0-9]{4})$/).test(year.val())) {
				yearInfo.html("Invalid year range.");
				flag = 0;
			}else{
				yearInfo.html("");
			}
		}*/
		
		if(!validateEmpty(vName, vInfo, "vehicle part")){
			flag = 0;
		}
		
		/*if(!validateEmpty(coolgearpn1Name, coolgearpn1Info, "COOLGEAR part number 1")){
			flag = 0;
		}*/		
		
		if(vName.val()!=""){	 
			if(!CheckAlphanumeric(vName, vInfo)){
				flag = 0;
			} 	 
		}
		
		/*if(vcarmakerpnName.val()!=""){	 
			if(!CheckAlphanumeric(vcarmakerpnName, vcarmakerpnInfo)){
				flag = 0;
			} 	 
		}*/
		
		/*if(vdensopnName.val()!=""){	 
			if(!CheckAlphanumeric(vdensopnName, vdensopnInfo)){
				flag = 0;
			} 	 
		}*/
		
		/*if(coolgearpn1Name.val()!=""){	 
			if(!CheckAlphanumeric(coolgearpn1Name, coolgearpn1Info)){
				flag = 0;
			} 	 
		}*/
		
		/*if(coolgearpn2Name.val()!=""){	 
			if(!CheckAlphanumeric(coolgearpn2Name, coolgearpn2Info)){
				flag = 0;
			} 	 
		}*/
		var formData = new FormData($('#formVehicle')[0]); 
		formData.append("txt_vehicle_description", CKEDITOR.instances['txt_vehicle_description'].getData());
		
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_vehicles",
				//data: {"vehicleId": $("#vehicleId").val(),"txt_first_name":$("#txt_first_name").val(),"txt_last_name":$("#txt_last_name").val(),"email_address":$("#txt_email_address").val(),"contact_number":$("#txt_contact_number").val(),"user_type":$("#user_type").val(),"country":$("#country").val(),"state":$("#txt_state").val(),"city":$("#txt_city").val()},
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add'); 
				}
				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteVehicle()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected vehicles.") == true)
		{
		  $("#loading").show();
		  //$("#loading").html('Please wait while deleting data');	
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_vehicles",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				$("#loading").hide();
				alert((json.ids).length+" vehicle(s) has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one vehicle.');
	}
}


</script>
<?php $this->load->view("footer"); ?>
