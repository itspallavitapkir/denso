<?php $this->load->view("header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">General</li>
										<li class="crumb-trail">Manage Categories</li>
									</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#vehicleCatModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteVehicleCat()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="category">Vehicle Category</option> 
												</select>
												</div>
											</div>
											<!--<div class="dataTables_filter pull-left">
												<label>Search:<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onClick="changePaginate(0,'v_cat_id','DESC');" value="Search">
												<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable(0,'v_cat_id','DESC');" value="Refresh">
											</div>-->
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn">
                                                    	<input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar3">
                                                   		<input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search">
                                                    </div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3">
                                                    	<input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,v_cat_id,DESC"/>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="vehicleCatModel" class="popup-basic taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Vehicle Category Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formVehicleCat" name="formVehicleCat" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label">Vehicle Category<span class="validationerror">*</span></label>
				  <div class="col-lg-8">
					<input type="text" id="txt_vehicle_cat" name="txt_vehicle_cat" class="form-control input-sm" placeholder="Vehicle category">
					<span id="catInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div> 
			</div>
			<div class="section row mbn browse">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label">Upload Background Image(Max upload limit 5 mb) </label>
				  <div class="col-lg-8 text_inpu_new"> 
					<input type="file" id="pdf_file" name="pdf_file" class="">
					<div id="backg"></div>
					<input type="hidden" id="pdf_file_hidden" />
					<span id="pdfInfo"  class="text-danger"></span> 
					<div class="loading-progress"></div>
				  </div>
				</div>
			  </div> 
			</div>   
			  <!-- end section --> 
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="vehicleCatId" id="vehicleCatId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 
<script>
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'v_cat_id','DESC');
		}
		setTimeout(function(){  $("#vehicleCatModel .mfp-close").trigger( "click" ); }, 3000);
	}
}

function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getVehicleCatList();
} 
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add Vehicle Category");
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});
$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	if(flag){
		changePaginate(0,'v_cat_id','DESC');
	}
});
$(document).on("click","#edit",function() {
	
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option');
	//alert(id);
	//return false;
	$("#vehicleCatId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>administration/getVehicleCatById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_vehicle_cat").val(json.cat['category']);
				$("#backg").html(json.delete_back);
				$("#add").trigger("click");
				$("#vehicleCatModel .panel-title").html("Edit Vehicle category");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#vehicleCatModel .mfp-close",function() {
	$("#vehicleCatId").val("");
	$("#catInfo").html("");
	document.getElementById("formVehicleCat").reset();
	location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked"); 
	$("#dd_searchBy").val(''); 
	$(".multiselect-container li").removeClass("active");
	changePaginate(0,'v_cat_id','DESC'); 
}

getVehicleCatList();

function getVehicleCatList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	
	var search = $("#txt_search").val();
	$("#loading").show();
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/list_vehicle_category",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
				}
				else
				{
					$("#table").html(json.table);
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
		});
}
$("#pdf_file").change(function (){
	
   var pdfName = $('#pdf_file').val();
   $("#pdf_file_hidden").val(pdfName);
   
});

$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'v_cat_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getVehicleCatList();
			}
	});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var catId = $("#vehicleCatId"); 
		var vcat = $("#txt_vehicle_cat");
			var vcatInfo = $("#catInfo");  		
		var pdf_file = $("#pdf_file");
			var pdfInfo = $("#pdfInfo"); 
		var flag=1; 
		if(!validateEmpty(vcat, vcatInfo, "vehicle category")){
			flag = 0;
		} 
		
		if(vcat.val()!=""){
			if(!CheckAlphanumericWithSomeChars(vcat, vcatInfo)){
				flag = 0;
			} 
		}
		
		if($("#pdf_file_hidden").val() != ""){
			//$(".loading-progress").show();
			var fname = $("#pdf_file_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			
			if(lastVar != 'jpg' && lastVar != 'JPG' && lastVar != 'png' && lastVar != 'PNG'){ 
				pdfInfo.html("Please upload jpg/png file only.<br/>"+fname_e+" is not a jpg/png file.");
				flag = 0;
			}
		}
		
		var formData = new FormData($('#formVehicleCat')[0]);   
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false, 
				url: "<?php echo base_url(); ?>administration/save_vehicle_category",
				//data: {"vehicleCatId": $("#vehicleCatId").val(),"txt_vehicle_cat":$("#txt_vehicle_cat").val()},
				data:formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				$("#btn_save").removeAttr('disabled','disabled');
				showMessages(json);
			});
		} 
	});		
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}


function changeStatus(cid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>administration/change_category_status",
			data: {"catId":cid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
		});
}

function deleteVehicleCat()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("All sub categories under this vehicle category will also be deleted.Are you sure you want to continue?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_vehicle_categories",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getVehicleCatList();
				alert((json.ids).length+" categories has been deleted.");
				
			});
		}  
	}
	else {
	  alert('Please select at least one category.');
	}
}

function deleteVehicleCatackground(Id)
{
		
		if(confirm("Are you sure you want to continue?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_vehicle_category_background",
				data: {"ids":Id},
			}).success(function (json) {
				getVehicleCatList();
				alert("Background removed successfully.");
				$("#backg").html("");
			});
		}  
}

</script>
<?php $this->load->view("footer"); ?>
