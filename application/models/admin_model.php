<?php
class Admin_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getPaymentTypes($cond = array())
    {
		$this->db->select("idcompany_payment_types AS id,description,seq_number", FALSE);
		$this->db->from(TB_PAYMENT_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getPaymentTypesCount($cond = array())
    {
		$this->db->select("COUNT(idcompany_payment_types) AS cnt", FALSE);
		$this->db->from(TB_PAYMENT_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getPaymentTypesPerPage($cond = array(), $start = 0, $orderBy = array())
    {
		$this->db->select("idcompany_payment_types AS id,description,seq_number", FALSE);
		$this->db->from(TB_PAYMENT_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    function getTaxTypes($cond = array())
    {
		$this->db->select("idcompany_tax_type AS id,name,idcompany_tax_type", FALSE);
		$this->db->from(TB_TAX_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxTypesCount($cond = array())
    {
		$this->db->select("COUNT(idcompany_tax_type) AS cnt", FALSE);
		$this->db->from(TB_TAX_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxTypesPerPage($cond = array(), $start = 0, $orderBy = array())
    {
		$this->db->select("idcompany_tax_type AS id,name,idcompany_tax_type", FALSE);
		$this->db->from(TB_TAX_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxAuthorities($cond = array())
    {
		$this->db->select("idcompany_tax_authority AS id,taxauthority,idcompany_tax_authority", FALSE);
		$this->db->from(TB_TAX_AUTHORITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxAuthoritiesCount($cond = array())
    {
		$this->db->select("COUNT(idcompany_tax_authority) AS cnt", FALSE);
		$this->db->from(TB_TAX_AUTHORITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxAuthoritiesPerPage($cond = array(), $start = 0, $orderBy = array())
    {
		$this->db->select("idcompany_tax_authority AS id,taxauthority,idcompany_tax_authority", FALSE);
		$this->db->from(TB_TAX_AUTHORITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxDetails($cond = array())
    {
		$this->db->select("idcompany_taxes AS id,description,idcompany_taxes", FALSE);
		$this->db->from(TB_COMPANY_TAXES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxDetailsCount($cond = array())
    {
		$this->db->select("COUNT(idcompany_taxes) AS cnt", FALSE);
		$this->db->from(TB_COMPANY_TAXES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxDetailsPerPage($cond = array(), $start = 0, $orderBy = array())
    {
		$this->db->select("idcompany_taxes AS id,description,idcompany_taxes", FALSE);
		$this->db->from(TB_COMPANY_TAXES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
}
?>
