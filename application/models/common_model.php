<?php
class Common_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function insert($table,$data)
    {
        $this->db->insert($table,$data);
        //echo $this->db->last_query();die;
        return $this->db->insert_id();
    }
    
    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
       // echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function update_data($table,$where=array(),$data)
    {
       $res= $this->db->update($table,$data,$where);
        return $res;
    }

    
    
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        return $this->db->affected_rows();
    }
    
}
?>
