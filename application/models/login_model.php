<?php
class Login_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function validUser($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    function checkUserStatus($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();//die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    } 

    //function to fetch banner
    
    function fetchBannerForUser($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    
    function fetchPublicBannerForUser($table,$fields,$where=array()){
    	$this->db->select($fields, FALSE);
		$this->db->from($table);
		$this->db->join('dn_country', 'dn_country.country_id = dn_banner.country','LEFT');  
		
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
}
?>
