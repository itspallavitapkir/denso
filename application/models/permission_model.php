<?php
class Permission_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getUserPermission($company_id,$role,$moduleId)
    {
		$this->db->select("*", FALSE);
		$this->db->from(TB_USER_PERMISSION);
		$this->db->where("idmodule", $moduleId);
		//$this->db->where("parentmodulename", $parent_module);
		$this->db->where("idcompany", $company_id);
		$this->db->where("iduserrole", $role);
		//$this->db->join(TB_USER_PERMISSION, TB_MODULE.".idmodule = ".TB_USER_PERMISSION.".idmodule");
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getUserRoleName($role)
    {
		$this->db->select("role", FALSE);
		$this->db->from(TB_USER_ROLE);
		$this->db->where("iduserrole", $role);
		$query = $this->db->get();
		$arrResult = $query->result_array();
		return $arrResult[0]["role"];
	}
    
    function getAccessPermission($company_id,$role,$moduleId)
    {
		$this->db->select("*", FALSE);
		$this->db->from(TB_USER_PERMISSION);
		$this->db->where("idmodule", $moduleId);
		$this->db->where("idcompany", $company_id);
		$this->db->where("iduserrole", $role);
		$query = $this->db->get();
		return $query->result_array();
    }
    
}
?>
