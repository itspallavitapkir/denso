<?php
class Administration_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getPaymentTypes($cond = array())
    {
		$this->db->select("idcompany_payment_types AS id,description,seq_number", FALSE);
		$this->db->from(TB_PAYMENT_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getPaymentTypesCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(idcompany_payment_types) AS cnt", FALSE);
		$this->db->from(TB_PAYMENT_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getPaymentTypesPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("idcompany_payment_types AS id,description,seq_number", FALSE);
		$this->db->from(TB_PAYMENT_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getServiceRates($cond = array())
    {
		$this->db->select("idcompany_service_rates AS id,description,loadrate,rate,min_charge", FALSE);
		$this->db->from(TB_SERVICE_RATES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getServiceRatesCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(idcompany_service_rates) AS cnt", FALSE);
		$this->db->from(TB_SERVICE_RATES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getServiceRatesPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("idcompany_service_rates AS id,description,loadrate,rate,min_charge", FALSE);
		$this->db->from(TB_SERVICE_RATES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxAuthorities($cond = array())
    { 
		$this->db->select("idcompany_tax_authority AS id,taxauthority", FALSE);
		$this->db->from(TB_TAX_AUTHORITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getTaxAuthoritiesCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(idcompany_tax_authority) AS cnt", FALSE);
		$this->db->from(TB_TAX_AUTHORITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxAuthoritiesPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("idcompany_tax_authority AS id,taxauthority", FALSE);
		$this->db->from(TB_TAX_AUTHORITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxDetails($cond = array())
    { 
		$this->db->select("idcompany_taxes AS id,description,taxpercent,includedinprice,onprofiltonly,any_price,use_tiered_method,idcompany_tax_authority", FALSE);
		$this->db->from(TB_COMPANY_TAXES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getTaxDetailsCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(idcompany_taxes) AS cnt", FALSE);
		$this->db->from(TB_COMPANY_TAXES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxDetailsPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("idcompany_taxes AS id,description", FALSE);
		$this->db->from(TB_COMPANY_TAXES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxType($cond = array())
    { 
		$this->db->select("idcompany_tax_type AS id,name", FALSE);
		$this->db->from(TB_TAX_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getTaxTypeCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(idcompany_tax_type) AS cnt", FALSE);
		$this->db->from(TB_TAX_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getTaxTypePerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("idcompany_tax_type AS id,name", FALSE);
		$this->db->from(TB_TAX_TYPES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getUserRole($cond = array(),$like = array(),$orderBy = array())
    {
		$this->db->select("iduserrole AS id,role", FALSE);
		$this->db->from(TB_USER_ROLE);
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	/********** Modeuls *********/
	function getAllParentModules()
    {
		$this->db->select("parentmodulename AS parent", FALSE);
		$this->db->from(TB_MODULE);
		$this->db->order_by("parentmodulename", "ASC");
		$this->db->group_by("parentmodulename"); 
		$query = $this->db->get();
		return $query->result_array();
    }
	function getAllModules($cond = array())
    {
		$this->db->select("idmodule AS id,name", FALSE);
		$this->db->from(TB_MODULE);
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("name", "ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
    }
    /********** Modeuls *********/
     function getstaffmanagement($cond = array())
    {
		//$this->db->select("idcompany_staff AS id,firstname,lastname,address,city,state,pin,idcompany_user,is_active", FALSE);
		$this->db->select("".TB_COMPANY_USER.".idcompany_user AS id,".TB_COMPANY_USER.".username,".TB_COMPANY_USER.".password,".TB_COMPANY_USER.".secret_ques,".TB_COMPANY_USER.".secret_amswer,firstname,middlename,lastname,address,city,state,pin,ssn,employee_type,wage,commission,idcompany_location,idtax_type_applicable,tax_id,".TB_STAFF.".is_active,".TB_COMPANY_USER.".iduserrole,role", FALSE);
		$this->db->from(TB_COMPANY_USER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->join(TB_STAFF, TB_COMPANY_USER.".idcompany_user = ".TB_STAFF.".idcompany_user");
		$this->db->join(TB_USER_ROLE, TB_COMPANY_USER.".iduserrole = ".TB_USER_ROLE.".iduserrole");
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
    }
    
    function getstaffmanagementCount($cond = array(),$like = array())
    {	
		$this->db->select("COUNT(".TB_COMPANY_USER.".idcompany_user) AS cnt", FALSE);
		$this->db->from(TB_COMPANY_USER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$this->db->join(TB_STAFF, TB_COMPANY_USER.".idcompany_user = ".TB_STAFF.".idcompany_user");
		$this->db->join(TB_USER_ROLE, TB_COMPANY_USER.".iduserrole = ".TB_USER_ROLE.".iduserrole");
		$query = $this->db->get();
		return $query->result_array();
	}
    
    function getStaffManagementPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("".TB_COMPANY_USER.".idcompany_user AS id,firstname,lastname,address,city,state,pin,".TB_STAFF.".is_active,role", FALSE);
		$this->db->from(TB_COMPANY_USER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$this->db->join(TB_STAFF, TB_COMPANY_USER.".idcompany_user = ".TB_STAFF.".idcompany_user");
		$this->db->join(TB_USER_ROLE, TB_COMPANY_USER.".iduserrole = ".TB_USER_ROLE.".iduserrole");
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		return $query->result_array();
    }
    function getLocation($cond = array())
    { 
		$this->db->select("idcompany_location AS id,location_name", FALSE);
		$this->db->from(TB_COMPANY_LOCATION);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
	
	/************* Update Company Users *************/
	/*$this->db->select("".TB_COMPANY_USER.".idcompany_user AS id,firstname,middlename,
	 * lastname,address,city,state,pin,ssn,employee_type,wage,commission,idcompany_location,idtax_type_applicable,tax_id,
	 * ".TB_STAFF.".is_active,".TB_COMPANY_USER.".iduserrole,role", FALSE);
	function update($username,$pwd,$role,$question,$answer,$fname,$mname,$lname,$address,$city,$state,$pin,$ssn,$emp_type,
	$wage,$commission,$taxid,$location,$isactive,$companyuserid,$idcompany)
    {	
		$this->db->set('u.username', 'Pekka');  
		$this->db->set('u.password', 'Kuronen');  
		$this->db->set('u.iduserrole', 'Kuronen');  
		$this->db->set('u.secret_ques', 'Kuronen');  
		$this->db->set('u.secret_amswer', 'Kuronen');  
		$this->db->set('s.firstname', 'Kuronen');  
		$this->db->set('s.middlename', 'Kuronen');  
		$this->db->set('s.lastname', 'Suomi Oy');  
		$this->db->set('s.address', 'Mannerheimtie 123, Helsinki Suomi');  
		$this->db->set('s.city', 'Suomi Oy'); 
		$this->db->set('s.state', 'Suomi Oy'); 
		$this->db->set('s.pin', 'Suomi Oy'); 
		$this->db->set('s.ssn', 'Suomi Oy'); 
		$this->db->set('s.employee_type', 'Suomi Oy'); 
		$this->db->set('s.wage', 'Suomi Oy'); 
		$this->db->set('s.commission', 'Suomi Oy'); 
		$this->db->set('s.tax_id', 'Suomi Oy'); 
		$this->db->set('s.idcompany_location', 'Suomi Oy'); 
		$this->db->set('s.is_active', 'Suomi Oy');  
		$this->db->where('u.idcompany_user', 1);  
		$this->db->where('u.idcompany', $idcompany); 
		$this->db->where('u.idcompany_user = s.idcompany_user');  
		$this->db->update('TB_COMPANY_USER as u, TB_STAFF as s');
	return $this->db->affected_rows();
	$updateArr = $this->common_model->update(TB_COMPANY_USER as u, TB_STAFF as s,array("idcompany" => get_user_data("company_id"),
	"idcompany_user" => $this->encrypt->decode($postData["idcompany_user"])),
	array(u.".username" => $postData["userName"],u.".password" => $postData["userName"],u.".iduserrole" => $postData["userName"],
	u.".secret_ques" => $postData["userName"],u.".secret_amswer" => $postData["userName"],s.".firstname" => $postData["userName"],
	s.".middlename" => $postData["userName"],s.".lastname" => $postData["userName"],s.".address" => $postData["userName"],
	s.".city" => $postData["userName"],s.".state" => $postData["userName"],s.".pin" => $postData["userName"],
	s.".ssn" => $postData["userName"],s.".employee_type" => $postData["userName"],s.".wage" => $postData["userName"],
	s.".commission" => $postData["userName"],s.".tax_id" => $postData["userName"],s.".idcompany_location" => $postData["userName"],
	s.".is_active" => $postData["userName"]));
							
	}	
		*/
    
}
?>
