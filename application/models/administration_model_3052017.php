<?php
class Administration_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
     
    
    /*get contry list*/
    
    function getCountry()
    {
		$this->db->select("country_id ,name", FALSE);
		$this->db->from(TB_COUNTRY);
		$this->db->order_by("name","ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    /*function getCountryById($cond = array())
    {
		$this->db->select("country_id ,name", FALSE);
		$this->db->from(TB_COUNTRY);
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }*/
    
    function getNewCategory()
    {
		$this->db->select("news_cat_id ,news_category", FALSE);
		$this->db->from(TB_NEWS_CATEGORY);
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVehicleCategory()
    {
		$this->db->select("v_cat_id ,category", FALSE);
		$this->db->from(TB_CATEGORY);
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function checkVehicleBrandExists($cond)
    {
		$this->db->select("v_brand_id ,brand", FALSE);
		$this->db->from(TB_BRAND);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    }
    
    function checkVehicleModelExists($cond)
    {
		$this->db->select("v_brand_id ,v_model", FALSE);
		$this->db->from(TB_MODEL);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    }
    
    
    
     
    function getVehicleBrand()
    {
		$this->db->select("v_brand_id ,brand", FALSE);
		$this->db->from(TB_BRAND);
		$this->db->order_by("brand","asc");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVehicleModel()
    {
		$this->db->select("v_model_id ,v_brand_id,v_model", FALSE);
		$this->db->from(TB_MODEL);
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getUserRole($cond = array(),$like = array(),$orderBy = array())
    {
		$this->db->select("access_id AS id,role,permission_downloadpdf,permission_download_ai", FALSE);
		$this->db->from(TB_USER_ACCESS);
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$query = $this->db->get(); 
		
		return $query->result_array();
	}
    
    function getAllUsers($cond = array())
    {
		$this->db->select("user_id AS id,first_name,last_name,email_address,password,super_admin_access,ai_file_access,v_cat_id,v_sub_cat_id,pdf_file_access,country,company,dn_status,user_type", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    //get banner images names
   /* function get_banner_imgname($data)
    {
		$this->db->select("b_id AS id,user_type,country,banner_image,status,start_date,end_date,category_id,sub_category_id", FALSE);
		$this->db->from(TB_BANNER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
		//print_r($users[0]);die;
		
    }*/

    //function to get all banners
    function getAllBanners($cond = array())
    {
		$this->db->select("b_id AS id,start_date,end_date,user_type,status,banner_image,country", FALSE);
		$this->db->from(TB_BANNER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAllAdmin($cond = array())
    {
		$this->db->select("id,username", FALSE);
		$this->db->from(TB_ADMIN);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
     
    
    function getUsersCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(user_id) AS cnt", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    //function to get banners count
    function getBannersCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(b_id) AS cnt", FALSE);
		$this->db->from(TB_BANNER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    //function to get banners per page 
    function getBannersPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("b_id AS id,user_type,title,status,banner_image,country,start_date,end_date", FALSE);
		$this->db->from(TB_BANNER);

		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->like($k, $v,'after');
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		// echo $this->db->last_query();die;
		return $query->result_array();
    }

     
    
    function getUsersPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("user_id AS id,login_count,session_id,in_time,last_activity,user_type,first_name,last_name,email_address,super_admin_access,v_cat_id,v_sub_cat_id,ai_file_access,pdf_file_access,country,company,dn_status,contact_details,lang_translator", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->like($k, $v,'after');
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		/* echo $this->db->last_query();
		 die();*/
		return $query->result_array();
    }
    
    
    function getUsersLastLoginActivityInFormat($cond = array())
    {
		$date = date("Y-m-d m:s"); 
		$this->db->select("user_id AS id,CONCAT(
   FLOOR(HOUR(TIMEDIFF('".$date."',last_activity)) / 24), ' days, ',
   MOD(HOUR(TIMEDIFF( '".$date."',last_activity)), 24), ' hours, ',
   MINUTE(TIMEDIFF('".$date."',last_activity)), ' minutes, ',
   SECOND(TIMEDIFF('".$date."',last_activity)), ' seconds')
AS last_login", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		} 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getUserById($cond = array())
    {
		$this->db->select("user_id AS id,session_id,user_type,first_name,last_name,email_address,super_admin_access,v_cat_id,v_sub_cat_id,ai_file_access,pdf_file_access,country,company,contact_details,lang_translator,password", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    //function to edit banner
    function getBannerById($cond = array())
    {
		$this->db->select("b_id AS id,user_type,country,banner_image,status,start_date,end_date,category_id,sub_category_id", FALSE);
		$this->db->from(TB_BANNER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
   function getAllNewsCategory($cond = array())
    {
		$this->db->select("news_cat_id AS id,news_category", FALSE);
		$this->db->from(TB_NEWS_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    
    function getNewsCatCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(news_cat_id) AS cnt", FALSE);
		$this->db->from(TB_NEWS_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getNewsCatPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("news_cat_id AS id,news_category", FALSE);
		$this->db->from(TB_NEWS_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getNewsCatById($cond = array())
    {
		$this->db->select("news_cat_id AS id,news_category", FALSE);
		$this->db->from(TB_NEWS_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
	
	 
		
	function getAllNews($cond = array())
    {
		$this->db->select("news_id AS id,news_cat_id,news_heading,v_cat_id,v_sub_cat_id,news_description,country,date_modified,news_file,news_filename", FALSE);
		$this->db->from(TB_NEWS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllNewsForThilandUserCountry($cond=array(),$search_in,$search_other)
    {
		$this->db->select("news_id AS id,news_cat_id,news_heading,v_cat_id,v_sub_cat_id,news_description,country,date_modified,news_file,news_filename", FALSE);
		$this->db->from(TB_NEWS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		 
		if($search_in != ""){
			$this->db->where("FIND_IN_SET(".$search_in.",country) !=",0); 
		}
		if($search_other!=""){
			$this->db->where("FIND_IN_SET('18',country) =",0); 
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    } 
    
   function getAllNewsDistinct($join=array(),$search_in,$search_other)
    {
		$this->db->select("distinct(`dn_news`.news_cat_id),`dn_news`.news_id as id,`dn_news_category`.news_category,`dn_news`.country,`dn_news`.news_id,news_heading,v_cat_id,v_sub_cat_id,news_description,country,`dn_news`.date_modified", FALSE);
		$this->db->from(TB_NEWS);
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v);
		}
		
		if($search_in != ""){
			$this->db->where("FIND_IN_SET('".$search_in."',country) !=",0); 
		}
		
		if($search_other!=""){
			$this->db->where("FIND_IN_SET('".$search_other."',country) =",0); 
		}
		$this->db->order_by('`dn_news`.news_cat_id,`dn_news`.date_modified','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    } 
    
    function getNewsCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(news_id) AS cnt", FALSE);
		$this->db->from(TB_NEWS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getNewsPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join=array())
    {
		$this->db->select("news_id AS id,`dn_news`.news_cat_id,`dn_news_category`.news_category,news_heading,v_cat_id,v_sub_cat_id,news_description,country,news_filename,news_file", FALSE);
		$this->db->from(TB_NEWS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $query;die;
		return $query->result_array();
    }
    
    
    function getNewsComment($cond = array(), $start = 0,$join=array())
    {
		$this->db->select("comment_id AS id,`dn_users`.email_address,comments,status", FALSE);
		$this->db->from(TB_COMMENT);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		 
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		  
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		//echo $query;die;
		return $query->result_array();
    }
    
    
    
    
    function getNewsSingleComment($cond = array(), $join=array())
    {
		$this->db->select("comment_id AS id,`dn_users`.email_address,comments,status", FALSE);
		$this->db->from(TB_COMMENT);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}		
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		//echo $query;die;
		return $query->result_array();
    }
    
    
     function getNewsComments($cond = array(),$join=array())
    {
		$this->db->select("comment_id AS id,news_id,`dn_users`.email_address,`dn_users`.first_name,`dn_users`.last_name,`dn_news_comment`.date_added,comments,status", FALSE);
		$this->db->from(TB_COMMENT);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}		
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		//echo $query;die;
		return $query->result_array();
    }
    
    
    function getNewsById($cond = array())
    {
		$this->db->select("news_id AS id,`dn_news`.news_cat_id,,news_heading,v_cat_id,v_sub_cat_id,news_description,country,news_file,news_filename", FALSE);
		$this->db->from(TB_NEWS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		//echo $query;die;
		
		return $query->result_array();
    }	
    
    
    function getNewsCommentById($cond = array())
    {
		$this->db->select("comment_id AS id,comments,status", FALSE);
		$this->db->from(TB_COMMENT);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getAllResources($cond = array())
    {
		$this->db->select("resource_id AS id,product_name,product_description,countries,resource_categories,resource_subcategories,pdf_file,ai_file,pdf_name,ai_file_name", FALSE);
		$this->db->from(TB_RESOURCES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    function getAllResourcesForUsers($cond = array(),$cond_cat,$cond_sub_cat)
    {
		$this->db->select("resource_id AS id,product_name,product_description,countries,resource_categories,resource_subcategories,pdf_file,ai_file,pdf_name,ai_file_name", FALSE);
		$this->db->from(TB_RESOURCES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		
		if($cond_cat != ""){
		$explode_cat = explode(',',$cond_cat);
		$i=0;
		$cnt = count($explode_cat);

		$q ="";
		foreach($explode_cat as $cat):
			if($i<$cnt-1){
				//$this->db->where("FIND_IN_SET('$cat',resource_categories) !=0");

				$q .= "FIND_IN_SET('$cat',resource_categories) !=0 OR ";
			}
			$i++;
		endforeach;
		if($q != ""){ $q = substr($q,0,-3);}
		$this->db->where("(".$q.")");
	}
	else{
		//$this->db->where("FIND_IN_SET('',resource_categories) !=",0);
	}


	if($cond_sub_cat!=""){
		$explode_sub_cat = explode(',',$cond_sub_cat);
		$j=0;
		$cnt = count($explode_sub_cat);
		 $s="";
		foreach($explode_sub_cat as $sub_cat):
			if($j<$cnt-1){
				//$this->db->or_where("FIND_IN_SET('$sub_cat',resource_subcategories) !=",0);
				$s .= "FIND_IN_SET('$sub_cat',resource_subcategories) !=0 OR ";
			}
			$j++;

		endforeach;
		if($s != ""){ $s = substr($s,0,-3);}
		$this->db->or_where("(".$s.")");
	}
	else{
		//$this->db->where("FIND_IN_SET('',resource_subcategories) !=",0);
	}

	if($cond_cat == "" && $cond_sub_cat == ""){
		$this->db->where("FIND_IN_SET(' ',resource_categories) !=",0);
		$this->db->where("FIND_IN_SET(' ',resource_subcategories) !=",0);
	}
		 
		if($cond_cat == "" && $cond_sub_cat == ""){
			$this->db->where("FIND_IN_SET(' ',resource_categories) !=",0);
			$this->db->where("FIND_IN_SET(' ',resource_subcategories) !=",0);
		} 
		 
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
		
    } 
    
    function getResourcesCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(resource_id) AS cnt", FALSE);
		$this->db->from(TB_RESOURCES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getResourcesPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("resource_id AS id,product_name,product_description,countries,resource_categories,resource_subcategories,pdf_file,ai_file,pdf_name,ai_file_name", FALSE);
		$this->db->from(TB_RESOURCES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getResourceById($cond = array())
    {
		$this->db->select("resource_id AS id,product_name,product_description,countries,resource_categories,resource_subcategories,pdf_file,ai_file,pdf_name,ai_file_name", FALSE);
		$this->db->from(TB_RESOURCES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    
    /*vechicle category*/
    function getAllVehicleCategory($cond = array())
    {
		$this->db->select("v_cat_id AS id,category,background_img,status,status_pmk", FALSE);
		$this->db->from(TB_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function checkVehicleCategoryExists($cond)
    {
		$this->db->select("v_cat_id AS id,category,background_img,status,status_pmk", FALSE);
		$this->db->from(TB_CATEGORY);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    }
    
    function getVehicleCatCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(v_cat_id) AS cnt", FALSE);
		$this->db->from(TB_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getVehicleCatPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("v_cat_id AS id,category,background_img,status,status_pmk", FALSE);
		$this->db->from(TB_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVehicleCatById($cond = array())
    {
		$this->db->select("v_cat_id AS id,category,background_img,status", FALSE);
		$this->db->from(TB_CATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    /*
     * Vehicle sub categories
     * 
     * */ 
     
    function getAllVehicleSubCategory($cond = array())
    {
		$this->db->select("v_sub_cat_id AS id,v_cat_id,sub_category,background_img,status", FALSE);
		$this->db->from(TB_SUBCATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
//		echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function checkVehicleSubCategoryExists($cond)
    {
		$this->db->select("v_sub_cat_id AS id,v_cat_id,sub_category,background_img,status", FALSE);
		$this->db->from(TB_SUBCATEGORY);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    }
    
    function getVehicleSubCatCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(v_sub_cat_id) AS cnt", FALSE);
		$this->db->from(TB_SUBCATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getVehicleSubCatPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join = array())
    {
		$this->db->select("v_sub_cat_id AS id,dn_categories.category,dn_categories.v_cat_id,sub_category,".TB_SUBCATEGORY.".background_img,".TB_SUBCATEGORY.".status", FALSE);
		$this->db->from(TB_SUBCATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVehicleSubCatById($cond = array())
    {
		$this->db->select("v_sub_cat_id AS id,v_cat_id,sub_category,background_img", FALSE);
		$this->db->from(TB_SUBCATEGORY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    /* 
     * Vehicle brand
     * */
     
    function getAllVehicleBrand($cond = array())
    {
		$this->db->select("v_brand_id AS id,brand", FALSE);
		$this->db->from(TB_BRAND);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    function getAllVehicleBrandAuto($cond = array())
    {
		$this->db->select("b.v_brand_id,b.brand", FALSE);
		$this->db->from('dn_brand b');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    function getVehicleBrandCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(v_brand_id) AS cnt", FALSE);
		$this->db->from(TB_BRAND);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getVehicleBrandPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("v_brand_id AS id,brand", FALSE);
		$this->db->from(TB_BRAND);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getVehicleBrandById($cond = array())
    {
		$this->db->select("v_brand_id AS id,brand", FALSE);
		$this->db->from(TB_BRAND);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
     
    
     /* 
     * Vehicle model
     * */
     
    function getAllVehicleModel($cond = array())
    {
		
		$this->db->select("v_model_id AS id,v_brand_id,v_model", FALSE);
		$this->db->from(TB_MODEL);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllVehicleModelAuto($cond = array())
    {
		$this->db->select("m.v_model_id,m.v_model", FALSE);
		$this->db->from('dn_model m');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
	}
	
	function getAllVehicleYearAuto($cond = array())
    {
		$this->db->select("distinct year,vehicle_id", FALSE);
		$this->db->from('dn_vehicle v');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->group_by('year'); 
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
	}
	
    
    function getVehicleModelCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(v_model_id) AS cnt", FALSE);
		$this->db->from(TB_MODEL);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVehicleModelPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join = array())
    {
		$this->db->select("v_model_id AS id,brand,v_model", FALSE);
		$this->db->from(TB_MODEL);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getVehicleModelById($cond = array())
    {
		$this->db->select("v_model_id AS id,v_brand_id,v_model", FALSE);
		$this->db->from(TB_MODEL);
		$this->db->order_by("v_model","asc");
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
     
    /*
     * vehicle info
     * 
     * */ 
     
    function getAllVehicles($cond = array())
    {
		$this->db->select("vehicle_id AS id,vehicle_name,p_no,v_image,v_act_filename,vehicle_description,v_brand_id,v_model_id,year,color,type,v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllVehiclesDetails($cond = array(),$join=array())
    {
		$this->db->select("vehicle_id AS id,vehicle_name,p_no,v_image,v_act_filename,vehicle_description,`dn_brand`.brand,`dn_model`.v_model,`dn_vehicle`.v_brand_id,`dn_vehicle`.v_model_id,year,color,type,v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,"LEFT OUTER");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAllVehicleCarPartNumber($cond = array(),$join=array())
    {
		//$this->db->select("vehicle_id AS id,`dn_brand`.brand,`dn_model`.v_model,`dn_vehicle`.v_brand_id,`dn_vehicle`.v_model_id,year,color,type,v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		$this->db->select("distinct(v.car_maker_pn)", FALSE);
		$this->db->from('dn_vehicle v');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllVehicleYear()
    {
		//$this->db->select("vehicle_id AS id,`dn_brand`.brand,`dn_model`.v_model,`dn_vehicle`.v_brand_id,`dn_vehicle`.v_model_id,year,color,type,v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		$this->db->select("distinct(year) as year", FALSE);
		$this->db->from('dn_vehicle v');
		$this->db->order_by('year','asc'); 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getVehiclesCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(vehicle_id) AS cnt", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v, "LEFT");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVehiclePerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join = array())
    {
		$this->db->select("vehicle_id AS id,vehicle_name,p_no,v_image,v_act_filename,brand,vehicle_description,v_model,year,color,type,category,`dn_vehicle`.v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v, "LEFT");
		}
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getVechicleById($cond = array())
    {
		$this->db->select("vehicle_id AS id,vehicle_name,p_no,v_image,v_act_filename,vehicle_description,v_brand_id,v_model_id,year,color,type,v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function checkVehiclePartExists($cond)
    {
		$this->db->select("vehicle_id AS id,vehicle_name", FALSE);
		$this->db->from(TB_VEHICLE);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    }
    
    /*
     * countries
     * */
     
     
     function getAllContries($cond = array())
    {
		
		$this->db->select("country_id AS id,name", FALSE);
		$this->db->from(TB_COUNTRY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    } 
     
    function getCountryCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(country_id) AS cnt", FALSE);
		$this->db->from(TB_COUNTRY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getCountryPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("country_id AS id,name", FALSE);
		$this->db->from(TB_COUNTRY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getCountryById($cond = array())
    {
		$this->db->select("country_id AS id,name", FALSE);
		$this->db->from(TB_COUNTRY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("name","ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    
    function getDownloadByUserId($cond = array())
    {
		$this->db->select("id,user_id,download_pdf,download_ai,date_modified", FALSE);
		$this->db->from(TB_DOWNLOADS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("id","DESC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getViewedNewsByUserId($cond = array(),$join=array())
    {
		$this->db->select("v_news_id,user_id,`dn_news`.news_id,`dn_news`.news_heading,`dn_news_viewed`.date_modified", FALSE);
		$this->db->from(TB_VIEWED_NEWS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v);
		}
		$this->db->order_by("v_news_id","DESC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
     function getViewedSearchPartByUserId($cond = array(),$join=array())
    {
		$this->db->select("v_search_id,user_id,search_part_id,`dn_vehicle`.cool_gear_pn1,`dn_vehicle`.vehicle_name,`dn_viewed_search`.date_modified", FALSE);
		$this->db->from(TB_VIEWED_SEARCH);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v);
		}
		$this->db->order_by("v_search_id","DESC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
     function getLoggedInUser($cond = array(),$join=array())
    {
		$this->db->select("stat_id,`dn_users`.user_id,`dn_users`.email_address,`dn_user_statistics`.login_time,`dn_user_statistics`.logout_time,`dn_user_statistics`.date_modified", FALSE);
		$this->db->from(TB_USER_STAT);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v);
		}
		$this->db->order_by("stat_id","DESC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getTopVisitedResourcePDF()
    {
		$this->db->select("count( download_pdf ) AS cnt, download_pdf", FALSE);
		$this->db->from(TB_DOWNLOADS); 
		$this->db->where("download_pdf != ''");
		$this->db->group_by("download_pdf");
		$this->db->order_by("cnt","DESC");
		
		$this->db->limit(5);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getTopVisitedResourceAI()
    {
		$this->db->select("count( download_ai ) AS cnt, download_ai", FALSE);
		$this->db->from(TB_DOWNLOADS); 
		$this->db->where("download_ai != ''");
		$this->db->group_by("download_ai");
		$this->db->order_by("cnt","DESC");
		$this->db->limit(5);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    /*function getTopVisitedPage()
    {
		$this->db->select("count( v_search_id ) AS cnt, search_part_id", FALSE);
		$this->db->from(TB_VIEWED_SEARCH); 
		$this->db->group_by("search_part_id");
		$this->db->order_by("v_search_id","DESC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }*/
    
    
    
    function getAllVehicleBySearch($cond = array(), $cond_search_year = "",$cond_search_carMaker = array(), $join = array(), $cond_cat="",$cond_sub_cat="",$start=0)
    {
		$this->db->select("vehicle_id AS id,`dn_brand`.brand,`dn_model`.v_model,`dn_vehicle`.v_brand_id,`dn_vehicle`.v_model_id,year,color,type,v_cat_id,v_sub_cat_id,car_maker_pn,denso_pn,cool_gear_pn1,cool_gear_pn2", FALSE);
		//$this->db->select("v.vehicle_id,v.car_maker_pn", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		if($cond_search_year != ""){
			$this->db->where($cond_search_year);
		}
		if($cond_search_carMaker != ""){
			$this->db->like($cond_search_carMaker,'match', 'after');
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"LEFT OUTER");
		}
		if($cond_cat!=""){
			$explode_cat = explode(',',$cond_cat);
			$i=0;
			$cnt = count($explode_cat);
			foreach($explode_cat as $cat):
				if($i<$cnt-1){
					$this->db->where("FIND_IN_SET('$cat',v_cat_id) !=",0);
				}
				$i++;
			endforeach;
		}
		if($cond_sub_cat!=""){
			$explode_sub_cat = explode(',',$cond_sub_cat);
			$j=0;
			$cnt = count($explode_sub_cat);
			foreach($explode_sub_cat as $sub_cat):
				if($j<$cnt-1){
					$this->db->where("FIND_IN_SET('$sub_cat',v_sub_cat_id) !=",0);
				}
				$j++;
			endforeach;
		}
		$this->db->limit(PER_PAGE, $start);
	
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

	function getAllVehicleBySearchCount($cond = array(), $cond_search_year = "",$cond_search_carMaker = array(), $join = array(), $cond_cat="",$cond_sub_cat="")
    {
		$this->db->select("count(vehicle_id) as cnt", FALSE);
		//$this->db->select("v.vehicle_id,v.car_maker_pn", FALSE);
		$this->db->from(TB_VEHICLE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		if($cond_search_year != ""){
			$this->db->where($cond_search_year);
		}
		if($cond_search_carMaker != ""){
			$this->db->like($cond_search_carMaker,'match', 'after');
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"LEFT OUTER");
		}
		if($cond_cat!=""){
			$explode_cat = explode(',',$cond_cat);
			$i=0;
			$cnt = count($explode_cat);
			foreach($explode_cat as $cat):
				if($i<$cnt-1){
					$this->db->where("FIND_IN_SET('$cat',v_cat_id) !=",0);
				}
				$i++;
			endforeach;
		}
		if($cond_sub_cat!=""){
			$explode_sub_cat = explode(',',$cond_sub_cat);
			$j=0;
			$cnt = count($explode_sub_cat);
			foreach($explode_sub_cat as $sub_cat):
				if($j<$cnt-1){
					$this->db->where("FIND_IN_SET('$sub_cat',v_sub_cat_id) !=",0);
				}
				$j++;
			endforeach;
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function checkPageAlreadyVisited($cond = array()){
		$this->db->select("page_visit_id,user_id,page_name,ip_address", FALSE);
		$this->db->from(TB_VIEWED_PAGE);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	function checkViewedNewsAlreadyExists($cond = array()){
		$this->db->select("v_news_id,user_id,v_news_id", FALSE);
		$this->db->from(TB_VIEWED_NEWS);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	function checkSearchAlreadyExists($cond = array()){
		$this->db->select("v_search_id,user_id,search_part_id,ip_address", FALSE);
		$this->db->from(TB_VIEWED_SEARCH);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	function checkDownloadResourceAlreadyVisited($cond = array()){
		$this->db->select("id,user_id,download_pdf,download_ai,ip_address", FALSE);
		$this->db->from(TB_DOWNLOADS);
		
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	
	
	function getTopVisitedPage()
    {
		$this->db->select("count( page_name ) AS cnt, page_name", FALSE);
		$this->db->from(TB_VIEWED_PAGE); 
		$this->db->where("page_name != ''");
		$this->db->group_by("page_name");
		$this->db->order_by("cnt","DESC");
		$this->db->limit(5);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
	
	/*
	 * DENSO PHASE - II Starts - 07012016
	 * 
	 * */
	
	function getAllResourcesSK($cond = array())
    {
		$this->db->select("res_sk_id AS id,user_type,country,user_id,resources_sk_pdf,resources_sk_ai,date_modified", FALSE);
		$this->db->from(TB_RESOURCES_SK);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getAllResourcesSKCheck($resources_id,$user_id,$country_id)
    {
		$this->db->select("res_sk_id AS id,user_type,country,user_id,resources_sk_pdf,resources_sk_ai,date_modified", FALSE);
		$this->db->from(TB_RESOURCES_SK);
		
		if($resources_id != ""){
			
			$this->db->where("res_sk_id in ($resources_id)",NULL,FALSE);
			
		}
		if($resources_id != ""){ 
			$this->db->where("FIND_IN_SET(".$country_id.",country) !=",0); 
		}
		if($user_id != ""){
			$this->db->where("FIND_IN_SET(".$user_id.",user_id) != ",0); 
		}
		
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
     
	 
	function getResourcesSKCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(res_sk_id) AS cnt", FALSE);
		$this->db->from(TB_RESOURCES_SK);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getResourcesSKPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("res_sk_id AS id,user_type,country,user_id,resources_sk_pdf,resources_sk_ai,date_modified", FALSE);
		$this->db->from(TB_RESOURCES_SK);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
				
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getResourceSKById($cond = array())
    {
		$this->db->select("res_sk_id AS id,user_type,country,user_id,resources_sk_pdf,resources_sk_ai,date_modified", FALSE);
		$this->db->from(TB_RESOURCES_SK);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getUserByKeyword($like = array(),$cond=array(),$explode_user)
    {
		$this->db->select("user_id AS id,first_name,last_name", FALSE);
		$this->db->from(TB_USERS);
		foreach ($like AS $k => $v)
		{
			$this->db->like($k,$v);
		}
		if($cond != ""){
			$where = "";
			$i=0;
			$q="";
			$cnt = count($cond);
			foreach($cond as $v):
				
				if($i<$cnt-1){
					
					$q .="country = ".$v." OR ";
				}
			$i++;
			
			endforeach;	
			if($q != ""){ $q = substr($q,0,-3);}
			$this->db->where("(".$q.")");
		}
		
		if($explode_user != ""){
			$explode_user = explode(',',$explode_user);
			$i=0;
			$cnt = count($explode_user);
			$where = "";
			$i=0;
			$q="";
			$cnt = count($explode_user);
			foreach($explode_user as $v):
				if($v !=""){
					if($i<$cnt-1){
						
						$q .="user_id != ".$v." AND ";
					}
				}
			$i++;
			
			endforeach;	
			if($q != ""){ $q = substr($q,0,-4);}
			$this->db->where("(".$q.")");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    function getUserByUserToListId($cond,$country_expl=array())
    {
		$this->db->select("user_id AS id,first_name,last_name", FALSE);
		$this->db->from(TB_USERS);
		
		 
		if($cond != ""){
			$explode_user = explode(',',$cond);
			$i=0;
			$cnt = count($explode_user);
			$where = "";
			foreach($explode_user as $v):
				
				if($i<$cnt-1){
					
					$q .="user_id = ".$v." OR ";
				}
			$i++;
			
			endforeach;	
			if($q != ""){ $q = substr($q,0,-3);}
			$this->db->where("(".$q.")");
		}
		
		if($country_expl != ""){
			$con = substr($country_expl,0,-1);
			$this->db->where("country in (".$con.")");
		}
		 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
    
    function getUserByUserListId($cond,$country_expl = array())
    {
		$this->db->select("user_id AS id,first_name,last_name", FALSE);
		$this->db->from(TB_USERS);
		
		if($cond != ""){
			$explode_user = explode(',',$cond);
			$i=0;
			$cnt = count($explode_user);
			$where = "";
			foreach($explode_user as $v):
				
				if($i<$cnt-1){
					
					$q .="user_id = ".$v." OR ";
				}
			$i++;
			
			endforeach;	
			if($q != ""){ $q = substr($q,0,-3);}
			$this->db->where("(".$q.")");
		}
		if($country_expl!=""){
			$where = "";
			$i=0;
			$q="";
			$cnt = count($country_expl);
			foreach($country_expl as $v):
				
				if($i<$cnt-1){
					
					$q .="country = ".$v." OR ";
				}
			$i++;
			
			endforeach;	
			if($q != ""){ $q = substr($q,0,-3);}
			$this->db->where("(".$q.")");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    /*get langauages */
    
    function getLanguages($cond = array())
    {
		$this->db->select("lang_id AS id,language,lang_code", FALSE);
		$this->db->from(TB_LANG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
	
	
	/*
	 * Pages List
	 * */ 
	 
	function getAllPagesSK($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,section_name,category,sub_category,page_data,page_description,resources_id,public_access,offline_reading", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAllPagesSubCatSK($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,section_name,category,sub_category,page_data,page_description,resources_id,public_access,offline_reading", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->where("dn_pages.sub_category IS NULL");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
	 
	function getPagesSKCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(page_id) AS cnt", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getPagesSKPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join = array())
    {
		$this->db->select("page_id AS id,".TB_PAGES.".lang_id,".TB_LANG.".language,".TB_PAGES.".section_name,".TB_CATEGORY.".category,".TB_SUBCATEGORY.".sub_category,page_description,".TB_PAGES.".resources_id", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
				
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    /*
	 * Pages List - English version data
	 * */ 
	 
	function getAllPagesDataSK($cond = array())
    {
		$this->db->select("pd_id AS id,lang_id,section_name,category,sub_category,page_data,page_description,resources_id,public_access,offline_reading", FALSE);
		$this->db->from(TB_PAGES_DATA);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
     
	 
	function getPagesDataSKCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(pd_id) AS cnt", FALSE);
		$this->db->from(TB_PAGES_DATA);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getPagesDataSKPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join = array())
    {
		$this->db->select("pd_id AS id,section_name,".TB_CATEGORY.".category,".TB_SUBCATEGORY.".sub_category,page_data,resources_id,public_access,offline_reading", FALSE);
		$this->db->from(TB_PAGES_DATA);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
				
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllVehicleCategoryOrder($order = array())
    {
		$this->db->select("v_cat_id AS id,category,status", FALSE);
		$this->db->from(TB_CATEGORY);
		foreach ($order AS $k => $v)
		{
			$this->db->order_by($k,$v);
		}
		$this->db->where("status_pmk","Active");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllVehicleCategoryOrderLimit($order = array())
    {
		$this->db->select("v_cat_id AS id,category,status", FALSE);
		$this->db->from(TB_CATEGORY);
		foreach ($order AS $k => $v)
		{
			$this->db->order_by($k,$v);
		}
		$this->db->where("status","Active");
		$this->db->limit(1,0);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllPagesDataSKLimit($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,section_name,category,sub_category,page_data,page_description,resources_id,public_access", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->limit(1,0);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getPagesDataSKSections($cond = array(), $join = array())
    {
		$this->db->select("page_id AS id,".TB_PAGES.".lang_id,language,section_name,".TB_CATEGORY.".category,sub_category,page_data,page_description,resources_id,public_access", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->where("(dn_pages.sub_category=0 OR dn_pages.sub_category IS NULL)");
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getPagesDataSKSubcatSections($cond = array(), $join = array())
    {
		$this->db->select("page_id AS id,".TB_PAGES.".lang_id,language,section_name,".TB_CATEGORY.".category,".TB_SUBCATEGORY.".sub_category,page_data,page_description,resources_id,public_access", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getPagesSectionLangauages($cond = array(),$join=array())
    {
		$this->db->select("page_id AS id,".TB_PAGES.".lang_id,language", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->where("(dn_pages.sub_category=0 OR dn_pages.sub_category IS NULL)"); 
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getPagesSubCatSectionLangauages($cond = array(),$join=array())
    {
		$this->db->select("page_id AS id,".TB_PAGES.".lang_id,language", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function checkPagesSectionLangauages($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,section_name,category,sub_category", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getPagesDataSKSectionsResources($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,resources_id,public_access,section_name", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$this->db->where("(dn_pages.sub_category=0 OR dn_pages.sub_category IS NULL)"); 
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getPagesDataSKSectionsCatResources($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,resources_id,public_access,section_name", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$this->db->where("(dn_pages.sub_category=0 OR dn_pages.sub_category IS NULL)"); 
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getPagesDataSKSectionsSubCatResources($cond = array())
    {
		$this->db->select("page_id AS id,lang_id,resources_id,public_access,section_name", FALSE);
		$this->db->from(TB_PAGES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
}
?>
