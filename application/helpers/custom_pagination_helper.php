<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	function get_pagination_config($start = 0, $total = 0, $column, $order, $clickEvent = "getList") {
		$config['click'] = $clickEvent;
		$config['total_rows'] = $total;
		$config['per_page'] = PER_PAGE;
		$config['current_page'] = $start;
		$config['order_column'] = $column;
		$config['order'] = $order;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = FALSE;
		$config['prev_link'] = FALSE;
    
		//$config['first_link'] = 'First';
		//$config['first_tag_open'] = '<li>';
		//$config['first_tag_close'] = '</li>';
		//$config['last_link'] = 'Last';
		//$config['last_tag_open'] = '<li>';
		//$config['last_tag_close'] = '</li>';
		//$config['next_link'] = 'Next';
		//$config['next_tag_open'] = '<li>';
		//$config['next_tag_close'] = '</li>';
		//$config['prev_link'] = 'Previous';
		//$config['prev_tag_open'] = '<li>';
		//$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li CLASS="paginate_button">';
		$config['num_tag_close'] = '</li>';
		return $config;
    }    
?>
