<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_user_logged_in()
{
	$CI = & get_instance();
	if($CI->session->userdata("auth_user"))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function checkPermissionHeader(){
		$CI = & get_instance();
		$userData = $CI->session->userdata("auth_user");
		//print_r($userData);die;
		
		/*check whether user is having lang translator enabled*/
		
		$username = $userData['username'];
		$cond = array("email_address" => $username,"lang_translator" => '1');
		$lang_res = $CI->login_model->validUser(TB_USERS,"email_address,lang_translator",$cond);
		// If lang translator is 1 then show only SKW else show AMW and SKW.
		//echo "sdfs".count($lang_res) ;die;
		if(count($lang_res) == 0){
			return 1; // show AMW and SKW
		}
		else{
			return 0; // Show SKW ONLY
		}
		
	}

function is_densouser_logged_in()
{
	$CI = & get_instance();
	if($CI->session->userdata("auth_densouser"))
	{ 
		return true;
	}
	else
	{
		$userData[0] = $CI->session->userdata("auth_densouser");
		$userstat = $CI->session->userdata("stat_id");
		//print_r($userData[0]);die;
		$cond = array("user_id"=>$userData[0]["user_id"]); 
		$data_update = array("session_id"=>"");
		
		$CI->common_model->update(TB_USERS,$cond,$data_update);	
		
		$cond_stat = array("user_id" => $userData[0]["user_id"],"stat_id"=>$userstat); 
		 
		$data_update1 = array("logout_time" => date("Y-m-d H:i:s"),"date_modified" => date("Y-m-d H:i:s"));
		$CI->common_model->update(TB_USER_STAT,$cond_stat,$data_update1);
		
		return false;
	}
}


function get_user_data($var="")
{
	
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user"); 
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}


function get_densouser_data($var="")
{
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_densouser"))
		{
			$sessionArr = $CI->session->userdata("auth_densouser");
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}

function get_user_permission($moduleId)
{
	if(is_numeric($moduleId))
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user");
			$CI = get_instance();
			$CI->load->model("permission_model");
			$arrResult = $CI->permission_model->getUserPermission($sessionArr["company_id"],$sessionArr["role"],$moduleId);
			return $arrResult[0];
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
  

function is_ajax_request()
{
	$CI = & get_instance();
	if(!$CI->input->is_ajax_request()) {
	  exit('No direct script access allowed');
	}
	else
	{
		return true;
	}
	
}


if(!function_exists('pr')){
    function pr($array){
        echo "<pre>";
        print_r($array);
    }
}

?>
