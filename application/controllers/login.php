<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("administration_model");
		$this->load->library("form_validation");
	}
	
	
	public function index()
	{ 
		if(is_user_logged_in())
		{
			redirect("administration");
			exit;
		}
		else if($this->input->post())
		{
			$postData = $this->input->post();
			
			$this->session->set_flashdata("postData", $postData);
			if($postData["txt_username"] == "" && $postData["txt_password"] == "")
			{
				$this->session->set_flashdata("msg", 'Please enter Username & Password.');
				redirect("login"); exit;
			} 
			else if($postData["txt_username"] == "")
			{
				$this->session->set_flashdata("msg", 'Please enter Username.');
				redirect("login"); exit;
			}
			else if($postData["txt_password"] == "")
			{   
				$this->session->set_flashdata("msg", 'Please enter Password.');
				redirect("login"); exit;
			}
			
			$cond = array("username" => $postData["txt_username"],"password" => md5($postData["txt_password"]));
			
			$userData = $this->login_model->validUser(TB_ADMIN,"id,username,password",$cond);
			//echo $this->db->last_query();die;   
			//die;
			if(count($userData) > 0)
			{
				$this->session->set_userdata("auth_user", $userData[0]);
				
				redirect("administration");
				exit;
			}
			else
			{
				$this->session->set_flashdata("msg", 'The Username or Password do not match, Please try again.');
				redirect("adminlogin"); exit;
			}
		}
		else
		{
			$this->load->view("administrator/login");
		}
	}
	
	public function logout()
	{
		//$this->session->sess_destroy();
		$this->session->unset_userdata("auth_user");
		redirect("adminlogin"); exit;
	}
	
	public function logoutuser()
	{
		
		$userData[0] = $this->session->userdata("auth_densouser");
		$userstat = $this->session->userdata("stat_id");
		//print_r($userstat);
		$cond = array("user_id"=>$userData[0]["user_id"]); 
		$data_update = array("session_id"=>"");
		$this->common_model->update(TB_USERS,$cond,$data_update);	
		
		$cond_stat = array("user_id" => $userData[0]["user_id"],"stat_id"=>$userstat); 
		 
		$data_update1 = array("logout_time" => date("Y-m-d H:i:s"),"date_modified" => date("Y-m-d H:i:s"));
		$this->common_model->update(TB_USER_STAT,$cond_stat,$data_update1);
		
		$this->session->unset_userdata("auth_densouser");
		redirect("Index"); exit;
	}
	
	public function logoutuserOnSessionExpire()
	{
		
		$userData[0] = $this->session->userdata("auth_densouser");
		$userstat = $this->session->userdata("stat_id");
		//print_r($userstat);
		$cond = array("user_id"=>$userData[0]["user_id"]); 
		$data_update = array("session_id"=>"");
		$this->common_model->update(TB_USERS,$cond,$data_update);	
		
		$cond_stat = array("user_id" => $userData[0]["user_id"],"stat_id"=>$userstat); 
		 
		$data_update1 = array("logout_time" => date("Y-m-d H:i:s"),"date_modified" => date("Y-m-d H:i:s"));
		$this->common_model->update(TB_USER_STAT,$cond_stat,$data_update1);
		
		$this->session->unset_userdata("auth_densouser"); 
	}
	
	public function save_password()
	{   
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{
			$postData = $this->input->post();
			$userid = get_user_data("username");
			
			$cond = array('password' => md5($postData['currentPassword']),'username'=>$userid);
			$currentUser = $this->login_model->validUser(TB_ADMIN,'password',$cond);
			
			if($postData['currentPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter current password.</div>')); exit;
			}
			else if($postData['newPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter new password.</div>')); exit;
			}
			else if($postData['confirmPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter confirm password.</div>')); exit;
			}
			else if($postData['confirmPassword'] != $postData['newPassword'])
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>New password and Confirm password must be same.</div>')); exit;
			}
			else if(strlen($postData['newPassword']) < 5)
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Password must be more than 5 characters.</div>')); exit;
			}
			else if($currentUser[0]['password'] != md5($postData['currentPassword']))
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter correct current password.</div>')); exit;
			}
			else if($currentUser[0]['password'] == md5($postData['newPassword']))
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>New password and old password can\'t be same.</div>')); exit;
			}
			else
			{
				$cond = array("username" => $userid);
				$updateData = array("password" => md5($postData['newPassword']));
				$result = $this->common_model->update(TB_ADMIN,$cond,$updateData);
				if($result)
				{
					echo json_encode(array('status' => 'success','msg1' => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password has been updated successfully.</div>')); exit;
				}
				else
				{
					echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Error occured during updating Password.</div>')); exit;
				}
			}
		}
	}
	
}
?>
