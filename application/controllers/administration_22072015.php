<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("administration_model");
		$this->load->model("permission_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->profilepicpath =  realpath('profile_pic');
		$this->pdfpath =  realpath('pdf_files');
		$this->aipath =  realpath('ai_files');
		$this->vehiclepartspath =  realpath('vehicle_parts');
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	/************ Start Payment Types **************/
	public function index()
	{
		
		if(!is_user_logged_in())
		{
			redirect('logout'); exit;
		}
		else
		{ 
			
			$data["country"] = $this->administration_model->getCountry();
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
			$this->load->view("administrator/users",$data);
			//$this->load->view("content");
		}
	}
	
	
	
	/********* User Access ***********/
	public function UserAccess()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$userAccess = $this->administration_model->getUserRole(); 
			$data['userAccess'] = "";
			$data['userAccess'] .='<div class="panel-body"><div class="userrleheader-tle userrleheader-inner table-bordered"> 
									<div class="userrleheader-tle table-bordered" style="overflow:hidden;">
										<div class="pull-left roletitle roletitle-first">User Types</div>
										<div class="pull-left roletitle">Donwload PDf</div>    
										<div class="pull-left roletitle">Download AI Files</div>                                                                                                                                                 
									</div>
									';
				if($userAccess!=""){
				$n=1;	
				foreach($userAccess as $access):
				$checked_pdf = ($access['permission_downloadpdf'] == '1') ?	'checked' : '';
				$checked_ai = ($access['permission_download_ai'] == '1') ?	'checked' : '';
				$data['userAccess'] .='<div class="userrleheader-tle userrleheader-inner table-bordered">
										<div class="pull-left roletitle roletitle-first">'.$access['role'].'</div>
											<div class="pull-left roletitle">
												<div class="checkbox-custom mbn mln form-group">
													<input  '.$checked_pdf.' type="checkbox" checkaccess id="pdf_'.$n.'" name="pdf_'.$n.'">
													<label for="pdf_'.$n.'" class="pln">&nbsp;</label>
												</div>
											</div>
											<div class="pull-left roletitle">
												<div class="checkbox-custom mbn mln form-group">
													<input '.$checked_ai.' type="checkbox" class="checkaccess" id="ai_'.$n.'" name="ai_'.$n.'">
													<label for="ai_'.$n.'" class="pln">&nbsp;</label>
												</div>
											</div>  
										</div>';												
									
			    $n++;
			    endforeach;
			}
			else{
				$data['userAccess'] .='No User Types found in system !! Please check database';
			}
			$this->load->view("administrator/user_access",$data);
		}
	}
	
	public function SaveUserAccessSettings()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{   
				$postData = $this->input->post();  
				$data=""; 
				
				//if($postData){ 
					//foreach($postData as $key=>$val): 
					
						$pdf_1  = (isset($postData['pdf_1']) && $postData['pdf_1'] =="on") ? '1' :0;
						$ai_1   = (isset($postData['ai_1']) && $postData['ai_1']  =="on") ? '1' :0;
						$pdf_2  = (isset($postData['pdf_2']) && $postData['pdf_2'] =="on") ? '1' :0;
						$ai_2   = (isset($postData['ai_2']) && $postData['ai_2']  =="on") ? '1' :0;
						$pdf_3  = (isset($postData['pdf_3']) && $postData['pdf_3'] =="on") ? '1' :0;
						$ai_3   = (isset($postData['ai_3']) && $postData['ai_3']  =="on") ? '1' :0;
						$data_update=array();
						$cond=array(); 
						
						
						if($pdf_1 == 1 || $pdf_1 == 0 || $ai_1 ==1 || $ai_1 == 0){
								$pdf_array = ($pdf_1)?array("permission_downloadpdf" => '1'):array("permission_downloadpdf" => '0');
								$ai_array = ($ai_1)?array("permission_download_ai" => '1'):array("permission_download_ai" => '0');
								$data_update1 =array_merge($pdf_array,$ai_array); 
								$cond1 = array("access_id" => "1");
								$this->common_model->update(TB_USER_ACCESS,$cond1,$data_update1);	
								//echo $this->db->last_query();
								
						}
						if($pdf_2 == 1 || $pdf_2 == 0 || $ai_2 ==1 || $ai_2 == 0){
								$pdf_array = ($pdf_2)?array("permission_downloadpdf" => '1'):array("permission_downloadpdf" => '0');
								$ai_array = ($ai_2)?array("permission_download_ai" => '1'):array("permission_download_ai" => '0');
								$data_update2 =array_merge($pdf_array,$ai_array); 
								$cond2 = array("access_id" => "2");
								$this->common_model->update(TB_USER_ACCESS,$cond2,$data_update2);	
								//echo $this->db->last_query();
						}
						if($pdf_3 == 1 || $pdf_3 == 0 || $ai_3 ==1 || $ai_3 == 0){
								$pdf_array = ($pdf_3)?array("permission_downloadpdf" => '1'):array("permission_downloadpdf" => '0');
								$ai_array = ($ai_3)?array("permission_download_ai" => '1'):array("permission_download_ai" => '0');
								$data_update3 = array_merge($pdf_array,$ai_array); 
								$cond3 = array("access_id" => "3");
								$this->common_model->update(TB_USER_ACCESS,$cond3,$data_update3);	
								//echo $this->db->last_query();
						} 
						
					//endforeach;
				//}
				
				$userAccess = $this->administration_model->getUserRole(); 
				
				$data['userAccess'] = "";
				$data['success'] = "";
				$data['userAccess'] .='<div class="panel-body"><div class="userrleheader-tle userrleheader-inner table-bordered"> 
										<div class="userrleheader-tle table-bordered" style="overflow:hidden;">
											<div class="pull-left roletitle roletitle-first">User Types</div>
											<div class="pull-left roletitle">Donwload PDf</div>    
											<div class="pull-left roletitle">Download AI Files</div>                                                                                                                                                 
										</div>
										';
					if($userAccess!=""){
					$n=1;	
					foreach($userAccess as $access):
					$checked_pdf = ($access['permission_downloadpdf'] == '1') ?	'checked' : '';
					$checked_ai = ($access['permission_download_ai'] == '1') ?	'checked' : '';
					$data['userAccess'] .='<div class="userrleheader-tle userrleheader-inner table-bordered">
											<div class="pull-left roletitle roletitle-first">'.$access['role'].'</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input  '.$checked_pdf.' type="checkbox" id="pdf_'.$n.'"  name="pdf_'.$n.'">
														<label for="pdf_'.$n.'" class="pln">&nbsp;</label>
													</div>
												</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input '.$checked_ai.' type="checkbox" id="ai_'.$n.'"  name="ai_'.$n.'">
														<label for="ai_'.$n.'" class="pln">&nbsp;</label>
													</div>
												</div>  
											</div>';												
										
					$n++;
					endforeach;
					//$data['success']  ="<div class='p5 mbn alert-dismissable pln success'>Settings updated successfully</div>";	
				}
				else{
					$data['userAccess'] .='No User Types found in system !! Please check database';
				} 
				echo json_encode(array("userAccess" => $data['userAccess'], "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Settings updated successfully.</div>'));
			}
		}
	}
	/********* User Access ***********/
	private function genarateRandomPassword($length=10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$randomstring ='';
		for($i=0;$i<=$length;$i++){
			$randomstring .= $characters[rand(0,strlen($characters) -1)];
		}
		return $randomstring;
	}
	
	public function getUsers()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data["country"] = $this->administration_model->getCountry();
			$cond = array();
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
			
			$this->load->view("administrator/users",$data);
		}
	}
	
	
	public function getUserById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("user_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getUserById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);
				 
		}
	}
	
	public function save_users() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["userId"] == "")
				{   
					$isValid = $this->validateUser($postData);
					//print_r($isValid);die;
					if($isValid["status"] == 1)
					{ 
						$cond = array("email_address"=>$postData["txt_email_address"]);
						$users = $this->administration_model->getAllUsers($cond);
						$data["users"] = $users;
						//$randpwd = trim($this->genarateRandomPassword(10)); 
						$randpwd = $postData["txt_password"];  
						$hostname = $this->config->item('hostname');
						if(count($users) == 0)
						{
							if(isset($postData["super_admin_access"]) && ($postData["super_admin_access"] =="on" || $postData["super_admin_access"] =="1"))
								$super_admin_access = "1";
							else
								$super_admin_access = "0";
							if(isset($postData["pdf_file_access"]) && ($postData["pdf_file_access"] =="on" || $postData["pdf_file_access"] =="1"))
								$pdf_file_access = "1";
							else
								$pdf_file_access = 0;
							if(isset($postData["ai_file_access"]) && ($postData["ai_file_access"] =="on" || $postData["ai_file_access"] =="1"))
								$ai_file_access = "1";
							else
								$ai_file_access = "0";
							
							
							$data_resource = "";
							$data_subresource="";
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .= $data2.',';
								endforeach;
							}
							
							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .= $data2.',';
								endforeach;
							}				
							
							$insertId = $this->common_model->insert(TB_USERS,array("first_name" => $postData["txt_first_name"], "last_name" => $postData["txt_last_name"],"email_address" => $postData["txt_email_address"],"password" => md5($randpwd),"pdf_file_access" => $pdf_file_access,"v_cat_id"=>$data_resource,"v_sub_cat_id"=>$data_subresource,"company" => $postData["txt_company"], "contact_details" => $postData["txt_contact_number"],"password" => md5($postData["txt_password"]),"country" => $postData["country"],"ai_file_access" => $ai_file_access,"super_admin_access"=> $super_admin_access,"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{  
								/*
								 * insert user in as admin in admin tbl if having super admin access
								 * */
								 
								 if($super_admin_access == 1){
									 $insertId = $this->common_model->insert(TB_ADMIN,array("username" => $postData["txt_email_address"],"password" => md5($randpwd),"date_modified" => date('Y-m-d H:i:s')));
									  
								 }
								 	
								/*
								 * Send email of registration
								 * 
								 * */
								$super_admin_text = "";
								
								if($super_admin_access=="1"){
									$super_admin_text .= "You are added as admin on Denso<br/>Your login credetials are the same as above<br/>Click the link below to login to your admin account.<br/><br/>
									  <a href='".$hostname."adminlogin'>".$hostname."adminlogin</a><br/><br/>";
								}
								
								
								$username = $postData["txt_first_name"]." ".$postData["txt_last_name"];
								$emailaddress = $postData["txt_email_address"];
								$config['mailtype'] ='html';
								$config['charset'] ='iso-8859-1';
								$this->email->initialize($config);
								$this->messageBody  = "Hello $username<br/>
									 We have added you on denso as user<br/>
									 Your login credentials are below:<br/> 
									 Email Address : $emailaddress<br/>
									 Password : $randpwd<br/>
									 Click the link below to login to your user account.<br/><br/>
									 <a href='".$hostname."'>".$hostname."</a>
									 <br/><br/>
									 $super_admin_text
									 <br/>If link is not working properly then copy and paste the link in browser	 
									 <br/><br/>
									 									 
									 Regards,<br/>Desno";
								 //echo $this->messageBody;
								 //die;
								$this->email->from(EMAIL_FROM, 'Denso');
								$this->email->to("$emailaddress");

								$this->email->subject('Denso - Login credentials');
								$this->email->message($this->messageBody);	
								$this->email->send();	 
								  
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Users has been added successfully and mail sent with the login credentials</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This user has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{ 
					$isValid = $this->validateUser($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("user_id !=" => $this->encrypt->decode($postData["userId"]), "email_address"=>$postData["txt_email_address"]);
						$cond1 = array("user_id" => $this->encrypt->decode($postData["userId"]));
						$users = $this->administration_model->getAllUsers($cond);
						$userbyid = $this->administration_model->getUserById($cond1); 
						$data["users"] = $users; 
						$randpwd = $postData["txt_password"]; 
						$txt_heading="";
						if(count($users) == 0)
						{  
							
							if(isset($postData["super_admin_access"]) && ($postData["super_admin_access"] =="on" || $postData["super_admin_access"] ==1))
								$super_admin_access = "1";
							else
								$super_admin_access = "0";
							if(isset($postData["pdf_file_access"]) && ($postData["pdf_file_access"] =="on" || $postData["pdf_file_access"] ==1))
								$pdf_file_access = "1";
							else
								$pdf_file_access = "0";
							if(isset($postData["ai_file_access"]) && ($postData["ai_file_access"] =="on" || $postData["ai_file_access"] ==1))
								$ai_file_access = "1";
							else
								$ai_file_access = "0";
							
							$data_resource ="";
							$data_subresource="";
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .= $data2.',';
								endforeach;
							}
							
							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .= $data2.',';
								endforeach;
							}	
								
							$updateArr = $this->common_model->update(TB_USERS,$cond1,array("first_name" => $postData["txt_first_name"], "last_name" => $postData["txt_last_name"],"email_address" => $postData["txt_email_address"],"pdf_file_access" => $pdf_file_access,"company" => $postData["txt_company"],"v_cat_id"=>$data_resource,"v_sub_cat_id"=>$data_subresource, "contact_details" => $postData["txt_contact_number"],"country" => $postData["country"],"ai_file_access" => $ai_file_access,"super_admin_access"=> $super_admin_access,"date_modified" => date('Y-m-d H:i:s')));
							
							$updatepassword=0; 
							if(isset($postData["txt_password"]) && $postData["txt_password"]!="") {
								
								$updatepassword = $this->common_model->update(TB_USERS,$cond1,array("password" => md5($randpwd)));
								//die;
							}
							 
														 
							if($updateArr)
							{
								
								 if($super_admin_access == 0){
									 $this->common_model->delete(TB_ADMIN,array("username" => $postData["txt_email_address"]));
								 }
								 elseif($super_admin_access == 1){
										   
											$cond = array("username"=>$postData["txt_email_address"]);
											$admin = $this->administration_model->getAllAdmin($cond); 
											
											
											
											if(count($admin)==1){									 
											
												
												if(isset($postData["txt_password"]) && $postData["txt_password"]!="") {
													$updatepassword = $this->common_model->update(TB_ADMIN,$cond,array("password" => md5($randpwd))); 
												}
												if($updatepassword){
													$txt_heading = "We have changed your password<br/>";
												}
												else{
													$txt_heading = "We have added you on Denso - Admin<br/>";
												}  
											}
											else if(count($admin)==0){
														$cond = array("email_address"=>$postData["txt_email_address"]);
														$getpassword = $this->administration_model->getAllUsers($cond);
														//print_r($getpassword);
														$insertId = $this->common_model->insert(TB_ADMIN,array("username" => $postData["txt_email_address"],"password" => $getpassword["0"]["password"],"date_modified" => date('Y-m-d H:i:s'))); 
														$txt_heading = "We have added you on Denso - Admin<br/>";
														$randpwd ="[Password is same as user]";
											}
													
											$email_address  = $postData["txt_email_address"]; 	  
								 }
								/*
								 * Send email of registration
								 * 
								 * */
								if($updatepassword || $insertId){
									$hostname = $this->config->item('hostname');
												$username = $postData["txt_first_name"]." ".$postData["txt_last_name"];
												$emailaddress = $postData["txt_email_address"];
												$config['mailtype'] ='html';
												$config['charset'] ='iso-8859-1';
												$this->email->initialize($config);
												$this->messageBody  = "Hello,<br/>
													 $txt_heading
													 Your login credentials are below:<br/> 
													 Email Address : $emailaddress<br/>
													 Password : $randpwd<br/>
													 Click the link below to login to your account.<br/><br/>
													 <a href='".$hostname."'>".$hostname."</a>
													 <br/><br/>If link is not working properly then copy and paste the link in browser	 
													 <br/><br/>Regards,<br/>Desno";
												 //echo $this->messageBody;
												 //die;
												$this->email->from(EMAIL_FROM, 'Denso');
												$this->email->to("$emailaddress");

												$this->email->subject('Denso - Login credentials');
												$this->email->message($this->messageBody);	
												$this->email->send();
								}
								
								$cond2 = array("user_id" => $this->encrypt->decode($postData["userId"]));
								$users = $this->administration_model->getAllUsers($cond2); 
								 
								if($users["0"]["dn_status"] == "Active"){
										$status_text = "Make Inactive";
										$status = "Inactive";
									}
									else{
										$status_text = "Make Active";
										$status = "Active";
									}
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
										<td class="text-center">'.$postData["txt_first_name"].'</td>
										<td>'.$postData["txt_last_name"].'</td>
										<td>'.$postData["txt_email_address"].'</td>
										<td>'.$postData["txt_company"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
											<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($postData["userId"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["userId"]).'" href="javascript:void(0)">'.$status_text.'</a>
										</td>';
										//echo $this->encrypt->decode($postData["userId"]);die;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Users has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Users has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This email address has been already taken.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_users() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getUsersCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("first_name","last_name","email_address","company");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "user_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$users = $this->administration_model->getUsersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'first_name\',\''.$corder[0].'\')">First Name</th>
								<th class="'.$css[1].'" onclick="changePaginate(0,\'last_name\',\''.$corder[1].'\')">Last Name</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'email_address\',\''.$corder[2].'\')">Email Address</th>
								<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'company\',\''.$corder[3].'\')">Company</th>
								<th class="text-center">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($users)>0)
				{
						if(count($users) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($users as $user) {  
							
							if($user["dn_status"] == "Active"){
								$status_text = "Make Inactive";
								$status = "Inactive";
							}
							else{
								$status_text = "Make Active";
								$status = "Active";
							}
							
					$table .= '<tr id="row_'.$user["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($user["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$user["first_name"].'</td>
							<td>'.$user["last_name"].'</td>
							<td>'.$user["email_address"].'</td>
							<td>'.($user["company"]?$user["company"]:"N/A").'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($user["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
								<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($user["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($user["id"]).'" href="javascript:void(0)">'.$status_text.'</a>
							</td>
						  </tr>
						  <tr class="collapse" id="collapseTable"><td colspan="6">This is content</td>
						  </tr>
						  ';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($users)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	
	public function change_user_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["userId"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "Active"){
						$status_update = "Active";
					}
					else if($postData["status"] == "Inactive"){
						$status_update = "Inactive";
					}
					
					$cond = array("user_id" => $this->encrypt->decode($postData["userId"]));
					$users = $this->administration_model->getAllUsers($cond);
					
					if(count($users) != 0)
					{ 
						$updateArr = $this->common_model->update(TB_USERS,$cond,array("dn_status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "Active"){
								$status_text = "Make Inactive";
								$status = "Inactive";
							}
							else{
								$status_text = "Make Active";
								$status = "Active";
							}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
									<td class="text-center">'.$users["0"]["first_name"].'</td>
									<td>'.$users["0"]["last_name"].'</td>
									<td>'.$users["0"]["email_address"].'</td>
									<td>'.$users["0"]["company"].'</td>
									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
										<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["userId"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["userId"]).'" href="javascript:void(0)">'.$status_text.'</a>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => "User Status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "User Status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	
	public function validateUser($postData)
	{   
		/*if(!ctype_alpha($postData["txt_first_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>First name require character only.</div>');
		}
		else if(!ctype_alpha($postData["txt_last_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Last name require character only.</div>');
		}
		else if(!ctype_alnum($postData["txt_company"]) && isset($postData["txt_company"]) && $postData["txt_company"]!="")
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Company require character only.</div>');
		}*/
		
		if (!preg_match('/^\+(?:[0-9] ?){6,14}[0-9]$/', $postData['txt_contact_number']) && $postData['txt_contact_number']!="") {
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Invalid phone number, the number should start with a plus sign, followed by the country code and national number</div>');
		}
		return array("status" => 1);
	}
	
	
	public function delete_users() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("user_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_USERS,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	public function getNewsCategory()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data = "";
			$this->load->view("administrator/news_cat",$data);
		}
	}
	
	
	public function getNewsCatById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("news_cat_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getNewsCatById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);  
		}
	}
	
	public function save_news_category()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["newsCatId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("news_category" => $postData["txt_news_cat"]);
						$newscat = $this->administration_model->getAllNewsCategory($cond);
						$data["newscat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_NEWS_CATEGORY,array("news_category" => $postData["txt_news_cat"],"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News Category has been added successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("news_cat_id !=" => $this->encrypt->decode($postData["newsCatId"]), "news_category" => $postData["txt_news_cat"]);
						$newscat = $this->administration_model->getAllNewsCategory($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["newscat"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_NEWS_CATEGORY,array("news_cat_id" => $this->encrypt->decode($postData["newsCatId"])),array("news_category" => $postData["txt_news_cat"],"date_modified" => date('Y-m-d H:i:s')));
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["newsCatId"].'"></td>
										<td class="text-center">'.$postData["txt_news_cat"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["newsCatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["newsCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News category has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This category has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_news_category() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getNewsCatCount($cond,$like);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("news_category");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "news_cat_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$newscat = $this->administration_model->getNewsCatPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'news_category\',\''.$corder[0].'\')">News category</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($newscat)>0)
				{
						if(count($newscat) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($newscat as $cat) { 
							$arr = array();
							$arr[0] = $cat["news_category"]; 
							//$arr[3] = $types["id"];
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["news_category"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($newscat)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getNewsCatList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateNewsCategory($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_news_categories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("news_cat_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_NEWS_CATEGORY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	public function getNews()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data["country"] = $this->administration_model->getCountry();
			$data["newscategory"] = $this->administration_model->getNewCategory();
			$cond = array();
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
			$this->load->view("administrator/news",$data);
		}
	}
	
	
	public function getNewsById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("news_id" => $this->encrypt->decode($postData["id"]));
			$news = $this->administration_model->getNewsById($cond);
			//print_r($users);die;
			echo json_encode($news[0]);
				 
		}
	}
	
	public function getNewsCommentById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("comment_id" => $this->encrypt->decode($postData["id"]));
			$comments = $this->administration_model->getNewsCommentById($cond);
			//print_r($users);die;
			echo json_encode($comments[0]);
				 
		}
	}
	
	public function save_news() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//print_r($postData);die; 	
				if($postData["newsId"] == "")
				{ 
					
					$data_country = "";
							
					if(!empty($postData["dd_country"])){
						foreach($postData["dd_country"] as $data2):
								$data_country .= $data2.',';
						endforeach;
					}
					
					$data_resource = "";
					$data_subresource="";
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .= $data2.',';
								endforeach;
							}
							
							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .= $data2.',';
								endforeach;
							}				
					$isValid = $this->validateNews($postData);
					if($isValid["status"] == 1)
					{
						 
							$insertId = $this->common_model->insert(TB_NEWS,array("news_cat_id" => $postData["news_cat_id"], "news_heading" => $postData["txt_news_heading"],"news_description" => $postData["txt_description"],"country" => $data_country,"v_cat_id" => $data_resource,"v_sub_cat_id" => $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News has been added successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						 
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{ 
					$isValid = $this->validateNews($postData);
					if($isValid["status"] == 1)
					{
						/*$cond = array("news_id" => $this->encrypt->decode($postData["newsId"]));
						$users = $this->administration_model->getAllNews($cond);
						$data["users"] = $users;
						
						if(count($users) == 0)
						{*/ 
						
						
						$data_country = "";
							
						if(!empty($postData["dd_country"])){
							foreach($postData["dd_country"] as $data2):
									$data_country .= $data2.',';
							endforeach;
						}
						
						$data_resource = "";
						$data_subresource="";
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .= $data2.',';
								endforeach;
							}
							
							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .= $data2.',';
								endforeach;
							}
						
							$updateArr = $this->common_model->update(TB_NEWS,array("news_id" => $this->encrypt->decode($postData["newsId"])),array("news_cat_id" => $postData["news_cat_id"],"news_heading" => $postData["txt_news_heading"], "news_description" => $postData["txt_description"],"country" => $data_country,"v_cat_id" => $data_resource,"v_sub_cat_id" => $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
							if($updateArr)
							{ 
								$cond_newcat = array("news_cat_id" => $postData["news_cat_id"]);
								$new_cat = $this->administration_model->getNewsCatById($cond_newcat); 
								//print_r($country);
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["newsId"].'"></td>
										<td class="text-center">'.$new_cat["0"]["news_category"].'</td>
										<td>'.$postData["txt_news_heading"].'</td>  
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["newsId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["newsId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News has been updated successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}
						/*}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This News has been exists.</div>')); exit;
						}*/
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	
	public function save_news_comment() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//print_r($postData);die; 	
				if($postData["commentId"] != "")
				{   
					$updateArr = $this->common_model->update(TB_COMMENT,array("comment_id" => $this->encrypt->decode($postData["commentId"])),array("comments" => $postData["comments"]));
					if($updateArr)
					{ 
						$cond_comments = array();
						$comment = $this->administration_model->getNewsCommentById($cond_comments); 
						 		
						echo json_encode(array("status" => 1,"action" => "modify","id"=>$this->encrypt->decode($postData["commentId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News comment has been updated successfully.</div>')); exit;
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
					}  
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	
	public function list_news() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				 
				$cond = array();
				$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id");
				//$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id",TB_COUNTRY => TB_NEWS.".country = ".TB_COUNTRY.".country_id");
				
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getNewsCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("news_category","news_heading");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "news_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$news_details = $this->administration_model->getNewsPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%" id="report">';
						$table .= '<thead>
									  <tr>
										<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
										<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'news_category\',\''.$corder[0].'\')">News Category</th>
										<th class="'.$css[1].'" width="45%" onclick="changePaginate(0,\'news_heading\',\''.$corder[1].'\')">News Heading</th>
										<th class="text-center" width="15%">Action</th>
									  </tr>
									</thead>
									<tbody>';
				}
				if(count($news_details)>0)
				{
						if(count($news_details) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($news_details as $news) { 
							$join_comment = array(TB_USERS => TB_COMMENT.".user_id = ".TB_USERS.".user_id");
							$cond_comment = array("news_id" => $news['id']);
							$comment_details = $this->administration_model->getNewsComment($cond_comment,$postData["start"],$join_comment);
							$cond_comment_status = array("news_id" => $news['id'],"status"=>"Unpublished");
							$comment_to_be_aprroved = $this->administration_model->getNewsComment($cond_comment_status,$postData["start"],$join_comment);
							$com_id = $news["id"];
							$text_to_approve="";
							if(count($comment_to_be_aprroved)>0){
								$comment_count  = count($comment_to_be_aprroved);
								$text_to_approve =" (<span id='count_$com_id'><strong>$comment_count</strong></span> comment(s) to be approved)";
							}
							 
							//$arr[3] = $types["id"];
							$table .= '<tr id="row_'.$news["id"].'" onclick="showmore(this.id);" >
									<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($news["id"]).'" name="check" id="check" class="chk"></td>
									<td class="text-center">'.$news["news_category"].'</td>
									<td>'.$news["news_heading"].$text_to_approve.'</td> 
									<td class="text-center" row_'.$news["id"].'">
										<a title="Edit" alt="Edit" id="edit" class="editNews" data-option="'.$this->encrypt->encode($news["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									</td>
								  </tr>';
								 if(count($comment_details)!=0){
									 
									 $table .= '<tr class="thisshow"><td style="padding:0" colspan="4"><table class="table table-striped table-hover dataTable no-footer"><tr>
													<th width="10%" class="text-center"></th>
													<th width="20%" class="text-center" width="20%">Email Address</th>
													<th width="20%">Comments</th>
													<th class="text-center" width="15%"></th>
												  </tr>';
									 
									 
									 foreach($comment_details as $comment): 
									   
										if($comment['status']=="Published"){
											$text_status = "Make Unpublish";
											$status="Unpublished";
										}
										else{
											$text_status = "Make Publish";
											$status="Published";
										}
									  
									  
									  $table .='<tr id="rowc_'.$comment["id"].'">
											<td ></td>
											<td class="text-center">'.$comment['email_address'].'</td> 
											<td>'.$comment['comments'].'
											</td>
											<td class="text-center">
											
											<a title="Change Status" alt="Change Status" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($news["id"]).'\',\''.$this->encrypt->encode($comment["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($comment["id"]).'" href="javascript:void(0)">'.$text_status.'</a>
											|
											<a class="model-open" title="Edit Comment" alt="Edit Comment" id="editComment" data-option="'.$this->encrypt->encode($comment["id"]).'" href="#newsComment"><i class="fa fa-edit" data-toggle="modal"></i></a>
											|
											<a title="Delete Comment" alt="Delete" id="deleteComment" class="deleteNewsComment" onclick="deleteNewsComment(\''.$this->encrypt->encode($comment["id"]).'\')" data-option="'.$this->encrypt->encode($comment["id"]).'" href="javascript:void(0)">Delete</a>
											</td>
										</tr>';
									 endforeach;
									 $table .='</table></td></tr>';
								 }
								 else{
									  $table .='<tr class="thisshow">
											<td colspan="4" align="center">No Comments Yet</td>
										</tr>';
								 } 
						}
				}
				if($postData["start"] == 0)
				{
						if(count($news_details)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="5">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getNewsList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateNews($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_first_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}
		else if(!ctype_alpha($postData["txt_last_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		
		return array("status" => 1);
	}
	
	
	public function change_comment_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["commentId"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "Published"){
						$status_update = "Published";
					}
					else if($postData["status"] == "Unpublished"){
						$status_update = "Unpublished";
					}
					
					$join_comment = array(TB_USERS => TB_COMMENT.".user_id = ".TB_USERS.".user_id");
					
					$cond = array("comment_id" => $this->encrypt->decode($postData["commentId"]));
					$comments = $this->administration_model->getNewsSingleComment($cond,$join_comment );
					
					if(count($comments) != 0)
					{ 
						$updateArr = $this->common_model->update(TB_COMMENT,$cond,array("status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "Published"){
								$text_status = "Make Unpublish";
								$status = "Unpublished";
							}
							else{
								$text_status = "Make Publish";
								$status = "Published";
							}
							 
							$cond_comment_status = array("news_id" => $this->encrypt->decode($postData["newsId"]),"status"=>"Unpublished");
							$comment_to_be_aprroved = $this->administration_model->getNewsComment($cond_comment_status,0,$join_comment);
							$comment_count='';
							if(count($comment_to_be_aprroved)>0){
								$comment_count  = count($comment_to_be_aprroved);
							}
							
							$row =' <td width="10%" class="text-center"></td>
											<td width="20%"  class="text-center">'.$comments["0"]['email_address'].'</td>
											<td width="20%" >'.$comments["0"]['comments'].'</td>
											<td width="15%"  class="text-center"> 
												<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($postData["newsId"]).'\',\''.$this->encrypt->encode($comments["0"]["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($comments["0"]["id"]).'" href="javascript:void(0)">'.$text_status.'</a>
												|
												<a class="model-open" title="Edit Comment" alt="Edit Comment" id="editComment" data-option="'.$this->encrypt->encode($comments["0"]["id"]).'" href="#newsComment"><i class="fa fa-edit" data-toggle="modal"></i></a>
												|
												<a title="Delete Comment" alt="Delete" id="deleteComment" class="deleteNewsComment" onclick="deleteNewsComment(\''.$this->encrypt->encode($comments["0"]["id"]).'\')" data-option="'.$this->encrypt->encode($comments["0"]["id"]).'" href="javascript:void(0)">Delete</a>
												
											</td>
										';
							
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"comcount"=>$comment_count,"nid"=>$this->encrypt->decode($postData["newsId"]),"id"=>$this->encrypt->decode($postData["commentId"]), "msg" => "News comment status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "News comment status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	public function delete_news_cooment() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 
				$cond = array("comment_id" => $this->encrypt->decode($postData["ids"]));
				$this->common_model->delete(TB_COMMENT,$cond);
					  
				echo json_encode(array("status" => 1,"msg" => "News comment deleted successfully")); exit;
				
			}
		}
	}
	
	public function delete_news() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("news_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_NEWS,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	public function getResources()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			
			$data["country"] = $this->administration_model->getCountry();
			$data["categories"] = $this->administration_model->getAllVehicleCategory();
			$this->load->view("administrator/resources",$data);
		}
	}
	
	
	public function getResourceById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("resource_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getResourceById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);
				 
		}
	}
	
	public function save_resources() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["resourceId"] == "")
				{   
					$isValid = $this->validateResource($postData,$_FILES);
					if($isValid["status"] == 1)
					{ 
						$cond = array();
						$resources = $this->administration_model->getAllResources($cond);
						$data["resources"] = $resources;
						
						$data_country = "";
						//print_r($postData["dd_country"]);
						
						if(!empty($postData["dd_country"])){ 
							foreach($postData["dd_country"] as $data1):
									$data_country .= $data1.',';
							endforeach;
						}
						
						$data_resource = "";
						$data_subresource = "";
						//print_r($postData["dd_resources"]);die;
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						
						$insertId = $this->common_model->insert(TB_RESOURCES,array("product_name" => $postData["txt_product_name"], "product_description" => $postData["txt_product_description"],"countries" => $data_country,"resource_categories" =>  $data_resource,"resource_subcategories" =>  $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
						if($insertId)
						{ 
							$ai_update  = array();
							$ai_update = array();
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$act_pdf_name = $_FILES['pdf_file']['name'];
								$pdf_update = array("pdf_file" => $pdf_file,"pdf_name"=>$act_pdf_name);
								
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$pdf_file.'">'.$act_pdf_name.'</a>';		
								$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $insertId),$pdf_update);
							}
							else{
								//$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$resourcebyid["0"]["pdf_file"].'">'.$resourcebyid["0"]["pdf_file"].'</a>';		
							}
							if(isset($_FILES['ai_file']['name']) && $_FILES['ai_file']['name'] != ""){ 
								
								//@unlink($this->aipath.'/'.$resourcebyid["0"]["ai_file"]);
								//$this->validatefile->do_upload('ai_file'); 
								$ai_file = $isValid["0"]["ai_file"]; 
								$act_ai_name = $_FILES['ai_file']['name']; 
								$ai_update = array("ai_file" => $ai_file,"ai_file_name" => $act_ai_name);
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$ai_file.'">'.$act_ai_name.'</a>';
								$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $insertId),$ai_update);
							}			
							else{ 
								//$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$resourcebyid["0"]["ai_file"].'">'.$resourcebyid["0"]["ai_file"].'</a>';
							}
							
							echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Resources has been added successfully...!!!</div>')); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
						} 
					}
					else
					{
						echo json_encode($isValid);
					}  
				}
				else
				{ 
					$isValid = $this->validateResource($postData,$_FILES);
					//print_r($isValid);
					if($isValid["status"] == 1)
					{
						 
						$cond1 = array("resource_id" => $this->encrypt->decode($postData["resourceId"])); 
						$resourcebyid = $this->administration_model->getResourceById($cond1); 
						$data["resources"] = $resourcebyid;  
						if($_FILES){
							$this->validatefile->data(); 
							$pdf_update = array();
							$ai_update = array();
							$ai_update_act_file = array();
							$data_country = "";
							$data_subresource="";
							//print_r($postData["dd_country"]);
							
							if(!empty($postData["dd_country"])){ 
								foreach($postData["dd_country"] as $data1):
										$data_country .= $data1.',';
								endforeach;
							}
							
							$data_resource = "";
							
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .= $data2.',';
								endforeach;
							}
							
							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .= $data2.',';
								endforeach;
							}
							$txt_update = array("product_name" => $postData["txt_product_name"], "product_description" => $postData["txt_product_description"],"countries" => $data_country,"resource_categories" =>  $data_resource,"resource_subcategories" =>  $data_subresource,"date_modified" => date('Y-m-d H:i:s'));
							
							//echo $_FILES['pdf_file']['size'];
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								
								//@unlink($this->pdfpath.'/'.$resourcebyid["0"]["pdf_file"]);
								
								
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$act_pdf_name = $_FILES['pdf_file']['name'];
								$pdf_update = array("pdf_file" => $pdf_file,"pdf_name"=>$act_pdf_name);
								
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$pdf_file.'">'.$act_pdf_name.'</a>';		
							}
							else{
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$resourcebyid["0"]["pdf_file"].'">'.$resourcebyid["0"]["pdf_file"].'</a>';		
							}
							if(isset($_FILES['ai_file']['name']) && $_FILES['ai_file']['name'] != ""){ 
								
								//@unlink($this->aipath.'/'.$resourcebyid["0"]["ai_file"]);
								//$this->validatefile->do_upload('ai_file'); 
								$ai_file = $isValid["0"]["ai_file"]; 
								$act_ai_name = $_FILES['ai_file']['name']; 
								$ai_update = array("ai_file" => $ai_file,"ai_file_name" => $act_ai_name);
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$ai_file.'">'.$act_ai_name.'</a>';
							}			
							else{ 
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$resourcebyid["0"]["ai_file"].'">'.$resourcebyid["0"]["ai_file"].'</a>';
							}
							
							$merge_update  = array_merge($pdf_update,$ai_update,$txt_update);
							//print_r($merge_update);die;
							$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $this->encrypt->decode($postData["resourceId"])),$merge_update);
							
						} 
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["resourceId"].'"></td>
										<td class="text-center">'.$postData["txt_product_name"].'</td> 
										<td>'.$pdf_file_update.'</td>
										<td>'.$ai_update_file.'</a></td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["resourceId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["resourceId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Resources has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}  
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_resources() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getResourcesCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("product_name","pdf_name","ai_file_name");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "resource_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$resources = $this->administration_model->getResourcesPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'product_name\',\''.$corder[0].'\')">Resource Name</th>
								<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'pdf_name\',\''.$corder[1].'\')">PDF Files</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'ai_file_name\',\''.$corder[2].'\')">AI Files</th> 
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($resources)>0)
				{
						if(count($resources) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($resources as $resource) { 
							 
							//$arr[3] = $types["id"];
					$table .= '<tr id="row_'.$resource["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($resource["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$resource["product_name"].'</td>
							<td><a target="_blank" href="'.base_url().'pdf_files/'.$resource["pdf_file"].'">'.$resource["pdf_name"].'</a></td>
							<td><a target="_blank" href="'.base_url().'ai_files/'.$resource["ai_file"].'">'.$resource["ai_file_name"].'</a></td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($resource["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($resources)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getResorcesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateResource($postData,$FILES)
	{   
		$_FILES = $FILES;
		
		if($_FILES!=""){
			$bool = false;
			$actual_pdf_file = array();
			$actual_ai_file = array();
			$merge_update = array();
			$cond1 = array("resource_id" => $this->encrypt->decode($postData["resourceId"])); 
			$resourcebyid = $this->administration_model->getResourceById($cond1);
			if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != ""){
				$ext = pathinfo($_FILES['pdf_file']['name'], PATHINFO_EXTENSION); 
				$pdf = 'pdf_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$pdf_name = $_FILES['pdf_file']['name']; 
				
				
				//echo $fname;die;
				$actual_pdf_file = $_FILES['pdf_file']['name'];
				
				$config = array( 
					'allowed_types' => 'pdf|PDF',
					'upload_path'   => $this->pdfpath,
					'file_name'		=> $pdf,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('pdf_file');
				$actual_pdf_file = array("act_pdf_file" => $actual_pdf_file,"pdf_file" => $pdf);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$pdf_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->pdfpath.'/'.$resourcebyid["0"]["pdf_file"]);
					$this->validatefile->do_upload('pdf_file');
				}
			}
			if(isset($_FILES['ai_file']['name']) && $_FILES['ai_file']['name'] != ""){
				$ext = pathinfo($_FILES['ai_file']['name'], PATHINFO_EXTENSION); 
				$ai = 'ai_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$ai_name = $_FILES['ai_file']['name']; 
				//echo $fname;die;
				$actual_ai_file = $_FILES['ai_file']['name'];
				$config = array( 
					'allowed_types' => 'ai|AI|zip|ZIP',
					'upload_path'   => $this->aipath,
					'file_name'		=> $ai,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('ai_file');
				$actual_ai_file = array("actual_ai_file" => $actual_ai_file,"ai_file" => $ai); 
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$ai_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->aipath.'/'.$resourcebyid["0"]["ai_file"]);
					$this->validatefile->do_upload('ai_file');
				}
				
				
			}
			$merge_update = array_merge($actual_pdf_file,$actual_ai_file);
			//print_r($data);die;
			//print_r($merge_update);die;
			if($bool){ 
				return array("status" => 1,$merge_update);
			}
		}
		
		/*if(!ctype_alpha($postData["txt_product_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_resources() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("resource_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_RESOURCES,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	/*
	 * Vehicle details
	 * 
	 * */
	 
	 
	public function getVehicleCategory()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data = "";
			$this->load->view("administrator/vehicle_cat",$data);
		}
	}
	
	
	public function getVehicleCatById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_cat_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getVehicleCatById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);  
		}
	}
	
	public function save_vehicle_category()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleCatId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("category" => $postData["txt_vehicle_cat"]);
						$newscat = $this->administration_model->getAllVehicleCategory($cond);
						$data["vehicleCat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_CATEGORY,array("category" => $postData["txt_vehicle_cat"]));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Vehicle Category has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleCategory($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_cat_id !=" => $this->encrypt->decode($postData["vehicleCatId"]), "category" => $postData["txt_vehicle_cat"]);
						$newscat = $this->administration_model->getAllVehicleCategory($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["vehicleCat"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_CATEGORY,array("v_cat_id" => $this->encrypt->decode($postData["vehicleCatId"])),array("category" => $postData["txt_vehicle_cat"]));
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleCatId"].'"></td>
										<td class="text-center">'.$postData["txt_vehicle_cat"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleCatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Category has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Category has been updated successfully...!!!</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This category has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_category() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getVehicleCatCount($cond,$like);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("category");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_cat_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$newscat = $this->administration_model->getVehicleCatPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'category\',\''.$corder[0].'\')">Vehicle category</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($newscat)>0)
				{
						if(count($newscat) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($newscat as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["category"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($newscat)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleCatList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicleCategory($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_categories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_cat_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_CATEGORY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	/* 
	 * Vehicle sub categories
	 * 
	 * */
	 
	 
	 public function getVehicleSubCategory()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$data['category'] = $this->administration_model->getAllVehicleCategory();;
			$this->load->view("administrator/vehicle_sub_cat",$data);
		}
	}
	
	
	public function getVehicleSubCatById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_sub_cat_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getVehicleSubCatById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);  
		}
	}
	
	public function getVehicleSubCatByCatId() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_cat_id" => $postData["id"]);
			$subcat = $this->administration_model->getVehicleSubCatById($cond);
			//print_r($subcat);die;
			
			if(count($subcat)>0){ 
				$html  ='<select class="select-sm arrow_new" name="dd_subresources[]" id="dd_subresources"  multiple="multiple" aria-controls="datatable3" class="form-control input-sm">';
				foreach($subcat as $cat):
					$html .='<option value="'.$cat['id'].'">'.$cat['sub_category'].'</option>';
				endforeach;
				$html .='</select>';
			}
			else{
				$html="";
			}
				echo $html;  
		}
	}
	
	
	public function save_vehicle_sub_category()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleSubCatId"] == "")
				{ 
					
					$isValid = $this->validateVehicleSubCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("v_cat_id" => $postData["cat_id"],"sub_category" => $postData["txt_vehicle_sub_cat"]);
						$newscat = $this->administration_model->getAllVehicleSubCategory($cond);
						$data["vehicleSubCat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_SUBCATEGORY,array("v_cat_id" => $postData["cat_id"],"sub_category" => $postData["txt_vehicle_sub_cat"]));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Vehicle Sub Category has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This sub category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleSubCategory($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_sub_cat_id !=" => $this->encrypt->decode($postData["vehicleSubCatId"]),"v_cat_id" => $postData["cat_id"], "sub_category" => $postData["txt_vehicle_sub_cat"]);
						$newscat = $this->administration_model->getAllVehicleSubCategory($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["vehicleCat"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_SUBCATEGORY,array("v_sub_cat_id" => $this->encrypt->decode($postData["vehicleSubCatId"])),array("sub_category" => $postData["txt_vehicle_sub_cat"],"v_cat_id" => $postData["cat_id"]));
							
							$cond_cat = array("v_cat_id" => $postData["cat_id"]);
							$category = $this->administration_model->getVehicleCatById($cond_cat);
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleSubCatId"].'"></td>
										<td class="text-center">'.$category["0"]["category"].'</td>
										<td class="text-center">'.$postData["txt_vehicle_sub_cat"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleSubCatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleSubCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Sub category has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleSubCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Sub category has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This sub category has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_sub_category() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				$join = array(TB_CATEGORY => TB_SUBCATEGORY.".v_cat_id = ".TB_CATEGORY.".v_cat_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getVehicleSubCatCount($cond,$like,$join);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("category","sub_category");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_sub_cat_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$newscat = $this->administration_model->getVehicleSubCatPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				
				$links = "";
				$table = "";
				
				if($postData["start"] == 0)
				{ 
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'category\',\''.$corder[0].'\')">Vehicle Category</th>
								<th class="text-center '.$css[1].'" width="20%" onclick="changePaginate(0,\'sub_category\',\''.$corder[1].'\')">Vehicle Sub Category</th>
								
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($newscat)>0)
				{
						if(count($newscat) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($newscat as $cat) {   
							/*if($cat["v_cat_id"]!=""){
								$cond_cat = array("v_cat_id" => $cat["v_cat_id"]);
								$category = $this->administration_model->getVehicleCatById($cond_cat);
								$catname = $category["0"]["category"];
							}
							else{
								$catname = "N/A";
							}*/
						$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["category"].'</td> 
							<td class="text-center">'.$cat["sub_category"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($newscat)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleSubCatList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicleSubCategory($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_vehicle_sub_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_sub_categories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_sub_cat_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_SUBCATEGORY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	/*
	 * Vehicle Brand
	 * */
	
	public function getVehicleBrand()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data = "";
			$this->load->view("administrator/vehicle_brand",$data);
		}
	}
	
	
	public function getVehicleBrandById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_brand_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getVehicleBrandById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);  
		}
	}
	
	public function save_vehicle_brand()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleBrandId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("brand" => $postData["txt_vehicle_brand"]);
						$newscat = $this->administration_model->getAllVehicleBrand($cond);
						$data["vehicleCat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_BRAND,array("brand" => ucwords($postData["txt_vehicle_brand"])));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Vehicle Category has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleBrand($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_brand_id !=" => $this->encrypt->decode($postData["vehicleBrandId"]), "brand" => $postData["txt_vehicle_brand"]);
						$newscat = $this->administration_model->getAllVehicleBrand($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["vehicleBrand"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_BRAND,array("v_brand_id" => $this->encrypt->decode($postData["vehicleBrandId"])),array("brand" => ucwords($postData["txt_vehicle_brand"])));
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleBrandId"].'"></td>
										<td class="text-center">'.$postData["txt_vehicle_brand"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleBrandId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleBrandId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Brands has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleBrandId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Brands has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This brands has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_brand() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{ 
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				
				$count = $this->administration_model->getVehicleBrandCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("brand");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_brand_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$brands = $this->administration_model->getVehicleBrandPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'brand\',\''.$corder[0].'\')">Vehicle Brand</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($brands)>0)
				{
						if(count($brands) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($brands as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["brand"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($brands)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleBrandList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				//print_r($table);
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicleBrand($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_brands() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_brand_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_BRAND,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	/*
	 * Vehicle Model
	 * */
	
	public function getVehicleModel()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data["brands"] = $this->administration_model->getVehicleBrand();
			$this->load->view("administrator/vehicle_model",$data);
		}
	}
	
	
	public function getVehicleModelById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_model_id" => $this->encrypt->decode($postData["id"]));
			$vmodel = $this->administration_model->getVehicleModelById($cond);
			//print_r($users);die;
			echo json_encode($vmodel[0]);  
		}
	}
	
	public function save_vehicle_model()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleModelId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("v_model" => ucwords($postData["txt_vehicle_model"]));
						$newscat = $this->administration_model->getAllVehicleModel($cond);
						$data["vehicleModel"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_MODEL,array("v_model" => ucwords($postData["txt_vehicle_model"]),"v_brand_id" => $postData["v_brand_id"]));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Vehicle Model has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This model has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleModel($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_model_id !=" => $this->encrypt->decode($postData["vehicleModelId"]),"v_brand_id" => $postData["v_brand_id"],"v_model" => ucwords($postData["txt_vehicle_model"]));
						
						$vmodels = $this->administration_model->getAllVehicleModel($cond);
						//echo $newscat;
						//echo $this->db->last_query();die;
						$data["vehicleModel"] = $vmodels;
						
						if(count($vmodels) == 0)
						{
							$updateArr = $this->common_model->update(TB_MODEL,array("v_model_id" => $this->encrypt->decode($postData["vehicleModelId"])),array("v_model" => ucwords($postData["txt_vehicle_model"]),"v_brand_id" => $postData["v_brand_id"]));
							
							if($updateArr)
							{
								
								/*retrive brand name*/
								
								$cond1 = array("v_brand_id" => $postData["v_brand_id"]);
								$brand= $this->administration_model->getVehicleBrandById($cond1);
								
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleModelId"].'"></td>
										<td class="text-center">'.$brand["0"]["brand"].'</td>
										<td>'.$postData["txt_vehicle_model"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="edit" data-option="'.$postData["vehicleModelId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleModelId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Model has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleModelId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Model has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This model has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_model() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				$join = array(TB_BRAND => TB_MODEL.".v_brand_id = ".TB_BRAND.".v_brand_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				
				$count = $this->administration_model->getVehicleModelCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("brand","v_model"); 
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_model_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$brands = $this->administration_model->getVehicleModelPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				//echo $this->db->last_query();
				$links = "";
				$table = ""; 
				//print_r($corder);
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'brand\',\''.$corder[0].'\')">Vehicle Brand</th>
								<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'v_model\',\''.$corder[1].'\')">Vehicle Model</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($brands)>0)
				{
						if(count($brands) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($brands as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["brand"].'</td> 
							<td class="text-center">'.$cat["v_model"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($brands)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleBrandList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				//print_r($table);
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicleModel($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_Model() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_model_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_MODEL,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	/*
	 * Manage vehicles*
	 */
	 public function getVehicles()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$cond = array();
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
			$data["brands"] = $this->administration_model->getVehicleBrand();
			$data["models"] = $this->administration_model->getVehicleModel();
			
			$this->load->view("administrator/vehicle_details",$data);
		}
	}
	
	 
	
	public function getVehicleById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("vehicle_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getVechicleById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);
				 
		}
	}
	
	public function getModelById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("v_brand_id" => $postData["id"]);
			$models = $this->administration_model->getVehicleModelById($cond); 
			$data ='';
			if(count($models)>0){
			$data = '<select class="select-sm arrow_new" name="vmodel" id="vmodel">'; 
			foreach($models as $model):
				$data .= '<option value="'.$model['id'].'">'.$model['v_model'].'</option>'; 
			endforeach;
			$data .='</select>'; 
			}
			else{
				$data .="No model available";
			}
			echo json_encode($data); 	 
		}
	}
	
	public function save_vehicles() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["vehicleId"] == "")
				{   
					$isValid = $this->validateVehicle($postData,$_FILES);
					//print_r($isValid);die;
					if($isValid["status"] == 1)
					{ 
						
						$cond = array(
						"vehicle_name" =>$postData["txt_vehicle_name"],
						"vehicle_description" => $postData["txt_vehicle_description"],
						"v_brand_id" => $brands,
						"v_model_id" =>$vmodel,
						"car_maker_pn" => $car_maker_pn ,
						"denso_pn" => $denso_pn,
						"cool_gear_pn1" => $cool_gear_pn1,
						"year" => $year,
						"color"=> $postData["txt_color"]
						);
						$vehicles = $this->administration_model->getAllVehicles($cond);
						$data["vehicles"] = $vehicles; 
						$data_resource = "";
						$data_subresource="";	
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						
						if(count($vehicles) == 0)
						{
							if(!isset($postData["vmodel"])) {
								$postData["vmodel"] = 'NULL';
							}
							$insertId = $this->common_model->insert(TB_VEHICLE,array("vehicle_name" => $postData["txt_vehicle_name"],"vehicle_description"=> $postData["txt_vehicle_description"],"v_cat_id" => $data_resource,"v_sub_cat_id"=>$data_subresource,"v_brand_id" => $postData["brands"],"v_model_id" =>$postData["vmodel"],"car_maker_pn" => $postData["txt_car_maker_pn"], "denso_pn" => $postData["txt_denso_pn"],"cool_gear_pn1" => $postData["txt_cool_gear_pn1"],"year" => $postData["year"],"type" => $postData["type"],"color"=>$postData["txt_color"],"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{ 
								if(isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != "" && $_FILES['v_image']['size']!=0){ 
									
									//Insert attchment after vehicle details
									$filename = $isValid["vehicle_file"];
									$updateArr = $this->common_model->update(TB_VEHICLE,array("vehicle_id" => $insertId),array("v_image" => $filename));
								}
								   
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Vehicle details has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This vehicle has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{ 
					$isValid = $this->validateVehicle($postData,$_FILES);
					if($isValid["status"] == 1)
					{  
						
						$vehicle_name = ($postData["txt_vehicle_name"]?$postData["txt_vehicle_name"]:"");
						$brands = (isset($postData["brands"])?$postData["brands"]:"");
						$vmodel = (isset($postData["vmodel"])?$postData["vmodel"]:"");
						$car_maker_pn = (isset($postData["txt_car_maker_pn"])?$postData["txt_car_maker_pn"]:"");
						$denso_pn = (isset($postData["txt_denso_pn"])?$postData["txt_denso_pn"]:"");
						$cool_gear_pn1 = (isset($postData["txt_cool_gear_pn1"])?$postData["txt_cool_gear_pn1"]:"");
						$year = (isset($postData["year"])?$postData["year"]:"");
						//$cool_gear_pn1 = ($postData["txt_cool_gear_pn1"]?$postData["txt_cool_gear_pn1"]:"");
						
						$cond = array("vehicle_id !=" => $this->encrypt->decode($postData["vehicleId"]),
									 "vehicle_name" => $vehicle_name,
									 "vehicle_description"=> $postData["txt_vehicle_description"],
									 "v_brand_id" => $brands,
									 "v_model_id" =>$vmodel,
									 "car_maker_pn" => $car_maker_pn ,
									 "denso_pn" => $denso_pn,
									 "cool_gear_pn1" => $cool_gear_pn1,
									 "year" => $year,
									 "color"=> $postData["txt_color"]
								);
						$cond1 = array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"]));
						$vehicles = $this->administration_model->getAllVehicles($cond);
						$userbyid = $this->administration_model->getVechicleById($cond1); 
						$data["vehicles"] = $vehicles; 
						$data_resource = "";
						$data_subresource="";
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						
						if(count($vehicles) == 0)
						{  
							if(!isset($postData["vmodel"])) {
								$postData["vmodel"] = 'NULL';
							}
							
							$updateArr = $this->common_model->update(TB_VEHICLE,array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"])),array("vehicle_name" => $postData["txt_vehicle_name"],"vehicle_description"=> $postData["txt_vehicle_description"],"v_cat_id" => $data_resource,"v_sub_cat_id" => $data_subresource,"v_brand_id" => $postData["brands"],"v_model_id" =>$postData["vmodel"],"car_maker_pn" => $postData["txt_car_maker_pn"], "denso_pn" => $postData["txt_denso_pn"],"cool_gear_pn1" => $postData["txt_cool_gear_pn1"],"year" => $postData["year"],"color"=>$postData["txt_color"],"type" => $postData["type"],"date_modified" => date('Y-m-d H:i:s')));
							
							if(isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != "" && $_FILES['v_image']['size']!=0){ 
									
									//Insert attchment after vehicle details
									$filename = $isValid["vehicle_file"];
									$updateArr = $this->common_model->update(TB_VEHICLE,array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"])),array("v_image" => $filename,"v_act_filename"=>$_FILES['v_image']['name']));
							}
							 
							if($updateArr)
							{
								//$catCond = array("v_cat_id" => $postData["category"]);
								//$vcat = $this->administration_model->getAllVehicleCategory($catCond);
								if(isset($postData["brands"]) && $postData["brands"]!="" && $postData["brands"]!="0"){
									$brandCond = array("v_brand_id" => $postData["brands"]);
									$vbrand = $this->administration_model->getAllVehicleBrand($brandCond);
									$vbrand_name  = $vbrand[0]["brand"];
									if(isset($postData["vmodel"]) && $postData["vmodel"]!="NULL"){
										$modelCond = array("v_model_id" => $postData["vmodel"]);
										$vmodel= $this->administration_model->getAllVehicleModel($modelCond); 
										$model = $vmodel[0]["v_model"];
									} 
									else{
										$model = "N/A";
									}
								}
								else{
									$vbrand_name  = "N/A";
									$model = "N/A";
								}
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleId"].'"></td>
										<td class="text-center">'.$postData["txt_vehicle_name"].'</td> 
										<td>'.$vbrand_name.'</td>
										<td>'.$model.'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
										//echo $this->encrypt->decode($postData["userId"]);die;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Vehicle details has been updated successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This vehicle details has been already taken.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				$join = array(TB_CATEGORY => TB_VEHICLE.".v_cat_id = ".TB_CATEGORY.".v_cat_id",TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getVehiclesCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("vehicle_name","brand","v_model");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "vehicle_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$vehicles = $this->administration_model->getVehiclePerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'vehicle_name\',\''.$corder[0].'\')">Vehicle Part Name</th>
								<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'brand\',\''.$corder[1].'\')">Vehicle Brand</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'v_model\',\''.$corder[2].'\')">Vehicle Model</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($vehicles)>0)
				{
						if(count($vehicles) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($vehicles as $vehicle) {  
					$table .= '<tr id="row_'.$vehicle["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($vehicle["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$vehicle["vehicle_name"].'</td>
							<td>'.($vehicle["brand"]?$vehicle["brand"]:"N/A").'</td>
							<td>'.($vehicle["v_model"]?$vehicle["v_model"]:"N/A").'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($vehicle["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($vehicles)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'totalrec' => count($vehicles),'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicle($postData,$FILES)
	{   
		$_FILES  = $FILES;
		if($_FILES!=""){
			if(isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != ""){
				
				$cond_exists = array("v_image" => $_FILES['v_image']['name']); 
				$vehicle_image = $this->administration_model->getVechicleById($cond_exists); 
				
					$ext = pathinfo($_FILES['v_image']['name'], PATHINFO_EXTENSION); 
					//$fname = 'vp_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
					$fname = $_FILES['v_image']['name'];
					 
					//echo $fname;die;
					$config = array( 
						'allowed_types' => 'jpg|JPG|png|PNG',
						'upload_path'   => $this->vehiclepartspath,
						'file_name'		=> $fname,
						'max_size'      => 5125
					); 
					
					$this->validatefile->initialize($config);
					$chk_valid_file = $this->validatefile->validate_file('v_image');
					 
					if(!$chk_valid_file){
						return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$this->validatefile->display_errors().'</div>');
					}
					else{
						if(count($vehicle_image) ==0){
							$cond1 = array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"])); 
							$vehiclebyid = $this->administration_model->getVechicleById($cond1);
								if(count($vehicle_image)==0){ 
									@unlink($this->vehiclepartspath.'/'.$vehiclebyid["0"]["v_image"]);
									$this->validatefile->data(); 
									$this->validatefile->do_upload('v_image'); 
								}
							return array("status" => 1, "vehicle_file" => $fname);
						}
					} 
				/*else{
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please check the vehicle part image name.The image name is already exists.</div>');
				}*/
			}
		}
		
		/*if(isset($_POST["txt_cool_gear_pn1"])){
			$cond_cgp = array("vehicle_id !=" => $postData["vehicleId"],"cool_gear_pn1" => $postData["txt_cool_gear_pn1"]); 
			$vehicle_part = $this->administration_model->getVechicleById($cond_cgp); 
			if($vehicle_part!=0){
				return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>The cool gear part number1 is already exists</div>');
			}
		}*/
		
		/*if(!ctype_alpha($postData["txt_vehicle_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		} */		
		/*elseif (!preg_match('/^\+(?:[0-9] ?){6,14}[0-9]$/', $postData['txt_contact_number']) && $postData['txt_contact_number']!="") {
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Invalid phone number, the number should start with a plus sign, followed by the country code and national number</div>');
		}*/
		return array("status" => 1);
	}
	
	
	public function delete_vehicles() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("vehicle_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_VEHICLE,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
	
	/*
	 * 
	 * add contries
	 * 
	 * */
	 
	 
	public function getCountry()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$data = "";
			$this->load->view("administrator/countries",$data);
		}
	}
	
	
	public function getCountriesById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("country_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getCountryById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);  
		}
	}
	
	public function save_countries()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["countryId"] == "")
				{ 
					
					$isValid = $this->validateCountry($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("name" => $postData["txt_country"]);
						$newscat = $this->administration_model->getAllContries($cond);
						$data["newscat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_COUNTRY,array("name" => $postData["txt_country"]));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Country has been added successfully</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This Country has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateCountry($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("country_id !=" => $this->encrypt->decode($postData["countryId"]), "name" => $postData["txt_country"]);
						$country = $this->administration_model->getAllContries($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["country"] = $country;
						
						if(count($country) == 0)
						{
							$updateArr = $this->common_model->update(TB_COUNTRY,array("country_id" => $this->encrypt->decode($postData["countryId"])),array("name" => $postData["txt_country"]));
							
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["countryId"].'"></td>
										<td class="text-center">'.$postData["txt_country"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["countryId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["countryId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Country has been updated successfully</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["countryId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Country has been updated successfully</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This country has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_countries() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getCountryCount($cond,$like);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("name");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "name";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{ 
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$country = $this->administration_model->getCountryPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'name\',\''.$corder[0].'\')">Country</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($country)>0)
				{
						if(count($country) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($country as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["name"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($country)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getCountriesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateCountry($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_country"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}
		*/ 
		return array("status" => 1);
	}
	
	
	public function delete_countries() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("country_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_COUNTRY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));
			}
		}
	}
	
public function importcsvproduct()
	{ob_start();
		$data = ""; 
		if(is_user_logged_in())
		{ 
			$data['message'] = $this->session->flashdata('message');
			if(isset($_FILES["csv"]["name"]) && $_FILES["csv"]["name"]!=""){
				$ext = pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION); // Getting extetion of the file
                $size = $_FILES['csv']['size'];
                
                if($size>=10000000){
					 $data['validation_message'] = "The file size should not be greater than 100MB"; 
					 $this->load->view("administrator/import_csv",$data);
				}
				
                elseif($ext == 'csv' || $ext == 'CSV'){
						if($size > 0){
						   $count=0;
						   $not_inserted =0;
						   $i=0;     
						   $duplicate=0;
							$linearray = array();
							$arr = array();
							$arrcheck = array();
							$vehi_cat ="";
							$vehi_sub_cat="";
							$proudct_no = "";
							$cool_gear_pn = "";
							$denso_pn = "";
							$car_maker_pn = "";
							$vehicle = "";
							$model = "";
							$year = "";
							$type = "";
							$color = "";
							$description = "";
							$vmodel="";
							$vbrand="";
							if (($handle = fopen($_FILES['csv']['tmp_name'], "r")) !== FALSE) {	
								$row = array();
								$merge_row = array();
								while (($linearray = fgetcsv($handle, 0, ",")) !== FALSE) { 
									$vehi_cat ="";
									$vehi_sub_cat="";
									$proudct_no = "";
									$cool_gear_pn = "";
									$denso_pn = "";
									$car_maker_pn = "";
									$vehicle = "";
									$model = "";
									$year = "";
									$vbrand="";
									$vmodel="";
									$color = "";
									$vehicle_sub_category_id="";
									$compose_image = NULL;
									//$count  =count($linearray);
									if($i>0)
									{ 
										$vehi_cat = trim($linearray[0]);
										$vehi_sub_cat = trim($linearray[1]);
										$cool_gear_pn = trim($linearray[2]);
										$denso_pn = trim($linearray[3]);
										$car_maker_pn = trim($linearray[4]);
										$vehicle = trim($linearray[5]);
										$model = trim($linearray[6]);
										$year = trim($linearray[7]?trim($linearray[7]):"");
										$type = ($linearray[8]?trim($linearray[8]):"");
										$color = ($linearray[9]?trim($linearray[9]):"");
										$description = trim($linearray[10]?trim($linearray[10]):"");
										
										if($vehi_sub_cat != ""){
											$vehi_name = $vehi_sub_cat;
										}else{
											$vehi_name = $vehi_cat;
										}
										
																				 
										if($vehi_cat != "") {
										
											if($vehi_cat !=""){
												$cond = array("category" => $vehi_cat);
												if($this->administration_model->checkVehicleCategoryExists($cond) == 1){
													/*if exists then pick the records from db*/
													$pcat = $this->administration_model->getAllVehicleCategory($cond);
													$vehicle_category_id = $pcat["0"]["id"];
												}
												else{ 
													/*if not exist then insert and pick the catid from database*/
													
													$insertId = $this->common_model->insert(TB_CATEGORY,array("category" => $vehi_cat));
													
													if($insertId){
														$pcat =  $this->administration_model->getAllVehicleCategory($cond);
														$vehicle_category_id = $pcat["0"]["id"];
													}
												}
											}
											
											if($vehi_sub_cat != "" && $vehi_cat != "" && $vehicle_category_id != ""){ 
												$cond_v_cat = array("category" => $vehi_cat);
												$cond_v_sub_cat = array("v_cat_id" => $vehicle_category_id,"sub_category" => $vehi_sub_cat); 
												 
												if($this->administration_model->checkVehicleSubCategoryExists($cond_v_sub_cat) == 0){
													/*if exists then pick the records from db*/
													$pcat = $this->administration_model->getAllVehicleCategory($cond_v_cat);
													$cid = $pcat["0"]["id"]; 
													$insertId = $this->common_model->insert(TB_SUBCATEGORY,array("v_cat_id" =>$cid,"sub_category" => $vehi_sub_cat)); 
													
													if($insertId){
														$vehicle_sub_category_id = $insertId;
													} 
												}
												else{
													$psubcat = $this->administration_model->getAllVehicleSubCategory($cond_v_sub_cat);
													$vehicle_sub_category_id = $psubcat["0"]["id"];  
												}  
											}
											
											
											if($vehicle !=""){
												$cond = array("brand" => $vehicle);
												if($this->administration_model->checkVehicleBrandExists($cond) == 1){
													/*if exists then pick the records from db*/
													$v_cat = $this->administration_model->getAllVehicleBrand($cond);
													$vbrand = $v_cat["0"]["id"];
													
												}
												else{ 
													/*if not exist then insert and pick the catid from database*/
													$insertId = $this->common_model->insert(TB_BRAND,array("brand" => $vehicle));
													
													if($insertId){
														$v_cat = $this->administration_model->getAllVehicleBrand($cond);
														$vbrand = $v_cat["0"]["id"];
													}
												}
											}
											 
											
											if($model !="" && $vehicle !="" && $vbrand != ""){
												 
												$cond_brand = array("brand" => $vehicle);
												$cond_model = array("v_brand_id" => $vbrand,"v_model" => $model); 
												 
												if($this->administration_model->checkVehicleModelExists($cond_model) == 0){
													/*if exists then pick the records from db*/
													$v_cat = $this->administration_model->getAllVehicleBrand($cond_brand);
													$vcategory = $v_cat["0"]["id"]; 
													$insertId = $this->common_model->insert(TB_MODEL,array("v_brand_id" =>$vcategory,"v_model" => $model)); 
													
													if($insertId){
														$vmodel = $insertId;
													} 
												} 
												
											}
											
											if($model !="" && $vehicle !=""){   
												$cond_model = array("v_brand_id" => $vbrand,"v_model" => $model); 
												/*get the v_model_id for further chekcing in duplicate records*/
												$v_model_data = $this->administration_model->getAllVehicleModel($cond_model);
												$vmodel = $v_model_data["0"]["id"];  
											}
											else if($vbrand!=""){
												$vbrand=$vbrand;
											}
											else{
												$vbrand=NULL;
											}
											
											if($model ==""){
												$vmodel = NULL;  
											}
											 
																					
											/*check whether its duplicate entry or not*/
											 
											$cond_part = array( "v_cat_id" => $vehicle_category_id,
																"v_sub_cat_id" => $vehicle_sub_category_id,
															    "vehicle_name" => $vehi_name, 
																"vehicle_description" => $description,
																"v_model_id" => $vmodel,
																"v_brand_id" => $vbrand,
																"type"  => $type,
																"color"	=> $color,
																"year" => $year,
																"car_maker_pn"  => $car_maker_pn,
																"denso_pn" => $denso_pn,
																"cool_gear_pn1" => $cool_gear_pn
															  );
																 				
										
										    if($this->administration_model->checkVehiclePartExists($cond_part) == 0){
											
											
											/*compose image name and insert into database*/
											
											if($cool_gear_pn != ""){
												$compose_image = $cool_gear_pn.'.jpg';
											}else if($denso_pn != ""){
												$compose_image = $denso_pn.'.jpg';
											}											 
											
											$insertId = $this->common_model->insert(TB_VEHICLE,
															array(
																"v_cat_id" => $vehicle_category_id,
																"v_sub_cat_id" => $vehicle_sub_category_id,
															    "vehicle_name" => $vehi_name, 
																"vehicle_description" => $description,
																"v_model_id" => $vmodel,
																"v_brand_id" => $vbrand,
																"type"  => $type,
																"color"	=> $color,
																"year" => $year,
																"car_maker_pn"  => $car_maker_pn,
																"denso_pn" => $denso_pn,
																"cool_gear_pn1" => $cool_gear_pn,
																"v_image"	=> $compose_image,
																"date_modified" => date("Y-m-d H:i:s")
															)
														);
													$count++;
											}
											else{
												$duplicate++;
											}
									 }
									 else{
										 $not_inserted++;
									 }
										
									} 
									
									$i++;
									
								} 
								
									
								$info=""; 
								$info .="Total <b>$count</b> record(s) are inserted<br/>";
								$info .="Total <b>$not_inserted</b> record(s) has been rejected<br/>"; 
								$info .="Total <b>$duplicate</b> record(s) are duplicate";
								
							}
							//die;
							$this->session->set_flashdata('message', $info);
							redirect('administration/importcsvproduct');
							//echo '<meta http-equiv="refresh" content="0; url='.base_url().'administration/importcsvproduct" />';
							//$data['count']  = $count++;
						}
					else{
						
						 $data['validation_message'] = "Invalid file format! Please select CSV file to upload the data"; 
						 $this->load->view("administrator/import_csv",$data);
					}
				}
			}
			else
			{
				
				$this->load->view("administrator/import_csv",$data);
			}
		}
		else{
			redirect(); exit;
		}  
		ob_end_flush();
	}
	
	
	public function getDownloadStatistics()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$topdownloadpdf = $this->administration_model->getTopVisitedResourcePDF();
			$data['downloadpdf'] = '';
			$j=1;
			foreach($topdownloadpdf as $tppdf):
				$data['downloadpdf'] .= '<br/><b>'.$j.'</b>.'.$tppdf['download_pdf'];
				$j++;
			endforeach;
			$downloadai = $this->administration_model->getTopVisitedResourceAI();
			$data['downloadai'] = '';
			$k=1;
			foreach($downloadai as $tpai):
				$data['downloadai'] .= '<br/><b>'.$k.'</b>.'.$tpai['download_ai'];
				$k++;
			endforeach;
			
			$topv = $this->administration_model->getTopVisitedPage();
			
			$data['topvisited'] = '';
			$i=1;
			foreach($topv as $tp): 
				$data['topvisited'] .= '<b>'.$i.'</b>.'.ucwords($tp['page_name']).'<br/>';
				$i++;
			endforeach;
			$cond = array("session_id !="=>'');
			$like = array(); 
			$data['usercount'] = $this->administration_model->getUsersCount($cond,$like);
			$this->load->view("administrator/download_statistics",$data);
		}
	}
	
	
	function download_statistics(){
		
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getUsersCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("first_name","last_name","email_address");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "user_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$users = $this->administration_model->getUsersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				/*if($postData["start"] == 0)
				{*/
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="2%"></th>
								<th class="text-center '.$css["0"].'" width="10%" onclick="changePaginate(0,\'first_name\',\''.$corder[0].'\')">First Name</th>
								<th class="'.$css[1].'" onclick="changePaginate(0,\'last_name\',\''.$corder[1].'\')">Last Name</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'email_address\',\''.$corder[2].'\')">Email Address</th>
								<th width="10%">Current Status</th> 
								<th class="text-center">Action</th>
							  </tr>
							</thead>
							<tbody>';
				//}
				if(count($users)>0)
				{
						if(count($users) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						$date_a = strtotime(date("Y-m-d H:i:s")); 
						foreach($users as $user) {   
							
							//$cond_act = array("user_id"=>$user["id"]);
							//$users_activity = $this->administration_model->getUsersLastLoginActivityInFormat($cond_act);
							///print_r($users_activity);
							if($user["last_activity"] != "" && $user["last_activity"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$last_activity_duration= date('d/m/Y H:i:s',strtotime($user["last_activity"]));
							}
							else
							{
								$last_activity_duration = "-";
							}
							if($user["in_time"] != "" && $user["in_time"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$in_time= date('d/m/Y H:i:s',strtotime($user["in_time"]));
							}
							else
							{
								$in_time = "-";
							}
							//$user["last_login"]; 
							
					$table .= '<tr id="row_'.$user["id"].'">
							<td></td>
							<td class="text-center">'.$user["first_name"].'</td>
							<td>'.$user["last_name"].'</td>
							<td>'.$user["email_address"].'</td>
							<td>'.($user["session_id"] != "" ? '<span class="label label-primary">Online</span>' : '<span class="label label-warning">Offline</span>').'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="status" class="viewstatistics" href="'.base_url().'administration/viewdownloadstatistics/?uid='.$user['id'].'&sort_by=1">View/Search Statistics Download</a>
							</td>						  
						  </tr>
						  ';
						}
						//die;
				}
				/*if($postData["start"] == 0)
				{*/
						if(count($users)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				//}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	function relativeTime($time, $short = false){
			$SECOND = 1;
			$MINUTE = 60 * $SECOND;
			$HOUR = 60 * $MINUTE;
			$DAY = 24 * $HOUR;
			$MONTH = 30 * $DAY; 
			//$t = strtotime(date("Y-m-d H:m:s"));
			$before = time() - $time;
			
			if ($before <= 0)
			{
				return "not yet";
			}

			if ($short){
				if ($before < 1 * $MINUTE)
				{
					return ($before <5) ? "Just now" : $before . " ago";
				}

				if ($before < 2 * $MINUTE)
				{
					return "1m ago";
				}

				if ($before < 45 * $MINUTE)
				{
					return floor($before / 60) . "m ago";
				}

				if ($before < 90 * $MINUTE)
				{
					return "1h ago";
				}

				if ($before < 24 * $HOUR)
				{

					return floor($before / 60 / 60). "h ago";
				}

				if ($before < 48 * $HOUR)
				{
					return "1d ago";
				}

				if ($before < 30 * $DAY)
				{
					return floor($before / 60 / 60 / 24) . "d ago";
				}


				if ($before < 12 * $MONTH)
				{
					$months = floor($before / 60 / 60 / 24 / 30);
					return $months <= 1 ? "1mo ago" : $months . "mo ago";
				}
				else
				{
					$years = floor  ($before / 60 / 60 / 24 / 30 / 12);
					return $years <= 1 ? "1y ago" : $years."y ago";
				}
			}

			if ($before < 1 * $MINUTE)
			{
				return ($before <= 1) ? "just now" : $before . " seconds ago";
			}

			if ($before < 2 * $MINUTE)
			{
				return "a minute ago";
			}

			if ($before < 45 * $MINUTE)
			{
				return floor($before / 60) . " minutes ago";
			}

			if ($before < 90 * $MINUTE)
			{
				return "an hour ago";
			}

			if ($before < 24 * $HOUR)
			{

				return (floor($before / 60 / 60) == 1 ? 'about an hour' : floor($before / 60 / 60).' hours'). " ago";
			}

			if ($before < 48 * $HOUR)
			{
				return "yesterday";
			}

			if ($before < 30 * $DAY)
			{
				return floor($before / 60 / 60 / 24) . " days ago";
			}

			if ($before < 12 * $MONTH)
			{

				$months = floor($before / 60 / 60 / 24 / 30);
				return $months <= 1 ? "one month ago" : $months . " months ago";
			}
			else
			{
				$years = floor  ($before / 60 / 60 / 24 / 30 / 12);
				return $years <= 1 ? "one year ago" : $years." years ago";
			}

			return "$time";
		}
		
	function viewdownloadstatistics(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			$getData = $this->input->get();
			if(isset($_GET['uid']) && $_GET['uid'] !=""){
				$cond = array("user_id"=>$getData['uid']);
				$cond_sort = array("user_id"=>$getData['uid'],"date_modified >=" => $fromdate,"date_modified <=" => $todate);
				$user_details = $this->administration_model->getAllUsers($cond);
				$data['user_name'] = $user_details["0"]["first_name"]." ".$user_details["0"]["last_name"]; 
				
				$this->load->view("administrator/view_download_statistics",$data);
			}
			else{
				redirect("administration/getDownloadStatistics"); exit;
			}
		}
	}
	
	function getUserDownloadStatistics(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$postData = $this->input->post(); 
			
			$cond_sort = array("user_id"=>$postData['uid'],"date_modified >=" => $postData['fromdate'],"date_modified <=" => $postData['todate']);
			$user_details = $this->administration_model->getAllUsers($cond);
			
			$user_name = $user_details["0"]["first_name"]." ".$user_details["0"]["last_name"];
			$user_resource_downloads = $this->administration_model->getDownloadByUserId($cond_sort);
			
			$cond_news = array("user_id"=>$postData['uid'],"date(dn_news_viewed.date_modified) >=" => $postData['fromdate'],"date(dn_news_viewed.date_modified) <=" => $postData['todate']);
			$join_news = array(TB_NEWS => TB_VIEWED_NEWS.".news_id = ".TB_NEWS.".news_id");
			
			$user_viewed_news = $this->administration_model->getViewedNewsByUserId($cond_news,$join_news);
			$cond_search = array("user_id"=>$postData['uid'],"date(dn_viewed_search.date_modified) >=" => $postData['fromdate'],"date(dn_viewed_search.date_modified) <=" => $postData['todate']);
			$join_search = array(TB_VEHICLE => TB_VIEWED_SEARCH.".search_part_id = ".TB_VEHICLE.".vehicle_id");
			$user_viewed_search_part = $this->administration_model->getViewedSearchPartByUserId($cond_search,$join_search);
			
			$cond_user = array("dn_user_statistics.user_id"=>$postData['uid'],"date(dn_user_statistics.date_modified) >=" => $postData['fromdate'],"date(dn_user_statistics.date_modified) <=" => $postData['todate']);
			$join_user = array(TB_USERS => TB_USER_STAT.".user_id = ".TB_USERS.".user_id");
			$user_history = $this->administration_model->getLoggedInUser($cond_user,$join_user);
			
			
			$table='<div class="col-md-12"><div class="tab-block mb25">
					<ul class="nav tabs-left">
			
						<li class="active">
							<a href="#tab12_1" data-toggle="tab">Downloaded Resources</a>
						</li>
						<li>
							<a href="#tab12_2" data-toggle="tab">Viewed News</a>
						</li>
						<li>
							<a href="#tab12_3" data-toggle="tab">Viewed Search Part</a>
						</li>
						<li>
							<a href="#tab12_4" data-toggle="tab">Logged In History</a>
						</li>
					</ul>
					<div class="tab-content" style="margin-top:10px;">';
			$table .= '<div id="tab12_1" class="tab-pane active">
			           <table class="table table-striped table-hover dataTable no-footer" width="100%" height="100%">';
						$table .= '<thead>
							  <tr> 
								<th width="40%" style="background:#F9F9F9 !important;" class="text-center">PDF Download</th>
								<th width="40%" class="text-center">AI File Download</th>
								<th width="20%" class="text-center">Download Date</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_resource_downloads)>0)
				{
					foreach($user_resource_downloads as $download) {
							$table .='<tr> 
										<td class="text-center">'.($download["download_pdf"]?$download["download_pdf"]:'-').'</td>
										<td class="text-center">'.($download["download_ai"]?$download["download_ai"]:'-').'</td>
										<td class="text-center">'.date("d/m/Y H:i:s",strtotime($download["date_modified"])).'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="4">No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';
			
			$table .='<div id="tab12_2" class="tab-pane">';
			
			$table .= '<table class="table table-striped table-hover dataTable no-footer" width="100%" style="width:100%" height="100%">';
						$table .= '<thead>
							  <tr>  
								<th style="background:#F9F9F9 !important;" width="70%" class="text-center">News Heading</th> 
								<th class="text-center"  width="30%">Viwed Date</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_viewed_news)>0)
				{
					foreach($user_viewed_news as $news) {
							$table .='<tr> 
										<td class="text-center">'.($news["news_heading"]?$news["news_heading"]:'-').'</td> 
										<td class="text-center">'.date("d/m/Y H:i:s",strtotime($news["date_modified"])).'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="3" >No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';
			 
			$table .='<div id="tab12_3" class="tab-pane">
			          <table class="table table-striped table-hover dataTable no-footer" width="100%" height="100%">';
						$table .= '<thead>
							  <tr> 
								<th style="background:#F9F9F9 !important;" class="text-center"  width="40%">Product Name</th> 
								<th class="text-center"  width="40%">COOLGEAR Part</th>  
								<th class="text-center"  width="20%">Viwed Date</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_viewed_search_part)>0)
				{
					foreach($user_viewed_search_part as $search) {
							$table .='<tr> 
										<td class="text-center">'.($search["vehicle_name"]?$search["vehicle_name"]:'-').'</td> 
										<td class="text-center">'.($search["cool_gear_pn1"]?$search["cool_gear_pn1"]:'-').'</td> 
										<td class="text-center">'.date("d/m/Y H:i:s",strtotime($search["date_modified"])).'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="4">No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';	
			
			$table .='<div id="tab12_4" class="tab-pane">
			          <table class="table table-striped table-hover dataTable no-footer" width="100%"  height="100%">';
						$table .= '<thead>
							  <tr> 
								<th style="background:#F9F9F9 !important;"  width="50%" class="text-center">Email Address</th> 
								<th  class="text-center"  width="25%">Login Time</th>  
								<th class="text-center"  width="25%">Logout Time</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_history)>0)
				{
					foreach($user_history as $user) {
						
							
							if($user["login_time"] != "" && $user["login_time"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$in_time= date('d/m/Y H:i:s',strtotime($user["login_time"]));
							}
							else
							{
								$in_time = "-";
							}
							if($user["logout_time"] != "" && $user["logout_time"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$out_time= date('d/m/Y H:i:s',strtotime($user["logout_time"]));
							}
							else
							{
								$out_time = "-";
							}
						
							$table .='<tr> 
										<td class="text-center" >'.($user["email_address"]?$user["email_address"]:'-').'</td> 
										<td class="text-center" >'.$in_time.'</td> 
										<td class="text-center" >'.$out_time.'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="4">No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';
			
			$table .='</div></div></div>';
			
			echo json_encode(array('table' => $table)); exit;		
		}
	}
}
?>
