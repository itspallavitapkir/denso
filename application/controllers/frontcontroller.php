<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FrontController extends CI_Controller{
       
    function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("administration_model");
		$this->load->library('form_validation');
		$this->load->library("email");
		$this->load->helper('download');   
		$this->no_cache();

		// initialise the reference to the codeigniter instance
        require_once APPPATH.'PHPExcel/Classes/PHPExcel.php';
        $excel = new PHPExcel();  
        $this->load->library('excel');   
       
	}
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	private function checkUserStatusDenso($uid){ 
			$cond = array('dn_status' => 'Active','user_id' => $uid);									
			$res = $this->login_model->checkUserStatus(TB_USERS,"email_address,password",$cond);  
			
			if ( $res == 0 ) { 
				$userstat = $this->session->userdata("stat_id");
				$cond_stat = array("user_id"=>$uid,"stat_id"=>$userstat);
				$data_update = array("logout_time"=>date("Y-m-d H:i:s"),"date_modified"=> date("Y-m-d H:i:s"));
				$this->common_model->update(TB_USER_STAT,$cond_stat,$data_update);
				
				$this->session->unset_userdata("auth_densouser");
				$this->session->unset_userdata("stat_id");
				redirect("index"); exit;
			} 
	}
	  
	function index(){   
		if(is_densouser_logged_in())
		{ 
			$this->session->unset_userdata('excelData');
			// $userdata = $this->session->userdata('auth_densouser');
			// //pr($userdata);  
			//  $user_type=$userdata['user_type'];
			//  $country=$userdata['country'];
			//  $v_cat_id=$userdata['v_cat_id'];
			//  $status='Active';
			  
			// $cond = array('user_type' => $user_type,'country' => $country,'category_id'=>$v_cat_id,'status'=>$status);
			// $bannerData = $this->login_model->fetchBannerForUser(TB_BANNER,"user_type,country,category_id,banner_image,start_date,end_date,status,title",$cond);

			// //pr($bannerData[0]);
			// $start= $bannerData[0]['start_date']."<br>";
			// $end= $bannerData[0]['end_date']."<br>";
			// $img= $bannerData[0]['banner_image']."<br>";
			// $current_date= date('m/d/Y H:i A');
			// if(($start<=$current_date)&&($end>=$current_date))
			// {
			// 	$image= $bannerData[0]['banner_image'];
			// 	echo UPLOAD_BANNER_PATH.$image;
			// }
			
			redirect('AMW/search'); exit;
		}
		else{ 
			$data['message'] = $this->session->flashdata('message');
			
			if (isset ($_POST['cmdLogin']) || isset ($_POST['cmdLoginSales'])) {  
				
				$this->form_validation->set_rules('uemail', 'Email Address', 'email_valid|required|xss_clean');
				$this->form_validation->set_rules('pwd', 'Password', 'required|xss_clean');
				if ($this->form_validation->run() == true) { 

					
					$useremail = $this->input->post('uemail');
					$password = md5($this->input->post('pwd'));
					
					$cond = array('email_address' => $useremail,'password' => $password);
					//pr($cond);

					$userData = $this->login_model->validUser(TB_USERS,"user_id,first_name,last_name,country,email_address,password,v_cat_id,v_sub_cat_id,login_count,user_type",$cond);
					
						//echo "test";die;
					if(count($userData) > 0)
					{	
						
						$cond = array('dn_status' => 'Active','email_address' => $useremail);									
						$res = $this->login_model->checkUserStatus(TB_USERS,"email_address,password",$cond);
						  
						if ( $res !== 0 ) {
							 //echo "login";die;			
							 $this->session->set_userdata("auth_densouser", $userData[0]);
							  
							 //login user statistics 
							 
							 $data_update = array("session_id"=>time(),"in_time"=>date("Y-m-d H:i:s"));
							 
							 $this->common_model->update(TB_USERS,$cond,$data_update);	
							 
							 $data_insert = array("user_id"=>$userData[0]["user_id"], "login_time"=> date("Y-m-d H:i:s"), "date_modified"=> date("Y-m-d H:i:s"));
							 //print_r($userData);die;
							 $stat_id = $this->common_model->insert(TB_USER_STAT,$data_insert);	
							 $data_update_cnt = array("login_count"=>$userData[0]["login_count"]+1);
							 
							 $this->common_model->update(TB_USERS,$cond,$data_update_cnt);
							 
							 $this->session->set_userdata("stat_id", $stat_id);			 
							 if (isset ($_POST['cmdLogin'])){
								 redirect('AMW/search'); 
							 }
							 else{
								 redirect('PMK/sale-kit-home'); 
							 }
						}
						else{  
								$data['validation_message'] = 'Your account is inactive';
								$data['email_address']	= trim($useremail); 
								$this->load->view('frontend/login', $data); 
						}
					}
					else{  
							$data['validation_message'] = 'Invalid email address or password';
							$data['email_address']		= trim($this->input->post('uemail')); 
							//print_r($data);die;
							$this->load->view('frontend/login', $data); 
					}
				}
			}
			else{

				$this->load->view('frontend/login',$data);
			}
		}
	}
	
	function forgotUserPassword(){ 
		   $data['title']  = 'Forgot Password';
		   $data['meta_description'] = '';
		   $data['meta_keyword'] = '';
		   $data['sucess_message'] = $this->session->flashdata('sucess_message');
		   //echo $this->session->userdata('tokenfrm');   
           $this->form_validation->set_rules('uemail', 'Email address', 'valid_email|xss_clean|required'); 
           if ($this->form_validation->run() !== false) { 
					$useremail = trim($this->input->post('uemail'));
                    $cond = array('email_address' => $useremail);
					$userData = $this->login_model->validUser(TB_USERS,"user_id,country,first_name,last_name,email_address,password,v_cat_id,v_sub_cat_id",$cond);
						//echo "test";die;
					if(count($userData) > 0)
					{	 
                        // $username = $this->loginmodel->getUserNameByUserEmail($uname);
                         $reset_key  = md5(uniqid(mt_rand(), true));
                         $datareset = array(
                              'dn_resetkey' =>  $reset_key,
                         ); 
                         $cond1 = array('email_address' =>  $useremail); 
                         $hostname = $this->config->item('hostname');
                         $this->common_model->update(TB_USERS,$cond1,$datareset);
                         $config['mailtype'] ='html';
                         $config['charset'] ='iso-8859-1';
                         $this->email->initialize($config);
                         $from  = EMAIL_FROM; 
						 $uname = $userData[0]['first_name']." ".$userData[0]['last_name'];
                         $this->messageBody  = "Hello $uname,<br/><br/>
                            We have received your request to reset your password. <br/> <br/>
                             Please click the link below to reset.<br/><br/>
                             <a href=".$hostname."resetuserpwd/?key=".$reset_key.">".$hostname."resetuserpwd/?key=".$reset_key."</a>
                             <br/><br/>If the link is not working properly, then copy and paste the link in your browser.<br/>
                             If you did not send this request, please ignore this email.
                             <br/><br/>Regards,<br/>DENSO AFTERMARKET WEBSITE";
						//echo $this->messageBody;
						//die;
                        
						 $this->email->from($from, 'DENSO ASIA AFTERMARKET');
						 $this->email->to("$useremail");

                         $this->email->subject('DENSO AFTERMARKET WEBSITE - Reset Password');
                         $this->email->message($this->messageBody);	
                             
                        if ( ! $this->email->send()){
                                $this->session->set_flashdata('sucess_message', "The email can not be send ! Server Error.");
                                 //$data['sucess_message'] ="The email can not be send ! Server Error";
                                redirect('forgotpassword');
                                 //$this->load->view('frontend/incls/layout', $data);
                        }else{
                                $this->session->set_flashdata('sucess_message', "The reset password link has been sent to your email address.");
                                //$data['sucess_message'] = "The reset password link has been sent to your email address.";
                                redirect('forgotpassword');
                                //$this->load->view('frontend/incls/layout', $data);
                        }
                    } 
                    else{
						$data['validation_message'] = "The email address is not found in our database";
                         //var_dump($data);
						 $this->load->view('frontend/forgot_password', $data);
					} 
           }
           else{
                $data['message'] = $this->session->flashdata('message');
                $this->load->view('frontend/forgot_password', $data);
           }
           
	}
	
	
	/*
	 * Reset password
	 * */
	
	 function resetUserPassword(){
       if(!isset($_GET['key'])){
             redirect('AMW/index/');
       }
       $data['messagereset'] = $this->session->flashdata('messagereset'); 
       $data['title']  = 'Reset Password';
	   $data['meta_description'] = '';
	   $data['meta_keyword'] = '';
       $cond_key = array("dn_resetkey" => $this->input->get('key'));
       if($this->login_model->checkUserStatus(TB_USERS,'email_address',$cond_key)){
		  
            $this->form_validation->set_rules('uemail', 'Email Address', 'valid_email|xss_clean|required'); 
            $this->form_validation->set_rules('pwd', 'New Password', 'xss_clean|required'); 
            $key  = $this->input->get('key');
            //echo $key;die;
            if ($this->form_validation->run() !== false) { 
				 $useremail    = trim($this->input->post('uemail'));
				 $password = md5($this->input->post('pwd'));
				 $datareset = array(
					  'password' => $password,
				 );
				 
				$cond = array('email_address' => $useremail, 'dn_resetkey' => $key);
				$userData = $this->login_model->validUser(TB_USERS,"user_id,country,first_name,last_name,email_address,password,v_cat_id,v_sub_cat_id",$cond);
				
				if(count($userData) > 0)
				{	  
					$this->common_model->update(TB_USERS,$cond,$datareset);
					$this->session->set_flashdata('messagereset', "Your password has been reset successfully.");
					$cond = array('email_address' => $useremail,'password' => $password);
					$res = $this->login_model->checkUserStatus(TB_USERS,"email_address,password",$cond);
					  
					if ( $res !== false ) {
						//echo "redirect";die; 
						 $cond1 = array('email_address' => $useremail);
						 $datareset = array(
							  'dn_resetkey' => '',
						 );
						$this->common_model->update(TB_USERS,$cond,$datareset);
						//$data_update = array("session_id"=>time(),"in_time"=>date("Y-m-d H:m:s")); 
						//$this->common_model->update(TB_USERS,$cond,$data_update);	
						 
						//$this->session->set_userdata("auth_densouser", $userData[0]);
						
						$this->session->set_flashdata("message","You have successfully reset the password.");
						redirect('index'); 
						exit;
					} 
				}	 
				else{
						$data['validation_message'] = "Invalid user email address or you are not registered user."; 
						$this->load->view('frontend/reset_password', $data);
				} 
            }
            else{
                $data['validation_message'] = (validation_errors() ? validation_errors() : $this->session->set_flashdata('message'));
                $this->load->view('frontend/reset_password', $data);
            }
        }
        else{ 
			
            $data['validation_message'] = "The key is invalid or You have already reset the password.";
            $this->load->view('frontend/reset_password', $data);
        } 
    }
   
     
	function news(){
		if(is_densouser_logged_in()){
			$data['title']  = 'News';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/AMW/news';
			$this->pagevisited('news');
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			$cond_country = array("user_id" => $userdata['user_id']);
			$country =  $this->administration_model->getAllUsers($cond_country);
			$in_thailand ="";
			$in_other = "";   
			$cond_th = array("country_id"=>$country["0"]["country"]);
			$country_id =  $this->administration_model->getAllContries($cond_th); 
			if($country_id["0"]["id"] == $country["0"]["country"]){
				$in_thailand = $country_id["0"]["id"];
				$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id"); 
				$news_details = $this->administration_model->getAllNewsDistinct($join,$in_thailand,$in_other);
				$data['html'] ='';
				$newsId = "";
				if(count($news_details)>0){
					 
					foreach($news_details as $news):
						
						$cond = array("news_cat_id" => $news['news_cat_id']);
						$news_cat = $this->administration_model->getAllNewsCategory($cond);  
						if($news["news_cat_id"] != $newsId){
							 $newsId = $news["news_cat_id"];
							$data['html'] .='<span class="heading_title">'.$news_cat["0"]["news_category"].'</span>'; 
						} 
						
						$data['html'] .='<ul class="news_list">
							<li><a href="'.base_url().'AMW/news_details/?nid='.base64_encode($news['id']).'">'.$news['news_heading'].'</a></li>
							<li class="month">Written on '.date('D, d F H:i:s', strtotime($news['date_modified'])).'</li>
						</ul>';
						//endforeach;
					endforeach;
				}
				else{
					$data['html'] ="No Records Available";
				} 
			}
			 
			$this->load->view('frontend/AMW/incls/layout', $data); 	
						
		}
		else{
			redirect("index");exit;
		}
	} 
	 
	 
	function news_details(){
		if(is_densouser_logged_in()){
			$data['title']  = 'News Details';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/AMW/news_details';
			$this->pagevisited('News Details'); 
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			
			$cond_country = array("user_id" => $userdata['user_id']);
			$country =  $this->administration_model->getAllUsers($cond_country);
			$in_thailand ="";
			$in_other = "";   
			
			
			if(isset($_GET['nid']) && $_GET['nid']!=""){
				$nid = base64_decode($_GET['nid']);
				
				
				/*insert viewed news for user*/
				$cond_viewed = array("user_id" => $userdata['user_id'],"news_id" => $nid,"date(date_modified)" => date('Y-m-d'));	
				if($this->administration_model->checkViewedNewsAlreadyExists($cond_viewed)==0)
				{
					$this->common_model->insert(TB_VIEWED_NEWS,array("user_id"=>$userdata['user_id'], "news_id"=>$nid, "date_modified" => date('Y-m-d H:i:s')));
				}
				$cond = array("news_id"=>$nid);
				//$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id"); 
				$data['news_details'] = $this->administration_model->getAllNewsForThilandUserCountry($cond,$in_thailand,$in_other);
				 
				/*News Comemtns
				 * */
				$cond_news = array("news_id"=>$nid,"status"=>"Published"); 
				$join_comment = array(TB_USERS => TB_COMMENT.".user_id = ".TB_USERS.".user_id"); 
				$data['news_comments'] = $this->administration_model->getNewsComments($cond_news,$join_comment);
				 
			}
			 
			$cond_th = array("country_id"=>$country["0"]["country"]);
			$country_id =  $this->administration_model->getAllContries($cond_th); 
			 
			if($country_id["0"]["id"] == $country["0"]["country"]){
				$in_thailand = $country_id["0"]["id"]; 
				$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id"); 
				$news_details = $this->administration_model->getAllNewsDistinct($join,$in_thailand,$in_other);
				$data['html'] ='';
				$newsId = "";
				if(count($news_details)>0){
					
					foreach($news_details as $news):
						 $data['html'] .='<div class="cate_righttop">';
						$cond = array("news_cat_id" => $news['news_cat_id']);
						$news_cat = $this->administration_model->getAllNewsCategory($cond);  
						if($news["news_cat_id"] != $newsId){
							 $newsId = $news["news_cat_id"];
							$data['html'] .='<h1>'.$news_cat["0"]["news_category"].'</h1>'; 
						} 
						
						$data['html'] .='<div class="catelist"><ul>
											<li><a href="'.base_url().'AMW/news_details/?nid='.base64_encode($news['id']).'">'.$news['news_heading'].'</a></li>
										</ul></div>';
						//endforeach;
						$data['html'] .='</div>';
						
					endforeach;
					
					
				}
				else{
					$data['html'] ="No Records Available";
				} 
			}
			
				 
			
			$this->load->view('frontend/AMW/incls/layout', $data); 	
		}
		else{
			redirect("index");exit;
		}
	} 
	
	
	function post_comment(){
		if(is_densouser_logged_in()){
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			$news_id = base64_decode($_POST['nid']);
			$comment = $_POST['comment']; 
			$userid  = $userdata['user_id'];
			session_start();
			if (empty($_SESSION['captcha']) || trim(strtolower($_POST['captcha'])) != $_SESSION['captcha']) {
				 
				echo json_encode(array("msg1" => '<div class="alert alert-danger">Please enter a valid captcha</div>')); exit;
			}
			else{
				$insertId = $this->common_model->insert(TB_COMMENT,array("user_id" =>$userid, "news_id" => $news_id, "comments" => $comment,"date_added" => date('Y-m-d H:i:s')));
				if($insertId)
				{  
					    /*
					     * sending email to admin of news comment
					     * */
					    
					    $cond = array("user_id" => $userdata["user_id"]);
					    $getuserdata = $this->administration_model->getUserById($cond);
					    
					    $hostname = $this->config->item('hostname'); 
					    $username = $getuserdata["0"]["first_name"]." ".$getuserdata["0"]["last_name"];
						$emailaddress = $postData["txt_email_address"];
						$config['mailtype'] ='html';
						$config['charset'] ='iso-8859-1';
						$this->email->initialize($config);
						$this->messageBody  = "Hello,<br/>
						
							You have received a news comment from $username<br/><br/>
							
							News comment details : $comment<br/><br/>
							
							Click below link to approve it<br/>
							<a href='".$hostname."/adminlogin'>".$hostname."/adminlogin</a>
							 <br/><br/>If link is not working properly then copy and paste the link in browser	 
							 <br/><br/>
																 
							 Regards,<br/>Desno";
						 //echo $this->messageBody;
						 //die;
						$this->email->from(EMAIL_FROM, 'Denso');
						//$this->email->to("swapnil.t@exceptionaire.co");
						//$this->email->to("rajit@exceptionaire.co");
						$this->email->to(EMAIL_FROM);
						$this->email->subject('Denso - Received News Comment');
						$this->email->message($this->messageBody);	
						$this->email->send();	 
						
					
						echo json_encode(array("msg" => '<div class="alert alert-success">You comment is posted successfully.Please wait for approval from administrator</div>')); exit;
				}
			}
		}
		else{
			redirect("AMW/index");exit;
		}
	}
	
	 
	function profile(){ 
		if(is_densouser_logged_in()){
			$this->pagevisited('profile');
			$data['message'] = $this->session->flashdata('message'); 
			//print_r($data['message']);die;
			$data['title']  = 'Profile';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/AMW/profile';
			//print_r($data);
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			$this->form_validation->set_rules('txt_contact_number', 'contact number', 'xss_clean|required');  
             
            if ($this->form_validation->run() !== false) { 
				$postData = $this->input->post();
				$updateuserpassword="";
				$isValid = $this->validateUser($postData); 
				if($isValid["status"] == 1){
					$cond1 = array("user_id" => $userdata['user_id']);
					
					//$updateArr = $this->common_model->update(TB_USERS,$cond1,array("first_name" => $postData["txt_first_name"], "last_name" => $postData["txt_last_name"],"company" => $postData["txt_company"], "contact_details" => $postData["txt_contact_number"],"country" => $postData["country"],"date_modified" => date('Y-m-d h:m:s')));
					$user = $this->administration_model->getUserById($cond1);
						
						
						$updateuser = $this->common_model->update(TB_USERS,$cond1,array("contact_details" => $postData["txt_contact_number"],"date_modified" => date('Y-m-d H:i:s')));
						
					
					if(isset($postData['txt_password']) && $postData['txt_password'] !=""){
						
						
						if($user["0"]["super_admin_access"] == '1'){
							
							/*Check if user having super admin access*/
							
							$cond_admin = array("username" => $userdata['email_address']);
							
							$count_admin = $this->administration_model->getAllAdmin($cond_admin);
							
							if($count_admin>0){
								
								$updateuserpassword = $this->common_model->update(TB_ADMIN,$cond_admin,array("password" => md5($postData["txt_password"])));
								
							} 
						}
						$updateuserpassword = $this->common_model->update(TB_USERS,$cond1,array("password" => md5($postData["txt_password"])));
						
					}
					
					if($updateuserpassword || $updateuser)
					{
						$this->session->set_flashdata('message','The User Profile updated successfully');
						redirect("profile");
					}
				}
				else{
					$data['validation_message'] = ($isValid["msg"] ? $isValid["msg"] : $this->session->set_flashdata($isValid["msg"]));
					
				}
			}
			else{
				$data['validation_message'] = (validation_errors() ? validation_errors() : $this->session->set_flashdata('message'));  
			} 
			 
			//$con_arr  =array();
			$data["AllContries"] = $this->administration_model->getAllContries(); 
			$cond = array("user_id" => $userdata['user_id']);
			$data['user'] = $this->administration_model->getUserById($cond); 
			$data['firstname'] = $data['user']["0"]['first_name'];
			$data['lastname'] =  $data['user']["0"]['last_name'];
			$data['email_address'] =  $data['user']["0"]['email_address'];
			$data['contact_details'] =  $data['user']["0"]['contact_details'];
			$data['company'] =  ($data['user']["0"]['company'] ? $data['user']["0"]['company'] : "-");
			
			if($data['user']["0"]['country']!=""){
				$data["country_id"]  =$data['user']["0"]['country'];
				$cond_country = array("country_id"=> $data['user']["0"]['country']);
				$data['country_details'] = $this->administration_model->getCountryById($cond_country);
				$data['country_name'] = $data['country_details']["0"]["name"]; 
			}
			else{
				$data['country_name'] = "-";
			}
			 
			$this->load->view('frontend/AMW/incls/layout', $data); 	 
		}
		else{
			redirect("index");exit;
		}
		 
	} 
	
	
	public function validateUser($postData)
	{   
		
		/*if(!ctype_alpha($postData["txt_first_name"]))
		{ 
			return array("status" => 2, "msg" => 'First name required character only');
		}
		else if(!ctype_alpha($postData["txt_last_name"]))
		{
			return array("status" => 2, "msg" => 'Last Name required character only');
		}
		else if(!ctype_alnum($postData["txt_company"]) && isset($postData["txt_company"]) && $postData["txt_company"]!="")
		{
			return array("status" => 2, "msg" => 'Company name required character only');
		}
		*/
		
		/*if (!preg_match('/^\+(?:[0-9] ?){6,14}[0-9]$/', $postData['txt_contact_number']) && $postData['txt_contact_number']!="") {
			return array("status" => 2, "msg" => 'Invalid phone number, the number should start with a plus sign, followed by the country code and national number');
		}*/
		
		return array("status" => 1);
	}
	
	
	
	function resources(){
		if(is_densouser_logged_in()){
			$this->pagevisited('resources');
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			$data['title']  = 'Resources';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/AMW/resource';
			
			$cond = array("user_id"=>$userdata["user_id"]);
			$user = $this->administration_model->getUserById($cond);
			$cond_r = array();
			//$join = array(TB_USERS => TB_RESOURCES);
			($user["0"]["pdf_file_access"] == '1'?$pdf_access = 1:$pdf_access = 0);
			($user["0"]["ai_file_access"] == '1'?$ai_access = 1:$ai_access = 0);
			 
			$allresources = $this->administration_model->getAllResourcesForUsers($cond_r,$user["0"]["v_cat_id"],$user["0"]["v_sub_cat_id"]);
			$data['data_resources'] = "";
			
			
			if(count($allresources)>0){
				 
				$i=0;
				$arrResources = array();
				foreach($allresources as $res):
				
					$allow=0;  
					$allow_resource=0;
					$allow_sub_resource=0;
					
					$res_country = explode(',',$res['countries']);    
					
					$res_resource = explode(',',$res['resource_categories']);
					
					$res_subresource = explode(',',$res['resource_subcategories']);
					
					
					
					/*
					 * Check if users country is applicable for download or not
					 * 
					 * */
					
					if($res_country){
						
						if(in_array($user["0"]["country"],$res_country)){
							$allow = 1;
						}
					}  
					
					
					
                    if($res['pdf_name'] != ""){
						$AttachmentpdfName = urlencode(base64_encode($res['pdf_name']));
					}
					if($res['ai_file_name'] != ""){
						$AttachmentaiName = urlencode(base64_encode($res['ai_file_name']));
					}  
					
					if($allow){ 
						$arrResources[] = array('product_name'=>$res['product_name'],'product_description'=>$res['product_description'],'AttachmentpdfName'=>$AttachmentpdfName,'AttachmentaiName'=>$AttachmentaiName,'pdf_name'=>$res['pdf_name'],'ai_file_name'=>$res['ai_file_name']);
						
						/*$data['data_resources'] .='
						   <tr class="prod_inst">
								<td>'.$res['product_name'].'</td>
								<td>'.($res['product_description']?$res['product_description']:'-').'</td>';
								
								if($res['pdf_file'] != ""){
									if($pdf_access){ 
										$data['data_resources'] .='<td><img src="'.base_url().'img/pdf_file.png" title="PDF File" alt="PDF file"><a class="download" href="'.base_url().'downloadPDFFile/?fn='.$AttachmentpdfName.'&fname='.$res['pdf_name'].'"><span>'.$res['pdf_name'].'</span></a></td>';
									}
									else{
										$data['data_resources'] .='<td><span>No Access</span></td>';
									}
								}
								else{
									$data['data_resources'] .='<td><span>-</span></td>';
								}
								
								if($res['ai_file'] != ""){
									if($ai_access){ 
										$data['data_resources'] .='<td><img src="'.base_url().'img/zip_file.png" title="AI File" alt="AI file"><a href="'.base_url().'downloadAIFile/?fn='.$AttachmentaiName.'&fname='.$res['ai_file_name'].'"><span>'.$res['ai_file_name'].'</span></a></td>';
									}
									else{
										$data['data_resources'] .='<td><span>No Access</span></td>';
									}
								}
								else{
									$data['data_resources'] .='<td><span>-</span></td>';
								}
								
							$data['data_resources'] .='</tr>';*/
					}
					//if(!$allow && !$allow_resource && !$allow_sub_resource){
						
						
				//}
					 
				endforeach; 
							
				if(count($arrResources)){ 
										 
					foreach($arrResources as $resource_arr){
					
					$data['data_resources'] .='
						   <tr class="prod_inst">
								<td>'.$resource_arr["product_name"].'</td>
								<td>'.($resource_arr["product_description"]?$resource_arr["product_description"]:'-').'</td>';

								if(trim($resource_arr["pdf_name"]) != ""){ 
									
									if($pdf_access){ 
										
										$data['data_resources'] .='<td><img src="'.base_url().'img/pdf_file.png" title="PDF File" alt="PDF file"><a class="download" target="_blank" href="'.base_url().'pdf_files/'.$resource_arr['pdf_name'].'"><span>'.$resource_arr["pdf_name"].'</span></a></td>';
									}
									else{
										$data['data_resources'] .='<td><span>No Access</span></td>';
									}
								 }
								else{
									$data['data_resources'] .='<td><span>No Files Available</span></td>';
							}
								
							if(trim($resource_arr["ai_file_name"]) != ""){
								if($ai_access){ 
									$data['data_resources'] .='<td><img src="'.base_url().'img/zip_file.png" title="AI File" alt="AI file"><a target="_blank" href="'.base_url().'ai_files/'.$resource_arr['ai_file_name'].'"><span>'.$resource_arr["ai_file_name"].'</span></a></td>';
								}
								else{
									$data['data_resources'] .='<td><span>No Access</span></td>';
								}
							}
							else{
								$data['data_resources'] .='<td><span>No Files Available</span></td>';
							}
								
							$data['data_resources'] .='</tr>';
							}
				}
				else{
					$data['data_resources'] ='<tr><td colspan="4">No Files to Download</td></tr>';
				}
				
			}
			else{
						$data['data_resources'] ='<tr><td colspan="4">No Files to Download</td></tr>';
			}
			 
			$this->load->view('frontend/AMW/incls/layout', $data); 	
		}
		else{
			redirect("index");exit;
		}
	} 
	
	
	public function downloadPDFFile(){
		if(is_densouser_logged_in()){
			
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			
				
			if(isset($_GET['fn'])){  
                $AttachmentName = base64_decode(urldecode($_GET['fn']));
                 
                $data = file_get_contents(base_url().'pdf_files/'.$AttachmentName); // Read the file's contents 
                $name = $AttachmentName;
                $fname = $_GET['fname'];
                
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}
                
				
				
				$cond1 = array("download_pdf" => $fname, "ip_address" => $ip,"date(date_modified)"=>date("Y-m-d H:i:s"));
				/*if($this->administration_model->checkDownloadResourceAlreadyVisited($cond1)==0){*/
					$data_download = array("user_id"=>$userdata['user_id'],"ip_address" => $ip,"download_pdf"=>$fname,"download_ai"=>"","date_modified"=>date("Y-m-d H:i:s"));
					
					$this->common_model->insert(TB_DOWNLOADS,$data_download);
				//}
				
				force_download($name, $data);
				//echo connection_status();
				//die;  
			}
			else{
				redirect("AMW/index");exit;
			}
		}
	}
	
	
	public function downloadAIFile(){
		if(is_densouser_logged_in()){	
			
			$userdata = $this->session->userdata('auth_densouser'); 
			$this->checkUserStatusDenso($userdata['user_id']);
			
			if(isset($_GET['fn'])){ 
				 
				$AttachmentName = base64_decode(urldecode($_GET['fn']));
				 
				$data = file_get_contents(base_url().'ai_files/'.$AttachmentName); // Read the file's contents 
				$name = $AttachmentName;
				$fname = $_GET['fname'];
				
				/*add to download statistics*/
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}
				 
                $cond1 = array("download_ai" => $fname, "ip_address" => $ip,"date(date_modified)"=>date("Y-m-d H:i:s"));
				/*if($this->administration_model->checkDownloadResourceAlreadyVisited($cond1)==0){*/
					$data_download = array("user_id"=>$userdata['user_id'],"ip_address" => $ip,"download_pdf"=>"","download_ai"=>$fname,"date_modified"=>date("Y-m-d H:i:s"));
					
					$this->common_model->insert(TB_DOWNLOADS,$data_download);
				//}
				force_download($name, $data); 
			}
			else{
				redirect("AMW/index");exit;
			}
		}
	}
	
	
	function search(){
		if(is_densouser_logged_in()){

			$this->pagevisited('search');
			$userdata = $this->session->userdata('auth_densouser'); 
			
			$data['title']  = 'Search';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			
			$data['main_content'] = 'frontend/AMW/search';
			
			$data['years'] = $this->administration_model->getAllVehicleYear();
			$status='Active';
			$reg_cat = explode(',', $userdata['v_cat_id']);
			
			$cond1 = array('v_cat_id'=>$reg_cat,'status'=>$status);
			//$cond1 = array('status'=>$status);

			$data['categories'] = $this->administration_model->getAllVehicleCategory($cond1);

			//code for banner
			$userdata = $this->session->userdata('auth_densouser');
			//pr($userdata);  
			 $user_type=$userdata['user_type'];
			 $country=$userdata['country'];
			 $v_cat_id=$userdata['v_cat_id'];
			 $status='Active';
			  
			$cond = array('user_type' => $user_type,'country' => $country,'status'=>$status);
			$bannerData = $this->login_model->fetchBannerForUser(TB_BANNER,"user_type,country,category_id,banner_image,start_date,end_date,status,title",$cond);

			//pr($bannerData[0]);
			$start= $bannerData[0]['start_date']."<br>";
			$end= $bannerData[0]['end_date']."<br>";
			$img= $bannerData[0]['banner_image']."<br>";
			$current_date= date('m/d/Y');
			$cat=$bannerData[0]['category_id'];

			$uary1 = explode(',',$v_cat_id);
    		$bary2 = explode(',',$cat);

    		$myArray = explode(' ', $start);
    		$start1  =$myArray[0];

    		$myArray = explode(' ', $end);
    		$end1  =$myArray[0];

    		$str= implode(' ',array_intersect($uary1,$bary2));
    		if(!empty($str)){
    			if(($start1<= $current_date)&&($end1>=$current_date)&& $str)
				{
					$image= $bannerData[0]['banner_image'];
				    $data['b_image']= UPLOAD_BANNER_PATH.$image;
				}
    		}
    		
			//end for banner code


			$this->load->view('frontend/AMW/incls/layout', $data); 	
		}
		else{
			redirect("index");exit;
		}
	}
	
	
	/*function search_result(){ 
			if(is_densouser_logged_in()){   
				$userdata = $this->session->userdata('auth_densouser'); 
				//$this->checkUserStatusDenso($userdata['user_id']);
				
				 
				
				
				$cond_merge=array();
				$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id"); 
				$cond_car_maker = array();
				$cond_brand = array();
				$cond_model = array();
				$cond_year = array();
				$cond_cat = array();
				$search_text="";
				
				if(isset($_POST['car_maker_part_no']) && $_POST['car_maker_part_no']!=""){
					$car_maker_pn =$_POST['data']['car_maker_part_no'];
					$cond_car_maker = array("car_maker_pn"=>$car_maker_pn);
					//$search_text .="<b>Car Maker Part No.</b>:".$car_maker_pn;
				}
				if(isset($_POST['selectedbrandid']) && $_POST['selectedbrandid']!=""){ 
					$cond_brand= array("`dn_vehicle`.v_brand_id"=>$_POST['selectedbrandid']); 
				}
				if(isset($_POST['selectedmodelid']) && $_POST['selectedmodelid']!=""){ 
					$cond_model = array("`dn_vehicle`.v_model_id"=>$_POST['selectedmodelid']);
				}
				if(isset($_POST['selyear']) && $_POST['selyear']){ 
					$cond_year = array("year"=>$_POST['selyear']);
				}
				$cond_cat = "";
				
				if(isset($_POST['dd_resources']) && $_POST['dd_resources']){ 
			
						if(!empty($_POST['dd_resources'])){
							foreach($_POST['dd_resources'] as $data2):
									$cond_cat .= $data2.',';
							endforeach;
						} 
				}
				
				$cond_sub_cat = "";
				if(isset($_POST['dd_subresources']) && $_POST['dd_subresources']){  
						if(!empty($_POST['dd_subresources'])){
							foreach($_POST['dd_subresources'] as $data2):
									$cond_sub_cat .= $data2.',';
							endforeach;
						} 
				}
				
				$cond_merge = array_merge($cond_car_maker,$cond_brand,$cond_model,$cond_year);
				
				$result = $this->administration_model->getAllVehicleBySearch($cond_merge,$join,$cond_cat,$start,$perPage);
			 
				$table_header .='<div>Total <b>'.count($result).'</b> Record(s) Found</div>';
				 
				$table_header .='
								 <tr class="top_product">
									 <th width="200">Cool Gear Part</th>
									 <th width="100">Brand</th>
									 <th width="200">Model</th>
									 <th width="150">Year</th>
									 <th width="100">Car Maker Part No</th> 
									 <th width="200">Denso Part No</th>
									 
								  </tr>';
				if(count($result)>0){
					
					foreach($result as $row):
						$table .='<tr class="prod_inst" >
									<td><a target="_blank" href="'.base_url().'details/?vid='.urlencode(base64_encode($row['id'])).'">'.$row['cool_gear_pn1'].'</a></td>
									<td>'.$row['brand'].'</a></td>
									<td>'.$row['v_model'].'</td>
									<td>'.$row['year'].'</td>
									<td>'.($row['car_maker_pn']?$row['car_maker_pn']:'-').'</td>
									<td>'.($row['denso_pn']?$row['denso_pn']:'-').'</td>
									
								  </tr>';
					endforeach; 
				}
				else{
					$table .='<tr class="prod_inst">
									<td colspan="6">No Matching Products Available</td> 
								  </tr>';
				}
				$table_header .=''; 
			}
			else{
				//echo "Access Denied";
			} 
			echo json_encode(array('table' => $table,'pagenum'=>$table_header));exit();
	}
	*/

	function export_to_excel(){
		if(is_densouser_logged_in()){   
			$postData1=$this->input->post();

			$arrcols=[];
			$arrcols[]="SrNo";
			if(!empty($postData1['category'])){
				$category= $postData1['category'];
				$arrcols[]="Category";
			}
			if(!empty($postData1['brand'])){
				$brand= $postData1['brand'];
				$arrcols[]="Brand";
			}
			if(!empty($postData1['model'])){
				$model= $postData1['model'];
				$arrcols[]="Model";
			}
			if(!empty($postData1['year'])){
				$year= $postData1['year'];
				$arrcols[]="Year";
			}
			if(!empty($postData1['part_no'])){
				$part_no= $postData1['part_no'];
				$arrcols[]="Denso Part No";
			}
			if(!empty($postData1['cool_gear_part_no'])){
				$cool_gear_part_no= $postData1['cool_gear_part_no'];
				$arrcols[]="COOL GEAR Part No";
			}
			if(!empty($postData1['product_type'])){
				$product_type= $postData1['product_type'];
				$arrcols[]="Product Type";
			}
			if(!empty($postData1['description'])){
				$description= $postData1['description'];
				$arrcols[]="Description";
			}
			if(!empty($postData1['car_maker_part_no'])){
				$car_maker_part_no= $postData1['car_maker_part_no'];
				$arrcols[]="Car Maker Part No";
			}

			$postData=$this->session->userdata('excelData');
			
		   //	pr($postData);

		   //	exit();

			$this->excel->setActiveSheetIndex(0);
	                //name the worksheet
	                $this->excel->getActiveSheet()->setTitle('Denso Export Report');

	                //set cell A1 content with some text

//	                $this->excel->getActiveSheet()->setCellValue('A3', 'Excel Sheet');

	            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);					 
	            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);					 
	            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);					 
	            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);						 
				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);	
				$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
				$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
				$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
				$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
				$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(22);
				
				// $colname=65;
				// $total=count($arrcols)+65;

				//   $i=0;
				//   for($colname; $colname <= $total; $colname++){
				//   	$this->excel->getActiveSheet()->setCellValue(chr($colname).'1',$arrcols[$i]);
				//   	$i++;
				//   }



	                 $this->excel->getActiveSheet()->setCellValue('A1', 'Sr.No.');
	                 if(!empty($category)){
	                	$this->excel->getActiveSheet()->setCellValue('B1', 'Category');
	                }
					if(!empty($brand)){
	                	$this->excel->getActiveSheet()->setCellValue('C1', 'Brand');
	                }
	                if(!empty($model)){
	                	 $this->excel->getActiveSheet()->setCellValue('D1', 'Model');
	                }
	                if(!empty($year)){
	                	$this->excel->getActiveSheet()->setCellValue('E1', 'Year');
	                }
	                if(!empty($part_no)){
	                	$this->excel->getActiveSheet()->setCellValue('F1', 'DENSO Part No');
	                }
	                if(!empty($cool_gear_part_no)){
	                	$this->excel->getActiveSheet()->setCellValue('G1', 'COOL GEAR Part No');
	                }
	                if(!empty($product_type)){
	                	 $this->excel->getActiveSheet()->setCellValue('H1', 'Product type');
	                }
	                if(!empty($car_maker_part_no)){
	                	 $this->excel->getActiveSheet()->setCellValue('I1', 'Car Maker Part No');
	                }
	                if(!empty($description)){
	                	 $this->excel->getActiveSheet()->setCellValue('J1', 'Description.');
	                }

	                //merge cell A1 until C1

//	                $this->excel->getActiveSheet()->mergeCells('A1:C1');

	                //set aligment to center for that merged cell (A1 to C1)

	                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	                //make the font become bold

	                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
	                $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);

	                //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);

	                $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

	       for($col = ord('A'); $col <= ord('I'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

	                 //change the font size
	                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
	                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	        }

	        $cond_merge=array();
				$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id"); 
				//$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id"); 
				$cond_car_maker = array();
				$cond_brand = array();
				$cond_model = array();
				$cond_year = array();
				$cond_cat = array();
				$search_text=""; 
				$cond_search_year = "";
				if(isset($postData['car_maker_part_no']) && $postData['car_maker_part_no']!=""){
					$car_maker_pn =$postData['car_maker_part_no'];
					$cond_car_maker = array("car_maker_pn"=>$car_maker_pn);
					//$search_text .="<b>Car Maker Part No.</b>:".$car_maker_pn;
				}
				if(isset($postData['selectedbrandid']) && $postData['selectedbrandid']!=""){ 
					$cond_brand= array("`dn_vehicle`.v_brand_id"=>$postData['selectedbrandid']); 
				}
				else if(isset($postData['txtbrand']) && $postData['txtbrand']!=""){
					$cond_brand= array("`dn_brand`.brand"=>$postData['txtbrand']); 
				}
				if(isset($postData['selectedmodelid']) && $postData['selectedmodelid']!=""){ 
					$cond_model = array("`dn_vehicle`.v_model_id"=>$postData['selectedmodelid']);
				}
				else if(isset($postData['txtmodel']) && $postData['txtmodel']!=""){
					$cond_brand= array("`dn_model`.v_model"=>$postData['txtmodel']); 
				}
				if(isset($postData['selyear']) && $postData['selyear']){ 
					
					if($postData['selyear'] =="COMMON" || $postData['selyear'] =="common"){
						$cond_search_year = "(year = 'common' || year='COMMON')"; 
					}
					else{
						$explode_year = explode('-',$postData['selyear']);
						
						if(count($explode_year)>1){
							$from_year = $explode_year["0"]; 
							if(is_numeric($from_year)){
								if($explode_year["1"] != ""){
									$to_year = $explode_year["1"];
								}
								else{
									$to_year = date('Y');
								}
								//$cond_year = array("year >=" => $from_year,"year <=" => $to_year);
								$cond_search_year = "((year = 'common' || year='COMMON') ||";
								$cond_search_year .= "(year >= '".$from_year."' AND year <= '".$to_year."'))";
							}
							else{
								//$cond_search_year = "(year = 'common' || year='COMMON') ||";
								$cond_search_year .= "(year = '".$postData['selyear']."')"; 
							}
						}
						
						else{
							
							$cond_search_year = "";
							$common_rec_cond="";
							$cond_search_year .= "(year = '".$postData['selyear']."' ";  
							$all_years = $this->administration_model->getAllVehicleYear();
							$input = $postData['selyear'];
							//print_r($all_years); 
							$from_year1 = '';
							$to_year1 = '';
							if(is_numeric($input)){
								
								
								
								foreach($all_years as $year){ 
								//get the years
								    
									$explode_year1 = explode('-',$year['year']); 
									
										if($explode_year1["1"] != "" && $explode_year1["0"]!=""){
											$from_year1 = $explode_year1["0"];
											$to_year1 = $explode_year1["1"];   
											if($input >= $from_year1 && $input <= $to_year1){
												if($input < date('Y')){
													$common_rec_cond =  "OR (year = 'common' || year='COMMON')";
													$cond_search_year .= " OR (year = '".$from_year1.'-'.$to_year1."')";
												}
											} 
										}
										if($explode_year1["0"] != "" &&  $explode_year1["1"] == ""){
											 
											$from_year1 = $explode_year1["0"];
											$to_year1 = $explode_year1["1"];  
											if($from_year1 <= $input){
												if($input <= date('Y')){
													$common_rec_cond =  "OR (year = 'common' || year='COMMON')";
													$cond_search_year .= " OR (year = '".$from_year1.'-'."')";
												}
											} 
										}  
								}
								//echo $cond_search_year;die;
							}   
							$cond_search_year .= "$common_rec_cond )";  
							
							//$cond_year = array("year" => $postData['selyear']); 
							//echo $cond_search_year;die;
						}
					}
				}
				$cond_cat = "";
				
				if(isset($postData['dd_resources']) && $postData['dd_resources']){ 
			
						if(!empty($postData['dd_resources'])){
							foreach($postData['dd_resources'] as $data2):
									$cond_cat .= $data2.',';
							endforeach;
						} 
				}
				
				$cond_sub_cat = "";
				if(isset($postData['dd_subresources']) && $postData['dd_subresources']){  
						if(!empty($postData['dd_subresources'])){
							foreach($postData['dd_subresources'] as $data2):
									$cond_sub_cat .= $data2.',';
							endforeach;
						} 
				}
				
				$cond_merge = array_merge($cond_brand,$cond_model);
				
				$count = $this->administration_model->getAllVehicleBySearchCount($cond_merge,$cond_search_year,$cond_car_maker,$join,$cond_cat,$cond_sub_cat);
				
				if(!isset($postData["start"]) || $postData["start"]==""){ $postData["start"] = 0; }
				
				
				$cond_id = array("`dn_vehicle`.vehicle_id"=>$postData1['check']);
				$result = $this->administration_model->getAllVehicleBySearchExcel($cond_merge,$cond_search_year,$cond_car_maker,$join,$cond_cat,$cond_sub_cat,$cond_id,$postData["start"]);

			
		         	$i=2;
		         	$sr=1;
					 foreach ($result as $val) 
					 {

			       $this->excel->getActiveSheet()->setCellValue('A'.$i,$sr); 
			       if(!empty($category)){
			       	$this->excel->getActiveSheet()->setCellValue('B'.$i,$val['category'].','.$val['sub_category']); 
			   		}
			   		if(!empty($brand)){
					$this->excel->getActiveSheet()->setCellValue('C'.$i,$val['brand']);
				    }
				    if(!empty($model)){
					$this->excel->getActiveSheet()->setCellValue('D'.$i,$val['v_model']);
				    }
				    if(!empty($year)){
					$this->excel->getActiveSheet()->setCellValue('E'.$i,$val['year']);
				    }
				    if(!empty($part_no)){
					$this->excel->getActiveSheet()->setCellValue('F'.$i,$val['denso_pn']);
				    }
				    if(!empty($cool_gear_part_no)){
					$this->excel->getActiveSheet()->setCellValue('G'.$i,$val['cool_gear_pn1']);
				    }
				    if(!empty($product_type)){
					$this->excel->getActiveSheet()->setCellValue('H'.$i,$val['type']);
				    }
				    if(!empty($car_maker_part_no)){
					$this->excel->getActiveSheet()->setCellValue('I'.$i,$val['car_maker_pn']);
				    }
				    if(!empty($description)){
					$this->excel->getActiveSheet()->setCellValue('J'.$i,$val['vehicle_description']);
				    }
					 	$i++;
					 	$sr++;
					 }

			   




	                //Fill data
	             	//$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');
	                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	                $this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	                $this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	                $this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	                $this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	                $this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	                $this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	                $this->excel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	                $this->excel->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	               // $this->excel->setReadDataOnly(true); 
 
	                $filename='DensoExportReport.xls'; //save our workbook as this file name

	                header('Content-Type: application/vnd.ms-excel'); //mime type
	                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	                header('Cache-Control: max-age=0'); //no cache
	                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
	                //if you want to save it as .XLSX Excel 2007 format
	                
	                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
	                //force user to download the Excel file without writing it to server's HD


	               $objWriter->save('php://output');
	               force_download($filename);
	                // pr($res);
	                // exit;
	               // echo json_encode(array('status' => $res)); exit;
			
		}
	}
	
	
	function search_result1(){ 
			if(is_densouser_logged_in()){   
				$userdata = $this->session->userdata('auth_densouser'); 
				//$this->checkUserStatusDenso($userdata['user_id']);
				$postData = $this->input->post();
				// pr($postData);
				// exit;
				$this->session->set_userdata('excelData',$postData);
				
				$cond_merge=array();
				$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id"); 
				$cond_car_maker = array();
				$cond_brand = array();
				$cond_model = array();
				$cond_year = array();
				$cond_cat = array();
				$search_text=""; 
				$cond_search_year = "";
				if(isset($postData['car_maker_part_no']) && $postData['car_maker_part_no']!=""){
					$car_maker_pn =$postData['car_maker_part_no'];
					$cond_car_maker = array("car_maker_pn"=>$car_maker_pn);
					//$search_text .="<b>Car Maker Part No.</b>:".$car_maker_pn;
				}
				if(isset($postData['selectedbrandid']) && $postData['selectedbrandid']!=""){ 
					$cond_brand= array("`dn_vehicle`.v_brand_id"=>$postData['selectedbrandid']); 
				}
				else if(isset($postData['txtbrand']) && $postData['txtbrand']!=""){
					$cond_brand= array("`dn_brand`.brand"=>$postData['txtbrand']); 
				}
				if(isset($postData['selectedmodelid']) && $postData['selectedmodelid']!=""){ 
					$cond_model = array("`dn_vehicle`.v_model_id"=>$postData['selectedmodelid']);
				}
				else if(isset($postData['txtmodel']) && $postData['txtmodel']!=""){
					$cond_brand= array("`dn_model`.v_model"=>$postData['txtmodel']); 
				}
				if(isset($postData['selyear']) && $postData['selyear']){ 
					
					if($postData['selyear'] =="COMMON" || $postData['selyear'] =="common"){
						$cond_search_year = "(year = 'common' || year='COMMON')"; 
					}
					else{
						$explode_year = explode('-',$postData['selyear']);
						
						if(count($explode_year)>1){
							$from_year = $explode_year["0"]; 
							if(is_numeric($from_year)){
								if($explode_year["1"] != ""){
									$to_year = $explode_year["1"];
								}
								else{
									$to_year = date('Y');
								}
								//$cond_year = array("year >=" => $from_year,"year <=" => $to_year);
								$cond_search_year = "((year = 'common' || year='COMMON') ||";
								$cond_search_year .= "(year >= '".$from_year."' AND year <= '".$to_year."'))";
							}
							else{
								//$cond_search_year = "(year = 'common' || year='COMMON') ||";
								$cond_search_year .= "(year = '".$postData['selyear']."')"; 
							}
						}
						
						else{
							
							$cond_search_year = "";
							$common_rec_cond="";
							$cond_search_year .= "(year = '".$postData['selyear']."' ";  
							$all_years = $this->administration_model->getAllVehicleYear();
							$input = $postData['selyear'];
							//print_r($all_years); 
							$from_year1 = '';
							$to_year1 = '';
							if(is_numeric($input)){
								
								
								
								foreach($all_years as $year){ 
								//get the years
								    
									$explode_year1 = explode('-',$year['year']); 
									
										if($explode_year1["1"] != "" && $explode_year1["0"]!=""){
											$from_year1 = $explode_year1["0"];
											$to_year1 = $explode_year1["1"];   
											if($input >= $from_year1 && $input <= $to_year1){
												if($input < date('Y')){
													$common_rec_cond =  "OR (year = 'common' || year='COMMON')";
													$cond_search_year .= " OR (year = '".$from_year1.'-'.$to_year1."')";
												}
											} 
										}
										if($explode_year1["0"] != "" &&  $explode_year1["1"] == ""){
											 
											$from_year1 = $explode_year1["0"];
											$to_year1 = $explode_year1["1"];  
											if($from_year1 <= $input){
												if($input <= date('Y')){
													$common_rec_cond =  "OR (year = 'common' || year='COMMON')";
													$cond_search_year .= " OR (year = '".$from_year1.'-'."')";
												}
											} 
										}  
								}
								//echo $cond_search_year;die;
							}   
							$cond_search_year .= "$common_rec_cond )";  
							
							//$cond_year = array("year" => $postData['selyear']); 
							//echo $cond_search_year;die;
						}
					}
				}
				$cond_cat = "";
				
				if(isset($postData['dd_resources']) && $postData['dd_resources']){ 
			
						if(!empty($postData['dd_resources'])){
							foreach($postData['dd_resources'] as $data2):
									$cond_cat .= $data2.',';
							endforeach;
						} 
				}
				$cond_sub_cat = "";
				if(isset($postData['dd_subresources']) && $postData['dd_subresources']){  
						if(!empty($postData['dd_subresources'])){
							foreach($postData['dd_subresources'] as $data2):
									$cond_sub_cat .= $data2.',';
							endforeach;
						} 
				}
				
				$cond_merge = array_merge($cond_brand,$cond_model);
				
				$count = $this->administration_model->getAllVehicleBySearchCount($cond_merge,$cond_search_year,$cond_car_maker,$join,$cond_cat,$cond_sub_cat);
				
				if(!isset($postData["start"]) || $postData["start"]==""){ $postData["start"] = 0; }
				
				$result = $this->administration_model->getAllVehicleBySearch($cond_merge,$cond_search_year,$cond_car_maker,$join,$cond_cat,$cond_sub_cat,$postData["start"]);
				$table='';
				if($postData["start"] == 0)
				{
					$table .='<div class="record_found">Total <b>'.$count[0]["cnt"].'</b> Record(s) Found</div>';

					$table .='<form id="export_to_excel" name="export_to_excel" method="post" action="'. base_url().'frontcontroller/export_to_excel"><div class="checkbox-set">
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="category" name="category" value="1"> Category</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="brand" name="brand" value="1"> Brand</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="model" name="model" value="1"> Model</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="year" name="year" value="1"> Year</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="part_no" name="part_no" value="1"> Denso Part No.</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="cool_gear_part_no" name="cool_gear_part_no" value="1"> Cool Gear Part No.</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="car_maker_part_no" name="car_maker_part_no" value="1"> Car Maker Part number </label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="product_type" name="product_type" value="1"> Product Type</label></div>
						<div class="theme-checkboxBlock checkbox"><label><input type="checkbox" id="description" name="description" value="1"> Product Description</label></div>';

					$table .='<input type="submit" id="export_to_excel" class="button search_field " data-option="'.$row['id'].'" value="Export Excel"></div>';

					$table .='<table cellpadding="0" border="0" class="table product_download">   
								<thead>
									<tr class="top_product">
										<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
										 <th width="200">Category</th>
										 <th width="100">Brand</th>
										 <th width="200">Model</th>
										 <th width="150">Year</th>
										 <th width="200">DENSO Part No</th>
										 <th width="200">COOL GEAR Part No.</th>
										 <th width="150">Details</th>
								  	</tr>
								</thead>
							<tbody>';

					$table .='';
				}
				if(count($result)>0){
					
					foreach($result as $row):
						$category = "";
						$comma="";
						if($row["v_cat_id"]!=""){
							 
							$explode_cat = explode(',',$row["v_cat_id"]);
							$i=0;
							$cnt = count($explode_cat);  
							if($cnt>1){  
								foreach($explode_cat as $cat): 
										
									if($i<$cnt-1){
										
										$cond_cat = array("v_cat_id" => $cat);
										$catname = $this->administration_model->getVehicleCatById($cond_cat);
										if($catname["0"]["category"] != ""){
											$comma =",";
										}
										$category .= $catname["0"]["category"].$comma; 
										
									}  
									$i++;
								endforeach; 
							}
							else{
								$cond_cat = array("v_cat_id" => $row["v_cat_id"]);
										$catname = $this->administration_model->getVehicleCatById($cond_cat);
										if($catname["0"]["category"] != ""){
											$comma =",";
										}
										$category .= $catname["0"]["category"].$comma; 
										
							}  
						} 
						$subcategory = "";
						$comma1="";
						if($row["v_sub_cat_id"]!=""){
							
							
							$explode_subcat = explode(',',$row["v_sub_cat_id"]);
							$j=0;
							$cnt2 = count($explode_subcat); 
							if($cnt2>1){  
								foreach($explode_subcat as $subcat):
									
									if($j<$cnt2-1){
										  
										$cond_subcat = array("v_sub_cat_id"=> $subcat);
										$subcatname = $this->administration_model->getAllVehicleSubCategory($cond_subcat);
										if($subcatname["0"]["sub_category"] != ""){
											$comma1 =",";
										}
										$subcategory .= $subcatname["0"]["sub_category"].$comma1; 
										
									}  
									$j++;
								endforeach; 
							}  
							else{
										$cond_subcat = array("v_sub_cat_id"=> $row["v_sub_cat_id"]);
										$subcatname = $this->administration_model->getAllVehicleSubCategory($cond_subcat); 
										if($subcatname["0"]["sub_category"] != ""){
											$comma1 =",";
										}
										$subcategory .= $subcatname["0"]["sub_category"].$comma1;
										 
							}
						}
						
						 
						$table .='<tr class="prod_inst" >
									<td class="text-center" width="10%"><input type="checkbox" value="'.$row["id"].'" name="check[]" class="chk"></td>	
									<td>'.($category!="" || $subcategory!=""?$category.$subcategory:"-").'</td> 
									<td>'.($row['brand']!="" ||$row['brand']!=0?$row['brand']:"-").'</a></td>
									<td>'.($row['v_model']!="" || $row['v_model']!=0?$row['v_model']:"-").'</td>
									<td>'.($row['year']?$row['year']:"-").'</td>
									<td>'.($row['denso_pn']?$row['denso_pn']:'-').'</td>
				 					<td>'.($row['cool_gear_pn1']?$row['cool_gear_pn1']:"-").'</td>
									<td><button type="button" class="button search_field product_view_details" data-option="'.$row['id'].'" data-toggle="modal" data-target="#myModal">View Product</button></td>
								  </tr></form>';
					endforeach; 
					 
					$table .='<tr class="ajax-loading-div"><td><img src="'.base_url().'img/ajax-loader.gif" /></td></tr>';
				}
				if($postData["start"] == 0)
				{
					if(count($result)==0)
					{
					$table .='<tr class="prod_inst">
									<td colspan="8">No Matching Products Available</td> 
								  </tr>';
					}
					$table .='</tbody>
						</table>';
				}
				 
			}
			else{
				//echo "Access Denied";
			} 
			
			$config = get_pagination_config($postData["start"], $count[0]["cnt"], '', 'desc', "searchRecord");
			$this->pagination->initialize($config);
			
			$to = $postData["start"]+PER_PAGE;			
			
			if($to > $count[0]["cnt"])
			{
				$to = $count[0]["cnt"];
				$paginate = $to;
			}
			else
			{
				$paginate = $to;
			}
			
			echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
	}
	 
	function getViewDetailsByProduct(){
		if(is_ajax_request())
		{
			if(is_densouser_logged_in()){
				$postData = $this->input->post(); 
				$userdata = $this->session->userdata('auth_densouser');
				$id = $postData["id"];
				$cond = array("vehicle_id" =>$postData["id"]);
				$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id");
				$vehicledetails = $this->administration_model->getAllVehiclesDetails($cond,$join);
				$html = '';
				if(count($vehicledetails)>0){
					
					if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
						$ip = $_SERVER['HTTP_CLIENT_IP'];
					} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					} else {
						$ip = $_SERVER['REMOTE_ADDR'];
					} 
					$cond1 = array("user_id" => $userdata['user_id'],"search_part_id" => $id, "ip_address" => $ip,"date(date_modified)" => date('Y-m-d'));
					if($this->administration_model->checkSearchAlreadyExists($cond1)==0){ 
						$this->common_model->insert(TB_VIEWED_SEARCH,array("user_id" => $userdata['user_id'],"ip_address" => $ip, "search_part_id" => $id, "date_modified" => date('Y-m-d H:i:s')));
					}	
					
				$html .='<div class="container detail_page">
						 <div class="row">
						 <div class="col-lg-6">';
						 
						 if($vehicledetails[0]["v_image"]!="" && file_exists(realpath('vehicle_parts').'/'.$vehicledetails[0]["v_image"])){
							$html .='<img alt="'.$vehicledetails[0]["v_image"].'" title="'.$vehicledetails[0]["v_image"].'" src="'.base_url().'vehicle_parts/'.$vehicledetails[0]["v_image"].'">';
						 }else{
							$html .='<img alt="No Image" src="'.base_url().'images/no-image.jpg">';
						 }
						 
						 $html .='<div class="car_disc">
							 <div class="car_name"><strong>Additional Info</strong></div>
							 <p class="discription_text">'.($vehicledetails[0]["vehicle_description"]?$vehicledetails[0]["vehicle_description"]:'-').'</p>
						 </div>
						 </div>
						  <div id="inform" class="col-lg-6">
							
						  <div class="car_name">'.(($vehicledetails[0]["brand"] || $vehicledetails[0]["v_model"])?$vehicledetails[0]["brand"]." ".$vehicledetails[0]["v_model"]:"").'</div>
						  <p class="discription_text"></p>
						  <div class="detail_info">
							<ul class="car_detail_struc"> 	
								<li>		 
								<span>Vehicle Brand</span>
								<p>'.($vehicledetails[0]["brand"]?$vehicledetails[0]["brand"]:"-").'</p>
								</li>
								<li>
								<span>Vehicle Model</span>
								<p>'.($vehicledetails[0]["v_model"]?$vehicledetails[0]["v_model"]:"-").'</p>
								</li>
								<li>
								<span>Vehicle Year	</span>
								<p>'.($vehicledetails[0]["year"]?$vehicledetails[0]["year"]:"-").'</p>
								</li>
								<li>
								<span>Car Maker Part No.</span>
								<p>'.($vehicledetails[0]["car_maker_pn"]?$vehicledetails[0]["car_maker_pn"]:"-").'</p>
								</li>
								<li>
								<span>DENSO Part No.</span>
								<p>'.($vehicledetails[0]["denso_pn"]?$vehicledetails[0]["denso_pn"]:"-").'</p>
								</li>
								<li>
								<span>COOL GEAR Part No.</span>
								<p>'.($vehicledetails[0]["cool_gear_pn1"]?$vehicledetails[0]["cool_gear_pn1"]:"-").'</p>
								</li>
								<li>
								<span>Product Type</span>
								<p>'.($vehicledetails[0]["type"]?$vehicledetails[0]["type"]:"-").'</p>
								</li>

								<li>
								<span>Description</span>
								<p>'.($vehicledetails[0]["color"]?$vehicledetails[0]["color"]:"-").'</p>
								</li>
							</ul> 
						  </div> 
						  </div> 
						 </div>
						 </div>';
						}
				echo json_encode(array("prod_html" => $html));exit;
			}
		}
	}
	
	function autosuggetion_result_brand(){
			if(is_densouser_logged_in()){
				$cond = array();
				$data = array();
				$brands = $this->administration_model->getAllVehicleBrandAuto($cond);
				
				foreach($brands as $row):
						$data[] = $row; 
				endforeach; 

				# JSON-encode the response
				echo json_encode($data);exit;
			} 
	}
	
	
	function autosuggetion_result_model(){
		 
			if(is_densouser_logged_in()){  
				$cond = array();
				$data = array();
				$models = $this->administration_model->getAllVehicleModelAuto($cond);
				
				foreach($models as $row):
						$data[] = $row; 
				endforeach;
				  
				# JSON-encode the response
				echo json_encode($data);exit;
			} 
	}
	
	function autosuggetion_result_model_bybrand(){
		 
			if(is_densouser_logged_in()){  
				$postdata = file_get_contents("php://input");
				$request = json_decode($postdata);
				$brid = $request->brid;
				$cond = array("v_brand_id" => $brid);   
				  
				$data = array();
				$models = $this->administration_model->getAllVehicleModelAuto($cond);
				
				foreach($models as $row):
						$data[] = $row; 
				endforeach;
				  
				# JSON-encode the response
				echo json_encode($data);exit;
			} 
	}
	
	function autosuggetion_result_year(){
		 
			if(is_densouser_logged_in()){  
				$cond = array();
				$data = array();
				$models = $this->administration_model->getAllVehicleYearAuto($cond);
				
				foreach($models as $row):
						$data[] = $row; 
				endforeach;
				  
				# JSON-encode the response
				echo json_encode($data);exit;
			} 
	}
	
	function autosuggetion_result_car_maker(){
		 
			if(is_densouser_logged_in()){ 
				$cond = array();
				$data = array();
				//$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id");
				$join = array();
				$vehicles = $this->administration_model->getAllVehicleCarPartNumber($cond,$join);
				
				foreach($vehicles as $row):
						$data[] = $row; 
				endforeach;
				  
				# JSON-encode the response
				echo json_encode($data);exit;
			} 
	}
	
	function details(){
		if(is_densouser_logged_in()){
			$data['title']  = 'Product Details';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/detail';
			$userdata = $this->session->userdata('auth_densouser');
			$this->pagevisited('Product Details');
			$this->checkUserStatusDenso($userdata['user_id']);
			if(isset($_GET['vid']) && $_GET['vid']!=""){
				
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}
				$vid  = urldecode(base64_decode($_GET['vid']));
				$cond1 = array("user_id" => $userdata['user_id'],"search_part_id" => $vid, "ip_address" => $ip,"date(date_modified)" => date('Y-m-d'));
				if($this->administration_model->checkSearchAlreadyExists($cond1)==0){ 
					$this->common_model->insert(TB_VIEWED_SEARCH,array("user_id" => $userdata['user_id'],"ip_address" => $ip, "search_part_id" => $vid, "date_modified" => date('Y-m-d H:i:s')));
				}
				
				
				
				$cond = array("vehicle_id"=>$vid);
				$join = array(TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id");
				$vdetails = $this->administration_model->getAllVehiclesDetails($cond,$join);
				
				$data['vehicledetails'] = $vdetails["0"];
				$this->load->view('frontend/incls/layout', $data); 	
			}
			else{
				redirect("search");exit;
			}
		}
		else{
			redirect("index");exit;
		}
	} 
	  
	function pagevisited($pagename){ 
		if(is_densouser_logged_in()){
			$userdata = $this->session->userdata('auth_densouser'); 
			//$this->checkUserStatusDenso($userdata['user_id']);
			$postData = $pagename;
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			
			$cond1 = array("user_id" => $userdata["user_id"],"page_name" => $postData, "ip_address" => $ip,"date(date_added)" => date("Y-m-d"));
			if($this->administration_model->checkPageAlreadyVisited($cond1)==0){
				$id = $this->common_model->insert(TB_VIEWED_PAGE,array("user_id" => $userdata["user_id"],"ip_address" => $ip,"page_name" => $postData,"date_added" => date("Y-m-d H:i:s")));				
				
			}
			//echo json_encode($id);
		}
	}  
	
	/*DENSO PHASE II Starts*/
	
	/*
	 * 
	 * Sales Kit sections
	 * 
	 * 
	 * */
	
	
	function sales_kit_home(){
		//if(is_densouser_logged_in()){
			$data['title']  = 'Welcome To Products Market Kit';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/PMK/index';
			$this->pagevisited('skwhome');
			$userdata = $this->session->userdata('auth_densouser'); 
			//$this->checkUserStatusDenso($userdata['user_id']);
			
			$html="";
			$order = array("category" => "ASC");
			$limit = array();
			$category_list =  $this->administration_model->getAllVehicleCategoryOrder($order,$limit);
		//	print_r($category_list);die;
			if(!empty($category_list)){
				$html .= '<ul class="nav navbar-nav">';
							foreach($category_list as $cat):
							
							$cond_page  = array("category" =>$cat['id'],"lang_id"=>'1');
							$page_id = $this->administration_model->getPagesDataSKSectionsResources($cond_page);
							//print_r($page_id);die;
							if($page_id[0]['id']!=""){
								$pageid = $page_id[0]['id'];
							}
							else{
								$pageid = "";
							}
						    $html .= '<li>
									  <a href="'.base_url().'PMK/sections-category/?cat_id='.$cat['id'].'&id='.$pageid.'">'.$cat['category'].'</a>
										<div class="dropdown">
											<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
											<span class="caret"></span></button>';
											
											$cond_subcat = array("v_cat_id" => $cat['id']);
											$subcat_list = $this->administration_model->getAllVehicleSubCategory($cond_subcat);
											if(!empty($subcat_list)){
												
												$html .= '<ul class="dropdown-menu">';
													foreach($subcat_list as $subcat):
																$html .= '<li class="active"><a href="'.base_url().'PMK/sections-sub-category/?cat_id='.$subcat['v_cat_id'].'&sub_cat_id='.$subcat['id'].'&id='.$pageid.'">'.$subcat['sub_category'].'</a></li>';
													endforeach;		  
												$html .= '</ul>';	
											}
							$html .='</div>
								  </li>';
							endforeach;
				$html .='</ul>';
				
			}
			
			$order = array("category" => "ASC");
			
			$category_id =  $this->administration_model->getAllVehicleCategoryOrderLimit($order);
			$data['cid']  = $category_id[0]['id'];
			$cond_page = array("lang_id"=>'1');
			$section = $this->administration_model->getAllPagesDataSKLimit($cond_page);
			
			$data['sname']  = $section[0]['section_name'];
			$join = array(TB_CATEGORY => TB_PAGES.".category = ".TB_CATEGORY.".v_cat_id",TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id");
			$cond_page_lang = array("dn_pages.category"=>$category_id[0]['id']); 
			
			$sections = $this->administration_model->getPagesDataSKSections($cond_page_lang, $join);
			
			$section_html = "";
			$lang = "";
			$i=0;
			if(!empty($sections)){
				foreach($sections as $row):
						if($i==0)
							$active = 'class="active"';
						else
							$active = '';	
							
						$section_html .= '<li '.$active.' role="presentation"><a href="#Category_'.$i.'" aria-controls="Category" role="tab" data-toggle="tab">'.$row['section_name'].'</a></li>';
						
						$lang .= '<option value="'.$row['lang_id'].'">'.$row['langauage'].'</option>';
							
						$i++;
				endforeach;
			}
			
			//die;
			$data['cat_html'] = $html;
			
			$this->load->view('frontend/PMK/incls/layout', $data); 	
			
		//}
		
	}
	
	
	function section_by_category(){
			if(is_densouser_logged_in()){
					
					$userdata = $this->session->userdata('auth_densouser');

					$this->checkUserStatusDenso($userdata['user_id']);
					$user_id = $userdata['user_id']; 					
					/*get country Id*/					
					$cond_country = array("user_id" => $user_id);
					$country_id = $this->administration_model->getUserById($cond_country);
			}	
			else{
					$user_id = ""; 
			}
			
			$data['title']  = 'Welcome To Sales Kit';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/PMK/sections_cat';
			
			$html="";
			$order = array("category" => "ASC");
			$limit = array();
			$category_list =  $this->administration_model->getAllVehicleCategoryOrder($order,$limit);

			 // pr($category_list);
		  //    die;
			$cat_id = (isset($_GET['cat_id'])?$_GET['cat_id']:'');
			
			$cond_page  = array("cat_id" => $cat['id'], "lang_id" => '1');
			$page_id = $this->administration_model->getPagesDataBySectionsResources($cond_page);
			
			  
			if(!empty($category_list)){
				
				$class_cat_active = "";
				
				$html .= '<ul class="nav navbar-nav">';
					foreach($category_list as $cat):
					$cond_page  = array("cat_id" =>$cat['id'],"lang_id"=>'1');
							$page_id = $this->administration_model->getPagesDataBySectionsResources($cond_page);
							//print_r($page_id);die;
							if($page_id[0]['id']!=""){
								$pageid = $page_id[0]['id'];
							}
							else{
								$pageid = "";
							}
					if($cat['id'] ==  $cat_id){
						$class_cat_active = 'class="active"';
					}
					else{
						$class_cat_active = '';
					}
					
					$html .= '<li '.$class_cat_active.'>
							  <a id="'.$cat['category'].'" href="'.base_url().'PMK/sections-category/?cat_id='.$cat['id'].'&id='.$pageid.'">'.$cat['category'].'</a>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
									<span class="caret"></span></button>';
									
									$cond_subcat = array("v_cat_id" => $cat['id']);
									$subcat_list = $this->administration_model->getAllVehicleSubCategory($cond_subcat);
									// pr($subcat_list);
									// die();
									if(!empty($subcat_list)){
										
										$html .= '<ul class="dropdown-menu">';
											foreach($subcat_list as $subcat):
														$html .= '<li class="active"><a href="'.base_url().'PMK/sections-sub-category/?cat_id='.$subcat['v_cat_id'].'&sub_cat_id='.$subcat['id'].'&id='.$pageid.'">'.$subcat['sub_category'].'</a></li>';
											endforeach;		  
										$html .= '</ul>';	
									}
					$html .='</div>
						  </li>';
					endforeach;
				$html .='</ul>';
				
			}
			
			$order = array("category" => "ASC");
			
			$cond_name = array("v_cat_id" => $cat_id);
			
			$cat_name = $this->administration_model->getAllVehicleCategory($cond_name);
			// pr($cat_name);
			// exit;

			//$sub_cat_id = (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:'');
			
			$join = array(TB_CATEGORY => TB_SECTION.".cat_id = ".TB_CATEGORY.".v_cat_id",TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id",TB_SECTION => TB_PAGES.".section_id = ".TB_SECTION.".s_id");
			if($user_id != ""){
				$cond_page_lang = array( "dn_section.cat_id" => $cat_id, "dn_pages.lang_id" => '1',TB_CATEGORY.".status_pmk" => "Active"); 
			}
			else{
				$cond_page_lang = array( "dn_section.category" => $cat_id,"dn_pages.lang_id" => '1', "public_access" => '1',TB_CATEGORY.".status_pmk" => "Active"); 
			}

			// pr($cond_page_lang);
			// die();
			
			
			$sections = $this->administration_model->getPagesDataSKSections($cond_page_lang, $join);
			echo $this->db->last_query();
			exit;
			$section_html = "";
			$page_description = "";
			$resources_html="";
			$lang = "";
			$i=1;
			$section_array = array();
			if(!empty($sections)){
				$sname="";
				$lang_text="";
				foreach($sections as $row):
						array_push($section_array,$row['section_name']);
						if($i==1)
							$active = 'active';
						else
							$active = '';	
						
						if($sname != $row['section_name']){
							$sname = $row['section_name'];
							$attch_section = str_replace(" ","_",$row['section_name']);
							$section_html .= '<li  class="sections_li '.$active.'" role="presentation"><a id="'.str_replace(" ","_",$row['section_name']).'" data-id="'.$row['id'].'"  href="#Category_'.$row['lang_id'].'_'.$attch_section.'" aria-controls="Category" role="tab" data-toggle="tab" data-attr="'.$row['section_name'].'">'.$row['section_name'].'</a></li>';
						 
							if(!empty($lang_res)){
								//$lang .= '<option value="'.$row['lang_id'].'">'.$row['language'].'</option>';
							}
						}
						if($cat_name[0]['background_img'] != ""){ 
							$background_img = base_url()."/background_img/".$cat_name[0]['background_img'];
						}
						else{
							$background_img = "";	
						}
						
						if($row['lang_id'] == 1){
							$page_description .='<div style="background: rgba(0, 0, 0, 0) url('.$background_img.') no-repeat fixed center top;overflow: auto;
   
    "  data-cat = "'.$row['section_name'].'_'.$row['lang_id'].'" role="tabpanel" class="tab-pane '.$active.'"  id="Category_1_'.$attch_section.'" >'.$row['page_data'].'
								</div>
							   ';
						}
						 
													
						$i++;
				endforeach;
			}
			//echo $page_description;
			//die;
			
			
			
			$data['cat_html'] = $html;
			
			if($section_html != ""){
				$data['section_html'] = $section_html;
				$data['section_array'] = $section_array;
				//$data['languages']  = $lang;
				$data['cat_name'] = $cat_name[0]['category'];
				$data['page_description'] = $page_description;
				//echo $resources_html;
				//$data['resources_html'] = $resources_html;
				//print_r($data['resources_html']);die;
				
			}
			else{			
				$data['section_html'] = "no_rec";
			}
			//print_r($data['section_html']);die;
			//die;
			$this->load->view('frontend/PMK/incls/layout', $data); 	
			
		//}
		
	}
	
	
	function section_by_sub_category(){
		
			if(is_densouser_logged_in()){
					
					$userdata = $this->session->userdata('auth_densouser');
					$this->checkUserStatusDenso($userdata['user_id']);
					$user_id = $userdata['user_id']; 					
					/*get country Id*/					
					$cond_country = array("user_id" => $user_id);
					$country_id = $this->administration_model->getUserById($cond_country);
			}	
			else{
					$user_id = ""; 
			}
			
			$data['title']  = 'Welcome To Sales Kit';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/PMK/sections_sub_cat';
			
			$html="";
			$order = array("category" => "ASC");
			$limit = array();
			$category_list =  $this->administration_model->getAllVehicleCategoryOrder($order,$limit);
			$cat_id = (isset($_GET['cat_id'])?$_GET['cat_id']:'');
			$sub_cat_id = (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:'');
			$section_array = array();
			if(!empty($category_list)){

				
				$class_cat_active = "";
				
				$html .= '<ul class="nav navbar-nav">';
					foreach($category_list as $cat):
					
					$cond_page  = array("category" =>$cat['id'],"lang_id"=>'1');
					$page_id = $this->administration_model->getPagesDataSKSectionsResources($cond_page);
					//print_r($page_id);die;
					if($page_id[0]['id']!=""){
						$pageid = $page_id[0]['id'];
					}
					else{
						$pageid = "";
					}
					
					if($cat['id'] ==  $cat_id){
						$class_cat_active = 'class="active"';
					}
					else{
						$class_cat_active = '';
					}
					
					$html .= '<li '.$class_cat_active.'>
							  <a href="'.base_url().'PMK/sections-category/?cat_id='.$cat['id'].'&id='.$pageid.'">'.$cat['category'].'</a>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
									<span class="caret"></span></button>';
									
									$cond_subcat = array("v_cat_id" => $cat['id']);
									$subcat_list = $this->administration_model->getAllVehicleSubCategory($cond_subcat);
									if(!empty($subcat_list)){
										
										$html .= '<ul class="dropdown-menu">';
											foreach($subcat_list as $subcat):
														$html .= '<li class="active"><a href="'.base_url().'PMK/sections-sub-category/?cat_id='.$subcat['v_cat_id'].'&sub_cat_id='.$subcat['id'].'&id='.$pageid.'">'.$subcat['sub_category'].'</a></li>';
											endforeach;		  
										$html .= '</ul>';	
									}
					$html .='</div>
						  </li>';
					endforeach;
				$html .='</ul>';
				
			}
			
			$order = array("category" => "ASC");
			$cond_name = array("v_cat_id" => $cat_id);
			$cat_name = $this->administration_model->getAllVehicleCategory($cond_name);
			$cond_subcat = array("v_sub_cat_id" => $sub_cat_id);
			
			$subcat_name = $this->administration_model->getAllVehicleSubCategory($cond_subcat);
			//print_r($subcat_name);die;
			//$sub_cat_id = (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:'');
			
			$join = array(TB_CATEGORY => TB_PAGES.".category = ".TB_CATEGORY.".v_cat_id",TB_SUBCATEGORY => TB_PAGES.".sub_category = ".TB_SUBCATEGORY.".v_sub_cat_id",TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id");
			if($user_id != ""){
				$cond_page_lang = array( "dn_pages.category" => $cat_id,"dn_pages.lang_id" => '1',"dn_pages.sub_category" => $sub_cat_id,TB_CATEGORY.".status_pmk" => "Active"); 
			}
			else{
				$cond_page_lang = array( "dn_pages.category" => $cat_id,"dn_pages.lang_id" => '1',"dn_pages.sub_category" => $sub_cat_id, "public_access" => '1',TB_CATEGORY.".status_pmk" => "Active"); 
			}
			
			
			$sections = $this->administration_model->getPagesDataSKSubcatSections($cond_page_lang, $join);
			//echo $this->db->last_query();die;
			$section_html = "";
			$page_description = "";
			$lang = "";
			$resources_html="";
			
			
			$i=1;
			if(!empty($sections)){
				$sname="";
				$lang_text="";
				$page_description="";
				foreach($sections as $row):
				array_push($section_array,$row['section_name']);
						if($i==1)
							$active = 'active';
						else
							$active = '';	
						
						if($sname != $row['section_name']){
							$sname = $row['section_name'];
							$attch_section = str_replace(" ","_",$row['section_name']);
							$section_html .= '<li  class="sections_li '.$active.'" role="presentation"><a  data-id="'.$row['id'].'" href="#Category_'.$row['lang_id'].'_'.$attch_section.'" aria-controls="Category" role="tab" data-toggle="tab" data-attr="'.$row['section_name'].'">'.$row['section_name'].'</a></li>';
						 
							if(!empty($lang_res)){
								//$lang .= '<option value="'.$row['lang_id'].'">'.$row['language'].'</option>';
							}
						}
						if($subcat_name[0]['background_img'] != ""){ 
							$background_img = base_url()."/background_img/".$subcat_name[0]['background_img'];
						}
						else{
							$background_img = "";	
						}
						
						if($row['lang_id'] == 1){
							$page_description .='<div style="background: rgba(0, 0, 0, 0) url('.$background_img.')  no-repeat fixed center top;overflow: auto;
   
    "   data-cat = "'.$row['section_name'].'_'.$row['lang_id'].'" role="tabpanel" class="tab-pane '.$active.'"  id="Category_1_'.$attch_section.'" >'.$row['page_data'].'
								</div>
							   ';
						}
						else{
							/*$page_description .='<div style="background: rgba(0, 0, 0, 0) url('.$background_img.') no-repeat scroll center center;
   
    float: left;" role="tabpanel" data-cat = "'.$row['section_name'].'_'.$row['lang_id'].'" class="tab-pane" id="Category_'.$i.'">'.$row['page_description'].'
								</div>
							   ';*/
						}
													
						$i++;
				endforeach;
				//die;
			}
			
			//die;
			$data['cat_html'] = $html;
			
			if($section_html != ""){
				$data['section_html'] = $section_html;
				$data['section_array'] = $section_array;
				//$data['languages']  = $lang;
				$data['cat_name'] = $cat_name[0]['category'];
				$data['sub_cat_name'] = $subcat_name[0]['sub_category'];
				
				$data['page_description'] = $page_description;
				
				//print_r($data['page_description']);die;
				$data['resources_html'] = $resources_html;
				
			}
			else{			
				$data['section_html'] = "no_rec";
			}
			//print_r($data['section_html']);die;
			
			$this->load->view('frontend/PMK/incls/layout', $data); 	
			
		//}
		
	}
	
	function list_section(){
		if(is_ajax_request()){
			
			/*if(is_densouser_logged_in()){
				
			}*/
			
			if(is_densouser_logged_in()){
					
					$userdata = $this->session->userdata('auth_densouser');
					$this->checkUserStatusDenso($userdata['user_id']);
					$user_id = $userdata['user_id']; 					
					/*get country Id*/					
					$cond_country = array("user_id" => $user_id);
					$country_id = $this->administration_model->getUserById($cond_country);
			}	
			else{
					$user_id = ""; 
			}
			
			//echo $country_id[0]['country'];
			
				$postData = $this->input->post(); 
				
				$cat_id = $postData['cat_id'];
				$page_id = $postData['page_id'];
				$sub_cat_id = "";
				if(isset($postData['sub_cat_id']) && $postData['sub_cat_id'] != ""){
					$sub_cat_id = $postData['sub_cat_id'];
				}
				
				$section_name = $postData['section_name'];
				$attch_section = str_replace(" ","_",$section_name);
				$join_lang = array(TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id");
				if($sub_cat_id != ""){
					$cond_lang = array("section_name" => $section_name, "category" => $cat_id, "sub_category" => $sub_cat_id);
					$lang_res = $this->administration_model->getPagesSubCatSectionLangauages($cond_lang,$join_lang);
				}
				else{
					$cond_lang = array("section_name" => $section_name, "category" => $cat_id);
					
					$lang_res = $this->administration_model->getPagesSectionLangauages($cond_lang,$join_lang);
				}
				
				$lang = "";
				
				foreach($lang_res as $row):
						$lang .= '<option value="'.$row['lang_id'].'">'.$row['language'].'</option>';
				endforeach; 
				
				$page_descr = "";
				
				$lang_id = $postData['lang_id'];
				if($lang_id != ""){
					
					//$sub_cat_id = (isset($_GET['sub_cat_id'])?$_GET['sub_cat_id']:'');
					
					if($sub_cat_id != ""){ 
						$cond_page_lang = array("section_name" => $section_name,"dn_lang.lang_id" => $lang_id,"dn_pages.category" => $cat_id,"dn_pages.sub_category" => $sub_cat_id);
						
						$join = array(TB_CATEGORY => TB_PAGES.".category = ".TB_CATEGORY.".v_cat_id",TB_SUBCATEGORY => TB_PAGES.".sub_category = ".TB_SUBCATEGORY.".v_sub_cat_id",TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id");
						$page_description = $this->administration_model->getPagesDataSKSubcatSections($cond_page_lang, $join);
					}
					else{
						$cond_page_lang = array("section_name" => $section_name,"dn_lang.lang_id" => $lang_id,"dn_pages.category" => $cat_id);
						$join = array(TB_CATEGORY => TB_PAGES.".category = ".TB_CATEGORY.".v_cat_id",TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id");
						$page_description = $this->administration_model->getPagesDataSKSections($cond_page_lang, $join);
						//echo $this->db->last_query();
					}
					
					
					if($lang_id == 1){
						$page_descr = $page_description[0]['page_data'];
					}
					else{
						$page_descr = $page_description[0]['page_description'];
					}
				}
				
				$resources_html="";
				if($sub_cat_id != ""){
					$cond_page  = array("lang_id" => 1, "category"=>$cat_id ,"sub_category"=>$sub_cat_id,"section_name" => $section_name); 
					$sections_resources = $this->administration_model->getPagesDataSKSectionsSubCatResources($cond_page);
				}
				else{
					$cond_page  = array("lang_id" => 1, "category"=>$cat_id, "section_name" => $section_name);
					$sections_resources = $this->administration_model->getPagesDataSKSectionsCatResources($cond_page);
				}
				
				//echo $this->db->last_query();
				if(!empty($sections_resources)){
						$resources_ids = substr($sections_resources[0]['resources_id'],0,-1);//	die;
						//echo $country_id[0]['country'];
						if(isset($country_id) && !empty($country_id) && $resources_ids != ""){
							
							
							$resources_res = $this->administration_model->getAllResourcesSKCheck($resources_ids,$user_id,$country_id[0]['country']);
						//	echo $this->db->last_query();//die;
							if(!empty($resources_res)){
									$link = "";
								
									foreach($resources_res as $row):	
										$link = base_url()."sk_files/".$row['resources_sk_pdf'];
										$resources_html .= "<br/><a target='_blank' class='resources_link' href='".$link."'>".$row['resources_sk_pdf']."</a><br/>";
									endforeach;
							}
							//echo $this->db->last_query();die;
						}
				}
				//echo $resources_html;die;
				echo json_encode(array("lang" => $lang, "page_description" => $page_descr, "resources" => $resources_html, "section" => $attch_section));exit;
				
			
		}
	}	
	
}
?>
