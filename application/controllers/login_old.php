<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("administration_model");
		$this->load->library("form_validation");
	}
	
	public function index()
	{ 
		if(is_user_logged_in())
		{
			redirect("administration");
			exit;
		}
		else if($this->input->post())
		{
			$postData = $this->input->post();
			$this->session->set_flashdata("postData", $postData);
			if($postData["txt_company"] == "" && $postData["txt_username"] == "" && $postData["txt_password"] == "")
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Company identifier, Username & Password.</div>');
				redirect(); exit;
			}
			else if($postData["txt_company"] == "")
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Company identifier.</div>');
				redirect(); exit;
			}
			else if($postData["txt_username"] == "")
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Username.</div>');
				redirect(); exit;
			}
			else if($postData["txt_password"] == "")
			{   
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Password.</div>');
				redirect(); exit;
			}
			
			$cond = array("company_identifier" => $postData["txt_company"],"username" => $postData["txt_username"],"password" => $postData["txt_password"],"is_active" => "Y");
			$join = array(TB_COMPANY => TB_COMPANY_USER.".idcompany = ".TB_COMPANY.".idcompany");
			$userData = $this->login_model->validUser(TB_COMPANY_USER,"username,".TB_COMPANY.".idcompany AS company_id,".TB_COMPANY_USER.".idcompany_user AS userid,".TB_COMPANY.".name AS company_name,".TB_COMPANY_USER.".iduserrole AS role",$cond,$join);
			if(count($userData) > 0)
			{
				$this->session->set_userdata("auth_user", $userData[0]);
				redirect("administration");
				exit;
			}
			else
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">The Username or Password do not match, Please try again.</div>');
				redirect(); exit;
			}
		}
		else
		{
			$this->load->view("login");
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(); exit;
	}
	public function save_password()
	{   
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{
			$postData = $this->input->post();
			$userid = get_user_data("userid");
			//echo $userid;die;
			$cond = array('password' => $postData['currentPassword'],'idcompany_user'=>$userid);
			$currentUser = $this->login_model->validUser(TB_COMPANY_USER,'password',$cond);
		
			if($postData['currentPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter current password.</div>')); exit;
			}
			else if($postData['newPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter new password.</div>')); exit;
			}
			else if($postData['confirmPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter confirm password.</div>')); exit;
			}
			else if($postData['confirmPassword'] != $postData['newPassword'])
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>New password and Confirm password must be same.</div>')); exit;
			}
			else if(strlen($postData['newPassword']) < 5)
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Password must be more than 5 characters.</div>')); exit;
			}
			else if($currentUser[0]['password'] != $postData['currentPassword'])
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter correct current password.</div>')); exit;
			}
			else if($currentUser[0]['password'] == $postData['newPassword'])
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>New password and old password can\'t be same.</div>')); exit;
			}
			else
			{
				$cond = array("idcompany_user" => $userid);
				$updateData = array("password" => $postData['newPassword']);
				$result = $this->common_model->update(TB_COMPANY_USER,$cond,$updateData);
				if($result)
				{
					echo json_encode(array('status' => 'success','msg' => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password has been updated successfully.</div>')); exit;
				}
				else
				{
					echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Error occured during updating Password.</div>')); exit;
				}
			}
		}
	}
	
	public function taxAuthorities()
	{ 
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$cond = array("idcompany" => get_user_data("company_id"));
			$data["taxauthorities"] = $this->administration_model->getTaxAuthorities($cond);
			$this->load->view("administrator/taxauthorities",$data);
		}
	}
	
	public function list_tax_authorities()
	{ 	if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$cond = array("idcompany" => get_user_data("company_id"));
				$like = array();
				if($postData["search"] != "" && $postData["searchBy"] != "")
				{
					$like = array("taxauthority" => trim($postData["search"])); 
				}
				//print_r($like);die;
				$count = $this->administration_model->getTaxAuthoritiesCount($cond,$like);
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				//if(!isset($postData["order_column"])){ $order_column = "seq_number"; }else{ $order_column = $postData["order_column"]; }
				
				$orderColumns = array("taxauthority");
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "idcompany_tax_authority";
					$postData["order"] == "ASC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$taxauthorities = $this->administration_model->getTaxAuthoritiesPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
					$table = '<table class="table table-striped table-hover dataTable no-footer" cellspacing="0" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="sorting" width="60%" class="'.$css[0].'" width="60%" onclick="changePaginate(0,\'taxauthority\',\''.$corder[0].'\')">Tax Authority</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($taxauthorities)>0){
							if(count($taxauthorities) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
							foreach($taxauthorities as $types) { 
						$table .= '<tr id="row_'.$types["id"].'">
								<td>'.$types["taxauthority"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" class="editTaxAuthorities" data-option="'.$types["id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									</td>
							  </tr>';
							}
						}
				if($postData["start"] == 0)
				{
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getTaxAuthoritiesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function save_tax_authorities()
	{ 
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(27);
				
				$postData = $this->input->post();
				if($postData["taxauthoritiesId"] == "")
				{
					if($permissionArr["permission_add"])
					{
						$isValid = $this->validateTaxAuthorities($postData);
						if($isValid["status"] == 1)
						{
							$insertId = $this->common_model->insert(TB_TAX_AUTHORITY,array("taxauthority" => $postData["taxdescription"],"idcompany" => get_user_data("company_id")));
								if($insertId)
								{
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Tax Authorities has been added successfully...!!!</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
						}
						else
						{
							echo json_encode($isValid);
						}
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "add", "msg"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You do not have permission to accsess this.</div>')); exit;
					}
				}
				else
				{
					if($permissionArr["permission_update"])
					{
						$isValid = $this->validateTaxAuthorities($postData);
						if($isValid["status"] == 1)
						{
							$updateArr = $this->common_model->update(TB_TAX_AUTHORITY,array("idcompany" => get_user_data("company_id"),"idcompany_tax_authority" => $postData["taxauthoritiesId"]),array("taxauthority" => $postData["taxdescription"]));
								if($updateArr)
								{
									$row = '<td>'.$postData["taxdescription"].'</td>
											<td class="text-center">
												<a title="Edit" alt="Edit" class="editTaxAuthorities" data-option="'.$postData["taxauthoritiesId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
											</td>';
								
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$postData["taxauthoritiesId"], "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Tax Authorities has been updated successfully...!!!</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								}
						}
						else
						{
							echo json_encode($isValid);
						}
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "modify", "msg"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You do not have permission to update payment type.</div>')); exit;
					}
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function validateTaxAuthorities($postData)
	{
		if(!ctype_alpha($postData["taxdescription"]))
		{
			//return array("status" => 2, "msg" => "This fields require character only.");
		}
		return array("status" => 1);
	}
	
	public function get_tax_authorities()
	{ 
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(27);
				$postData = $this->input->post();
				$cond = array("idcompany" => get_user_data("company_id"),"idcompany_tax_authority" => $postData["taxauthoritiesId"]);
				$TaxAuthorities = $this->administration_model->getTaxAuthorities($cond);
				echo json_encode($TaxAuthorities[0]);
			}
		}
	}
	
	public function taxDetails()
	{ 	if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$this->load->view("administrator/taxdetails");
		}
	}
	
	public function list_tax_details()
	{ 	if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$cond = array("idcompany" => get_user_data("company_id"));
				$like = array();
				if($postData["search"] != "" && $postData["searchBy"] != "")
				{
					$like = array($postData["searchBy"] => trim($postData["search"])); 
				}
				//print_r($like);die;
				$count = $this->administration_model->getTaxDetailsCount($cond,$like);
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				//if(!isset($postData["order_column"])){ $order_column = "seq_number"; }else{ $order_column = $postData["order_column"]; }
				
				$orderColumns = array("description");
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "description";
					$postData["order"] == "ASC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$taxdetails = $this->administration_model->getTaxDetailsPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" cellspacing="0" width="100%">';
						$table .= '<thead>
							  <tr>
							  <th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="sorting" width="60%" class="'.$css[0].'" width="60%" onclick="changePaginate(0,\'description\',\''.$corder[0].'\')">Tax Details</th>
								<!--<th class="text-center" width="15%">Action</th>-->
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($taxdetails)>0){
							if(count($taxdetails) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
							foreach($taxdetails as $details) { 
								$arr = array();
								$arr[0] = $details["description"];
								$arr[1] = $details["id"];
							$table .= '<tr id="row_'.$details["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($details["id"]).'" name="check" id="check" class="chk"></td>
								<td>'.$details["description"].'</td>
								<!--<td class="text-center">
									<a title="Edit" alt="Edit" class="editTaxDetails" data-option="'.$details["id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									</td>-->
							  </tr>';
							}
						}
				if($postData["start"] == 0)
				{		if(count($taxdetails)==0)
						{
							$table .= '<tr id="row_'.$details["id"].'"><td class="text-center" colspan="3">No Record Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getTaxDetailsList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				echo json_encode(array('table' => $table, 'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function delete_tax_details() // Delete Tax Details
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(30);
				if($permissionArr["permission_delete"])
				{
					$postData = $this->input->post();
					$arrDelete = array();
					for($i = 0; $i < count($postData["ids"]); $i++)
					{
						$isdelete = 0;
						$cond = array("idcompany" => get_user_data("company_id"),"idcompany_taxes" => $this->encrypt->decode($postData["ids"][$i]));
						$isdelete = $this->common_model->delete(TB_COMPANY_TAXES,$cond);
						if($isdelete)
						{
							$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
						}
					}
					echo json_encode(array("ids" => $arrDelete));
				}
				else
				{
					echo json_encode(array("status" => 2,"action" => "delete", "msg"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You do not have permission to delete tax details.</div>')); exit;
				}
			}
		}
	}
	
	public function taxType()
	{ 	if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$cond = array("idcompany" => get_user_data("company_id"));
			$this->load->view("administrator/taxtype");
		}
	}
	
	public function list_tax_type()
	{ 	if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$cond = array("idcompany" => get_user_data("company_id"));
				$like = array();
				if($postData["search"] != "" && $postData["searchBy"] != "")
				{
					$like = array($postData["searchBy"] => trim($postData["search"])); 
				}
				//print_r($like);die;
				$count = $this->administration_model->getTaxTypeCount($cond,$like);
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				//if(!isset($postData["order_column"])){ $order_column = "seq_number"; }else{ $order_column = $postData["order_column"]; }
				
				$orderColumns = array("name");
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "name";
					$postData["order"] == "ASC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$taxtype = $this->administration_model->getTaxTypePerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				//if($postData["order"] == "asc"){ $reorder = "desc";}
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" cellspacing="0" width="100%">';
							$table .= '<thead>
							  <tr>
							  <th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="sorting" width="60%" class="'.$css[0].'" width="60%" onclick="changePaginate(0,\'name\',\''.$corder[0].'\')">Tax Type</th>
								<!--<th class="text-center" width="15%">Action</th>-->
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($taxtype)>0)
				{
							if(count($taxtype) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
							foreach($taxtype as $types) { 
								$arr = array();
								$arr[0] = $types["name"];
								$arr[1] = $types["id"];
							$table .= '<tr id="row_'.$types["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($types["id"]).'" name="check" id="check" class="chk"></td>
								<td>'.$types["name"].'</td>
								<!--<td class="text-center">
									<a title="Edit" alt="Edit" class="editTaxType" data-option="'.$types["id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									</td>-->
							  </tr>';
							}
				}
				if($postData["start"] == 0)
				{
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getTaxTypesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function delete_tax_types() // Delete Payment Type
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(31);
				if($permissionArr["permission_delete"])
				{
					$postData = $this->input->post();
					$arrDelete = array();
					for($i = 0; $i < count($postData["ids"]); $i++)
					{
						$isdelete = 0;
						$cond = array("idcompany" => get_user_data("company_id"),"idcompany_tax_type" => $this->encrypt->decode($postData["ids"][$i]));
						$isdelete = $this->common_model->delete(TB_TAX_TYPES,$cond);
						if($isdelete)
						{
							$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
						}
					}
					echo json_encode(array("ids" => $arrDelete));
				}
				else
				{
					echo json_encode(array("status" => 2,"action" => "delete", "msg"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You do not have permission to delete tax type.</div>')); exit;
				}
			}
		}
	}
	
	/*----------Start Service Rates-------------*/
	public function list_service_rates()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$cond = array("idcompany" => get_user_data("company_id"));
				$like = array();
				if($postData["search"] != "" && $postData["searchBy"] != "")
				{
					$like = array($postData["searchBy"] => trim($postData["search"])); 
				}
				$count = $this->administration_model->getServiceRatesCount($cond,$like);
				
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("description","loadrate","rate","min_charge");
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "idcompany_service_rates";
					$postData["order"] == "ASC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$servicerates = $this->administration_model->getServiceRatesPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" cellspacing="0" width="100%">';
						$table .= '<thead>
							  <tr >
								<th class="'.$css["0"].'" width="10%" onclick="changePaginate(0,\'description\',\''.$corder[0].'\')">Description</th>
								<th class="'.$css[1].'" width="10%" onclick="changePaginate(0,\'loadrate\',\''.$corder[1].'\')">Load Rate</th>
								<th class="'.$css[2].'" width="10%" onclick="changePaginate(0,\'rate\',\''.$corder[2].'\')">Rate</th>
								<th class="'.$css[3].'" width="10%" onclick="changePaginate(0,\'min_charge\',\''.$corder[3].'\')">Min Charge</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($servicerates)>0){
							if(count($servicerates) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
							foreach($servicerates as $service) { 
						$table .= '<tr id="row_'.$service["id"].'">
								<td >'.$service["description"].'</td>
								<td>'.$service["loadrate"].'</td>
								<td>'.$service["rate"].'</td>
								<td>'.$service["min_charge"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" class="editServiceRates" data-option="'.$service["id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									<!--<a href="javascript:void(0)" onclick="deleteListCelebrity(this)" data-option="'.$service["description"].'" data-ref="'.$start.'" data-id="'.$service["id"].'"><i class="fa fa-times-circle text-white"></i></a>-->
								</td>
							  </tr>';
							}
				}
				if($postData["start"] == 0)
				{		if(count($servicerates)==0)
						{
							$table .= '<tr id="row_'.$types["id"].'"><td class="text-center" colspan="5">No Record Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getServiceRatesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function save_service_rates()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(28);
				
				$postData = $this->input->post();
				if($postData["servicerateId"] == "")
				{
					if($permissionArr["permission_add"])
					{
						$isValid = $this->validateServiceRate($postData);
						if($isValid["status"] == 1)
						{
							$insertId = $this->common_model->insert(TB_SERVICE_RATES,array("description" => $postData["txt_description"],"loadrate" => $postData["txt_loadrate"], "rate" => $postData["txt_rate"], "min_charge" => $postData["txt_min_charge"], "idcompany" => get_user_data("company_id")));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Service Rate has been added successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);
						}
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "add", "msg"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You do not have permission to accsess this.</div>')); exit;
					}
				}
				else
				{
					if($permissionArr["permission_update"])
					{
						$isValid = $this->validateServiceRate($postData);
						if($isValid["status"] == 1)
						{ 
							$updateArr = $this->common_model->update(TB_SERVICE_RATES,array("idcompany" => get_user_data("company_id"),"idcompany_service_rates" =>$postData["servicerateId"]),array("description" => $postData["txt_description"],"loadrate" => $postData["txt_loadrate"], "rate" => $postData["txt_rate"], "min_charge" => $postData["txt_min_charge"]));
							if($updateArr)
							{
								$row = '<td >'.$postData["txt_description"].'</td>
										<td>'.$postData["txt_loadrate"].'</td>
										<td>'.$postData["txt_rate"].'</td>
										<td>'.$postData["txt_min_charge"].'</td>
										<td class="text-center"><a title="Edit" alt="Edit" class="editServiceRates" data-option="'.$postData["servicerateId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$postData["servicerateId"], "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Service Rate has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);
						}
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "modify", "msg"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You do not have permission to update payment type.</div>')); exit;
					}
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function validateServiceRate($postData)
	{
		if(!is_numeric($postData["txt_loadrate"]))
		{
			return array("status" => 2, "msg" => "This fields require numbers only.");
		}
		else if(!is_numeric($postData["txt_rate"]))
		{
			return array("status" => 2, "msg" => "This fields require numbers only.");
		}
		else if(!is_numeric($postData["txt_min_charge"]))
		{
			return array("status" => 2, "msg" => "This fields require numbers only.");
		}
		else if(!ctype_alpha($postData["txt_description"]))
		{
			//return array("status" => 2, "msg" => "This fields require character only.");
		}
		return array("status" => 1);
	}
	
	public function get_service_rate()
	{ 
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(28);
				$postData = $this->input->post();
				$cond = array("idcompany" => get_user_data("company_id"),"idcompany_service_rates" =>$postData["servicerateId"]);
				$ServiceRate = $this->administration_model->getServiceRates($cond);
				echo json_encode($ServiceRate[0]);
			}
		}
	}
	
	/*----------End Service Rates------------- Completed */
	
}
?>
