<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("administration_model");
		$this->load->library("form_validation");
	}
	
	public function index()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			//echo "<pre>"; print_r(get_user_permission(27)); exit;
			$cond = array("idcompany" => get_user_data("company_id"));
			$data["paymentTypes"] = $this->administration_model->getPaymentTypes($cond);
			$this->load->view("administrator/home",$data);
		}
	}
	
	public function types()
	{
		if(is_ajax_request())
		{
			sleep(5);
			echo "Avadhut Shankar Yadav";
		}
	}
}
?>
