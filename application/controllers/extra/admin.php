<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->load->model("administration_model");
		$this->load->model("admin_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		
		//no_cache();
	}
	
	public function index()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$cond = array("idcompany" => get_user_data("company_id"));
			$data["paymentTypes"] = $this->administration_model->getPaymentTypes($cond);
			$this->load->view("administrator/home",$data);
		}
	}
	
	public function save_payment_types()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(27);
				if($permissionArr["permission_add"])
				{
					$postData = $this->input->post();
					if(!is_numeric($postData["txt_sequence_number"]))
					{
						echo json_encode(array("status" => 2, "msg" => "This fields require numbers only.")); exit;
					}
					else if(!ctype_alpha($postData["txt_description"]))
					{
						echo json_encode(array("status" => 2, "msg" => "This fields require character only.")); exit;
					}
					
					$cond = array("idcompany" => get_user_data("company_id"),"seq_number" => $postData["txt_sequence_number"]);
					$paymentTypes = $this->administration_model->getPaymentTypes($cond);
					$data["paymentTypes"] = $paymentTypes;
					if(count($paymentTypes) == 0)
					{
						$insertId = $this->common_model->insert(TB_PAYMENT_TYPES,array("seq_number" => $postData["txt_sequence_number"], "description" => $postData["txt_description"],"idcompany" => get_user_data("company_id")));
						if($insertId)
						{
							echo json_encode(array("status" => 1, "msg" => "Payment Type has been added successfully...!!!")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2, "msg" => "Opps, there is problem please try again.")); exit;
						}
					}
					else
					{
						echo json_encode(array("status" => 2, "msg" => "This sequence number has been already taken.")); exit;
					}
				}
				else
				{
					echo json_encode(array("status" => 2, "msg"=>"You do not have permission to accsess this.")); exit;
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_payment_types()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$cond = array("idcompany" => get_user_data("company_id"));
				$count = $this->administration_model->getPaymentTypesCount($cond);
				
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				if(!isset($postData["order_column"])){ $order_column = "seq_number"; }else{ $order_column = $postData["order_column"]; }
				
				$cond = array("idcompany" => get_user_data("company_id"));
				$paymentTypes = $this->administration_model->getPaymentTypesPerPage($cond,$postData["start"],array($order_column => $postData["order"]));
				$links = "";
				
				$table = '<table class="table table-bordered table-hover table-striped tablesorter">';
						if(count($paymentTypes)>0){ 
						$table .= '<thead>
							  <tr>
								<th width="7%" onclick="getPaymentTypeList()">Sequence Number</th>
								<th width="12%">Description</th>
								<th class="text-center" width="10%">Action</th>
							  </tr>
							</thead>
							<tbody>';
							if(count($paymentTypes) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
							foreach($paymentTypes as $types) { 
						$table .= '<tr id="row_'.$types["id"].'">
								<td>'.$types["seq_number"].'</td>
								<td>'.$types["description"].'</td>
								<td class="text-center">
									<a href="'.base_url().'celebrity/add_celebrity/'.$types["id"].'">Edit</a> | 
									<a href="javascript:void(0)" onclick="deleteListCelebrity(this)" data-option="'.$types["description"].'" data-ref="'.$start.'" data-id="'.$types["id"].'">Delete</a>
								</td>
							  </tr>';
							}
						}else {
						$table .= '<tr>
								<td colspan="3">No Record Found</td>
							  </tr>';
						}
						$table .= '</tbody>
						</table>';
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getPaymentTypeList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
				}
				if(count($paymentTypes)>0){
					$links = '<div class="row"><div class="col-lg-6">Showing '.($postData["start"]+1).' to '.($to).' of '.$count[0]['cnt'].' entries</div><div class="col-lg-6">'.$this->pagination->create_links().'</div></div>';
					$links .= '<input type="hidden" name="txt_rows" id="txt_rows" value="'.count($paymentTypes).'"/><input type="hidden" name="txt_start" id="txt_start" value="'.$start.'"/><input type="hidden" name="txt_astart" id="txt_astart" value="'.$postData["start"].'"/></div></div>';
				}
				echo json_encode(array('table' => $table, 'links' => $links)); exit;
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	/*  Save Tax Type */ 
	 
	public function save_tax_types()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(27);
				if($permissionArr["permission_add"])
				{
					$postData = $this->input->post();
					if(!is_numeric($postData["txt_sequence_number"]))
					{
						echo json_encode(array("status" => 2, "msg" => "This fields require numbers only.")); exit;
					}
					else if(!ctype_alpha($postData["txt_description"]))
					{
						echo json_encode(array("status" => 2, "msg" => "This fields require character only.")); exit;
					}
					
					$cond = array("idcompany" => get_user_data("company_id"),"seq_number" => $postData["txt_sequence_number"]);
					$paymentTypes = $this->administration_model->getPaymentTypes($cond);
					$data["paymentTypes"] = $paymentTypes;
					if(count($paymentTypes) == 0)
					{
						$insertId = $this->common_model->insert(TB_PAYMENT_TYPES,array("seq_number" => $postData["txt_sequence_number"], "description" => $postData["txt_description"],"idcompany" => get_user_data("company_id")));
						if($insertId)
						{
							echo json_encode(array("status" => 1, "msg" => "Payment Type has been added successfully...!!!")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2, "msg" => "Opps, there is problem please try again.")); exit;
						}
					}
					else
					{
						echo json_encode(array("status" => 2, "msg" => "This sequence number has been already taken.")); exit;
					}
				}
				else
				{
					echo json_encode(array("status" => 2, "msg"=>"You do not have permission to accsess this.")); exit;
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	/* List Of Tax types */
	public function list_tax_types()
	{ 
		//if(is_ajax_request())
		//{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$cond = array("idcompany" => get_user_data("company_id"));
				//$count = $this->administration_model->getPaymentTypesCount($cond);
				$count = $this->admin_model->getTaxTypesCount($cond);
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				if(!isset($postData["order_column"])){ $order_column = "idcompany_tax_type"; }else{ $order_column = $postData["order_column"]; }
				
				$cond = array("idcompany" => get_user_data("company_id"));
				//$paymentTypes = $this->administration_model->getTaxTypesPerPage($cond,$postData["start"],array($order_column => $postData["order"]));
				$taxTypes = $this->admin_model->getTaxTypesPerPage($cond,$postData["start"],array($order_column => $postData["order"]));
				$links = "";
				
				$table = '<table class="table table-bordered table-hover table-striped tablesorter">';
						if(count($taxTypes)>0){ 
						$table .= '<thead>
							  <tr>
								<th width="7%" onclick="getPaymentTypeList()">Sequence Number</th>
								<th width="12%">Tax Type</th>
								<th class="text-center" width="10%">Action</th>
							  </tr>
							</thead>
							<tbody>';
							if(count($taxTypes) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
							foreach($taxTypes as $types) { 
						$table .= '<tr id="row_'.$types["id"].'">
								<td>'.$types["idcompany_tax_type"].'</td>
								<td>'.$types["name"].'</td>
								<td class="text-center">
									<a href="'.base_url().'celebrity/add_taxtype/'.$types["id"].'">Edit</a> | 
									<a href="javascript:void(0)" onclick="deleteTaxType(this)" data-option="'.$types["name"].'" data-ref="'.$start.'" data-id="'.$types["id"].'">Delete</a>
								</td>
							  </tr>';
							}
						}else {
						$table .= '<tr>
								<td colspan="3">No Record Found</td>
							  </tr>';
						}
						$table .= '</tbody>
						</table>';
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getTaxTypeList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
				}
				if(count($taxTypes)>0){
					$links = '<div class="row"><div class="col-lg-6">Showing '.($postData["start"]+1).' to '.($to).' of '.$count[0]['cnt'].' entries</div><div class="col-lg-6">'.$this->pagination->create_links().'</div></div>';
					$links .= '<input type="hidden" name="txt_rows" id="txt_rows" value="'.count($taxTypes).'"/><input type="hidden" name="txt_start" id="txt_start" value="'.$start.'"/><input type="hidden" name="txt_astart" id="txt_astart" value="'.$postData["start"].'"/></div></div>';
				}
				echo json_encode(array('table' => $table, 'links' => $links)); exit;
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		//}
	}
	/* Save Tax Types */
	public function save_tax_types()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$permissionArr = get_user_permission(27);
				if($permissionArr["permission_add"])
				{
					$postData = $this->input->post();
					if(!is_numeric($postData["txt_sequence_number"]))
					{
						echo json_encode(array("status" => 2, "msg" => "This fields require numbers only.")); exit;
					}
					else if(!ctype_alpha($postData["description"]))
					{
						echo json_encode(array("status" => 2, "msg" => "This fields require character only.")); exit;
					}
					
					$cond = array("idcompany" => get_user_data("company_id"),"seq_number" => $postData["txt_sequence_number"]);
					$paymentTypes = $this->administration_model->getPaymentTypes($cond);
					$data["paymentTypes"] = $paymentTypes;
					if(count($paymentTypes) == 0)
					{
						$insertId = $this->common_model->insert(TB_PAYMENT_TYPES,array("seq_number" => $postData["txt_sequence_number"], "description" => $postData["txt_description"],"idcompany" => get_user_data("company_id")));
						if($insertId)
						{
							echo json_encode(array("status" => 1, "msg" => "Payment Type has been added successfully...!!!")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2, "msg" => "Opps, there is problem please try again.")); exit;
						}
					}
					else
					{
						echo json_encode(array("status" => 2, "msg" => "This sequence number has been already taken.")); exit;
					}
				}
				else
				{
					echo json_encode(array("status" => 2, "msg"=>"You do not have permission to accsess this.")); exit;
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
}
?>
