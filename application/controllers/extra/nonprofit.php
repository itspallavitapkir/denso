<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nonprofit extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("db_model");
		$this->load->library("form_validation");
		no_cache();
	}
	
	public function index()
	{
		if(!is_admin_logged_in())
		{
			redirect("/siteadmin"); exit;
		}
		else
		{
			$this->load->view("siteadmin/non_profit_list",$data);
		}
	}
	
	public function add_non_profit($id = "")
	{
		if(!is_admin_logged_in())
		{
			redirect("/siteadmin"); exit;
		}
		else
		{
			$data["lastbreadcrumb"] = "Add Non-Profit";
			if($id != "")
			{
				$cond = array("non_profit_id" => $id);
				$data["nonProfitData"] = $this->db_model->get_row(TB_NON_PROFITS,"*",$cond);
				if(count($data["nonProfitData"]) == 0)
				{
					redirect("/nonprofit"); exit;
				}
				else
				{
					$data["lastbreadcrumb"] = "Edit Non-Profit";
				}
			}
			
			$data["breadcrumb"] = array("Home" => site_url(array('siteadmin', 'dashboard')),
										"Non Profits" => site_url(array('nonprofit', ''))
										);
			$data["recentNonProfits"] = $this->db_model->get_rows(TB_NON_PROFITS, "non_profit_id, non_profit_name", array(), array(), array(), array("non_profit_id" => "DESC"), array("per_page" => 15, "start" => 0));
			$data["countryData"] = $this->db_model->get_rows(TB_COUNTRY, "country_id,country_name", array(), array(), array(), array("country_name"=>"ASC"));
			$this->load->view("siteadmin/non_profit_add",$data);
		}
	}
	
	public function save_non_profit()
	{
		$postData = $this->input->post();
		
		$this->form_validation->set_rules("dd_country", "Country", "required|regex_match[/^[0-9 _]+$/i]");
		$this->form_validation->set_message("required", "This field is required.");
		$this->form_validation->set_message("regex_match", "This field is not valid.");
		
		$this->form_validation->set_rules("txt_name", "Non-Profit Name", "required|xss_clean|regex_match[/^[a-z0-9 _]+$/i]|max_length[50]");
		$this->form_validation->set_message("required", "This field is required.");
		$this->form_validation->set_message("regex_match", "This field is not valid.");
		$this->form_validation->set_message("max_length", "This field can accept only less than 50 charachters.");
		
		$this->form_validation->set_rules("txt_url", "Non-Profit URL", "is_valid_url");
		$this->form_validation->set_message("is_valid_url", "This field is not valid.");
		
		$this->form_validation->set_rules("txt_email", "Non-Profit Email", "required|valid_email");//|is_unique[users.email]
		$this->form_validation->set_message("required", "This field is required.");
		$this->form_validation->set_message("valid_email", "This field is not valid.");
		
		$this->form_validation->set_rules("txt_fund", "Fund", "regex_match[/^[0-9 _]+$/i]");
		$this->form_validation->set_message("regex_match", "This field is not valid.");
		
		if ($this->form_validation->run() === false)
	    {
			echo validation_error($postData);
	    }
		else
		{
			if($postData["txt_fund"] == ""){$postData["txt_fund"] = 0;}
			$updateData = array(
							"non_profit_country" => trim($postData["dd_country"]),
							"non_profit_name" => trim($postData["txt_name"]),
							"non_profit_email" => trim($postData["txt_email"]),
							"non_profit_url" => trim($postData["txt_url"]),
							"non_profit_fund" => trim($postData["txt_fund"]),
							);
			if($postData["txt_non_profit_id"] != "")
			{
				$cond = array("non_profit_id" => $postData["txt_non_profit_id"]);
				$result = $this->db_model->update(TB_NON_PROFITS,$cond,$updateData);
			}
			else
			{
				$result = $this->db_model->save(TB_NON_PROFITS,$updateData);
			}
			echo json_encode($result);
		}
	}
	
	public function delete_non_profit()
	{
		$postData = $this->input->post();
		$deleteData = array("non_profit_id"=>$postData["id"]);
		$result = $this->db_model->delete(TB_NON_PROFITS,$deleteData);
		$result["start"] = $postData["start"];
		$result["row_id"] = $postData["id"];
		echo json_encode($result); exit;
	}
	
	public function delete_non_profits()
	{
		$postData = $this->input->post();
		$deleteData = array("key"=>"non_profit_id", "val" => $postData["ids"]);
		$result = $this->db_model->bulkDelete(TB_NON_PROFITS,$deleteData);
		if($result['num'] >= PER_PAGE)
		{
			$result["start"] = $postData["start"]-1;
		}
		else
		{
			$result["start"] = $postData["start"];
		}
		$result["row_ids"] = $postData["ids"];
		echo json_encode($result); exit;
	}
	
	public function non_profit_send_email()
	{
		$postData = $this->input->post();
		if($postData["subject"] != "" && $postData["message"] != "" && count($postData["ids"])>0)
		{
			$nonProfits = $this->db_model->get_rows(TB_NON_PROFITS, "non_profit_name,non_profit_email", array(), array(), array(), array(), array(), array("non_profit_id" => array("where_in" => $postData['ids'])));
			$this->load->library("email");
			$this->email->subject($postData["subject"]);
			$this->email->message($postData["message"]);	
			$notSend = 0;
			$send = 0;
			$error = '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button> Error in sending mail to following Email-Ids</br>';
			$error1 = "";
			foreach($nonProfits AS $nonProfit)
			{
				$this->email->from("admin@club4causes.com", "Admin");
				$this->email->to($nonProfit["non_profit_email"]); 
				if($this->email->send())
				{
					$send++;
				}
				else
				{
					//$notArr[$notSend] = $nonProfit["non_profit_email"];
					$error1 .= " ".$nonProfit["non_profit_email"].",";
					$notSend++;
				}
			}
			$error .= substr($error1,0,-1).'</div>';
			echo json_encode(array("status" => "success", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'.$send.' Emails has been sent.</div>',"send" => $send,"notsend" => $notSend, "error" => $error));
		}
		else
		{
			echo json_encode(array("status" => "error", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please fill all the required fields.</div>'));
		}
	}
	
	public function validation_error($postData)
	{
		$arror = array();
		foreach($postData as $key => $val)
		{
			if(form_error($key))
			{
				$error[$key] = strip_tags(form_error($key));
			}
		}
		return json_encode(array("status" => "error","msg" => $error));
	}
	
	public function ajax_non_profit_list()
	{
		$postData = $this->input->post();
		$cond = array();
		$like = array();
		if($postData["searchBy"] == "name" && $postData["searchText"] != "")
		{
			$like = array("non_profit_name" => $postData["searchText"]);
		}
		else if($postData["searchBy"] == "email" && $postData["searchText"] != "")
		{
			$like = array("non_profit_email" => $postData["searchText"]);
		}
		$count = $this->db_model->get_rows(TB_NON_PROFITS,"count(non_profit_name) AS cnt",$cond, $like, array());
		$orderColumns = array("name","email","url","fund");
		if(!in_array($postData["orderBy"],$orderColumns))
		{
			$column = "non_profit_name";
			$postData["order"] == "DESC";
			$order_column = "name";
		}
		else
		{
			$column = "non_profit_".$postData["orderBy"];
			$order_column = $postData["orderBy"];
		}
		
		foreach($orderColumns AS $k => $v)
		{
			if($postData["orderBy"] != $v)
			{
				$corder[$k] = "ASC";
			}
			else
			{
				if($postData["order"] == "ASC")
				{
					$order = "DESC";
					$corder[$k] = "DESC";
				}
				else
				{
					$order = "ASC";
					$corder[$k] = "ASC";
				}
			}
		}
		
		$nonProfitData = $this->db_model->get_rows(TB_NON_PROFITS,"*",$cond, $like, array(), array($column => $order), array("per_page" => PER_PAGE, "start" => $postData["start"]));
		$links = "";
		$table = '<table class="table table-bordered table-hover table-striped tablesorter">';
				if(count($nonProfitData)>0){ 
				$table .= '<thead>
					  <tr>
						<th class="text-center" width="2%"><input type="checkbox" onChange="selectAll(this)" id="selecctall"/></th>
						<th onclick="getList(0,\'name\',\''.$corder[0].'\')" width="12%">Non-Profit Name <i class="fa fa-sort"></i></th>
						<th onclick="getList(0,\'email\',\''.$corder[1].'\')" width="14%">Non-Profit Email <i class="fa fa-sort"></i></th>
						<th onclick="getList(0,\'url\',\''.$corder[2].'\')" width="19%">Non-Profit URL <i class="fa fa-sort"></i></th>
						<th onclick="getList(0,\'fund\',\''.$corder[3].'\')" width="10%">Non-Profit Fund <i class="fa fa-sort"></i></th>
						<th class="text-center" width="8%">Action</th>
					  </tr>
					</thead>
					<tbody>';
					if(count($nonProfitData) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
					foreach($nonProfitData as $nonprofit) { 
				$table .= '<tr id="row_'.$nonprofit["non_profit_id"].'">
						<td class="text-center"><input class="checkbox" type="checkbox" name="chk_non_profit[]" value="'.$nonprofit["non_profit_id"].'"></td>
						<td>'.$nonprofit["non_profit_name"].'</td>
						<td>'.$nonprofit["non_profit_email"].'</td>
						<td>'.$nonprofit["non_profit_url"].'</td>
						<td>'.$nonprofit["non_profit_fund"].'</td>
						<td class="text-center">
							<a href="'.base_url().'nonprofit/add_non_profit/'.$nonprofit["non_profit_id"].'">Edit</a> | 
							<a href="javascript:void(0)" onclick="deleteNonProfit(this)" data-option="'.$nonprofit["non_profit_name"].'" data-ref="'.$start.'" data-id="'.$nonprofit["non_profit_id"].'">Delete</a>
						</td>
					  </tr>';
					}
				}else {
				$table .= '<tr>
						<td colspan="6">No Record Found</td>
					  </tr>';
				}
				$table .= '</tbody>
				</table>';
		$config = get_pagination_config($postData["start"], $count[0]['cnt'], $order_column, $postData["order"]);
		$this->pagination->initialize($config);
		$to = $postData["start"]+PER_PAGE;
		if($to > $count[0]['cnt'])
		{
			$to = $count[0]['cnt'];
		}
		if(count($nonProfitData)>0){
			$links = '<div class="row"><div class="col-lg-6">Showing '.($postData["start"]+1).' to '.($to).' of '.$count[0]['cnt'].' entries</div><div class="col-lg-6">'.$this->pagination->create_links().'</div></div>';
			$links .= '<div class="row"><div class="col-lg-3"><select name="dd_action" id="dd_action" class="form-control"><option value="">Select Action</option><option value="delete">Bulk Delete</option><option value="email">Bulk Send Mail</option></select></div><div class="col-lg-3"><input type="button" name="btn_bulk" id="btn_bulk" onclick="changeAction()" class="btn btn-primary" value="Submit"/>';
			$links .= '<input type="hidden" name="txt_rows" id="txt_rows" value="'.count($nonProfitData).'"/><input type="hidden" name="txt_start" id="txt_start" value="'.$start.'"/></div></div>';
		}
		echo json_encode(array('table' => $table, 'links' => $links));
	}
	
	/************* non profit end **************/	
}
?>
