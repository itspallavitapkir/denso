<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ApiController extends CI_Controller{
       
    function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("administration_model");
		$this->load->library('form_validation');
		$this->load->library("email");
		$this->load->helper('download');   
		$this->no_cache();
	}
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}

	public function login(){
		$data=$this->input->post();
		$email = $data['email'];
		$password = md5($data['pwd']);
		if(!empty($email) and !empty($password)){
			$cond = array("email_address" =>$email,"password"=>$password);
			$users = $this->administration_model->getUserById($cond);
			
			if(!empty($users)){
				if($users[0]['dn_status'] == 'Active'){

					echo json_encode(array(
						'response'=>"success",
						'status'=>"success",
						'user_id'=>''.$users[0]['id'].'',
						'first_name'=>''.$users[0]['first_name'].'',
						'is_active'=>''.$users[0]['dn_status'].''
						));
			  	    exit;
					
				}else{
					echo json_encode(array(
						'response'=>"success",
						'status'=>"success",
						'user_id'=>''.$users[0]['id'].'',
						'first_name'=>''.$users[0]['first_name'].'',
						'is_active'=>''.$users[0]['dn_status'].'',
						'msg'=>"Your account is inactive"
						));
			  	    exit;
				}

			}else{
				$error = array('Status' => "Failed", "msg" => "Invalid Email address or Password");
						$this->response($this->json($error), 400);
			}
			
		
		}
	}


	public function login1(){
		$data=$this->input->post();
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				//$this->response('',406);
				$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
				$this->response($this->json($error), 400);
			}

		$email = (isset($data['email'])?ucfirst($data['email']):"");
		$password = (isset($data['pwd'])?$data['pwd']:"");
			// if(isset($this->_request['email']))
			// 	$email = $this->_request['email'];		
			// if(isset($this->_request['pwd']))	
			// 	$password = $this->_request['pwd'];
			
			//echo  json_encode(array('email'=>$email,'password'=>$password)); exit;
			// Input validations
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					
					$sql = mysql_query("SELECT * FROM dn_users WHERE email_address = '$email' AND password = '".md5($password)."' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						//$result['Status'] = 'Success';
						while($row = mysql_fetch_array($sql,MYSQL_ASSOC)){
							
							if($row['dn_status'] == 'Active'){
							
$usr_id  = $row["user_id"];
$sql = mysql_query("insert into dn_user_statistics (user_id,login_time,date_modified) values('.$usr_id.','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')", $this->db);

								$login_count = $row['login_count']+1;
								$sql = mysql_query("update dn_users set login_count = $login_count WHERE email_address = '$email'", $this->db);

mysql_query("insert into dn_user_statistics (user_id,login_time,date_modified) values(".$row["user_id"].",'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')", $this->db); 
								$sql_insert_stat = mysql_insert_id();								if($sql_insert_stat){
									$row['stat_id'] = $sql_insert_stat;
								}
								$result = $row;
								$result['Status'] = 'Success';
								$result['is_active'] = 1;
								$result['msg'] = "Successful logged in";
								$this->response($this->json($result), 200);
							}
							elseif($row['dn_status'] == 'Inactive'){
								$result = $row;
								$result['Status'] = 'Success';
								$result['is_active'] = 0;
								$result['msg'] = "Your account is inactive";
								$this->response($this->json($result), 200);
								//$error = array('Status' => "Success", 'is_active' => '0',"msg" => "Your account is inactive");
								//$this->response($this->json($result), 200);
							}
							
							
							
							// If success everythig is good send header as "OK" and user details
						}
						
						
					}
					else{
						//$this->response('', 204);	// If no records "No Content" status
						$error = array('Status' => "Failed", "msg" => "Invalid Email address or Password");
						$this->response($this->json($error), 400);
					}
				}
			}
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('Status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		}  

	/* 
	 *  Webservice to fetch banner image
	 *  user_id : <USER ID>
	 *  country_name : <USER Country>
	 */


	function fetchBanner()
	{	
		$data=$this->input->post();
		// $user_id=  json_encode($data['user_id']);
		// $country_name =  json_encode($data['country_name']);
		
	 	$country = (isset($data['country_name'])?ucfirst($data['country_name']):"");
		$user_id = (isset($data['user_id'])?$data['user_id']:"");
	
			if(!empty($user_id)){
				$cond = array('user_id'=>$user_id);
				$users = $this->administration_model->getUserById($cond);
				$user_type = $users[0]['user_type'];
				$country = $users[0]['country'];
			    $v_cat_id=$users[0]['v_cat_id'];
				$status='Active';
				$cond = array('user_type' => $user_type,'country' => $country,'status'=>$status);
				$bannerData = $this->login_model->fetchBannerForUser(TB_BANNER,"user_type,country,category_id,banner_image,start_date,end_date,status,title",$cond);
				 // pr($users);
				 // exit;
				$start= $bannerData[0]['start_date'];
				$end= $bannerData[0]['end_date'];
				$img= $bannerData[0]['banner_image'];
				$current_date= date('m/d/Y');
				$cat=$bannerData[0]['category_id'];

				$uary1 = explode(',',$v_cat_id);
	    		$bary2 = explode(',',$cat);

	    		$myArray = explode(' ', $start);
	    		$start1  =$myArray[0];

	    		$myArray = explode(' ', $end);
	    		$end1  =$myArray[0];

	    		$str= implode(' ',array_intersect($uary1,$bary2));
	    		
	    		if(!empty($str)){
	    			if(($start1<= $current_date)&&($end1>=$current_date)&& $str)
					{
						$image= $bannerData[0]['banner_image'];
					    $banner_path = base_url().UPLOAD_BANNER_PATH.$image;
			  			//echo json_encode($banner_path);
			  			echo json_encode(array(
	                                    'response'=>"success",                                   
	                                    'response_message'=>"success",                                  
	                                    "image"=>''.$banner_path.''                                    
	                                    )); 
			  			exit;
					}
	    		}
			}else{
				//echo  json_encode(array('country'=>$country)); exit;
				$cond_cnt = array('user_type' =>'3','name' => $country,'dn_banner.status'=>'Active');
				$bannerData = $this->login_model->fetchPublicBannerForUser(TB_BANNER,"dn_banner.user_type,dn_banner.country,dn_banner.category_id,dn_banner.banner_image,dn_banner.start_date,dn_banner.end_date,dn_banner.status,dn_banner.title,dn_country.name,dn_country.country_id",$cond_cnt);
			    $start= $bannerData[0]['start_date'];
				$end= $bannerData[0]['end_date'];
				$img= $bannerData[0]['banner_image'];
				$current_date= date('m/d/Y');

	    		$myArray = explode(' ', $start);
	    		$start1  =$myArray[0];
	    		$myArray = explode(' ', $end);
	    	    $end1  =$myArray[0];


	    		
    			if(($start1<= $current_date)&&($end1>=$current_date))
				{
					$image= $bannerData[0]['banner_image'];
					if(!empty($image)){
						 $banner_path = base_url().UPLOAD_BANNER_PATH.$image;
					}
		  			//echo json_encode($banner_path);
		  			echo json_encode(array(
                                    'response'=>"success",                                   
                                    'response_message'=>"success",                                  
                                    "image"=>''.$banner_path.''                                    
                                    )); 
		  			exit;
				}else{
					$banner_path = base_url().'img/default-banner-Image.jpeg';
					echo json_encode(array(
                                    'response'=>"success",                                   
                                    'response_message'=>"success",                                  
                                    "image"=>''.$banner_path.''                                    
                                    )); 
					exit;
				}
			}
	}

	public function get_request_method(){
			return $_SERVER['REQUEST_METHOD'];
	}

	public function iosnotification()
	{		

		$gcmRegID =trim('e59f88707ab2c8a44bacfee7244d418bbd6c943d87fe57a182b5cdebb0b54007');	
		$pushMessage = 'hi Dino';	
		// Put your private key's passphrase here:
		$passphrase = 'star do shine';

		$pemFile='/var/www/html/denso/densocertkeycc.pem';

			$ctx = stream_context_create();
			stream_context_set_option($ctx,'ssl','local_cert',$pemFile);
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client( 
				'ssl://gateway.sandbox.push.apple.com:2195',$err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if(!$fp){
				echo json_encode(array('result'=>'fail to connect IOS'));	
			}


		// Create the payload body
		$body['aps'] = array(
			'alert' => $pushMessage
			);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $gcmRegID) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		print_r($result); exit;
		if(!$result){
  			echo json_encode(array(
                'response_code'=>"Fail",                                   
                'response_message'=>"Your have been delete booking id details.!"
                )); exit; 
		}
		else
		{
		   echo json_encode(array(
		        'response_code'=>"Success",                                   
		        'response_message'=>"get success message.!"
		        )); exit; 
		}	 
		fclose($fp);
		exit;
	}

}
?>


