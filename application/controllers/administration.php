<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("administration_model");
		$this->load->model("permission_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("email");
		$this->load->library("Excelreader");
		$this->load->library("ValidateFile");
		$this->load->library('upload');
		//$this->load->library("richtexteditor");
		$this->profilepicpath =  realpath('profile_pic');
		$this->newdocpath =  realpath('news_files');
		$this->pdfpath =  realpath('pdf_files');
		$this->aipath =  realpath('ai_files');
		$this->skpdfpath =  realpath('sk_files');
		$this->backgroundpath =  realpath('background_img');
		
		$this->vehiclepartspath =  realpath('vehicle_parts');
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	/************ Start Payment Types **************/
	
	public function checkPermission(){
		$userData = $this->session->userdata("auth_user");
		//print_r($userData);die;
		
		/*check whether user is having lang translator enabled*/
		$username = $userData['username'];
		$cond = array("email_address" => $username,"lang_translator" => '1');
		$lang_res = $this->login_model->validUser(TB_USERS,"email_address,lang_translator",$cond);
		// If lang translator is 1 then show only SKW else show AMW and SKW.
		//echo "count:".count($lang_res) ;die;
		if(count($lang_res) == 0){
			return 1; // show AMW and SKW
		}
		else{
			return 0; // Show SKW ONLY
		}
		
	}
	
	public function index()
	{
		
		if(!is_user_logged_in())
		{
			redirect('logout'); exit;
		}
		else
		{ 	
			if($this->checkPermission() == 1){
				$data["country"] = $this->administration_model->getCountry();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				$this->load->view("administrator/users",$data);
				//$this->load->view("content");
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	
	/********* User Access ***********/
	public function UserAccess()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$userAccess = $this->administration_model->getUserRole(); 
			$data['userAccess'] = "";
			$data['userAccess'] .='<div class="panel-body"><div class="userrleheader-tle userrleheader-inner table-bordered"> 
									<div class="userrleheader-tle table-bordered" style="overflow:hidden;">
										<div class="pull-left roletitle roletitle-first">User Types</div>
										<div class="pull-left roletitle">Donwload PDf</div>    
										<div class="pull-left roletitle">Download AI Files</div>                                                                                                                                                 
									</div>
									';
				if($userAccess!=""){
				$n=1;	
				foreach($userAccess as $access):
				$checked_pdf = ($access['permission_downloadpdf'] == '1') ?	'checked' : '';
				$checked_ai = ($access['permission_download_ai'] == '1') ?	'checked' : '';
				$data['userAccess'] .='<div class="userrleheader-tle userrleheader-inner table-bordered">
										<div class="pull-left roletitle roletitle-first">'.$access['role'].'</div>
											<div class="pull-left roletitle">
												<div class="checkbox-custom mbn mln form-group">
													<input  '.$checked_pdf.' type="checkbox" checkaccess id="pdf_'.$n.'" name="pdf_'.$n.'">
													<label for="pdf_'.$n.'" class="pln">&nbsp;</label>
												</div>
											</div>
											<div class="pull-left roletitle">
												<div class="checkbox-custom mbn mln form-group">
													<input '.$checked_ai.' type="checkbox" class="checkaccess" id="ai_'.$n.'" name="ai_'.$n.'">
													<label for="ai_'.$n.'" class="pln">&nbsp;</label>
												</div>
											</div>  
										</div>';												
									
			    $n++;
			    endforeach;
			}
			else{
				$data['userAccess'] .='No User Types found in system !! Please check database';
			}
			$this->load->view("administrator/user_access",$data);
		}
	}
	
	public function SaveUserAccessSettings()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{   
				$postData = $this->input->post();  
				$data=""; 
				
				//if($postData){ 
					//foreach($postData as $key=>$val): 
					
						$pdf_1  = (isset($postData['pdf_1']) && $postData['pdf_1'] =="on") ? '1' :0;
						$ai_1   = (isset($postData['ai_1']) && $postData['ai_1']  =="on") ? '1' :0;
						$pdf_2  = (isset($postData['pdf_2']) && $postData['pdf_2'] =="on") ? '1' :0;
						$ai_2   = (isset($postData['ai_2']) && $postData['ai_2']  =="on") ? '1' :0;
						$pdf_3  = (isset($postData['pdf_3']) && $postData['pdf_3'] =="on") ? '1' :0;
						$ai_3   = (isset($postData['ai_3']) && $postData['ai_3']  =="on") ? '1' :0;
						$data_update=array();
						$cond=array(); 
						
						
						if($pdf_1 == 1 || $pdf_1 == 0 || $ai_1 ==1 || $ai_1 == 0){
								$pdf_array = ($pdf_1)?array("permission_downloadpdf" => '1'):array("permission_downloadpdf" => '0');
								$ai_array = ($ai_1)?array("permission_download_ai" => '1'):array("permission_download_ai" => '0');
								$data_update1 =array_merge($pdf_array,$ai_array); 
								$cond1 = array("access_id" => "1");
								$this->common_model->update(TB_USER_ACCESS,$cond1,$data_update1);	
								//echo $this->db->last_query();
								
						}
						if($pdf_2 == 1 || $pdf_2 == 0 || $ai_2 ==1 || $ai_2 == 0){
								$pdf_array = ($pdf_2)?array("permission_downloadpdf" => '1'):array("permission_downloadpdf" => '0');
								$ai_array = ($ai_2)?array("permission_download_ai" => '1'):array("permission_download_ai" => '0');
								$data_update2 =array_merge($pdf_array,$ai_array); 
								$cond2 = array("access_id" => "2");
								$this->common_model->update(TB_USER_ACCESS,$cond2,$data_update2);	
								//echo $this->db->last_query();
						}
						if($pdf_3 == 1 || $pdf_3 == 0 || $ai_3 ==1 || $ai_3 == 0){
								$pdf_array = ($pdf_3)?array("permission_downloadpdf" => '1'):array("permission_downloadpdf" => '0');
								$ai_array = ($ai_3)?array("permission_download_ai" => '1'):array("permission_download_ai" => '0');
								$data_update3 = array_merge($pdf_array,$ai_array); 
								$cond3 = array("access_id" => "3");
								$this->common_model->update(TB_USER_ACCESS,$cond3,$data_update3);	
								//echo $this->db->last_query();
						} 
						
					//endforeach;
				//}
				
				$userAccess = $this->administration_model->getUserRole(); 
				
				$data['userAccess'] = "";
				$data['success'] = "";
				$data['userAccess'] .='<div class="panel-body"><div class="userrleheader-tle userrleheader-inner table-bordered"> 
										<div class="userrleheader-tle table-bordered" style="overflow:hidden;">
											<div class="pull-left roletitle roletitle-first">User Types</div>
											<div class="pull-left roletitle">Donwload PDf</div>    
											<div class="pull-left roletitle">Download AI Files</div>                                                                                                                                                 
										</div>
										';
					if($userAccess!=""){
					$n=1;	
					foreach($userAccess as $access):
					$checked_pdf = ($access['permission_downloadpdf'] == '1') ?	'checked' : '';
					$checked_ai = ($access['permission_download_ai'] == '1') ?	'checked' : '';
					$data['userAccess'] .='<div class="userrleheader-tle userrleheader-inner table-bordered">
											<div class="pull-left roletitle roletitle-first">'.$access['role'].'</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input  '.$checked_pdf.' type="checkbox" id="pdf_'.$n.'"  name="pdf_'.$n.'">
														<label for="pdf_'.$n.'" class="pln">&nbsp;</label>
													</div>
												</div>
												<div class="pull-left roletitle">
													<div class="checkbox-custom mbn mln form-group">
														<input '.$checked_ai.' type="checkbox" id="ai_'.$n.'"  name="ai_'.$n.'">
														<label for="ai_'.$n.'" class="pln">&nbsp;</label>
													</div>
												</div>  
											</div>';												
										
					$n++;
					endforeach;
					//$data['success']  ="<div class='p5 mbn alert-dismissable pln success'>Settings updated successfully</div>";	
				}
				else{
					$data['userAccess'] .='No User Types found in system !! Please check database';
				} 
				echo json_encode(array("userAccess" => $data['userAccess'], "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Settings updated successfully.</div>'));
			}
		}
	}
	
	
	/********* User Access ***********/
	private function genarateRandomPassword($length=10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$randomstring ='';
		for($i=0;$i<=$length;$i++){
			$randomstring .= $characters[rand(0,strlen($characters) -1)];
		}
		return $randomstring;
	}


	//function to add subquestions
	public function submit_sub_questions(){
	if(is_user_logged_in())
	{
		if($this->checkPermission() == 1){
			$postData = $this->input->post();  
			// pr($postData);
			// exit;
			if($postData["question_id"] != "")//add banner
			{  	

				$insertId = $this->common_model->insert(TB_QUE_OPTIONS,array(
					"q_id" =>$postData['question_id'],
					"question"=>$postData['txt_question'],
					"status"=>$postData['status'],
					"option1" =>$postData['txt_option1'],
					"option2" =>$postData['txt_option2'],
					"option3" => $postData["txt_option3"],
					"option4" => $postData["txt_option4"],
					"correct_answer"=> $postData['answers_1']));

					if($insertId)
					{	
						echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records added successfully</div>')); exit;
					}
					 else{
						echo json_encode(array("status" => 2,"msg" => 'Something went wrong, Try again')); exit;
					} 
				
			}//edit quetionary
			else{
				echo json_encode(array("status" => 2,"msg" => 'Something went wrong, Try again')); exit;
			}
		}
	}	
	}

	// function to update sub questions

	public function edit_sub_questions(){
		if(is_user_logged_in())
		{
			if($this->checkPermission() == 1)
			{
				$postData = $this->input->post();  
			   //pr($postData);

			$cond = array("o_id"=>$postData["o_id"]);
			
			$updateArr = $this->common_model->update_data(TB_QUE_OPTIONS,$cond,array("question" => $postData["txt_question"], "option1" => $postData["txt_option1"],"option2" => $postData["txt_option2"],"option3" => $postData["txt_option3"],"option4" => $postData["txt_option4"],"correct_answer"=>$postData["answers_1"],"status"=>$postData["status"]));
			// echo $this->db->last_query();
			// die();

			if($updateArr)
				{	
					echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records updated successfully.</div>')); exit;
				}  
			else{
				echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">Record not updated</div>')); exit;
				}

				
			}
		}
	}




	//add quetionary
	public function submit_questions(){
			if(is_user_logged_in())
			{

				if($this->checkPermission() == 1){
					
					$postData = $this->input->post();  
						// pr($postData);
						// die;
					if($postData["questionId"] == "")//add banner
					{  	
						$data_resource = "";
						$data_subresource="";
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}

						if(!empty($postData["dd_subresources"])){
									foreach($postData["dd_subresources"] as $data2):
											$data_subresource .= $data2.',';
									endforeach;
								}
					 
							$insertId = $this->common_model->insert(TB_QUESTIONNAIRE,array("start_date" =>$postData['start_date'],"end_date"=>$postData['end_date'],"title" =>$postData['txt_title'],"promo_amt" =>$postData['txt_promo_amount'],"promo_code" => $postData["txt_promocode"],"country" => $postData["country"],"user_type"=> $postData['user_type'],"category_id"=>$data_resource,"sub_category_id"=>$data_subresource,"date_modified" => date('Y-m-d H:i:s')));

							   
								if($insertId)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records added successfully</div>')); exit;
								}
								 else{
									echo json_encode(array("status" => 2,"msg" => 'Something went wrong, Try again')); exit;
								} 
						
					}//edit quetionary
					else{
						// pr($postData);
						// die();
							$data_resource = "";
							$data_subresource="";
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .=$data2.',';
								endforeach;
							}

							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .=$data2.',';
								endforeach;
							}
							 $cond1 = array("id" => $this->encrypt->decode($postData["questionId"]));
						//  pr($postData);
						// die;
$updateArr = $this->common_model->update_data(TB_QUESTIONNAIRE,$cond1,array(
	"start_date"=>trim($postData["start_date"]),
	"end_date"=>trim($postData["end_date"]),
	"title"=>trim($postData["txt_title"]),
	"promo_amt"=>trim($postData["txt_promo_amount"]),
	"promo_code"=>trim($postData["txt_promocode"]),
	"category_id"=>trim($data_resource),
	"sub_category_id"=>trim($data_subresource),
	"user_type"=>trim($postData["user_type"]),
	"country"=>trim($postData["country"]),
	"date_modified"=> date('Y-m-d H:i:s')));
							//echo "update->".$updateArr;
							//echo $this->db->last_query();
							//exit;
							  if($updateArr)
							  {	
								   echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records updated successfully.</div>')); exit;
							   }	
							else{
						           echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">Record not updated</div>')); exit;
					          } 
						

					} //end edit

				}
			}
	
		
	}
	// functio to load quetionaries page 
	public function addQuestionnaire(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				$data["country"] = $this->administration_model->getCountry();
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				$this->load->view("administrator/addquestionnaire",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}


	//function to load language 
	public function getLanguages(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				
				//$data["country"] = $this->administration_model->getCountry();
				$cond = array();
				//$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				
				$this->load->view("administrator/languages",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}

	//function to load push notification 
	public function getPushnotification(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				
				$data["country"] = $this->administration_model->getCountry();
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				
				$this->load->view("administrator/pushManagement",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}

	// function to manage section
	public function getSection(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				//$data["country"] = $this->administration_model->getCountry();
				$cond = array();
				$cond_cat = array("status_pmk"=>"Active");
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond_cat);
				
				$this->load->view("administrator/sectionManagement",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}

	// functio to load quetionaries page 
	public function getQuestionnaire(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				
				$data["country"] = $this->administration_model->getCountry();
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				
				$this->load->view("administrator/questionnaire",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}

	// function add_sub_questions(){
	public function getSubQuestionnaire($id){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				$cond = array();
				$data['id']=$id;
				$this->load->view("administrator/addquestionnaire",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}

	// function to get other languages(){
	public function getOtherLanguages($id){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				$cond = array();
				$data['section_id']=$id;
				$cond_cat = array("status_pmk"=>"Active");
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond_cat);
			$data['sections']=$this->administration_model->getAllSections();

			$cond_all_lang=array('lang_id !='=>'1','section_id'=>$id);
			 $data['addedSectionLang'] = $this->administration_model->getAllreadyAddedLanguages($cond_all_lang);


			 $lang_idArr=array_map(function($e1){ return $e1['lang_id']; },$data['addedSectionLang']);

			 //echo count($lang_idArr);


			 if(count($lang_idArr)==0){
			 	$data['languages']=$this->administration_model->getLanguages();
			 }else{
			 	$cond_id = array('lang_id' =>array_filter($lang_idArr));
			    $data['languages']=$this->administration_model->getOtherLanguage($cond_id);
  
			 }
			 //echo $this->db->last_query();

			 
				$this->load->view("administrator/otherlanguages",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
  

	
	public function getUsers()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			//echo $this->checkPermission()
			if($this->checkPermission() == 1){
				
				$data["country"] = $this->administration_model->getCountry();
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				
				$this->load->view("administrator/users",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	//function to get all banners
	public function getBanners()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
			$data["country"] = $this->administration_model->getCountry();
			$this->load->view("administrator/banners",$data);
		}
	} 

	//get sub quetionary by ID
	public function getSubQuetionaryById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("o_id" => $postData["id"]);
			// pr($cond);
			// exit
			$quetions = $this->administration_model->getSubQuetionaryById($cond);
			//print_r($quetions);die;
		
			echo json_encode($quetions);exit;	
		}
	}

	//get quetionary by ID
	public function getQuetionaryById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("id" => $this->encrypt->decode($postData["id"]));
			$quetions = $this->administration_model->getQuetionaryById($cond);
			//print_r($quetions);die;
		
			echo json_encode($quetions);exit;	
		}
	}

	

	// delete quetionary by id
	public function deleteSubQuetionaryById() // Get Single User
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				// pr($postData);
				// exit;
				//echo  count($postData["o_id"]);
				$arrDelete = array();
				for($i = 0; $i < count($postData["o_id"]); $i++)
				{
					
					$isdelete = 0;
					$cond = array("o_id" => $postData["o_id"][$i]);
					
					$this->common_model->delete(TB_QUE_OPTIONS,$cond);
					
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record deleted successfully."));exit;
				
				 
			}
		}
	}

	// funcion to get a single user
	public function getSectionById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("s_id" => $postData["id"]);
			$sections = $this->administration_model->getSectionById($cond);
			//print_r($users[0]);die;
			//echo'sdfsd'.$pass= $users[0]['password'];
		//	echo md5($pass);die();
		//echo$this->encrypt->decode($pass);die;
			echo json_encode($sections[0]);exit;	
		}
	}

	// funcion to get a single lang
	public function getLanguageById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("lang_id" => $postData["id"]);
			$sections = $this->administration_model->getLanguageById($cond);
			//print_r($users[0]);die;
			//echo'sdfsd'.$pass= $users[0]['password'];
		//	echo md5($pass);die();
		//echo$this->encrypt->decode($pass);die;
			echo json_encode($sections[0]);exit;	
		}
	}

	
	public function getOtherLangById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("page_id" => $postData['id']);
		    $otherlang = $this->administration_model->getAllPagesSK($cond);
			//print_r($users[0]);die;
			//echo'sdfsd'.$pass= $users[0]['password'];
		//	echo md5($pass);die();
		//echo$this->encrypt->decode($pass);die;
			echo json_encode($otherlang[0]);exit;	
		}
	}
	  
	
	public function getUserById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("user_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getUserById($cond);
			//print_r($users[0]);die;
			//echo'sdfsd'.$pass= $users[0]['password'];
		//	echo md5($pass);die();
		//echo$this->encrypt->decode($pass);die;
			echo json_encode($users[0]);exit;	
		}
	}

	//function to edit banner

	public function getBannerById() // Get Single Banner
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("b_id" => $this->encrypt->decode($postData["id"]));
			$banners = $this->administration_model->getBannerById($cond);
			//print_r($users[0]);die;
			echo json_encode($banners[0]);exit;	
		}
	}


	//banners add/edit type

	public function save_banners()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				if($this->checkPermission() == 1){
					
					$postData = $this->input->post();  
						// print_r($postData);
						//die;
					if($postData["bannerId"] == "")//add banner
					{  	
						$data_resource = "";
						$data_subresource="";
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}

						if(!empty($postData["dd_subresources"])){
									foreach($postData["dd_subresources"] as $data2):
											$data_subresource .= $data2.',';
									endforeach;
								}
						$arr=array();		
						$_path = $this->upload_file($_FILES);
						$arr=json_decode($_path);

					 	//$start_date = date("Y-m-d",strtotime($postData['start_date']));
					 	//$end_date   = date("Y-m-d",strtotime($postData['end_date']));
						
						if($arr->status==1){
							$banner_image=$arr->msg;
							
							$insertId = $this->common_model->insert(TB_BANNER,array("start_date" =>$postData['start_date'],"end_date"=>$postData['end_date'],"status"=>$postData['status'],"banner_image" =>$banner_image,"country" => $postData["country"],"user_type"=> $postData['user_type'],"category_id"=>$data_resource,"sub_category_id"=>$data_subresource,"date_modified" => date('Y-m-d H:i:s')));
							      // echo $this->db->last_query();
							     //  die();
							
								if($insertId)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records added successfully</div>')); exit;
								}  
						}else{
							$this->session->set_flashdata('Error',$arr->msg);
							//echo $arr->msg;exit;
							echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">'.$arr->msg.'</div>')); exit;
						}
					}//edit banner
					else{
						$data_resource = "";
						$data_subresource="";
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}

						if(!empty($postData["dd_subresources"])){
									foreach($postData["dd_subresources"] as $data2):
											$data_subresource .= $data2.',';
									endforeach;
								}
						$cond1 = array("b_id" => $this->encrypt->decode($postData["bannerId"]));
						if($postData["upload_flag"]=='1'){ //if image update
						//if(!empty($postData["imgflag"])){ //if image update
							$arr=array();		
							$_path = $this->upload_file($_FILES);
							$arr=json_decode($_path);

							if($arr->status==1){
								$banner_image=$arr->msg;
								//$start_date = date("Y-m-d",strtotime($postData['start_date']));
					 		 	//$end_date   = date("Y-m-d",strtotime($postData['end_date']));
								$updateArr = $this->common_model->update(TB_BANNER,$cond1,array("start_date" => $postData['start_date'], "end_date" => $postData['end_date'],"status" => $postData["status"],"status" => $postData['status'],"banner_image" => $banner_image,"country"=>$postData["country"],"user_type"=>$postData['user_type'], "category_id" => $data_resource,"sub_category_id" => $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
							     //  echo $this->db->last_query();
							     //  die();
							
								if($updateArr)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records updated successfully.</div>')); exit;
								}  
							}else{
								echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">'.$arr->msg.'</div>')); exit;
							}
						}else{
							//$start_date = date("Y-m-d",strtotime($postData['start_date']));
					 		//$end_date   = date("Y-m-d",strtotime($postData['end_date']));
							$updateArr = $this->common_model->update(TB_BANNER,$cond1,array("start_date" => $postData['start_date'], "end_date" => $postData['end_date'],"status" => $postData["status"],"status" => $postData['status'],"country"=>$postData["country"],"user_type"=>$postData['user_type'], "category_id" => $data_resource,"sub_category_id" => $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
							      // echo $this->db->last_query();
							     //  die();
							
								if($updateArr)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records updated successfully.</div>')); exit;
								}  
						}	

					}//end edit
					
				}//end check permission
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}

	/*
	* Desc:Function to upload banner file
	*/
	public function upload_file() {

		$upload_path = UPLOAD_BANNER_PATH;

		$filename="banner_image";
		if (!file_exists($upload_path)) {
			$mask = umask(0);
			mkdir($upload_path,0777,true);
			umask($mask);
		}

		$config['upload_path'] = UPLOAD_BANNER_PATH;
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
		$config['max_size'] = '2048';
		//$config['max_width']  = '300';
		$config['min_width']  = '1500';
		///$config['max_height']  = '720';
		$config['min_height']  = '514';
		$name = md5(date('Y-m-d:h:m:s'));
		$config['file_name'] = $name; 

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
	  
	   if (! $this->upload->do_upload('banner_image')) { 
		   $msg = $this->upload->display_errors();
	   	   return(json_encode(array("status" => 2, "msg"=>$msg)));  
		}  
		else{
				$upload_data = array('upload_data' => $this->upload->data()); // get data
				$upload_file = $upload_data['upload_data']['full_path']; // get file path
				chmod($upload_file,0777);
				$upload_data1 = $this->upload->data(); 
				$file_name = $upload_data1['file_name']; // get file name 
				return(json_encode(array("status" => 1, "msg"=>$file_name))); 
				//return  $file_name;		
		}      
		
	}	

	// function to add questionary
	public function save_questionary(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				if($this->checkPermission() == 1){
					$postData = $this->input->post();  
					//pr($postData);die;
					if($postData["questionId"] == "")
					{   
						$isValid = $this->validateUser($postData); //check validations
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							
								$data_resource = "";
								$data_subresource="";
								if(!empty($postData["dd_resources"])){
									foreach($postData["dd_resources"] as $data2):
											$data_resource .= $data2.',';
									endforeach;
								}
								
								if(!empty($postData["dd_subresources"])){
									foreach($postData["dd_subresources"] as $data2):
											$data_subresource .= $data2.',';
									endforeach;
								}
								//added by pallavi for user type add
								if(!empty($postData["user_type"])){
									$user_type=$postData["user_type"];		
								}
								$insertId = $this->common_model->insert(TB_USERS,array("first_name" => $postData["txt_first_name"], "last_name" => $postData["txt_last_name"],"email_address" => $postData["txt_email_address"],"pdf_file_access" => $pdf_file_access,"v_cat_id"=>$data_resource,"v_sub_cat_id"=>$data_subresource,"company" => $postData["txt_company"], "contact_details" => $postData["txt_contact_number"],"password" => md5($postData["txt_password"]),"country" => $postData["country"],"ai_file_access" => $ai_file_access,"super_admin_access"=> $super_admin_access,"user_type"=> $user_type,"lang_translator"=> $lang_translator,"date_modified" => date('Y-m-d H:i:s')));
								if($insertId)
								{  
									
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success">Users has been added successfully and mail sent with the login credentials</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger">Opps, there is problem please try again.</div>')); exit;
								}
							}
							else
							{	
								echo json_encode(array("status" => 2,"action" => "add", "msg" =>"<div class='alert alert-danger'>This Email address has been already available.</div>")); exit;
							}
						}
						else   //validation false
						{
							echo json_encode($isValid);
						} 
						 
					}
				}
		}

	}

	//save section 
	public function save_section() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				if($this->checkPermission() == 1)
				{
					$postData = $this->input->post();  
						// print_r($postData);
						// die;

					if($postData["sectionId"] == "")//add banner
					{  

						$cond = array("name" => $postData["name"],"cat_id"=>$postData['sel_cat'],"subcat_id"=>$postData['sel_subcat']);
						$sections = $this->administration_model->getSectionById($cond);

						if(count($sections)==0){
							$insertId = $this->common_model->insert(TB_SECTION,array("cat_id"=>$postData['sel_cat'],"subcat_id"=>$postData['sel_subcat'],"name"=>$postData['name']));

								if($insertId)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records added successfully</div>')); exit;
								} 
								else
								{	
									echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">Opps! Something went wrong</div>')); exit;
								}
						}else{
							echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">This section is allredy exists.</div>')); exit;
						}
					    
					}//edit section
					else{

						$cond = array("name" => $postData["name"],"cat_id"=>$postData['sel_cat'],"subcat_id"=>$postData['sel_subcat']);
						$sections = $this->administration_model->getSectionById($cond);

						if(count($sections)<=1){
							$cond1=array('s_id'=>$postData['sectionId']);

					   		$updateArr = $this->common_model->update_data(TB_SECTION,$cond1,array("name" => $postData["name"],"cat_id"=>$postData['sel_cat'],"subcat_id"=>$postData['sel_subcat']));

								if($updateArr)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records updated successfully</div>')); exit;
								} 
								else
								{	
									echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">Opps! Something went wrong</div>')); exit;
								}
						}else{
							echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">This section is allredy exists.</div>')); exit;
						}
						
					    

					}
				}
			}
		}
	}


	//save language 
	public function save_language() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				if($this->checkPermission() == 1)
				{
					$postData = $this->input->post();  
					
					if($postData["languageId"] == "")//add banner
					{  	
						$cond_lng = array('language'=>$postData['language'],"lang_code"=>$postData['lang_code']);

						$ckExist = $this->administration_model->getLanguageById($cond_lng);

						if(count($ckExist)==0){
							 $insertId = $this->common_model->insert(TB_LANG,array("lang_code"=>$postData['lang_code'],"language"=>$postData['language']));
					    			// echo $this->db->last_query();
					    			// die();
								if($insertId)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records added successfully</div>')); exit;
								} 
								else
								{	
									echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">Opps! Something went wrong</div>')); exit;
								}
							}else{
								echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">This language is allredy exists</div>')); exit;
							}
						
					   		
					}//edit section
					else{
					    $cond1=array('lang_id'=>$postData['languageId']); 
					    $ckExist = $this->administration_model->getLanguageById($cond1);
					    //  echo "string".$this->db->last_query();
					    // die();
					    if(count($ckExist==1)){
					    	$updateArr = $this->common_model->update_data(TB_LANG,$cond1,array("lang_code" => $postData["lang_code"],"language"=>$postData['language']));
					   
								if($updateArr)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records updated successfully</div>')); exit;
								} 
								else
								{	
									echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">Opps! Something went wrong</div>')); exit;
								}
							}else{
								echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">This language is allredy exists.</div>')); exit;
							}
					    

					}
				}
			}
		}
	}

	
	public function save_pushnotification() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				if($this->checkPermission() == 1)
				{
					$postData = $this->input->post();  
						// print_r($postData);
						// die;
					if($postData["pushId"] == "")//add banner
					{  	
						$data_resource = "";
						$data_subresource="";
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						if(!empty($postData["dd_subresources"]))
						{
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
					    }
    			$cond=array();
				$cond= array('user_type'=>$postData['user_type'],'country'=>$postData['country']);
				$users = $this->administration_model->getUsersForNotification($cond,$data_resource);
				$emails=[];
				foreach ($users as $user) {
					$emails[]=$user['email_address'];	 
				}

				if($postData["is_sent_web"]==1){

					$config['mailtype'] ='html';
					$config['charset'] ='iso-8859-1';
					$this->email->initialize($config);
					$msg=$postData['txt_message'];
					 ob_start();?>
					<!DOCTYPE html>
<html>
<head>
<title></title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
    table{border-collapse: collapse !important;}
    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        .wrapper {
          width: 100% !important;
            max-width: 100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        .logo img {
          margin: 0 auto !important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        .mobile-hide {
          display: none !important;
        }

        .img-max {
          max-width: 100% !important;
          width: 100% !important;
          height: auto !important;
        }

        /* FULL-WIDTH TABLES */
        .responsive-table {
          width: 100% !important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        .padding {
          padding: 10px 5% 15px 5% !important;
        }

        .padding-meta {
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        .padding-copy {
             padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        .no-padding {
          padding: 0 !important;
        }

        .section-padding {
          padding: 50px 15px 50px 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        .mobile-button-container {
            margin: 0 auto;
            width: 100% !important;
        }

        .mobile-button {
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
            display: block !important;
        }

    }

    /* ANDROID CENTER FIX */
    div[style*="margin: 16px 0;"] { margin: 0 !important; }
</style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...
</div>

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#EB181E" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                        <a href="#" target="_blank" style="font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px; text-align:center;">
                            <img alt="Logo" src="logo.png" width="200"  style="display: inline-block; " border="0"> 
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
                <tr>
                    <td>
                        <!-- HERO IMAGE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding">Our Greatest Feature Ever</td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius, leo a ullamcorper feugiat, ante purus sodales justo, a faucibus libero lacus a est. Aenean at mollis ipsum.</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <!-- BULLETPROOF BUTTON -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="padding-top: 25px;" class="padding">
                                                <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                    <tr>
                                                        <td align="center" style="border-radius: 3px;" bgcolor="#EB181E"><a href="#" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; border: 1px solid #EB181E; display: inline-block;" class="mobile-button">Button Title &rarr;</a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
            <!-- UNSUBSCRIBE COPY -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
                <tr>
                    <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
                        � 2017 denso.com
                        <br>
                        <a href="#" target="_blank" style="color: #666666; text-decoration: none;">Unsubscribe</a>
                        <span style="font-family: Arial, sans-serif; font-size: 12px; color: #444444;">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                        <a href="#" target="_blank" style="color: #666666; text-decoration: none;">View this email in your browser</a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>

</body>
</html>

					<?php $this->messageBody  = ob_get_clean();
					//echo $this->messageBody;
					$this->email->from('asiamarketing@denso.com.sg');
					$this->email->to('tapkir.pallavi06@gmail.com');

					$this->email->subject('DENSO AFTERMARKET WEBSITE');
					$this->email->message($this->messageBody);	
					/*If the user is langauage translator then dont send email to him/her*/
						$res=$this->email->send();	
				}
				$insertId = $this->common_model->insert(TB_PUSH_NOTIFICATION,array("country" => $postData["country"]," 	is_send_web"=>$postData["is_sent_web"],
					    	"user_type"=> $postData['user_type'],"category_id"=>$data_resource,"sub_category_id"=>$data_subresource,"message"=>$postData['txt_message'],"date_modified" => date('Y-m-d H:i:s')));
						       // echo $this->db->last_query();
						       // die();
								if($insertId)
								{	
									echo json_encode(array("status" => 1,"msg" => '<div class="alert alert-success">Records added successfully</div>')); exit;
								} 
								else
								{	
									echo json_encode(array("status" => 2,"msg" => '<div class="alert alert-danger">'.$arr->msg.'</div>')); exit;
								}
					}//edit banner
				}
			}
		}
	}





	public function save_users() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				if($this->checkPermission() == 1){
					$postData = $this->input->post();  
					//print_r($postData);die;
					if($postData["userId"] == "")
					{   
						$isValid = $this->validateUser($postData); //check validations
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							$cond = array("email_address"=>$postData["txt_email_address"]);
							$users = $this->administration_model->getAllUsers($cond);
							$data["users"] = $users;
							//$randpwd = trim($this->genarateRandomPassword(10)); 
							$randpwd = $postData["txt_password"];  
							$hostname = $this->config->item('hostname');
							
							if(count($users) == 0)
							{
								if($postData["super_admin_access"] =="on" || $postData["super_admin_access"] =="1")
								{//echo "1if";
									$super_admin_access = "1";
								}
								else
								{//echo "1else";
									$super_admin_access = "0";
								}
								if($postData["pdf_file_access"] =="on" || $postData["pdf_file_access"] =="1")
								{
									//echo "2iff";
									$pdf_file_access = "1";
								}
								else
								{
									//echo "2else";
									$pdf_file_access = 0;
								}
								if($postData["ai_file_access"] =="on" || $postData["ai_file_access"] =="1")
								{
									$ai_file_access = "1";
								}
								else
								{
									//echo"3else";
									$ai_file_access = "0";
								}
								
								if($postData["lang_translator"] =="on" || $postData["lang_translator"] =="1")
								{
									//echo "4iff";
									$lang_translator = "1";
								}
								else
								{
									//echo "4elsee";
									$lang_translator = "0";
								}
								
								$data_resource = "";
								$data_subresource="";
								if(!empty($postData["dd_resources"])){
									foreach($postData["dd_resources"] as $data2):
											$data_resource .= $data2.',';
									endforeach;
								}
								
								if(!empty($postData["dd_subresources"])){
									foreach($postData["dd_subresources"] as $data2):
											$data_subresource .= $data2.',';
		 							endforeach;
								}
								//added by pallavi for user type add
								if(!empty($postData["user_type"])){
									$user_type=$postData["user_type"];		
								}
								$insertId = $this->common_model->insert(TB_USERS,array("first_name" => $postData["txt_first_name"], "last_name" => $postData["txt_last_name"],"email_address" => $postData["txt_email_address"],"pdf_file_access" => $pdf_file_access,"v_cat_id"=>$data_resource,"v_sub_cat_id"=>$data_subresource,"company" => $postData["txt_company"], "contact_details" => $postData["txt_contact_number"],"password" => md5($postData["txt_password"]),"country" => $postData["country"],"ai_file_access" => $ai_file_access,"super_admin_access"=> $super_admin_access,"user_type"=> $user_type,"lang_translator"=> $lang_translator,"date_modified" => date('Y-m-d H:i:s')));
								if($insertId)
								{  
									/*
									 * insert user in as admin in admin tbl if having super admin access
									 * */
									/*$this->session->set_flashdata('Success', 'Record added successfully');*/
									 
									 if($super_admin_access == 1 || $lang_translator == 1){
										 $insertId = $this->common_model->insert(TB_ADMIN,array("username" => $postData["txt_email_address"],"password" => md5($randpwd),"date_modified" => date('Y-m-d H:i:s')));	  
									 }
										
									/*
									 * Send email of registration
									 * 
									 * */
									//$super_admin_text = "";
									
									if($super_admin_access=="1"){
										$super_admin_text = "We have added you as a admin on
	DENSO Aftermarket Website. <br/>Your login details are the same as above<br/>Click the link below to login to your admin account.<br/><br/>
										  <a href='".$hostname."adminlogin'>".$hostname."adminlogin</a><br/><br/>";
									}else{
										$super_admin_text = "";
									}
									
									//$lang_translator_text = "";
									
									if($lang_translator == "1"){
										$lang_translator_text = "We have added you as a admin(Langauge Translator) for on
	DENSO Sales Kit Website. <br/>Your login details are the same as above<br/>Click the link below to login to your admin account.<br/><br/>
										  <a href='".$hostname."adminlogin'>".$hostname."adminlogin</a><br/><br/>";
									}else{
										$lang_translator_text = "";
									}
									
									
									$username = $postData["txt_first_name"]." ".$postData["txt_last_name"];
									$emailaddress = $postData["txt_email_address"];
									$config['mailtype'] ='html';
									$config['charset'] ='iso-8859-1';
									$this->email->initialize($config);
									$this->messageBody  = "Hello $username<br/><br/>
										 As per request from your friendly DENSO representative, we have added you as a user on
	DENSO Aftermarket Website. <br/>
										 Your login details are below:<br/> 
										 Email Address : $emailaddress<br/>
										 Password : $randpwd<br/>
										 Click the link below to login to your user account.<br/><br/>
										 <a href='".$hostname."'>".$hostname."</a>
										 <br/><br/>
										 $super_admin_text $lang_translator
										 <br/>If the link is not working properly then copy and paste the link in your browser	 
										 <br/><br/>
																			 
										 Regards,<br/>DENSO AFTERMARKET WEBSITE";
									 //echo $this->messageBody;
									
									$this->email->from('asiamarketing@denso.com.sg');
									$this->email->to($emailaddress);

									$this->email->subject('DENSO AFTERMARKET WEBSITE - Login details');
									$this->email->message($this->messageBody);	
									/*If the user is langauage translator then dont send email to him/her*/
									if($lang_translator==1){
										$res=$this->email->send();	
										//echo "Message sent status-".$res;
										echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success">Users has been added successfully and mail sent with the login credentials</div>')); exit;
									}
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success">Users has been added successfully</div>')); exit;
									
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger">Opps, there is problem please try again.</div>')); exit;
								}
							}
							else
							{	
								echo json_encode(array("status" => 2,"action" => "add", "msg" =>"<div class='alert alert-danger'>This Email address has been already available.</div>")); exit;
							}
						}
						else   //validation false
						{
							echo json_encode($isValid);
						} 
						 
					}
					else  // for edit
					{ 
						$isValid = $this->validateUser($postData);
						if($isValid["status"] == 1)
						{
							$cond = array("user_id !=" => $this->encrypt->decode($postData["userId"]), "email_address"=>$postData["txt_email_address"]);
							$cond1 = array("user_id" => $this->encrypt->decode($postData["userId"]));
							$users = $this->administration_model->getAllUsers($cond);
							$userbyid = $this->administration_model->getUserById($cond1); 
							$data["users"] = $users; 
							$randpwd = $postData["txt_password"]; 
							$txt_heading="";
							//echo count($users);die;
							if(count($users) == 0)
							{  
								
								if(isset($postData["super_admin_access"]) && ($postData["super_admin_access"] =="on" || $postData["super_admin_access"] ==1))
								{
									$super_admin_access = "1";
								}
								else
								{
									$super_admin_access = "0";
								}
								if(isset($postData["pdf_file_access"]) && ($postData["pdf_file_access"] =="on" || $postData["pdf_file_access"] ==1))
								{
									$pdf_file_access = "1";
								}
								else
								{
									$pdf_file_access = "0";
								}
								if(isset($postData["ai_file_access"]) && ($postData["ai_file_access"] =="on" || $postData["ai_file_access"] ==1))
								{
									$ai_file_access = "1";
								}
								else
								{
									$ai_file_access = "0";
								}
								
								if(isset($postData["lang_translator"]) && ($postData["lang_translator"] =="on" || $postData["lang_translator"] =="1"))
								{
									$lang_translator = "1";
								}
								else
								{
									$lang_translator = "0";
								}
								
								$data_resource ="";
								$data_subresource="";
								if(!empty($postData["dd_resources"])){
									foreach($postData["dd_resources"] as $data2):
											$data_resource .= $data2.',';
									endforeach;
								}
								
								if(!empty($postData["dd_subresources"])){
									foreach($postData["dd_subresources"] as $data2):
											$data_subresource .= $data2.',';
									endforeach;
								}	
								//added by pallavi for user type update
								if(!empty($postData["user_type"])){
									$user_type=$postData["user_type"];		
								}
								$updateArr = $this->common_model->update(TB_USERS,$cond1,array("first_name" => $postData["txt_first_name"], "last_name" => $postData["txt_last_name"],"email_address" => $postData["txt_email_address"],"pdf_file_access" => $pdf_file_access,"company" => $postData["txt_company"],"v_cat_id"=>$data_resource,"v_sub_cat_id"=>$data_subresource, "contact_details" => $postData["txt_contact_number"],"country" => $postData["country"],"ai_file_access" => $ai_file_access,"super_admin_access"=> $super_admin_access,"lang_translator"=> $lang_translator,"user_type"=> $user_type,"date_modified" => date('Y-m-d H:i:s')));

								$updatepassword=0; 
								// if(isset($postData["txt_password"]) && $postData["txt_password"]!="") {
									
									//$updatepassword = $this->common_model->update(TB_USERS,$cond1,array("password" => md5($randpwd)));
									//die;
								//}
								 
															 
								/*if($updateArr)
								{*/
									
									 if($super_admin_access == 0 ){
										 $this->common_model->delete(TB_ADMIN,array("username" => $postData["txt_email_address"]));
									 }
									 elseif($super_admin_access == 1){
											   
												$cond = array("username"=>$postData["txt_email_address"]);
												$admin = $this->administration_model->getAllAdmin($cond); 
												
												if(count($admin)==1){									 
													if(isset($postData["txt_password"]) && $postData["txt_password"]!="") {
														$updatepassword = $this->common_model->update(TB_ADMIN,$cond,array("password" => md5($randpwd))); 
													}
													if($updatepassword){
														$txt_heading = "We have changed your password<br/>";
													}
													else{
														$txt_heading = "We have added you on Denso - Admin<br/>";
													}  
												}
												else if(count($admin)==0 ){
															$cond = array("email_address"=>$postData["txt_email_address"]);
															$getpassword = $this->administration_model->getAllUsers($cond);
															//print_r($getpassword);
															$insertId = $this->common_model->insert(TB_ADMIN,array("username" => $postData["txt_email_address"],"password" => $getpassword["0"]["password"],"date_modified" => date('Y-m-d H:i:s'))); 
															$txt_heading = "We have added you on Denso - Admin<br/>";
															$randpwd ="[Password is same as user]";
												}
														
												$email_address  = $postData["txt_email_address"]; 	  
									 }
									 if($lang_translator == 1){
										 $cond = array("email_address"=>$postData["txt_email_address"]);
															$getpassword = $this->administration_model->getAllUsers($cond);
															//print_r($getpassword);
															$insertId = $this->common_model->insert(TB_ADMIN,array("username" => $postData["txt_email_address"],"password" => $getpassword["0"]["password"],"date_modified" => date('Y-m-d H:i:s'))); 
															$txt_heading = "We have added you on Denso - Language Translator<br/>";
															$randpwd ="[Password is same as user]";
									 }
									/*
									 * Send email of registration
									 * 
									 * */
									if($updatepassword || $insertId){
										$hostname = $this->config->item('hostname');
													$username = $postData["txt_first_name"]." ".$postData["txt_last_name"];
													$emailaddress = $postData["txt_email_address"];
													$config['mailtype'] ='html';
													$config['charset'] ='iso-8859-1';
													$this->email->initialize($config);
													$this->messageBody  = "Hello, $username<br/><br/>
														 $txt_heading
														 Your login details are below:<br/> 
														 Email Address : $emailaddress<br/>
														 Password : $randpwd<br/>
														 Click the link below to login to your account.<br/><br/>
														 <a href='".$hostname."'>".$hostname."</a>
														 <br/><br/><br/>If the link is not working properly, then copy and paste the link in your browser.
														  <br/><br/>If you did not send this request, please ignore this email.
														 <br/><br/>Regards,<br/>DENSO AFTERMARKET WEBSITE";
													 //echo $this->messageBody;
													 //die;
													$this->email->from(EMAIL_FROM, 'DENSO');
													$this->email->to("$emailaddress");

													$this->email->subject('DENSO - Login credentials');
													$this->email->message($this->messageBody);	
													if($lang_translator=='1'){
														$this->email->send();
													}
									}
									
									$cond2 = array("user_id" => $this->encrypt->decode($postData["userId"]));
									$users = $this->administration_model->getAllUsers($cond2); 
									 
									if($users["0"]["dn_status"] == "Active")
									{
										$status_text = "Make Inactive";
										$status = "Inactive";
									}
									else{
										$status_text = "Make Active";
										$status = "Active";
									}
									$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
											<td class="text-center">'.$postData["txt_first_name"].'</td>
											<td>'.$postData["txt_last_name"].'</td>
											<td>'.$postData["txt_email_address"].'</td>
											<td>'.$postData["txt_company"].'</td>
											<td class="text-center">
												<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
												<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($postData["userId"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["userId"]).'" href="javascript:void(0)">'.$status_text.'</a>
											</td>';
											//echo $this->encrypt->decode($postData["userId"]);die;
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => '<div class="alert  alert-success">Users has been updated successfully.</div>')); exit;
								/*}
								else
								{
									//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Users has been updated successfully.</div>')); exit;
								}*/
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger">This email address has been already taken.</div>')); exit;
							}
						}
						else
						{
							return json_encode($isValid);
						}
						
					}
				}
			}
			else
			{
				return json_encode(array("status" => 0, "msg"=>"Redirect to login page."));
			}
		}
	}


   //function  to change Sub Questions status
	public function change_questions_status(){
	 if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//pr($postData);die;
				if($postData["o_id"] != "" && $postData["status"] != "")
				{  
					//pr($postData);die();
					if($postData["status"] == "Active"){
						$status_update = "Active";
					}
					else if($postData["status"] == "Inactive"){
						$status_update = "Inactive";
					}
					
					$cond = array("o_id" => $postData["o_id"]);
					// pr($cond);
					// exit;
					$questions = $this->administration_model->getAllSubQuestions($cond);
					// echo $this->db->last_query();
					// pr($questions);die();

					if(count($questions) != 0)
					{ 

						$updateArr = $this->common_model->update(TB_QUE_OPTIONS,$cond,array("status" => $status_update));
						//echo $this->db->last_query();
						if($updateArr)
						{
							if($postData["status"] == "1"){
								$status_text = "Make Inactive";
								$status_active = "btn btn-xs btn-success";
								$status_inactive = "btn btn-xs  active btn-default";
								$status = "0";
							}
							else{
								$status_text = "Make Active";
								$status_active = "btn btn-xs  active btn-default";
								$status_inactive = "btn btn-xs btn-danger";
								$status = "1";
							}

							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["o_id"].'"></td>
									<td class="text-center">'.$questions["0"]["question"].'</td>
									<td>'.$questions["0"]["option1"].'</td>
									<td>'.$questions["0"]["option2"].'</td>
									<td>'.$banners["0"]["option3"].'</td>
									<td>'.$questions["0"]["option4"].'</td>
									<td>'.$questions["0"]["correct_answer"].'</td>


									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["o_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
										<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["o_id"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["o_id"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
										<div class="btn-group btn-toggle"> 
											<button onclick="changeStatus(\''.$postData["o_id"].'\',\'Active\')" class="'.$status_active.'">Active</button>
											<button onclick="changeStatus(\''.$postData["o_id"].'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
										</div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["o_id"]), "msg" => "Status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "Status cannot be change")); exit;
						}
					}
				}
			}
		}
	}

	//function to liast questions
	public function list_sub_questions(){
		if(is_user_logged_in())
		{ 
			if($this->checkPermission() == 1){	
				$postData = $this->input->post();

				$cond1 = array();
				$cond1 = array("q_id" => $postData["o_id"]);

				$count = $this->administration_model->getSubQuetionaryCount($cond1,$like);
				// echo $this->db->last_query();
				// pr($cond1);
				// pr($postData);
				// die();
				
				$orderColumns = array("question","option1","option2","option3","option4","correct_answer");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "o_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
					
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
					
					$Quetions = $this->administration_model->getSubQuestionsPerPage($cond1,array($order_column => $postData["order"]),$like);

					// echo $this->db->last_query();
					// pr($Quetions);
					// die();
					$links = "";
					$table = "";


					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>

									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css[4].'" width="18%" onclick="changePaginate(0,\'question\',\''.$corder[4].'\')">Quetions</th>
									<th class="text-center '.$css["0"].'" width="12%" onclick="changePaginate(0,\'option1\',\''.$corder[0].'\')">Option1</th>
									<th class="text-center '.$css["1"].'" width="12%" onclick="changePaginate(0,\'option2\',\''.$corder[1].'\')">Option2</th>
									<th class="'.$css[2].'" width="12%" onclick="changePaginate(0,\'option3\',\''.$corder[2].'\')">Option3</th>
									<th class="'.$css[3].'" width="12%" onclick="changePaginate(0,\'option4\',\''.$corder[3].'\')">Option4</th>
									<th class="'.$css[3].'" width="18%" onclick="changePaginate(0,\'correct_answer\',\''.$corder[3].'\')">Correct Answer</th>
									
									<th class="bg_white text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($Quetions)>0)
					{
							if(count($Quetions) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($Quetions as $Quetion) { 

								if($Quetion["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}

								
								/*<img src="'.base_url().'banners_img/'.$Banner["banner_image"].'" height="150" width="150" />*/
			  						
						$table .= '<tr id="row_'.$Quetion["o_id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$Quetion["o_id"].'" name="check" id="check" class="chk"></td>
								<td>'.ucfirst($Quetion["question"]).'</td>
								<td>'.ucfirst($Quetion["option1"]).'</td>
								<td>'.ucfirst($Quetion["option2"]).'</td>
								<td>'.ucfirst($Quetion["option3"]).'</td>
								<td>'.ucfirst($Quetion["option4"]).'</td>
								<td>'.'Option'.$Quetion["correct_answer"].'</td>
				 			
								<td class="text-center">
									<a title="Edit" alt="Edit Questionary" id="edit" class="editPayment" data-option="'.$Quetion["o_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									<div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$Quetion["o_id"].'\',\'Active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$Quetion["o_id"].'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($Quetion)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getBannerList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($Quetion),'paginate' => $paginate)); exit;
				}
			}
		// }
	}

	//function to list quetionary

	public function list_questionary(){
		// if(is_ajax_request())
		// {
			if(is_user_logged_in())
			{ 
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					// print_r($postData);
					 // die();
					$cond = array();
					$like = array();
					$start_date = date("m/d/Y",strtotime($postData['start_date']));
					$end_date   = date("m/d/Y",strtotime($postData['end_date']));

					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
							if($postData["searchBy"][$x] == "user_type")
							{
								$like['user_type'] = trim($postData["user_type"]); 	
							}
							// if($postData["searchBy"][$x] == "status")
							// {
							// 	$like['status'] = trim($postData["status"]); 	
							// }
							if($postData["searchBy"][$x] == "country")
							{
								$like['country'] = trim($postData["country"]); 	
							}
							if($postData["searchBy"][$x] == "start_date")
							{
								$like['start_date'] = $start_date; 	
							}
							if($postData["searchBy"][$x] == "end_date")
							{
								$like['end_date'] = $end_date; 	
							}
						}
					}

					// pr($like);
					// die();  

					$count = $this->administration_model->getQuetionaryCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("user_type","status","start_date","end_date","country");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					$country_data = $this->administration_model->getCountry();
					// pr($country_data);
					// die;
					$country_names=[];
					foreach ($country_data as $country) {
						//echo $country['name'];
						$country_names[$country['country_id']]=$country['name'];	
					}
					// pr($country_names);
					// die;
					
					$Quetions = $this->administration_model->getQuestionsPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);

					$links = "";
					$table = "";


					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>

									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="'.$css[4].'" width="20%" onclick="changePaginate(0,\'title\',\''.$corder[4].'\')">Question Set</th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'user_type\',\''.$corder[0].'\')">User Type</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'start_date\',\''.$corder[1].'\')">Start Date</th>
									<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'end_date\',\''.$corder[2].'\')">End Date</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[3].'\')">Country</th>

									<th class="bg_white text-center">Questions</th>
									
									<th class="bg_white text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($Quetions)>0)
					{
							if(count($Quetions) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($Quetions as $Quetion) { 

								if($Quetion["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}

								if($Quetion["user_type"] == "1"){
									$user_type = "Wholesaler";	
								}
								elseif ($Quetion["user_type"] == "2") {
									$user_type = "Dealers";	
								}else{
									$user_type = "NA";
								}
								
								/*<img src="'.base_url().'banners_img/'.$Banner["banner_image"].'" height="150" width="150" />*/
									
						$table .= '<tr id="row_'.$Quetion["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($Quetion["id"]).'" name="check" id="check" class="chk"></td>
								<td>'.ucfirst($Quetion["title"]).'</td>
								<td class="text-center" width="10%">'.$user_type.'</td>
								<td>'.$Quetion["start_date"].'</td>
								<td>'.$Quetion["end_date"].'</td>
								<td>'.$country_names[$Quetion["country"]].'</td>
								<td class="text-center">
									<a title="Add New Question" alt="Add New Question" id="add_question" class="add_question editPayment" data-option="'.$Quetion["id"].'" href="javascript:void(0)"><i class="fa fa-plus"></i></a>

									<!-- <a title="View All Questions" alt="View All Questions" id="view_question" class="editPayment" data-option="'.$Quetion["id"].'" href="javascript:void(0)">View </a>-->

									 <a title="View All Questions" alt="View All Questions" id="view_question" class="editPayment" data-option="'.$Quetion["id"].'" href="http://103.224.243.154/denso/administration/getSubQuestionnaire/'.$Quetion["id"].'"><i class="fa fa-eye"></i> </a>

									
								</td>
								<td class="text-center">
									<a title="Edit" alt="Edit Questionary" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($Quetion["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($Quetion)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getBannerList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($Quetion),'paginate' => $paginate)); exit;
				}
			}
		// }
	}

	

	//function for section list
	public function list_language()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					 //print_r($postData);
					 // die();
					$cond = array();
					$like = array();
					
					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
							
							
						}
					}

					// pr($like);
					// die();

					$count = $this->administration_model->getLanguageCount($cond,$like);
					// echo $this->db->last_query();
					// pr($like);
					// die();

					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("language","lang_code");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "lang_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					//$country_data = $this->administration_model->getCountry();
					// pr($country_data);
					// die;
					$country_names=[];
					foreach ($country_data as $country) {
						//echo $country['name'];
						$country_names[$country['country_id']]=$country['name'];	
					}
					// pr($country_names);
					// die;
					
					$sections = $this->administration_model->getLanguagePerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//pr($users);
					$links = "";
					$table = "";


					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'language\',\''.$corder[3].'\')">Language</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'lang_code\',\''.$corder[3].'\')">Language Code</th>
									<th class="text-center">Action</th>
									
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($sections)>0)
					{
							if(count($sections) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($sections as $section) { 

								
								
								/*<img src="'.base_url().'banners_img/'.$Banner["banner_image"].'" height="150" width="150" />*/

				$table .= '<tr id="row_'.$section["lang_id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$section["lang_id"].'" name="check" id="check" class="chk"></td>
								<td class="text-center" width="10%">'.$section["language"].'</td>
								<td class="text-center" width="10%">'.$section["lang_code"].'</td>
								
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$section["lang_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									
								</td>
							  </tr>
							  ';
									
						
							}
					}
					if($postData["start"] == 0)
					{
							if(count($sections)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getLanguageList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($sections),'paginate' => $paginate)); exit;
				}
			}
		}
	}

	//function for section list
	public function list_section()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					 //print_r($postData);
					 // die();
					$cond = array();
					$like = array();
					$join= array();


					
					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
							if($postData["searchBy"][$x] == "user_type")
							{
								$like['user_type'] = trim($postData["user_type"]); 	
							}
							
							if($postData["searchBy"][$x] == "country")
							{
								$like['country'] = trim($postData["country"]); 	
							}
							
						}
					}

					// pr($like);
					// die();

					$count = $this->administration_model->getSectionCount($cond,$like);
					// echo $this->db->last_query();
					// pr($like);
					// die();

					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array(" Section","Category","Sub Category");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "s_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					//$country_data = $this->administration_model->getCountry();
					// pr($country_data);
					// die;
					$country_names=[];
					foreach ($country_data as $country) {
						//echo $country['name'];
						$country_names[$country['country_id']]=$country['name'];	
					}
					// pr($country_names);
					// die;
					
					$sections = $this->administration_model->getSectionPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//pr($users);
					$links = "";
					$table = "";


					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'section\',\''.$corder[0].'\')">Section</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[3].'\')">Category</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[3].'\')">Subcategory</th>
									<th class="text-center">Action</th>
									
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($sections)>0)
					{
							if(count($sections) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($sections as $section) { 

								
								
								/*<img src="'.base_url().'banners_img/'.$Banner["banner_image"].'" height="150" width="150" />*/
									
						$table .= '<tr id="row_'.$section["s_id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$section["s_id"].'" name="check" id="check" class="chk"></td>
								<td class="text-center" width="10%">'.ucfirst($section["name"]).'</td>
								<td class="text-center" width="10%">'.ucfirst($section["category"]).'</td>
								
							   <td class="text-center" width="10%">'.($section["sub_category"]?ucfirst($section["sub_category"]):'NA').'</td> 
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$section["s_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($sections)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getPushnotificationList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($Pushnotifications),'paginate' => $paginate)); exit;
				}
			}
		}
	}


	//function for push list
	public function list_push_notification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					 //print_r($postData);
					 // die();
					$cond = array();
					$like = array();
					
					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
							if($postData["searchBy"][$x] == "user_type")
							{
								$like['user_type'] = trim($postData["user_type"]); 	
							}
							
							if($postData["searchBy"][$x] == "country")
							{
								$like['country'] = trim($postData["country"]); 	
							}
							
						}
					}

					// pr($like);
					// die();

					$count = $this->administration_model->getPushCount($cond,$like);
					// echo $this->db->last_query();
					// pr($like);
					// die();

					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("user_type","status","start_date","end_date","country");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					$country_data = $this->administration_model->getCountry();
					// pr($country_data);
					// die;
					$country_names=[];
					foreach ($country_data as $country) {
						//echo $country['name'];
						$country_names[$country['country_id']]=$country['name'];	
					}
					// pr($country_names);
					// die;
					$Pushnotifications = $this->administration_model->getPushPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//pr($users);
					$links = "";
					$table = "";


					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'user_type\',\''.$corder[0].'\')">User Type</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[3].'\')">Country</th>
									<th class="'.$css[4].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[3].'\')">Message</th>
									<th class="'.$css[4].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[4].'\')">Is sent web</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($Pushnotifications)>0)
					{
							if(count($Pushnotifications) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($Pushnotifications as $Pushnotification) { 

								if($Pushnotification["user_type"] == "1"){
									$user_type = "Wholesaler";	
								}
								elseif ($Pushnotification["user_type"] == "2") {
									$user_type = "Dealers";	
								}else{
									$user_type = "NA";
								}

								if ($Pushnotification["is_send_web"] == "1") {
									$is_send_web = "Yes";	
								}else{
									$is_send_web = "No";
								}

								
								/*<img src="'.base_url().'banners_img/'.$Banner["banner_image"].'" height="150" width="150" />*/
									
						$table .= '<tr id="row_'.$Pushnotification["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($Pushnotification["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center" width="10%">'.$user_type.'</td>
								<td>'.$country_names[$Pushnotification["country"]].'</td>
								<td class="text-center" width="10%">'.$Pushnotification["message"].'</td>
								<td class="text-center" width="10%">'.$is_send_web.'</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($Banner)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getPushnotificationList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($Pushnotifications),'paginate' => $paginate)); exit;
				}
			}
		}
	}

	//function for banner list
	public function list_banners()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					 //print_r($postData);
					 // die();
					$cond = array();
					$like = array();
					$start_date = date("m/d/Y",strtotime($postData['start_date']));
					$end_date   = date("m/d/Y",strtotime($postData['end_date']));

					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
							if($postData["searchBy"][$x] == "user_type")
							{
								$like['user_type'] = trim($postData["user_type"]); 	
							}
							if($postData["searchBy"][$x] == "status")
							{
								$like['status'] = trim($postData["status"]); 	
							}
							if($postData["searchBy"][$x] == "country")
							{
								$like['country'] = trim($postData["country"]); 	
							}
							if($postData["searchBy"][$x] == "start_date")
							{
								$like['start_date'] = $start_date; 	
							}
							if($postData["searchBy"][$x] == "end_date")
							{
								$like['end_date'] = $end_date; 	
							}
						}
					}

					// pr($like);
					// die();

					$count = $this->administration_model->getBannersCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("user_type","status","start_date","end_date","country");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "b_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					$country_data = $this->administration_model->getCountry();
					// pr($country_data);
					// die;
					$country_names=[];
					foreach ($country_data as $country) {
						//echo $country['name'];
						$country_names[$country['country_id']]=$country['name'];	
					}
					// pr($country_names);
					// die;
					
					$Banners = $this->administration_model->getBannersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//pr($users);
					$links = "";
					$table = "";


					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'user_type\',\''.$corder[0].'\')">User Type</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'start_date\',\''.$corder[1].'\')">Start Date</th>
									<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'end_date\',\''.$corder[2].'\')">End Date</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[3].'\')">Country</th>
									<th class="'.$css[4].'" width="20%" onclick="changePaginate(0,\'country\',\''.$corder[4].'\')">View</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($Banners)>0)
					{
							if(count($Banners) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($Banners as $Banner) { 

								if($Banner["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}

								if($Banner["user_type"] == "1"){
									$user_type = "Wholesaler";	
								}
								elseif ($Banner["user_type"] == "2") {
									$user_type = "Dealers";	
								}
								elseif ($Banner["user_type"] == "3") {
									$user_type = "Public";	
								}
								else{
									$user_type = "NA";
								}
								
								/*<img src="'.base_url().'banners_img/'.$Banner["banner_image"].'" height="150" width="150" />*/
									
						$table .= '<tr id="row_'.$Banner["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($Banner["id"]).'" name="check" id="check" class="chk"></td>
								
								<td class="text-center" width="10%">'.$user_type.'</td>
								<td>'.$Banner["start_date"].'</td>
								<td>'.$Banner["end_date"].'</td>
								<td>'.$country_names[$Banner["country"]].'</td>
								
								<td>
								<a href="'.base_url().'banners_img/'.$Banner["banner_image"].'" target="_blank">View Image</a>
								</td>
								
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($Banner["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
									<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($Banner["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($Banner["id"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
									<div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($Banner["id"]).'\',\'Active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($Banner["id"]).'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($Banner)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getBannerList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($Banners),'paginate' => $paginate)); exit;
				}
			}
		}
	
	}

	
	public function list_otherslanguages() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					// print_r($postData);
					 // die();
			$cond = array();
			$cond = array("section_id" => $postData["s_id"],"dn_pages.lang_id !="=> '1');
			$like = array();
					

					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								//$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 

							if($postData["searchBy"][$x] == "lang_id")
							{
								if($postData["lang_id"]!=""){
									$like['dn_pages.lang_id'] = trim($postData["lang_id"]);
								}	
							}
							if($postData["searchBy"][$x] == "cat_id")
							{
								$like['cat_id'] = trim($postData["cat_id"]);
							}
							// if($postData["searchBy"][$x] == "section_id")
							// {
							// 	$like['section_id'] = trim($postData["section_id"]);
							// }	

						}

					// 	pr($like);
					// die();

						
					}
					$count = $this->administration_model->getOtherLanguagesCount($cond,$like);
					// echo $this->db->last_query();
					// exit;
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("language","name","categories","sub_categories");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "page_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{	
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}  
					
					$users = $this->administration_model->getLanguagesPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'language\',\''.$corder[0].'\')">Language</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'name\',\''.$corder[1].'\')">Section</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'category\',\''.$corder[1].'\')">Category</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'sub_category\',\''.$corder[1].'\')">SubCategory</th>
										
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($users)>0)
					{
							if(count($users) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($users as $user) {  
								
									
						$table .= '<tr id="row_'.$user["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$user["page_id"].'" name="check" id="check" class="chk"></td>
								
							
								<td class="text-center">'.ucfirst($user["language"]).'</td>
								<td>'.ucfirst($user["name"]).'</td>
								<td>'.ucfirst($user["category"]).'</td>
								
								<td>'.($user["sub_category"]?ucfirst($user["sub_category"]):'NA').'</td> 
								
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$user["page_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($users)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($users),'paginate' => $paginate)); exit;
				}
			}
		}
	}
	
	public function list_users() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
				/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$postData = $this->input->post();
					 //print_r($postData);
					 // die();
					$cond = array();
					$like = array();
					
					//if($postData["search"] != "" && count($postData["searchBy"]) >= 0 )
					if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{

							if($postData["searchBy"][$x] != "" && $postData["searchBy"][$x] != "user_type")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
							if($postData["searchBy"][$x] == "user_type")
							{
								$like['user_type'] = trim($postData["user_type"]);
							}
							if($postData["searchBy"][$x] == "dn_status")
							{
								$like['dn_status'] = trim($postData["user_status"]);
							}

							// pr($like);
				   //          die();
							

						}
					}
					
						// 	pr($cond);
					 	 // pr($like);
					 	 // die();

					$count = $this->administration_model->getUsersCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("user_type","first_name","last_name","email_address","company");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "user_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}  
					
					$users = $this->administration_model->getUsersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'user_type\',\''.$corder[0].'\')">User Type</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'first_name\',\''.$corder[1].'\')">First Name</th>
									<th class="'.$css[2].'" onclick="changePaginate(0,\'last_name\',\''.$corder[2].'\')">Last Name</th>
									<th class="'.$css[3].'" width="20%" onclick="changePaginate(0,\'email_address\',\''.$corder[3].'\')">Email Address</th>
									<th class="'.$css[4].'" width="20%" onclick="changePaginate(0,\'company\',\''.$corder[4].'\')">Company</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($users)>0)
					{
							if(count($users) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($users as $user) {  
								
								if($user["dn_status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}

								if($user["user_type"] == "1"){
									$user_type = "Wholesaler";	
								}
								elseif ($user["user_type"] == "2") {
									$user_type = "Dealers";	
								}else{
									$user_type = "NA";
								}
									
						$table .= '<tr id="row_'.$user["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($user["id"]).'" name="check" id="check" class="chk"></td>
								
								<td class="text-center" width="10%">'.$user_type.'</td>
								<td class="text-center">'.ucfirst($user["first_name"]).'</td>
								<td>'.ucfirst($user["last_name"]).'</td>
								<td>'.$user["email_address"].'</td>
								<td>'.($user["company"]?ucfirst($user["company"]):"N/A").'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($user["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
									<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($user["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($user["id"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
									<div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["id"]).'\',\'Active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["id"]).'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($users)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($users),'paginate' => $paginate)); exit;
				}
			}
		}
	}

	//function  to change banner status
	public function change_banner_status(){
	 if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["bannerId"] != "" && $postData["status"] != "")
				{  
					//pr($postData);die();
					if($postData["status"] == "Active"){
						$status_update = "Active";
					}
					else if($postData["status"] == "Inactive"){
						$status_update = "Inactive";
					}
					
					$cond = array("b_id" => $this->encrypt->decode($postData["bannerId"]));
					$banners = $this->administration_model->getAllBanners($cond);

					if(count($banners) != 0)
					{ 

						$updateArr = $this->common_model->update(TB_BANNER,$cond,array("status" => $status_update));
						//echo $this->db->last_query();
						if($updateArr)
						{
							if($postData["status"] == "1"){
								$status_text = "Make Inactive";
								$status_active = "btn btn-xs btn-success";
								$status_inactive = "btn btn-xs  active btn-default";
								$status = "0";
							}
							else{
								$status_text = "Make Active";
								$status_active = "btn btn-xs  active btn-default";
								$status_inactive = "btn btn-xs btn-danger";
								$status = "1";
							}

							if($banners["0"]["user_type"] == "1"){
								$user_type = "Wholesaler";	
							}
							elseif($banners["0"]["user_type"] == "2") {
								$user_type = "Dealers";	
							}else{
								$user_type = "NA";
							}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["bannerId"].'"></td>
									<td class="text-center">'.$user_type.'</td>
									<td>'.$banners["0"]["start_date"].'</td>
									<td>'.$banners["0"]["end_date"].'</td>
									<td>'.$banners["0"]["country"].'</td>
									<td>'.$banners["0"]["country"].'</td>


									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["bannerId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
										<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["bannerId"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["bannerId"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
										<div class="btn-group btn-toggle"> 
											<button onclick="changeStatus(\''.$postData["bannerId"].'\',\'Active\')" class="'.$status_active.'">Active</button>
											<button onclick="changeStatus(\''.$postData["bannerId"].'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
										</div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["bannerId"]), "msg" => "Banner Status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "Banner Status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	//function to change 
	
	public function change_user_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["userId"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "Active"){
						$status_update = "Active";
					}
					else if($postData["status"] == "Inactive"){
						$status_update = "Inactive";
					}
					
					$cond = array("user_id" => $this->encrypt->decode($postData["userId"]));
					$users = $this->administration_model->getAllUsers($cond);
					
					if(count($users) != 0)
					{ 
						$updateArr = $this->common_model->update(TB_USERS,$cond,array("dn_status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}

								if($users["0"]["user_type"] == "1"){
									$user_type = "Wholesaler";	
								}
								elseif ($users["0"]["user_type"] == "2") {
									$user_type = "Dealers";	
								}else{
									$user_type = "Wholesaler";
								}


							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
									<td class="text-center">'.$user_type.'</td>
									<td class="text-center">'.$users["0"]["first_name"].'</td>
									<td>'.$users["0"]["last_name"].'</td>
									<td>'.$users["0"]["email_address"].'</td>
									<td>'.($user["0"]["company"]?$user["company"]:"N/A").'</td>
									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
										<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["userId"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["userId"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
										<div class="btn-group btn-toggle"> 
											<button onclick="changeStatus(\''.$postData["userId"].'\',\'Active\')" class="'.$status_active.'">Active</button>
											<button onclick="changeStatus(\''.$postData["userId"].'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
										</div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => "User Status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "User Status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	
	public function validateUser($postData)
	{   
		/*if(!ctype_alpha($postData["txt_first_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>First name require character only.</div>');
		}
		else if(!ctype_alpha($postData["txt_last_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Last name require character only.</div>');
		}
		else if(!ctype_alnum($postData["txt_company"]) && isset($postData["txt_company"]) && $postData["txt_company"]!="")
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Company require character only.</div>');
		}*/
		
		/*if (!preg_match('/^\+(?:[0-9] ?){6,14}[0-9]$/', $postData['txt_contact_number']) && $postData['txt_contact_number']!="") {
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Invalid phone number, the number should start with a plus sign, followed by the country code and national number</div>');
		}*/
		return array("status" => 1);
	}

	//delete quetionary
	public function delete_quetionary(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_QUESTIONNAIRE,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record deleted successfully."));exit;
			}
		}
	}

	
	public function delete_otherLanguage() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//pr($postData);
				
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("page_id" =>$postData["ids"][$i]);
					$isdelete = $this->common_model->delete(TB_PAGES,$cond);

					// echo $this->db->last_query();
					// die();
					if($isdelete)
					{
						$arrDelete[] = $postData["ids"][$i];
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record deleted successfully."));exit;
			}
		}
	}
	
	
	public function delete_users() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("user_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_USERS,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"User deleted successfully."));exit;
			}
		}
	}
	//function to delete push notification
	public function delete_push_notification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();

				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_PUSH_NOTIFICATION,$cond);
				
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);	
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record deleted successfully."));exit;
			}
		}
	}

	//function to delete language
	public function delete_language()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();

				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("lang_id" =>$postData["ids"][$i]);
					$isdelete = $this->common_model->delete(TB_LANG,$cond);

					// echo $this->db->last_query();
					// die();
				
					if($isdelete)
					{
						$arrDelete[] = $postData["ids"][$i];	
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record deleted successfully."));exit;
			}
		}
	}

	//function to delete Section
	public function delete_section()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();

				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("s_id" =>$postData["ids"][$i]);
					$isdelete = $this->common_model->delete(TB_SECTION,$cond);
					$cond_del = array("section_id" =>$postData["ids"][$i]);
					//pr($cond_del);    	
					$pages = $this->administration_model->checkPagesSectionLangauages($cond_del);

					if(count($pages)>0){
						foreach($pages as $row):
							$cond_row = array("section_id" => $row["section_id"]);
							$isdelete = $this->common_model->delete(TB_PAGES,$cond_row);
							// echo $this->db->last_query();
							// exit;
							$arrDelete[] = $row['page_id'];
						endforeach;	
					}

					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);	
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record deleted successfully."));exit;
			}
		}
	}
	//function to delete banners
	public function delete_banners() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();

				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("b_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_BANNER,$cond);
				
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
						//$res=$this->administration_model->get_banner_imgname($arrDelete);
						// {
						// 	//unlink('');
						// }
						// pr($res);
						// exit;
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Banner deleted successfully."));exit;
			}
		}
	}
	
	public function getNewsCategory()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
				if($this->checkPermission() == 1){
					$data = "";
					$this->load->view("administrator/news_cat",$data);
				}
				else{
					die("administration/getPagesListSK");
				}
		}
	}
	
	
	public function getNewsCatById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("news_cat_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getNewsCatById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);exit;  
		}
	}
	
	public function save_news_category()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["newsCatId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("news_category" => $postData["txt_news_cat"]);
						$newscat = $this->administration_model->getAllNewsCategory($cond);
						$data["newscat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_NEWS_CATEGORY,array("news_category" => $postData["txt_news_cat"],"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>News Category has been added successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("news_cat_id !=" => $this->encrypt->decode($postData["newsCatId"]), "news_category" => $postData["txt_news_cat"]);
						$newscat = $this->administration_model->getAllNewsCategory($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["newscat"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_NEWS_CATEGORY,array("news_cat_id" => $this->encrypt->decode($postData["newsCatId"])),array("news_category" => $postData["txt_news_cat"],"date_modified" => date('Y-m-d H:i:s')));
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["newsCatId"].'"></td>
										<td class="text-center">'.$postData["txt_news_cat"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["newsCatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["newsCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>News category has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This category has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_news_category() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getNewsCatCount($cond,$like);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("news_category");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "news_cat_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$newscat = $this->administration_model->getNewsCatPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'news_category\',\''.$corder[0].'\')">News category</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($newscat)>0)
				{
						if(count($newscat) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($newscat as $cat) { 
							$arr = array();
							$arr[0] = $cat["news_category"]; 
							//$arr[3] = $types["id"];
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["news_category"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($newscat)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getNewsCatList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateNewsCategory($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_news_categories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("news_cat_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_NEWS_CATEGORY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	public function getNews()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$data["country"] = $this->administration_model->getCountry();
				$data["newscategory"] = $this->administration_model->getNewCategory();
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				$this->load->view("administrator/news",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	public function getNewsById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			
			$cond = array("news_id" => $this->encrypt->decode($postData["id"]));
			$news = $this->administration_model->getNewsById($cond);
			//print_r($news);die;
			echo json_encode($news[0]);exit;
				 
		}
	}
	
	public function getNewsCommentById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("comment_id" => $this->encrypt->decode($postData["id"]));
			$comments = $this->administration_model->getNewsCommentById($cond);
			//print_r($users);die;
			echo json_encode($comments[0]);
				 
		}
	}
	
	public function save_news() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//print_r($postData);die; 	
				if($postData["newsId"] == "")
				{ 
					
					$data_country = "";
							
					if(!empty($postData["dd_country"])){
						foreach($postData["dd_country"] as $data2):
								$data_country .= $data2.',';
						endforeach;
					}
					
					$data_resource = "";
					$data_subresource="";
					if(!empty($postData["dd_resources"])){
						foreach($postData["dd_resources"] as $data2):
								$data_resource .= $data2.',';
						endforeach;
					}
					
					if(!empty($postData["dd_subresources"])){
						foreach($postData["dd_subresources"] as $data2):
								$data_subresource .= $data2.',';
						endforeach;
					}				
					$isValid = $this->validateNews($postData,$_FILES);
					
					if($isValid["status"] == 1)
					{
						 
							$insertId = $this->common_model->insert(TB_NEWS,array("news_cat_id" => $postData["news_cat_id"], "news_heading" => $postData["txt_news_heading"],"news_description" => $postData["txt_description"],"country" => $data_country,"v_cat_id" => $data_resource,"v_sub_cat_id" => $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{
								
								if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
									$pdf_file = $isValid["0"]["news_file"]; 
									$act_pdf_name = str_replace(" ","_",$isValid["0"]["news_filename"]);
									$pdf_update = array("news_file" => $pdf_file, "news_filename"=> $act_pdf_name);
									
									$pdf_file_update  = '<a target="_blank" href="'.base_url().'/news_files/'.$pdf_file.'">'.$act_pdf_name.'</a>';		
									$updateArr = $this->common_model->update(TB_NEWS,array("news_id" => $insertId),$pdf_update);
								}
								else if($postData["text_pdf_file"] !=""){
									$updateArr = $this->common_model->update(TB_NEWS,array("news_id" => $insertId),array("news_filename" => $postData["text_pdf_file"]));
								}
								
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News has been added successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						 
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{ 
					$isValid = $this->validateNews($postData,$_FILES);
					if($isValid["status"] == 1)
					{
						/*$cond = array("news_id" => $this->encrypt->decode($postData["newsId"]));
						$users = $this->administration_model->getAllNews($cond);
						$data["users"] = $users;
						
						if(count($users) == 0)
						{*/ 
						
						
						$data_country = "";
							
						if(!empty($postData["dd_country"])){
							foreach($postData["dd_country"] as $data2):
									$data_country .= $data2.',';
							endforeach;
						}
						
						$data_resource = "";
						$data_subresource="";
							if(!empty($postData["dd_resources"])){
								foreach($postData["dd_resources"] as $data2):
										$data_resource .= $data2.',';
								endforeach;
							}
							
							if(!empty($postData["dd_subresources"])){
								foreach($postData["dd_subresources"] as $data2):
										$data_subresource .= $data2.',';
								endforeach;
							}
						
							$updateArr = $this->common_model->update(TB_NEWS,array("news_id" => $this->encrypt->decode($postData["newsId"])),array("news_cat_id" => $postData["news_cat_id"],"news_heading" => $postData["txt_news_heading"], "news_description" => $postData["txt_description"],"country" => $data_country,"v_cat_id" => $data_resource,"v_sub_cat_id" => $data_subresource));
							if($updateArr)
							{ 
								$pdf_update =array();
								if($postData["text_pdf_file"] !=""){
									$updateArr = $this->common_model->update(TB_NEWS,array("news_id" => $this->encrypt->decode($postData["newsId"])),array("news_filename" => $postData["text_pdf_file"]));
								}
								elseif(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								
								//@unlink($this->pdfpath.'/'.$resourcebyid["0"]["pdf_file"]);
								
								
									$pdf_file = $isValid["0"]["news_file"]; 
								
									$act_pdf_name = str_replace(" ","_",$isValid["0"]["news_filename"]);
									$pdf_update = array("news_file" => $pdf_file,"news_filename"=>$act_pdf_name);
									
									//$pdf_file_update  = '<a target="_blank" href="'.base_url().'/news_files/'.$pdf_file.'">'.$act_pdf_name.'</a>';		
									$updateArr = $this->common_model->update(TB_NEWS,array("news_id" => $this->encrypt->decode($postData["newsId"])),$pdf_update);
								}
							
								
								$cond_newcat = array("news_cat_id" => $postData["news_cat_id"]);
								$new_cat = $this->administration_model->getNewsCatById($cond_newcat); 
								$cond_unews = array("news_id" => $this->encrypt->decode($postData["newsId"]));
								$new_file = $this->administration_model->getAllNews($cond_unews); 
								//print_r($country);
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["newsId"].'"></td>
										<td class="text-center">'.$new_cat["0"]["news_category"].'</td>
										<td>'.$postData["txt_news_heading"].'</td>'; 
										if($new_file["0"]["news_filename"]!=""){
											$row .='<td class="text-center"><a target="_blank" href="'.base_url().'news_files/'.$new_file["0"]["news_file"].'">'.$new_file["0"]["news_filename"].'</a></td>';
										}else{
											$row .='<td class="text-center">N/A</td>';
										} 
								$row .='<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["newsId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["newsId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>News has been updated successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}
						/*}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This News has been exists.</div>')); exit;
						}*/
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	
	public function save_news_comment() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//print_r($postData);die; 	
				if($postData["commentId"] != "")
				{   
					$updateArr = $this->common_model->update(TB_COMMENT,array("comment_id" => $this->encrypt->decode($postData["commentId"])),array("comments" => $postData["comments"],"date_added" => date('Y-m-d H:i:s')));
					if($updateArr)
					{ 
						$cond_comments = array();
						$comment = $this->administration_model->getNewsCommentById($cond_comments); 
						 		
						echo json_encode(array("status" => 1,"action" => "modify","id"=>$this->encrypt->decode($postData["commentId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>News comment has been updated successfully.</div>')); exit;
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
					}  
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	
	public function list_news() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				 
				$cond = array();
				$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id");
				//$join = array(TB_NEWS_CATEGORY => TB_NEWS.".news_cat_id = ".TB_NEWS_CATEGORY.".news_cat_id",TB_COUNTRY => TB_NEWS.".country = ".TB_COUNTRY.".country_id");
				
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getNewsCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("news_category","news_heading");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "news_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$news_details = $this->administration_model->getNewsPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%" id="report">';
						$table .= '<thead>
									  <tr>
										<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
										<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'news_category\',\''.$corder[0].'\')">News Category</th>
										<th class="'.$css[1].'" width="35%" onclick="changePaginate(0,\'news_heading\',\''.$corder[1].'\')">News Heading</th>
										<th class="text-center" width="25%">News Files</th>
										<th class="text-center" width="15%">Action</th>
									  </tr>
									</thead>
									<tbody>';
				}
				if(count($news_details)>0)
				{
						if(count($news_details) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($news_details as $news) { 
							$join_comment = array(TB_USERS => TB_COMMENT.".user_id = ".TB_USERS.".user_id");
							$cond_comment = array("news_id" => $news['id']);
							$comment_details = $this->administration_model->getNewsComment($cond_comment,$postData["start"],$join_comment);
							$cond_comment_status = array("news_id" => $news['id'],"status"=>"Unpublished");
							$comment_to_be_aprroved = $this->administration_model->getNewsComment($cond_comment_status,$postData["start"],$join_comment);
							$com_id = $news["id"];
							$text_to_approve="";
							if(count($comment_to_be_aprroved)>0){
								$comment_count  = count($comment_to_be_aprroved);
								$text_to_approve =" <span class='apprcomment' id='count_$com_id'>(<strong>$comment_count</strong> comment(s) to be approved)</span>";
							}
							 
							//$arr[3] = $types["id"];
							$table .= '<tr id="row_'.$news["id"].'" onclick="showmore(this.id);" >
									<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($news["id"]).'" name="check" id="check" class="chk"></td>
									<td class="text-center">'.$news["news_category"].'</td>
									<td>'.$news["news_heading"].$text_to_approve.'</td>';
									if($news["news_filename"]!=""){
										$table .='<td class="text-center"><a target="_blank" href="'.base_url().'news_files/'.$news["news_filename"].'">'.$news["news_filename"].'</a></td>';
									}else{
										$table .='<td class="text-center">N/A</td>';
									}
									$table .='<td class="text-center" row_'.$news["id"].'">
										<a title="Edit" alt="Edit" id="edit" class="editNews" data-option="'.$this->encrypt->encode($news["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									</td>
								  </tr>';
								 if(count($comment_details)!=0){
									 
									 $table .= '<tr class="thisshow"><td style="padding:0" colspan="5"><table class="table table-striped table-hover dataTable no-footer"><tr>
													<th width="10%" class="text-center"></th>
													<th width="20%" class="text-center" width="20%">Email Address</th>
													<th width="20%">Comments</th>
													<th class="text-center" width="15%"></th>
												  </tr>';
									 
									 
									 foreach($comment_details as $comment): 
									   
										if($comment['status']=="Published"){
											$text_status = "Make Unpublish";
											$status="Unpublished";
										}
										else{
											$text_status = "Make Publish";
											$status="Published";
										}
									  
									  
									  $table .='<tr id="rowc_'.$comment["id"].'">
											<td ></td>
											<td class="text-center">'.$comment['email_address'].'</td> 
											<td>'.$comment['comments'].'
											</td>
											<td class="text-center">
											
											<a title="Change Status" alt="Change Status" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($news["id"]).'\',\''.$this->encrypt->encode($comment["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($comment["id"]).'" href="javascript:void(0)">'.$text_status.'</a>
											|
											<a class="model-open" title="Edit Comment" alt="Edit Comment" id="editComment" data-option="'.$this->encrypt->encode($comment["id"]).'" href="#newsComment"><i class="fa fa-edit" data-toggle="modal"></i></a>
											|
											<a title="Delete Comment" alt="Delete" id="deleteComment" class="deleteNewsComment" onclick="deleteNewsComment(\''.$this->encrypt->encode($comment["id"]).'\')" data-option="'.$this->encrypt->encode($comment["id"]).'" href="javascript:void(0)">Delete</a>
											</td>
										</tr>';
									 endforeach;
									 $table .='</table></td></tr>';
								 }
								 else{
									  $table .='<tr class="thisshow">
											<td colspan="5" align="center">No Comments Yet</td>
										</tr>';
								 } 
						}
				}
				if($postData["start"] == 0)
				{
						if(count($news_details)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="5">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getNewsList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}

	
	public function validateNews($postData,$FILES)
	{
		$_FILES = $FILES;
		
		if($_FILES != ""){
			$bool = false;
			$actual_pdf_file_arr = array(); 
			$cond1 = array("news_id" => $this->encrypt->decode($postData["newsId"])); 
			$resourcebyid = $this->administration_model->getNewsById($cond1);
			if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != ""){
				$ext = pathinfo($_FILES['pdf_file']['name'], PATHINFO_EXTENSION); 
				if($ext == 'pdf' ||  $ext == 'PDF')
				{
					$ff = 'pdf_';
				}
				
				elseif($ext == 'doc' ||  $ext == 'DOC' ||  $ext == 'docx' ||   $ext == 'DOCX')
				{
					$ff = 'document_';
				}
				
				$pdf = $ff.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$pdf_name = $_FILES['pdf_file']['name']; 
				$actual_pdf_file = str_replace(" ","_".$_FILES['pdf_file']['name']);
				
				$config = array( 
					'allowed_types' => 'pdf|PDF|doc|DOC|docx|DOCX',
					'upload_path'   => $this->newdocpath,
					'file_name'		=> $pdf_name,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('pdf_file');
				
				$actual_pdf_file_arr = array("news_filename" => $actual_pdf_file,"news_file" => $pdf);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>'.$pdf_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->newdocpath.'/'.$resourcebyid["0"]["pdf_file"]);
					$this->validatefile->do_upload('pdf_file');
					$upload_data  = $this->validatefile->data();
					$actual_pdf_file_arr = array("news_filename" => $upload_data["file_name"],"news_file" => $pdf);
				}
			}
			
			//print_r($data);die;
			//print_r($merge_update);die;
			if($bool){ 
				return array("status" => 1,$actual_pdf_file_arr);
			}
		}
		
		/*if(!ctype_alpha($postData["txt_product_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function change_comment_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["commentId"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "Published"){
						$status_update = "Published";
					}
					else if($postData["status"] == "Unpublished"){
						$status_update = "Unpublished";
					}
					
					$join_comment = array(TB_USERS => TB_COMMENT.".user_id = ".TB_USERS.".user_id");
					
					$cond = array("comment_id" => $this->encrypt->decode($postData["commentId"]));
					$comments = $this->administration_model->getNewsSingleComment($cond,$join_comment );
					
					if(count($comments) != 0)
					{ 
						$updateArr = $this->common_model->update(TB_COMMENT,$cond,array("status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "Published"){
								$text_status = "Make Unpublish";
								$status = "Unpublished";
							}
							else{
								$text_status = "Make Publish";
								$status = "Published";
							}
							 
							$cond_comment_status = array("news_id" => $this->encrypt->decode($postData["newsId"]),"status"=>"Unpublished");
							$comment_to_be_aprroved = $this->administration_model->getNewsComment($cond_comment_status,0,$join_comment);
							$comment_count='';
							if(count($comment_to_be_aprroved)>0){
								$comment_count  = count($comment_to_be_aprroved);
							}
							
							$row =' <td width="10%" class="text-center"></td>
											<td width="20%"  class="text-center">'.$comments["0"]['email_address'].'</td>
											<td width="20%" >'.$comments["0"]['comments'].'</td>
											<td width="15%"  class="text-center"> 
												<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($postData["newsId"]).'\',\''.$this->encrypt->encode($comments["0"]["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($comments["0"]["id"]).'" href="javascript:void(0)">'.$text_status.'</a>
												|
												<a class="model-open" title="Edit Comment" alt="Edit Comment" id="editComment" data-option="'.$this->encrypt->encode($comments["0"]["id"]).'" href="#newsComment"><i class="fa fa-edit" data-toggle="modal"></i></a>
												|
												<a title="Delete Comment" alt="Delete" id="deleteComment" class="deleteNewsComment" onclick="deleteNewsComment(\''.$this->encrypt->encode($comments["0"]["id"]).'\')" data-option="'.$this->encrypt->encode($comments["0"]["id"]).'" href="javascript:void(0)">Delete</a>
												
											</td>
										';
							
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"comcount"=>$comment_count,"nid"=>$this->encrypt->decode($postData["newsId"]),"id"=>$this->encrypt->decode($postData["commentId"]), "msg" => "News comment status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "News comment status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	public function delete_news_cooment() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 
				$cond = array("comment_id" => $this->encrypt->decode($postData["ids"]));
				$this->common_model->delete(TB_COMMENT,$cond);
					  
				echo json_encode(array("status" => 1,"msg" => "News comment deleted successfully")); exit;
				
			}
		}
	}
	
	public function delete_news() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("news_id" => $this->encrypt->decode($postData["ids"][$i]));
					
					$news_data = $this->administration_model->getNewsById($cond);
					if(file_exists($this->newdocpath.'/'.$news_data[0]["news_filename"])) {
						unlink($this->newdocpath.'/'.$news_data[0]["news_filename"]);
					}
					
					$isdelete = $this->common_model->delete(TB_NEWS,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit();
			}
		}
	}
	
	
	
	public function getResources()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$data["country"] = $this->administration_model->getCountry();
				$data["categories"] = $this->administration_model->getAllVehicleCategory();
				$this->load->view("administrator/resources",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	public function getResourceById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("resource_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getResourceById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);exit;
				 
		}
	}
	
	
	public function save_resources() // Add/Edit Payment Type 
	{	
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//print_r($postData);die;
				foreach($postData['formData'] AS $key => $value){
					$val = $value["value"];
					if(strpos($value['name'], '[]')){
							$str = str_replace('[]','',$value['name']);
							$a[$str][] = $val;
					}else{
						$a[$value['name']] = $val;
					}
				}
				
				foreach($postData['dd_country'] AS $key => $value){
					$val = $value["value"];
					if(strpos($value['name'], '[]')){
							$str = str_replace('[]','',$value['name']);
							$b[] = $val;
					}else{
						$b[] = $val;
					}
				}
				$a['txt_product_description'] = $postData["txt_product_description"];
				$a['dd_country'] = $b;
				$postData = $a;
				//print_r($postData);die;
				if($postData["resourceId"] == "")
				{   
					$isValid = $this->validateResource($postData,$_FILES);
					if($isValid["status"] == 1)
					{ 
						$cond = array();
						$resources = $this->administration_model->getAllResources($cond);
						$data["resources"] = $resources;
						
						$data_country = "";
						//print_r($postData["dd_country"]);
						
						if(!empty($postData["dd_country"])){ 
							foreach($postData["dd_country"] as $data1):
									$data_country .= $data1.',';
							endforeach;
						}
						
						$data_resource = "";
						$data_subresource = "";
						//print_r($postData["dd_resources"]);die;
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						
						$insertId = $this->common_model->insert(TB_RESOURCES,array("product_name" => $postData["txt_product_name"], "product_description" => $postData["txt_product_description"],"countries" => $data_country,"resource_categories" =>  $data_resource,"resource_subcategories" =>  $data_subresource,"date_modified" => date('Y-m-d H:i:s')));
						if($insertId)
						{ 
							$pdf_update  = array();
							$ai_update = array();
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$act_pdf_name = str_replace(" ","_",$isValid["0"]["act_pdf_file"]);
								$pdf_update = array("pdf_file" => $pdf_file,"pdf_name" => $act_pdf_name);
								
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$act_pdf_name.'">'.$act_pdf_name.'</a>';		
								$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $insertId),$pdf_update);
							}
							else if($postData["text_pdf_file"] !=""){
								$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $insertId),array("pdf_name" => $postData["text_pdf_file"]));
							}
							if(isset($_FILES['ai_file']['name']) && $_FILES['ai_file']['name'] != ""){ 
								
								//@unlink($this->aipath.'/'.$resourcebyid["0"]["ai_file"]);
								//$this->validatefile->do_upload('ai_file'); 
								$ai_file = $isValid["0"]["ai_file"]; 
								$act_ai_name = str_replace(" ","_",$isValid["0"]["actual_ai_file"]); 
								$ai_update = array("ai_file" => $ai_file,"ai_file_name" => $act_ai_name);
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$act_ai_name.'">'.$act_ai_name.'</a>';
								$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $insertId),$ai_update);
							}			
							else if($postData["text_ai_file"] !=""){
								$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $insertId),array("ai_file_name" => $postData["text_ai_file"]));
							}
							
							echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Resources has been added successfully...!!!</div>')); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
						} 
					}
					else
					{
						echo json_encode($isValid);exit;
					}  
				}
				else
				{ 
					$isValid = $this->validateResource($postData,$_FILES);
					//print_r($isValid);
					if($isValid["status"] == 1)
					{
						 
						$cond1 = array("resource_id" => $this->encrypt->decode($postData["resourceId"])); 
						$resourcebyid = $this->administration_model->getResourceById($cond1); 
						$data["resources"] = $resourcebyid;  
						
						$data_country = "";
						$data_subresource="";
						//print_r($postData["dd_country"]);
						
						if(!empty($postData["dd_country"])){ 
							foreach($postData["dd_country"] as $data1):
									$data_country .= $data1.',';
							endforeach;
						}
						
						$data_resource = "";
						
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						$txt_update = array("product_name" => $postData["txt_product_name"], "product_description" => urldecode($postData["txt_product_description"]),"countries" => $data_country,"resource_categories" =>  $data_resource,"resource_subcategories" =>  $data_subresource,"date_modified" => date('Y-m-d H:i:s'));
						$pdf_update  = array();
						$ai_update = array();
						if($postData["text_pdf_file"] !="" || $postData["text_ai_file"] !=""){
							
							if($postData["text_pdf_file"] !=""){
								$pdf_update = array("pdf_name" => $postData["text_pdf_file"]);
								$act_pdf_name = $postData["text_pdf_file"];
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$act_pdf_name.'">'.$act_pdf_name.'</a>';
								//$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" =>$this->encrypt->decode($postData["resourceId"])),array("pdf_name" => $postData["text_pdf_file"]));
							}
							if($postData["text_ai_file"] !=""){
								$ai_update = array("ai_file_name" => $postData["text_ai_file"]);
								$act_ai_name=$postData["text_ai_file"];
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$act_ai_name.'">'.$act_ai_name.'</a>';
								//$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" =>$this->encrypt->decode($postData["resourceId"])),array("ai_file_name" => $postData["text_ai_file"]));
							}
							
							$merge_update  = array_merge($pdf_update,$ai_update,$txt_update);
							$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $this->encrypt->decode($postData["resourceId"])),$merge_update);
						}
						elseif($_FILES){
							$this->validatefile->data(); 
							$pdf_update = array();
							$ai_update = array();
							$ai_update_act_file = array();
							
							
							//echo $_FILES['pdf_file']['size'];
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								
								//@unlink($this->pdfpath.'/'.$resourcebyid["0"]["pdf_file"]);
								
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$act_pdf_name = str_replace(" ","_".$isValid["0"]["act_pdf_file"]);
								$pdf_update = array("pdf_file" => $pdf_file,"pdf_name"=>$act_pdf_name);
								
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$act_pdf_name.'">'.$act_pdf_name.'</a>';		
							}
							else{
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$resourcebyid["0"]["pdf_name"].'">'.$resourcebyid["0"]["pdf_file"].'</a>';		
							}
							if(isset($_FILES['ai_file']['name']) && $_FILES['ai_file']['name'] != ""){ 
								
								//@unlink($this->aipath.'/'.$resourcebyid["0"]["ai_file"]);
								//$this->validatefile->do_upload('ai_file'); 
								$ai_file = $isValid["0"]["ai_file"]; 
								$act_ai_name = str_replace(" ","_",$isValid["0"]["actual_ai_file"]); 
								$ai_update = array("ai_file" => $ai_file,"ai_file_name" => $act_ai_name);
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$act_ai_name.'">'.$act_ai_name.'</a>';
							}			
							else{ 
								$ai_update_file = '<a target="_blank" href="'.base_url().'/ai_files/'.$resourcebyid["0"]["ai_file_name"].'">'.$resourcebyid["0"]["ai_file"].'</a>';
							}
							
							$merge_update  = array_merge($pdf_update,$ai_update,$txt_update);
							//print_r($merge_update);die;
							$updateArr = $this->common_model->update(TB_RESOURCES,array("resource_id" => $this->encrypt->decode($postData["resourceId"])),$merge_update);
							
						} 
						
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["resourceId"].'"></td>
										<td class="text-center">'.$postData["txt_product_name"].'</td> 
										<td>'.$pdf_file_update.'</td>
										<td>'.$ai_update_file.'</a></td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["resourceId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["resourceId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Resources has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
							}  
					}
					else
					{
						echo json_encode($isValid);exit;
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_resources() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getResourcesCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("product_name","pdf_name","ai_file_name");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "resource_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$resources = $this->administration_model->getResourcesPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'product_name\',\''.$corder[0].'\')">Resource Name</th>
								<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'pdf_name\',\''.$corder[1].'\')">PDF Files</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'ai_file_name\',\''.$corder[2].'\')">AI Files</th> 
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($resources)>0)
				{
						if(count($resources) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($resources as $resource) { 
							 
							//$arr[3] = $types["id"];
					$table .= '<tr id="row_'.$resource["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($resource["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$resource["product_name"].'</td>
							<td><a target="_blank" href="'.base_url().'pdf_files/'.$resource["pdf_name"].'">'.$resource["pdf_name"].'</a></td>
							<td><a target="_blank" href="'.base_url().'ai_files/'.$resource["ai_file_name"].'">'.$resource["ai_file_name"].'</a></td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($resource["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($resources)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getResorcesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($resources),'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateResource($postData,$FILES)
	{   
		$_FILES = $FILES;
		
		if($_FILES!=""){
			$bool = false;
			$actual_pdf_file = array();
			$actual_ai_file = array();
			$merge_update = array();
			$cond1 = array("resource_id" => $this->encrypt->decode($postData["resourceId"])); 
			$resourcebyid = $this->administration_model->getResourceById($cond1);
			if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != ""){
				$ext = pathinfo($_FILES['pdf_file']['name'], PATHINFO_EXTENSION); 
				$pdf = 'pdf_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$pdf_name = $_FILES['pdf_file']['name']; 
				
				
				//echo $fname;die;
				$actual_pdf_file = $_FILES['pdf_file']['name'];
				
				$config = array( 
					'allowed_types' => 'pdf|PDF',
					'upload_path'   => $this->pdfpath,
					'file_name'		=> $pdf_name,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('pdf_file');
				$actual_pdf_file = array("act_pdf_file" => $actual_pdf_file,"pdf_file" => $pdf);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$pdf_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->pdfpath.'/'.$resourcebyid["0"]["act_pdf_file"]);
					$this->validatefile->do_upload('pdf_file');
					$upload_data  = $this->validatefile->data();
					$actual_pdf_file = array("act_pdf_file" => $upload_data["file_name"],"pdf_file" => $pdf);
				}
			}
			if(isset($_FILES['ai_file']['name']) && $_FILES['ai_file']['name'] != ""){
				$ext = pathinfo($_FILES['ai_file']['name'], PATHINFO_EXTENSION); 
				$ai = 'ai_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$ai_name = $_FILES['ai_file']['name']; 
				//echo $fname;die;
				$actual_ai_file = $_FILES['ai_file']['name'];
				$config = array( 
					'allowed_types' => 'ai|AI|zip|ZIP',
					'upload_path'   => $this->aipath,
					'file_name'		=> $ai_name,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('ai_file');
				$actual_ai_file = array("actual_ai_file" => $actual_ai_file,"ai_file" => $ai); 
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$ai_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->aipath.'/'.$resourcebyid["0"]["ai_file"]);
					$this->validatefile->do_upload('ai_file');
					$upload_data  = $this->validatefile->data();
					$actual_ai_file = array("actual_ai_file" => $upload_data["file_name"],"ai_file" => $ai); 
				}
				
				
			}
			$merge_update = array_merge($actual_pdf_file,$actual_ai_file);
			//print_r($data);die;
			//print_r($merge_update);die;
			if($bool){ 
				return array("status" => 1,$merge_update);
			}
		}
		
		/*if(!ctype_alpha($postData["txt_product_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}

	
	
	public function delete_resources() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("resource_id" => $this->encrypt->decode($postData["ids"][$i]));
					
					
					$resources_data = $this->administration_model->getResourceById($cond);
					
					if(file_exists($this->pdfpath.'/'.$resources_data[0]["pdf_name"])) {
						unlink($this->pdfpath.'/'.$resources_data[0]["pdf_name"]);
					}
					
					if(file_exists($this->aipath.'/'.$resources_data[0]["ai_file_name"])) {
						unlink($this->aipath.'/'.$resources_data[0]["ai_file_name"]);
					}
					$isdelete = $this->common_model->delete(TB_RESOURCES,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	/*
	 * Vehicle details
	 * 
	 * */
	 
	 
	public function getVehicleCategory()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$data = "";
				$this->load->view("administrator/vehicle_cat",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}	
		}
	}
	
	
	public function getVehicleCatById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_cat_id" => $this->encrypt->decode($postData["id"]));
			$category = $this->administration_model->getVehicleCatById($cond);
			//print_r($users);die;
			$html = '';
			if($category[0]['background_img']!=""){
				$html .='<a title="Delete" alt="Delete" id="delete" class="" data-option="'.$category[0]["id"].'" onclick="deleteVehicleCatackground('.$category[0]["id"].')" href="javascript:void(0)">Remove Backgound</a>';
			}
			
			echo json_encode(array("cat"=>$category[0], "delete_back" => $html));exit;
		}
	}
	
	public function save_vehicle_category()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				
				if($postData["vehicleCatId"] == "")
				{ 
					
					$isValid = $this->validateVehicleCategory($postData,$_FILES);
					if($isValid["status"] == 1)
					{ 
						$cond = array("category" => trim($postData["txt_vehicle_cat"]));
						$newscat = $this->administration_model->getAllVehicleCategory($cond);
						$data["vehicleCat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_CATEGORY,array("category" => trim($postData["txt_vehicle_cat"])));
							if($insertId)
							{
								
								if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$act_pdf_name = str_replace(" ","_",$isValid["0"]["act_pdf_file"]);
								$pdf_update = array("background_img"=>$act_pdf_name);
								
								$updateArr = $this->common_model->update(TB_CATEGORY,array("v_cat_id" => $insertId),$pdf_update);
								}
								
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Vehicle Category has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleCategory($postData,$_FILES);
					if($isValid["status"] == 1)
					{
						$cond = array("v_cat_id !=" => $this->encrypt->decode($postData["vehicleCatId"]), "category" => trim($postData["txt_vehicle_cat"]));
						$newscat = $this->administration_model->getAllVehicleCategory($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["vehicleCat"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_CATEGORY,array("v_cat_id" => $this->encrypt->decode($postData["vehicleCatId"])),array("category" => trim($postData["txt_vehicle_cat"])));
							
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								
								//@unlink($this->pdfpath.'/'.$resourcebyid["0"]["pdf_file"]);
								//print_r($isValid);die;
								$pdf_file = $isValid["0"]["pdf_file"]; 
								
								$act_pdf_name = str_replace(" ","_",$isValid["0"]["act_pdf_file"]);
								
								$pdf_update = array("background_img" => $act_pdf_name);
								
								$this->common_model->update(TB_CATEGORY,array("v_cat_id" =>$this->encrypt->decode($postData["vehicleCatId"])),$pdf_update);
								
							}
							if($updateArr)
							{
								
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleCatId"].'"></td>
										<td class="text-center">'.$postData["txt_vehicle_cat"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleCatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
						
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Category has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Category has been updated successfully...!!!</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This category has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);exit;
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_category() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getVehicleCatCount($cond,$like);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("category","background_img");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_cat_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$newscat = $this->administration_model->getVehicleCatPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="7%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="15%" onclick="changePaginate(0,\'category\',\''.$corder[0].'\')">Vehicle category</th>
								<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'background_img\',\''.$corder[1].'\')">Background Image</th>
								<th colspan="2"class="text-center" width="15%">Action
								</th>
							  </tr>
							</thead>
							<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							
							<td class="text-center">AMW</td>
							<td class="text-center">PMK</td>
							</tr>'
							;
				}
				if(count($newscat)>0)
				{
						if(count($newscat) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($newscat as $cat) {   
							if($cat["background_img"] != ""){
								$img = '<img src="'.base_url().'background_img/'.$cat["background_img"].'" width="200" height="150">';
							}
							else{
								$img = '-';
							}
						/*	
							if($cat["status"] == "Active"){
								$status_text = "Make Inactive";
								$status = "Inactive";
							}
							else{
								$status_text = "Make Active";
								$status = "Active";
							}
							*/
						        if($cat["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}
								if($cat["status_pmk"] == "Active"){
									$status_text = "Make Inactive";
									$status_active_pmk = "btn btn-xs btn-success";
									$status_inactive_pmk = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active_pmk = "btn btn-xs  active btn-default";
									$status_inactive_pmk = "btn btn-xs btn-danger";
									$status = "Active";
								}
							
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" ><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["category"].'</td> 
							<td class="text-center">'.$img.'</td>
							
							<td class="text-center">
							
							
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a> |
							<!--	<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
						  
							
							      <div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\'Active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
							</td>
							
							<td class="text-center">
							
							        <div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\'Active_pmk\')" class="'.$status_active_pmk.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\'Inactive_pmk\')" class="'.$status_inactive_pmk.'">Inactive</button>
								  </div>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($newscat)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleCatList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function change_category_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				
				
				if($postData["catId"] != "" && $postData["status"] != "")
				{  
				$cond = array("v_cat_id" => $this->encrypt->decode($postData["catId"]));
				$cat = $this->administration_model->getAllVehicleCategory($cond);
				              
				$status = $postData["status"];
				
			/*	switch ($status) {
				   case "Active":
						 $status_update = "Active";
						 break;
				   case "Inactive":
						 $status_update = "Inactive";
						 break;
				   case "Active_pmk":
						 $status_update_pmk = "Active";
						  
				   case "Inactive_pmk":
						 $status_update_pmk = "Inactive";
						
						 break;
                     }
                     */
					if($status == "Active")
					{
						 $status_update = "Active";
						 $status_text = "Make Inactive";
						 $status_active = "btn btn-xs btn-success";
						 $status_inactive = "btn btn-xs  active btn-default";
						 //$status = "Inactive";
						 $cond_s = array("v_cat_id" => $this->encrypt->decode($postData["catId"]),"status_pmk"=>$status_update);
				         $cat_s = $this->administration_model->getAllVehicleCategory($cond_s);
						 // var_dump($cat);
						 if($cat_s)
						 {
							$status_active_pmk = "btn btn-xs btn-success";
						    $status_inactive_pmk = "btn btn-xs  active btn-default";
							 
						  //  die("Active");
						 }else{
							$status_active_pmk = "btn btn-xs  active btn-default";
						    $status_inactive_pmk = "btn btn-xs btn-danger";
							// die("InActive");
						 }
					}
					if($status == "Inactive")
					{
						 $status_update = "Inactive";
						 $status_text = "Make Active";
						 $status_active = "btn btn-xs  active btn-default";
						 $status_inactive = "btn btn-xs btn-danger";
						// $status = "Active";
						 $cond_s = array("v_cat_id" => $this->encrypt->decode($postData["catId"]),"status_pmk"=>$status_update);
				         $cat_s = $this->administration_model->getAllVehicleCategory($cond_s);
						 if($cat_s)
						 {
							$status_active_pmk = "btn btn-xs  active btn-default";
						    $status_inactive_pmk = "btn btn-xs btn-danger";
							
						 }else{
							$status_active_pmk = "btn btn-xs btn-success";
						    $status_inactive_pmk = "btn btn-xs  active btn-default";
						 }
					}
					if($status == "Active_pmk")
					{
						$status_update_pmk = "Active";
						$status_text = "Make Inactive";
						$status_active_pmk = "btn btn-xs btn-success";
						$status_inactive_pmk = "btn btn-xs  active btn-default";
						//$status = "Inactive";
						 $cond_s = array("v_cat_id" => $this->encrypt->decode($postData["catId"]),"status"=>$status_update_pmk);
				         $cat_s = $this->administration_model->getAllVehicleCategory($cond_s);
						 if($cat_s)
						 {
							$status_active = "btn btn-xs btn-success";
						    $status_inactive = "btn btn-xs  active btn-default";
						 }else{
							$status_active = "btn btn-xs  active btn-default";
						    $status_inactive = "btn btn-xs btn-danger";
						 }
					}
					if($status == "Inactive_pmk")
					{
						$status_update_pmk = "Inactive";
						$status_text = "Make Active";
						$status_active_pmk = "btn btn-xs  active btn-default";
						$status_inactive_pmk = "btn btn-xs btn-danger";
						//$status = "Active";
						 $cond_s = array("v_cat_id" => $this->encrypt->decode($postData["catId"]),"status"=>$status_update_pmk);
				         $cat_s = $this->administration_model->getAllVehicleCategory($cond_s);
						 if($cat_s)
						 {
							$status_active = "btn btn-xs  active btn-default";
						    $status_inactive = "btn btn-xs btn-danger";
							
						 }else{
							$status_active = "btn btn-xs btn-success";
						    $status_inactive = "btn btn-xs  active btn-default";
						 }
					}
					/*//echo "status active  " .$status_active . "  <>  "; 
					//echo "status inactive  " .$status_inactive. "  <> "; 
					//echo "status_pmk active  " .$status_active_pmk. "  <> "; 
                   // echo "status_pmk Inactive  " .$status_inactive_pmk;
					//die();
					*/
					if(count($cat) != 0)
					{
						if($status_update)
						{
						$updateArr = $this->common_model->update(TB_CATEGORY,$cond,array("status" => $status_update));
						//echo $this->db->last_query();
						}
						if($status_update_pmk)
						{
						$updateArr = $this->common_model->update(TB_CATEGORY,$cond,array("status_pmk" => $status_update_pmk));
						//echo $this->db->last_query();
						}
					   // var_dump($updateArr);die;
						if($updateArr)
						{ 
							if($cat[0]["background_img"] != ""){
								$img = '<img src="'.base_url().'background_img/'.$cat[0]["background_img"].'" width="200" height="150">';
							}
							else{
								$img = '-';
							}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["catId"].'"></td>
									<td class="text-center">'.$cat["0"]["category"].'</td>
									<td>'.$img.'</td>
									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["catId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
									<!--	<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["catId"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["catId"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
									
									  <div class="btn-group btn-toggle"> 
								    	<button onclick="changeStatus(\''.$postData["catId"].'\',\'Active\')" class="'.$status_active.'">Active</button>
									    <button onclick="changeStatus(\''.$postData["catId"].'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
								     </div>
									
									</td>
									<td class="text-center">
										
									  <div class="btn-group btn-toggle"> 
								    	<button onclick="changeStatus(\''.$postData["catId"].'\',\'Active_pmk\')" class="'.$status_active_pmk.'">Active</button>
									    <button onclick="changeStatus(\''.$postData["catId"].'\',\'Inactive_pmk\')" class="'.$status_inactive_pmk.'">Inactive</button>
								     </div>
									
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["catId"]), "msg" => "Category status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "Category status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	
	public function change_subcategory_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["subcatId"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "Active"){
						$status_update = "Active";
					}
					else if($postData["status"] == "Inactive"){
						$status_update = "Inactive";
					}
					
					$cond = array("v_cat_id" => $this->encrypt->decode($postData["subcatId"]));
					$cat = $this->administration_model->getAllVehicleSubCategory($cond);
					
					if(count($cat) != 0)
					{ 
						$updateArr = $this->common_model->update(TB_SUBCATEGORY,$cond,array("status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}
							
							if($cat[0]["background_img"] != ""){
								$img = '<img src="'.base_url().'background_img/'.$cat[0]["background_img"].'" width="200" height="150">';
							}
							else{
								$img = '-';
							}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["subcatId"].'"></td>
									<td class="text-center">'.$cat["0"]["category"].'</td>
									<td>'.$img.'</td>
									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["subcatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
										<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["subcatId"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["subcatId"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
										<div class="btn-group btn-toggle"> 
											<button onclick="changeStatus(\''.$postData["subcatId"].'\',\'Active\')" class="'.$status_active.'">Active</button>
											<button onclick="changeStatus(\''.$postData["subcatId"].'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
										 </div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["subcatId"]), "msg" => "Category status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "Category status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	public function validateVehicleCategory($postData,$FILES)
	{
		$_FILES = $FILES;
		
		if($_FILES!=""){
			ini_set('upload_max_filesize', '500M');  
			$bool = false;
			$actual_pdf_file = array();
			$actual_ai_file = array();
			$merge_update = array();
			$cond1 = array("v_cat_id" => $this->encrypt->decode($postData["vehicleCatId"])); 
			$resourcebyid = $this->administration_model->getVehicleCatById($cond1);
			if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != ""){
				$ext = pathinfo($_FILES['pdf_file']['name'], PATHINFO_EXTENSION); 
				$pdf = 'pdf_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$pdf_name = $_FILES['pdf_file']['name']; 
				
				
				//echo $fname;die;
				$actual_pdf_file = $_FILES['pdf_file']['name'];
				
				$config = array( 
					'allowed_types' => 'png|PNG|jpg|JPG',
					'upload_path'   => $this->backgroundpath,
					'file_name'		=> $pdf_name,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('pdf_file');
				$actual_pdf_file = array("act_pdf_file" => $actual_pdf_file,"pdf_file" => $pdf);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$pdf_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->backgroundpath.'/'.$resourcebyid["0"]["background_img"]);
					$this->validatefile->do_upload('pdf_file');
					$upload_data  = $this->validatefile->data();
					$actual_pdf_file = array("act_pdf_file" => $upload_data["file_name"],"pdf_file" => $pdf);
				}
			}
			
			if($bool){ 
				return array("status" => 1,$actual_pdf_file);
			}
		}
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_categories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_cat_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_CATEGORY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	public function delete_vehicle_category_background() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				$cond = array("v_cat_id" => $postData["ids"]);
				$category = $this->administration_model->getAllVehicleCategory($cond);
				$filepath = $this->backgroundpath."/".$category[0]["background_img"];
				
				unlink($filepath);

				$cond = array("v_cat_id" => $postData["ids"]);
				$data_update = array("background_img" => "");
				$isdelete = $this->common_model->update(TB_CATEGORY,$cond,$data_update);
				
				
					
				echo json_encode(array("msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button"></button>The background removed successfully.</div>'));exit;
			}
		}
	}
	
	public function delete_vehicle_subcategory_background() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				$cond = array("v_sub_cat_id" => $postData["ids"]);
				$category = $this->administration_model->getAllVehicleSubCategory($cond);
				$filepath = $this->backgroundpath."/".$category[0]["background_img"];
				
				unlink($filepath);

				$cond = array("v_sub_cat_id" => $postData["ids"]);
				$data_update = array("background_img" => "");
				$isdelete = $this->common_model->update(TB_SUBCATEGORY,$cond,$data_update);
				
				
					
				echo json_encode(array("msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button"></button>The background removed successfully.</div>'));exit;
			}
		}
	}
	
	/* 
	 * Vehicle sub categories
	 * 
	 * */
	 
	 
	public function getVehicleSubCategory()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$cond= array("status"=>"Active");
				$data['category'] = $this->administration_model->getAllVehicleCategory($cond);
				$this->load->view("administrator/vehicle_sub_cat",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	public function getVehicleSubCatById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_sub_cat_id" => $this->encrypt->decode($postData["id"]));
			$category = $this->administration_model->getVehicleSubCatById($cond);
			//print_r($users);die;
			$html = '';
			if($category[0]['background_img']!=""){
				$html .='<a title="Delete" alt="Delete" id="delete" class="" data-option="'.$category[0]["id"].'" onclick="deleteVehicleSubCatBackground('.$category[0]["id"].')" href="javascript:void(0)">Remove Backgound</a>';
			}
			
			echo json_encode(array("cat"=>$category[0], "delete_back" => $html));exit;
		}
	}
	
	public function getVehicleSubCatByCatId() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_cat_id" => $postData["id"]);
			$subcat = $this->administration_model->getVehicleSubCatById($cond);
			//print_r($subcat);die;
			
			if(count($subcat)>0){ 
				$html  ='<select class="select-sm arrow_new" name="dd_subresources[]" id="dd_subresources"  multiple="multiple" aria-controls="datatable3" class="form-control input-sm">';
				foreach($subcat as $cat):
					$html .='<option value="'.$cat['id'].'">'.$cat['sub_category'].'</option>';
				endforeach;
				$html .='</select>';
			}
			else{
				$html="";
			}
				echo $html;  
		}
	}
	
	
	public function save_vehicle_sub_category()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleSubCatId"] == "")
				{ 
					
					$isValid = $this->validateVehicleSubCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("v_cat_id" => $postData["cat_id"],"sub_category" => $postData["txt_vehicle_sub_cat"]);
						$newscat = $this->administration_model->getAllVehicleSubCategory($cond);
						$data["vehicleSubCat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_SUBCATEGORY,array("v_cat_id" => $postData["cat_id"],"sub_category" => trim($postData["txt_vehicle_sub_cat"])));
							if($insertId)
							{
								
								if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
									$pdf_file = $isValid["0"]["pdf_file"]; 
									$act_pdf_name = str_replace(" ","_",$isValid["0"]["act_pdf_file"]);
									$pdf_update = array("background_img"=>$act_pdf_name);
									
									$updateArr = $this->common_model->update(TB_SUBCATEGORY,array("v_sub_cat_id" => $insertId),$pdf_update);
								}
								
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Vehicle Sub Category has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This sub category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);exit;
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleSubCategory($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_sub_cat_id !=" => $this->encrypt->decode($postData["vehicleSubCatId"]),"v_cat_id" => $postData["cat_id"], "sub_category" => trim($postData["txt_vehicle_sub_cat"]));
						$newscat = $this->administration_model->getAllVehicleSubCategory($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["vehicleCat"] = $newscat;
						
						if(count($newscat) == 0)
						{
							/*if sub category moved to the anather category then move pages category fro that sub category accordigly*/
							//before update 
							
							$cond_subcat = array("v_sub_cat_id" => $this->encrypt->decode($postData["vehicleSubCatId"]));
							$category_id = $this->administration_model->getVehicleSubCatById($cond_subcat);		
							//print_r($category_id);die;	
							$cond_catpage = array("category" => $postData["cat_id"],"sub_category" =>$this->encrypt->decode($postData["vehicleSubCatId"]));
							$page_category = $this->administration_model->getAllPagesSK($cond_catpage);
							
							if(count($page_category)==0){
								$update_data = array("category" =>$postData["cat_id"]);
								$this->common_model->update(TB_PAGES,array("category" => $category_id[0]['v_cat_id'],"sub_category" =>$this->encrypt->decode($postData["vehicleSubCatId"])),$update_data);
							}
							//die; 
							$updateArr = $this->common_model->update(TB_SUBCATEGORY,array("v_sub_cat_id" => $this->encrypt->decode($postData["vehicleSubCatId"])),array("sub_category" => $postData["txt_vehicle_sub_cat"],"v_cat_id" => $postData["cat_id"]));
							
							$cond_cat = array("v_cat_id" => $postData["cat_id"]);
							$category = $this->administration_model->getVehicleCatById($cond_cat);
							
							
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
									$pdf_file = $isValid["0"]["pdf_file"]; 
									$act_pdf_name = str_replace(" ","_",$isValid["0"]["act_pdf_file"]);
									$pdf_update = array("background_img"=>$act_pdf_name);
									
									$updateArr = $this->common_model->update(TB_SUBCATEGORY,array("v_sub_cat_id" =>$this->encrypt->decode($postData["vehicleSubCatId"])),$pdf_update);
							}
							
							if($updateArr)
							{
								
								
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleSubCatId"].'"></td>
										<td class="text-center">'.$category["0"]["category"].'</td>
										<td class="text-center">'.$postData["txt_vehicle_sub_cat"].'</td>
										
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleSubCatId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleSubCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Sub category has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleSubCatId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Sub category has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This sub category has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);exit;
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_sub_category() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array(TB_CATEGORY.".status" => "Active");
				$like = array();
				$join = array(TB_CATEGORY => TB_SUBCATEGORY.".v_cat_id = ".TB_CATEGORY.".v_cat_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getVehicleSubCatCount($cond,$like,$join);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("category","sub_category");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_sub_cat_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$newscat = $this->administration_model->getVehicleSubCatPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				
				$links = "";
				$table = "";
				
				if($postData["start"] == 0)
				{ 
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'category\',\''.$corder[0].'\')">Vehicle Category</th>
								<th class="text-center '.$css[1].'" width="20%" onclick="changePaginate(0,\'sub_category\',\''.$corder[1].'\')">Vehicle Sub Category</th>
								<th class="text-center '.$css[2].'" width="20%" onclick="changePaginate(0,\'background_img\',\''.$corder[2].'\')">Background Image</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($newscat)>0)
				{
						if(count($newscat) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($newscat as $cat) {   
							if($cat["background_img"] != ""){
								$img = '<img src="'.base_url().'background_img/'.$cat["background_img"].'" width="200" height="150">';
							}
							else{
								$img = '-';
							}
							 if($cat["status"] == "Active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "Inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "Active";
								}
							
						$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["category"].'</td> 
							<td class="text-center">'.$cat["sub_category"].'</td> 
							<td class="text-center">'.$img.'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
								<!--<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$postData["subcatId"].'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["subcatId"]).'" href="javascript:void(0)">'.$status_text.'</a>-->
								<div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\'Active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($cat["id"]).'\',\'Inactive\')" class="'.$status_inactive.'">Inactive</button>
								</div>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($newscat)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleSubCatList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION, 'start' => $postData["start"], 'totalrec' => count($newscat),'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicleSubCategory($postData,$FILES)
	{
		if($_FILES!=""){
			ini_set('upload_max_filesize', '500M');  
			$bool = false;
			$actual_pdf_file = array();
			$actual_ai_file = array();
			$merge_update = array();
			$cond1 = array("v_sub_cat_id" => $this->encrypt->decode($postData["vehicleSubCatId"])); 
			$resourcebyid = $this->administration_model->getVehicleSubCatById($cond1);
			if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != ""){
				$ext = pathinfo($_FILES['pdf_file']['name'], PATHINFO_EXTENSION); 
				$pdf = 'pdf_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$pdf_name = $_FILES['pdf_file']['name']; 
				
				
				//echo $fname;die;
				$actual_pdf_file = $_FILES['pdf_file']['name'];
				
				$config = array( 
					'allowed_types' => 'png|PNG|jpg|JPG',
					'upload_path'   => $this->backgroundpath,
					'file_name'		=> $pdf_name,
					'max_size'      => 50000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('pdf_file');
				$actual_pdf_file = array("act_pdf_file" => $actual_pdf_file,"pdf_file" => $pdf);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$pdf_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->backgroundpath.'/'.$resourcebyid["0"]["background_img"]);
					$this->validatefile->do_upload('pdf_file');
					$upload_data  = $this->validatefile->data();
					$actual_pdf_file = array("act_pdf_file" => $upload_data["file_name"],"pdf_file" => $pdf);
				}
			}
			
			if($bool){ 
				return array("status" => 1,$actual_pdf_file);
			}
		}
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_sub_categories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$cond_pages = array("sub_category" => $this->encrypt->decode($postData["ids"][$i]));
					$pages = $this->administration_model->getAllPagesDataSKLimit($cond_pages);
					$isdelete = 0;
					if(count($pages)==0){
						$cond = array("v_sub_cat_id" => $this->encrypt->decode($postData["ids"][$i]));
						
						$isdelete = $this->common_model->delete(TB_SUBCATEGORY,$cond);
						if($isdelete)
						{
							$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
						}
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	/*
	 * Vehicle Brand
	 * */
	
	public function getVehicleBrand()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$data = "";
				$this->load->view("administrator/vehicle_brand",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	public function getVehicleBrandById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_brand_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getVehicleBrandById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);exit;  
		}
	}
	
	public function save_vehicle_brand()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleBrandId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("brand" => $postData["txt_vehicle_brand"]);
						$newscat = $this->administration_model->getAllVehicleBrand($cond);
						$data["vehicleCat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_BRAND,array("brand" => ucwords($postData["txt_vehicle_brand"])));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Vehicle Category has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This category has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleBrand($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_brand_id !=" => $this->encrypt->decode($postData["vehicleBrandId"]), "brand" => $postData["txt_vehicle_brand"]);
						$newscat = $this->administration_model->getAllVehicleBrand($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["vehicleBrand"] = $newscat;
						
						if(count($newscat) == 0)
						{
							$updateArr = $this->common_model->update(TB_BRAND,array("v_brand_id" => $this->encrypt->decode($postData["vehicleBrandId"])),array("brand" => ucwords($postData["txt_vehicle_brand"])));
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleBrandId"].'"></td>
										<td class="text-center">'.$postData["txt_vehicle_brand"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleBrandId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleBrandId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Brands has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleBrandId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Brands has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This brands has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);exit;
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_brand() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{ 
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				
				$count = $this->administration_model->getVehicleBrandCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("brand");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_brand_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$brands = $this->administration_model->getVehicleBrandPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'brand\',\''.$corder[0].'\')">Vehicle Brand</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($brands)>0)
				{
						if(count($brands) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
						foreach($brands as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["brand"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($brands)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleBrandList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				//print_r($table);
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($brands),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	public function validateVehicleBrand($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_brands() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_brand_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_BRAND,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	/*
	 * Vehicle Model
	 * */
	
	public function getVehicleModel()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$data["brands"] = $this->administration_model->getVehicleBrand();
				$this->load->view("administrator/vehicle_model",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	public function getVehicleModelById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("v_model_id" => $this->encrypt->decode($postData["id"]));
			$vmodel = $this->administration_model->getVehicleModelById($cond);
			//print_r($users);die;
			echo json_encode($vmodel[0]);exit; 
		}
	}
	
	public function save_vehicle_model()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["vehicleModelId"] == "")
				{ 
					
					$isValid = $this->validateNewsCategory($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("v_model" => ucwords($postData["txt_vehicle_model"]));
						$newscat = $this->administration_model->getAllVehicleModel($cond);
						$data["vehicleModel"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_MODEL,array("v_model" => ucwords($postData["txt_vehicle_model"]),"v_brand_id" => $postData["v_brand_id"]));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Vehicle Model has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This model has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateVehicleModel($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("v_model_id !=" => $this->encrypt->decode($postData["vehicleModelId"]),"v_brand_id" => $postData["v_brand_id"],"v_model" => ucwords($postData["txt_vehicle_model"]));
						
						$vmodels = $this->administration_model->getAllVehicleModel($cond);
						//echo $newscat;
						//echo $this->db->last_query();die;
						$data["vehicleModel"] = $vmodels;
						
						if(count($vmodels) == 0)
						{
							$updateArr = $this->common_model->update(TB_MODEL,array("v_model_id" => $this->encrypt->decode($postData["vehicleModelId"])),array("v_model" => ucwords($postData["txt_vehicle_model"]),"v_brand_id" => $postData["v_brand_id"]));
							
							if($updateArr)
							{
								
								/*retrive brand name*/
								
								$cond1 = array("v_brand_id" => $postData["v_brand_id"]);
								$brand= $this->administration_model->getVehicleBrandById($cond1);
								
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleModelId"].'"></td>
										<td class="text-center">'.$brand["0"]["brand"].'</td>
										<td>'.$postData["txt_vehicle_model"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="edit" data-option="'.$postData["vehicleModelId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleModelId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Model has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleModelId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Model has been updated successfully.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This model has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle_model() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				$join = array(TB_BRAND => TB_MODEL.".v_brand_id = ".TB_BRAND.".v_brand_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				
				$count = $this->administration_model->getVehicleModelCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("brand","v_model"); 
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "v_model_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$brands = $this->administration_model->getVehicleModelPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				//echo $this->db->last_query();
				$links = "";
				$table = ""; 
				//print_r($corder);
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'brand\',\''.$corder[0].'\')">Vehicle Brand</th>
								<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'v_model\',\''.$corder[1].'\')">Vehicle Model</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($brands)>0)
				{
						if(count($brands) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($brands as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["brand"].'</td> 
							<td class="text-center">'.$cat["v_model"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($brands)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleBrandList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				//print_r($table);
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($brands),'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicleModel($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_news_cat"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	public function delete_vehicle_Model() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("v_model_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_MODEL,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	/*
	 * Manage vehicles*
	 */
	 public function getVehicles()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				$data["brands"] = $this->administration_model->getVehicleBrand();
				$data["models"] = $this->administration_model->getVehicleModel();
				
				$this->load->view("administrator/vehicle_details",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
			
		}
	}
	
	 
	
	public function getVehicleById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("vehicle_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getVechicleById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);exit;
				 
		}
	}
	
	public function getModelById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("v_brand_id" => $postData["id"]);
			$models = $this->administration_model->getVehicleModelById($cond); 
			$data ='';
			if(count($models)>0){
			$data = '<select class="select-sm arrow_new" name="vmodel" id="vmodel">'; 
			foreach($models as $model):
				$data .= '<option value="'.$model['id'].'">'.$model['v_model'].'</option>'; 
			endforeach;
			$data .='</select>'; 
			}
			else{
				$data .="No model available";
			}
			echo json_encode($data); exit;	 
		}
	}
	
	public function save_vehicles() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["vehicleId"] == "")
				{   
					$isValid = $this->validateVehicle($postData,$_FILES);
					//print_r($isValid);die;
					if($isValid["status"] == 1)
					{ 
						
						$cond = array(
						"vehicle_name" =>$postData["txt_vehicle_name"],
						"vehicle_description" => $postData["txt_vehicle_description"],
						"v_brand_id" => $brands,
						"v_model_id" =>$vmodel,
						"car_maker_pn" => $car_maker_pn ,
						"denso_pn" => $denso_pn,
						"cool_gear_pn1" => $cool_gear_pn1,
						"year" => $year,
						"color"=> $postData["txt_color"]
						);
						$vehicles = $this->administration_model->getAllVehicles($cond);
						$data["vehicles"] = $vehicles; 
						$data_resource = "";
						$data_subresource="";	
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						
						if(count($vehicles) == 0)
						{
							if(!isset($postData["vmodel"])) {
								$postData["vmodel"] = 'NULL';
							}
							$insertId = $this->common_model->insert(TB_VEHICLE,array("vehicle_name" => $postData["txt_vehicle_name"],"vehicle_description"=> $postData["txt_vehicle_description"],"v_cat_id" => $data_resource,"v_sub_cat_id"=>$data_subresource,"v_brand_id" => $postData["brands"],"v_model_id" =>$postData["vmodel"],"car_maker_pn" => $postData["txt_car_maker_pn"], "denso_pn" => $postData["txt_denso_pn"],"cool_gear_pn1" => $postData["txt_cool_gear_pn1"],"year" => $postData["year"],"type" => $postData["type"],"color"=>$postData["txt_color"],"date_modified" => date('Y-m-d H:i:s')));
							if($insertId)
							{ 
								if(isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != "" && $_FILES['v_image']['size']!=0){ 
									
									//Insert attchment after vehicle details
									$filename = $isValid["vehicle_file"];
									$updateArr = $this->common_model->update(TB_VEHICLE,array("vehicle_id" => $insertId),array("v_image" => $filename));
								}
								   
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Vehicle details has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This vehicle has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{ 
					$isValid = $this->validateVehicle($postData,$_FILES);
					if($isValid["status"] == 1)
					{  
						
						$vehicle_name = ($postData["txt_vehicle_name"]?$postData["txt_vehicle_name"]:"");
						$brands = (isset($postData["brands"])?$postData["brands"]:"");
						$vmodel = (isset($postData["vmodel"])?$postData["vmodel"]:"");
						$car_maker_pn = (isset($postData["txt_car_maker_pn"])?$postData["txt_car_maker_pn"]:"");
						$denso_pn = (isset($postData["txt_denso_pn"])?$postData["txt_denso_pn"]:"");
						$cool_gear_pn1 = (isset($postData["txt_cool_gear_pn1"])?$postData["txt_cool_gear_pn1"]:"");
						$year = (isset($postData["year"])?$postData["year"]:"");
						//$cool_gear_pn1 = ($postData["txt_cool_gear_pn1"]?$postData["txt_cool_gear_pn1"]:"");
						
						$cond = array("vehicle_id !=" => $this->encrypt->decode($postData["vehicleId"]),
									 "vehicle_name" => $vehicle_name,
									 "vehicle_description"=> $postData["txt_vehicle_description"],
									 "v_brand_id" => $brands,
									 "v_model_id" =>$vmodel,
									 "car_maker_pn" => $car_maker_pn ,
									 "denso_pn" => $denso_pn,
									 "cool_gear_pn1" => $cool_gear_pn1,
									 "year" => $year,
									 "color"=> $postData["txt_color"]
								);
						$cond1 = array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"]));
						$vehicles = $this->administration_model->getAllVehicles($cond);
						$userbyid = $this->administration_model->getVechicleById($cond1); 
						$data["vehicles"] = $vehicles; 
						$data_resource = "";
						$data_subresource="";
						if(!empty($postData["dd_resources"])){
							foreach($postData["dd_resources"] as $data2):
									$data_resource .= $data2.',';
							endforeach;
						}
						
						if(!empty($postData["dd_subresources"])){
							foreach($postData["dd_subresources"] as $data2):
									$data_subresource .= $data2.',';
							endforeach;
						}
						
						if(count($vehicles) == 0)
						{  
							if(!isset($postData["vmodel"])) {
								$postData["vmodel"] = 'NULL';
							}
							
							$updateArr = $this->common_model->update(TB_VEHICLE,array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"])),array("vehicle_name" => $postData["txt_vehicle_name"],"vehicle_description"=> $postData["txt_vehicle_description"],"v_cat_id" => $data_resource,"v_sub_cat_id" => $data_subresource,"v_brand_id" => $postData["brands"],"v_model_id" =>$postData["vmodel"],"car_maker_pn" => $postData["txt_car_maker_pn"], "denso_pn" => $postData["txt_denso_pn"],"cool_gear_pn1" => $postData["txt_cool_gear_pn1"],"year" => $postData["year"],"color"=>$postData["txt_color"],"type" => $postData["type"],"date_modified" => date('Y-m-d H:i:s')));
							
							if(isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != "" && $_FILES['v_image']['size']!=0){ 
									
									//Insert attchment after vehicle details
									$filename = $isValid["vehicle_file"];
									$updateArr = $this->common_model->update(TB_VEHICLE,array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"])),array("v_image" => $filename,"v_act_filename"=>$_FILES['v_image']['name']));
							}
							 
							if($updateArr)
							{
								//$catCond = array("v_cat_id" => $postData["category"]);
								//$vcat = $this->administration_model->getAllVehicleCategory($catCond);
								if(isset($postData["brands"]) && $postData["brands"]!="" && $postData["brands"]!="0"){
									$brandCond = array("v_brand_id" => $postData["brands"]);
									$vbrand = $this->administration_model->getAllVehicleBrand($brandCond);
									$vbrand_name  = $vbrand[0]["brand"];
									if(isset($postData["vmodel"]) && $postData["vmodel"]!="NULL"){
										$modelCond = array("v_model_id" => $postData["vmodel"]);
										$vmodel= $this->administration_model->getAllVehicleModel($modelCond); 
										$model = $vmodel[0]["v_model"];
									} 
									else{
										$model = "N/A";
									}
								}
								else{
									$vbrand_name  = "N/A";
									$model = "N/A";
								}
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["vehicleId"].'"></td>
										<td class="text-center">'.$postData["txt_vehicle_name"].'</td> 
										<td>'.$vbrand_name.'</td>
										<td>'.$model.'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["vehicleId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
										//echo $this->encrypt->decode($postData["userId"]);die;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["vehicleId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Vehicle details has been updated successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This vehicle details has been already taken.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_vehicle() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				$join = array(TB_CATEGORY => TB_VEHICLE.".v_cat_id = ".TB_CATEGORY.".v_cat_id",TB_BRAND => TB_VEHICLE.".v_brand_id = ".TB_BRAND.".v_brand_id",TB_MODEL => TB_VEHICLE.".v_model_id = ".TB_MODEL.".v_model_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getVehiclesCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("vehicle_name","brand","v_model");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "vehicle_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$vehicles = $this->administration_model->getVehiclePerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'vehicle_name\',\''.$corder[0].'\')">Vehicle Part Name</th>
								<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'brand\',\''.$corder[1].'\')">Vehicle Brand</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'v_model\',\''.$corder[2].'\')">Vehicle Model</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($vehicles)>0)
				{
						if(count($vehicles) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($vehicles as $vehicle) {  
					$table .= '<tr id="row_'.$vehicle["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($vehicle["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$vehicle["vehicle_name"].'</td>
							<td>'.($vehicle["brand"]?$vehicle["brand"]:"N/A").'</td>
							<td>'.($vehicle["v_model"]?$vehicle["v_model"]:"N/A").'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($vehicle["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($vehicles)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getVehicleList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($vehicles),'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateVehicle($postData,$FILES)
	{   
		$_FILES  = $FILES;
		if($_FILES!=""){
			if(isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != ""){
				
				$cond_exists = array("v_image" => $_FILES['v_image']['name']); 
				$vehicle_image = $this->administration_model->getVechicleById($cond_exists); 
				
					$ext = pathinfo($_FILES['v_image']['name'], PATHINFO_EXTENSION); 
					//$fname = 'vp_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
					$fname = $_FILES['v_image']['name'];
					 
					//echo $fname;die;
					$config = array( 
						'allowed_types' => 'jpg|JPG|png|PNG',
						'upload_path'   => $this->vehiclepartspath,
						'file_name'		=> $fname,
						'max_size'      => 5125
					); 
					
					$this->validatefile->initialize($config);
					$chk_valid_file = $this->validatefile->validate_file('v_image');
					 
					if(!$chk_valid_file){
						return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>'.$this->validatefile->display_errors().'</div>');
					}
					else{
						if(count($vehicle_image) ==0){
							$cond1 = array("vehicle_id" => $this->encrypt->decode($postData["vehicleId"])); 
							$vehiclebyid = $this->administration_model->getVechicleById($cond1);
								if(count($vehicle_image)==0){ 
									@unlink($this->vehiclepartspath.'/'.$vehiclebyid["0"]["v_image"]);
									$this->validatefile->data(); 
									$this->validatefile->do_upload('v_image'); 
								}
							return array("status" => 1, "vehicle_file" => $fname);
						}
					} 
				/*else{
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Please check the vehicle part image name.The image name is already exists.</div>');
				}*/
			}
		}
		
		/*if(isset($_POST["txt_cool_gear_pn1"])){
			$cond_cgp = array("vehicle_id !=" => $postData["vehicleId"],"cool_gear_pn1" => $postData["txt_cool_gear_pn1"]); 
			$vehicle_part = $this->administration_model->getVechicleById($cond_cgp); 
			if($vehicle_part!=0){
				return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>The cool gear part number1 is already exists</div>');
			}
		}*/
		
		/*if(!ctype_alpha($postData["txt_vehicle_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This fields require character only.</div>');
		} */		
		/*elseif (!preg_match('/^\+(?:[0-9] ?){6,14}[0-9]$/', $postData['txt_contact_number']) && $postData['txt_contact_number']!="") {
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Invalid phone number, the number should start with a plus sign, followed by the country code and national number</div>');
		}*/
		return array("status" => 1);
	}
	
	
	public function delete_vehicles() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("vehicle_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_VEHICLE,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	/*
	 * 
	 * add contries
	 * 
	 * */
	 
	 
	public function getCountry()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$data = "";
				$this->load->view("administrator/countries",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	public function getCountriesById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			$cond = array("country_id" => $this->encrypt->decode($postData["id"]));
			$users = $this->administration_model->getCountryById($cond);
			//print_r($users);die;
			echo json_encode($users[0]);  
		}
	}
	
	public function save_countries()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["countryId"] == "")
				{ 
					
					$isValid = $this->validateCountry($postData);
					if($isValid["status"] == 1)
					{ 
						$cond = array("name" => $postData["txt_country"]);
						$newscat = $this->administration_model->getAllContries($cond);
						$data["newscat"] = $newscat;
						if(count($newscat) == 0)
						{
							$insertId = $this->common_model->insert(TB_COUNTRY,array("name" => $postData["txt_country"]));
							if($insertId)
							{
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Country has been added successfully</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This Country has been already available.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{
					$isValid = $this->validateCountry($postData);
					if($isValid["status"] == 1)
					{
						$cond = array("country_id !=" => $this->encrypt->decode($postData["countryId"]), "name" => $postData["txt_country"]);
						$country = $this->administration_model->getAllContries($cond);
						//echo $newscat;
						//echo $this->db->last_query();
						$data["country"] = $country;
						
						if(count($country) == 0)
						{
							$updateArr = $this->common_model->update(TB_COUNTRY,array("country_id" => $this->encrypt->decode($postData["countryId"])),array("name" => $postData["txt_country"]));
							
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["countryId"].'"></td>
										<td class="text-center">'.$postData["txt_country"].'</td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["countryId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["countryId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Country has been updated successfully</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["countryId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Country has been updated successfully</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This country has been already exists.</div>')); exit;
						}
					}
					else
					{
						echo json_encode($isValid);
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_countries() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getCountryCount($cond,$like);
				
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("name");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "name";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{ 
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$country = $this->administration_model->getCountryPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'name\',\''.$corder[0].'\')">Country</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($country)>0)
				{
						if(count($country) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($country as $cat) {   
					$table .= '<tr id="row_'.$cat["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cat["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center">'.$cat["name"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cat["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($country)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getCountriesList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
			}
		}
	}
	
	public function validateCountry($postData)
	{
		//print_r($postData);
		/*if(!ctype_alpha($postData["txt_country"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This fields require character only.</div>');
		}
		*/ 
		return array("status" => 1);
	}
	
	
	public function delete_countries() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					
					
					$cond_user_country = array("country" => $this->encrypt->decode($postData["ids"][$i]));
					
					$users = $this->administration_model->getAllUsers($cond_user_country);
					
					if(count($users) == 0){
						$cond = array("country_id" => $this->encrypt->decode($postData["ids"][$i]));				
						$isdelete = $this->common_model->delete(TB_COUNTRY,$cond);
						if($isdelete)
						{
							$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
						}
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
public function importcsvproduct()
	{ob_start();
		$data = ""; 
		if(is_user_logged_in())
		{ 
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				
				$data['message'] = $this->session->flashdata('message');
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
				if(isset($_FILES["csv"]["name"]) && $_FILES["csv"]["name"]!=""){
					$item = array();
					       $dd_resources = $_POST['dd_resources'];
                            
								for ($i=0; $i<sizeof($dd_resources); $i++)
									{
										 $cond = "v_cat_id = " .$dd_resources[$i];
										 $isdelete = $this->common_model->delete(TB_VEHICLE,$cond);
									}
					
					$ext = strtolower(pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION)); // Getting extetion of the file
					$size = $_FILES['csv']['size'];
					
					if($size>=10000000){
						 $data['validation_message'] = "The file size should not be greater than 100MB"; 
						 $this->load->view("administrator/import_csv",$data);
					}
					
					elseif($ext == 'csv'){
							if($size > 0){
							   $count=0;
							   $not_inserted =0;
							   $i=0;     
							   $duplicate=0;
								$linearray = array();
								$arr = array();
								$arrcheck = array();
								$vehi_cat ="";
								$vehi_sub_cat="";
								$proudct_no = "";
								$cool_gear_pn = "";
								$denso_pn = "";
								$car_maker_pn = "";
								$vehicle = "";
								$model = "";
								$year = "";
								$type = "";
								$color = "";
								$description = "";
								$vmodel="";
								$vbrand="";
								if (($handle = fopen($_FILES['csv']['tmp_name'], "r")) !== FALSE) {	
									$row = array();
									$merge_row = array();
									while (($linearray = fgetcsv($handle, 0, ",")) !== FALSE) { 
										$vehi_cat ="";
										$vehi_sub_cat="";
										$proudct_no = "";
										$cool_gear_pn = "";
										$denso_pn = "";
										$car_maker_pn = "";
										$vehicle = "";
										$model = "";
										$year = "";
										$vbrand="";
										$vmodel="";
										$color = "";
										$vehicle_sub_category_id="";
										$compose_image = NULL;
										//$count  =count($linearray);
										if($i>0)
										{ 
											$vehi_cat = trim($linearray[0]);
											$vehi_sub_cat = trim($linearray[1]);
											$cool_gear_pn = trim($linearray[2]);
											$denso_pn = trim($linearray[3]);
											$car_maker_pn = trim($linearray[4]);
											$vehicle = trim($linearray[5]);
											$model = trim($linearray[6]);
											$year = trim($linearray[7]?trim($linearray[7]):"");
											$type = ($linearray[8]?trim($linearray[8]):"");
											$color = ($linearray[9]?trim($linearray[9]):"");
											$description = trim($linearray[10]?trim($linearray[10]):"");
											$vehicle_category_id="";
											$vehicle_sub_category_id="";
											if($vehi_sub_cat != ""){
												$vehi_name = $vehi_sub_cat;
											}else{
												$vehi_name = $vehi_cat;
											}
											
																					 
											if($vehi_cat != "") {
											
												if($vehi_cat !=""){
													$cond = array("category" => $vehi_cat);
													if($this->administration_model->checkVehicleCategoryExists($cond) == 1){
														/*if exists then pick the records from db*/
														$pcat = $this->administration_model->getAllVehicleCategory($cond);
														$vehicle_category_id = $pcat["0"]["id"];
													}
													else{ 
														/*if not exist then insert and pick the catid from database*/
														
														$insertId = $this->common_model->insert(TB_CATEGORY,array("category" => $vehi_cat));
														
														if($insertId){
															$pcat =  $this->administration_model->getAllVehicleCategory($cond);
															$vehicle_category_id = $pcat["0"]["id"];
														}
													}
												}
												
												if($vehi_sub_cat != "" && $vehi_cat != "" && $vehicle_category_id != ""){ 
													$cond_v_cat = array("category" => $vehi_cat);
													$cond_v_sub_cat = array("v_cat_id" => $vehicle_category_id,"sub_category" => $vehi_sub_cat); 
													 
													if($this->administration_model->checkVehicleSubCategoryExists($cond_v_sub_cat) == 0){
														/*if exists then pick the records from db*/
														$pcat = $this->administration_model->getAllVehicleCategory($cond_v_cat);
														$cid = $pcat["0"]["id"]; 
														$insertId = $this->common_model->insert(TB_SUBCATEGORY,array("v_cat_id" =>$cid,"sub_category" => $vehi_sub_cat)); 
														
														if($insertId){
															$vehicle_sub_category_id = $insertId;
														} 
													}
													else{
														$psubcat = $this->administration_model->getAllVehicleSubCategory($cond_v_sub_cat);
														$vehicle_sub_category_id = $psubcat["0"]["id"];  
													}  
												}
												
												
												if($vehicle !=""){
													$cond = array("brand" => $vehicle);
													if($this->administration_model->checkVehicleBrandExists($cond) == 1){
														/*if exists then pick the records from db*/
														$v_cat = $this->administration_model->getAllVehicleBrand($cond);
														$vbrand = $v_cat["0"]["id"];
														
													}
													else{ 
														/*if not exist then insert and pick the catid from database*/
														$insertId = $this->common_model->insert(TB_BRAND,array("brand" => $vehicle));
														
														if($insertId){
															$v_cat = $this->administration_model->getAllVehicleBrand($cond);
															$vbrand = $v_cat["0"]["id"];
														}
													}
												}
												 
												
												if($model !="" && $vehicle !="" && $vbrand != ""){
													 
													$cond_brand = array("brand" => $vehicle);
													$cond_model = array("v_brand_id" => $vbrand,"v_model" => $model); 
													 
													if($this->administration_model->checkVehicleModelExists($cond_model) == 0){
														/*if exists then pick the records from db*/
														$v_cat = $this->administration_model->getAllVehicleBrand($cond_brand);
														$vcategory = $v_cat["0"]["id"]; 
														$insertId = $this->common_model->insert(TB_MODEL,array("v_brand_id" =>$vcategory,"v_model" => $model)); 
														
														if($insertId){
															$vmodel = $insertId;
														} 
													} 
													
												}
												
												if($model !="" && $vehicle !=""){   
													$cond_model = array("v_brand_id" => $vbrand,"v_model" => $model); 
													/*get the v_model_id for further chekcing in duplicate records*/
													$v_model_data = $this->administration_model->getAllVehicleModel($cond_model);
													$vmodel = $v_model_data["0"]["id"];  
												}
												else if($vbrand!=""){
													$vbrand=$vbrand;
												}
												else{
													$vbrand=NULL;
												}
												
												if($model ==""){
													$vmodel = NULL;  
												}
												 
																						
												/*check whether its duplicate entry or not*/
												 
												$cond_part = array( "v_cat_id" => $vehicle_category_id,
																	"v_sub_cat_id" => $vehicle_sub_category_id,
																	"vehicle_name" => $vehi_name, 
																	"vehicle_description" => $description,
																	"v_model_id" => $vmodel,
																	"v_brand_id" => $vbrand,
																	"type"  => $type,
																	"color"	=> $color,
																	"year" => $year,
																	"car_maker_pn"  => $car_maker_pn,
																	"denso_pn" => $denso_pn,
																	"cool_gear_pn1" => $cool_gear_pn
																  );
																					
											
												if($this->administration_model->checkVehiclePartExists($cond_part) == 0){
												
												
												/*compose image name and insert into database*/
												
												if($cool_gear_pn != ""){
													$compose_image = $cool_gear_pn.'.jpg';
												}else if($denso_pn != ""){
													$compose_image = $denso_pn.'.jpg';
												}											 
												
												$insertId = $this->common_model->insert(TB_VEHICLE,
																array(
																	"v_cat_id" => $vehicle_category_id,
																	"v_sub_cat_id" => $vehicle_sub_category_id,
																	"vehicle_name" => $vehi_name, 
																	"vehicle_description" => $description,
																	"v_model_id" => $vmodel,
																	"v_brand_id" => $vbrand,
																	"type"  => $type,
																	"color"	=> $color,
																	"year" => $year,
																	"car_maker_pn"  => $car_maker_pn,
																	"denso_pn" => $denso_pn,
																	"cool_gear_pn1" => $cool_gear_pn,
																	"v_image"	=> $compose_image,
																	"date_modified" => date("Y-m-d H:i:s")
																)
															);
														$count++;
												}
												else{
													$duplicate++;
												}
										 }
										 else{
											 $not_inserted++;
										 }
											
										} 
										
										$i++;
										
									} 
									
										
									$info=""; 
									$info .="Total <b>$count</b> record(s) are inserted<br/>";
									$info .="Total <b>$not_inserted</b> record(s) has been rejected<br/>"; 
									$info .="Total <b>$duplicate</b> record(s) are duplicate";
									
								}
								//die;
								$this->session->set_flashdata('message', $info);
								redirect('administration/importcsvproduct');
								//echo '<meta http-equiv="refresh" content="0; url='.base_url().'administration/importcsvproduct" />';
								//$data['count']  = $count++;
							}
						else{
							 $data['validation_message'] = "The file is empty"; 
							 $this->load->view("administrator/import_csv",$data);
						}
					}
					elseif($ext == 'xls'){ 
						$fname = $_FILES['csv']['tmp_name'];
						
						$excel = new Excelreader;
						//$excel->setUTFEncoder('mb');
						$excel->setOutputEncoding('UTF-8');
						$excel->read($fname,'r');
						
						$count=0;
					   
						$i=0;     
						$duplicate=0;  
						$vehi_cat ="";
						$vehi_sub_cat="";
						$proudct_no = "";
						$cool_gear_pn = "";
						$denso_pn = "";
						$car_maker_pn = "";
						$vehicle = "";
						$model = "";
						$year = "";
						$type = "";
						$color = "";
						$description = "";
						$vmodel="";
						$vbrand="";
						$not_inserted =0;
									
						for($i=0; $i<1; $i++) {
							$numrows = $excel->sheets[$i]['numRows'];
							
							if($numrows <= 1){
								$data['validation_message'] = "The file is invalid/empty"; 
								$this->load->view("administrator/import_csv",$data);exit;
							}
							
							if($numrows > 0){
								$vehi_cat ="";
								$vehi_sub_cat="";
								$proudct_no = "";
								$cool_gear_pn = "";
								$denso_pn = "";
								$car_maker_pn = "";
								$vehicle = "";
								$model = "";
								$year = "";
								$vbrand="";
								$vmodel="";
								$color = "";
								$vehicle_sub_category_id="";
								$compose_image = NULL;
								$sheet = array();
								$k=1;
								foreach($excel->sheets[$i] as $shx):
									unset($shx[1]);
									
									for($j=2;$j<=$numrows;$j++){
										
										if($k==5){
										$vehi_cat = ($shx[$j][1]?trim($shx[$j][1]):"");
										$vehi_sub_cat = ($shx[$j][2]?trim($shx[$j][2]):"");
										$cool_gear_pn = ($shx[$j][3]?trim($shx[$j][3]):"");
										$denso_pn = ($shx[$j][4]?trim($shx[$j][4]):"");  
										$car_maker_pn = ($shx[$j][5]?trim($shx[$j][5]):"");
										$vehicle = ($shx[$j][6]?trim($shx[$j][6]):"");
										$model = ($shx[$j][7]?trim($shx[$j][7]):"");
										$year = ($shx[$j][8]?trim($shx[$j][8]):"");
										$type = ($shx[$j][9]?trim($shx[$j][9]):"");
										$color = ($shx[$j][10]?trim($shx[$j][10]):"");
										$description = ($shx[$j][11]?trim($shx[$j][11]):"");
										
										$vehicle_category_id="";
										$vehicle_sub_category_id="";
										if($vehi_sub_cat != ""){
												$vehi_name = $vehi_sub_cat;
											}else{
												$vehi_name = $vehi_cat;
											}
																					 
											if($vehi_cat != "") {
											
												if($vehi_cat !=""){
													$cond = array("category" => $vehi_cat);
													if($this->administration_model->checkVehicleCategoryExists($cond) == 1){
														/*if exists then pick the records from db*/
														$pcat = $this->administration_model->getAllVehicleCategory($cond);
														$vehicle_category_id = $pcat["0"]["id"];
													}
													else{ 
														/*if not exist then insert and pick the catid from database*/
														
														$insertId = $this->common_model->insert(TB_CATEGORY,array("category" => $vehi_cat));
														
														if($insertId){
															$pcat =  $this->administration_model->getAllVehicleCategory($cond);
															$vehicle_category_id = $pcat["0"]["id"];
														}
													}
												}
												
												if($vehi_sub_cat != "" && $vehi_cat != "" && $vehicle_category_id != ""){ 
													$cond_v_cat = array("category" => $vehi_cat);
													$cond_v_sub_cat = array("v_cat_id" => $vehicle_category_id,"sub_category" => $vehi_sub_cat); 
													 
													if($this->administration_model->checkVehicleSubCategoryExists($cond_v_sub_cat) == 0){
														/*if exists then pick the records from db*/
														$pcat = $this->administration_model->getAllVehicleCategory($cond_v_cat);
														$cid = $pcat["0"]["id"]; 
														$insertId = $this->common_model->insert(TB_SUBCATEGORY,array("v_cat_id" =>$cid,"sub_category" => $vehi_sub_cat)); 
														
														if($insertId){
															$vehicle_sub_category_id = $insertId;
														} 
													}
													else{
														$psubcat = $this->administration_model->getAllVehicleSubCategory($cond_v_sub_cat);
														$vehicle_sub_category_id = $psubcat["0"]["id"];  
													}  
												}
												
												
												if($vehicle !=""){
													$cond = array("brand" => $vehicle);
													if($this->administration_model->checkVehicleBrandExists($cond) == 1){
														/*if exists then pick the records from db*/
														$v_cat = $this->administration_model->getAllVehicleBrand($cond);
														$vbrand = $v_cat["0"]["id"];
														
													}
													else{ 
														/*if not exist then insert and pick the catid from database*/
														$insertId = $this->common_model->insert(TB_BRAND,array("brand" => $vehicle));
														
														if($insertId){
															$v_cat = $this->administration_model->getAllVehicleBrand($cond);
															$vbrand = $v_cat["0"]["id"];
														}
													}
												}
												 
												
												if($model !="" && $vehicle !="" && $vbrand != ""){
													 
													$cond_brand = array("brand" => $vehicle);
													$cond_model = array("v_brand_id" => $vbrand,"v_model" => $model); 
													 
													if($this->administration_model->checkVehicleModelExists($cond_model) == 0){
														/*if exists then pick the records from db*/
														$v_cat = $this->administration_model->getAllVehicleBrand($cond_brand);
														$vcategory = $v_cat["0"]["id"]; 
														$insertId = $this->common_model->insert(TB_MODEL,array("v_brand_id" =>$vcategory,"v_model" => $model)); 
														
														if($insertId){
															$vmodel = $insertId;
														} 
													} 
													
												}
												
												if($model !="" && $vehicle !=""){   
													$cond_model = array("v_brand_id" => $vbrand,"v_model" => $model); 
													/*get the v_model_id for further chekcing in duplicate records*/
													$v_model_data = $this->administration_model->getAllVehicleModel($cond_model);
													$vmodel = $v_model_data["0"]["id"];  
												}
												elseif($vbrand!=""){
													$vbrand=$vbrand;
												}
												else{
													$vbrand=NULL;
												}
												
												if($model ==""){
													$vmodel = NULL;  
												}
												 
																						
												/*check whether its duplicate entry or not*/
												 
												$cond_part = array( "v_cat_id" => $vehicle_category_id,
																	"v_sub_cat_id" => $vehicle_sub_category_id,
																	"vehicle_name" => $vehi_name, 
																	"vehicle_description" => $description,
																	"v_model_id" => $vmodel,
																	"v_brand_id" => $vbrand,
																	"type"  => $type,
																	"color"	=> $color,
																	"year" => $year,
																	"car_maker_pn"  => $car_maker_pn,
																	"denso_pn" => $denso_pn,
																	"cool_gear_pn1" => $cool_gear_pn
																  );
																					
											
												if($this->administration_model->checkVehiclePartExists($cond_part) == 0){
												
												
												/*compose image name and insert into database*/
												
												if($cool_gear_pn != ""){
													$compose_image = $cool_gear_pn.'.jpg';
												}else if($denso_pn != ""){
													$compose_image = $denso_pn.'.jpg';
												}											 
												
												$insertId = $this->common_model->insert(TB_VEHICLE,
																array(
																	"v_cat_id" => $vehicle_category_id,
																	"v_sub_cat_id" => $vehicle_sub_category_id,
																	"vehicle_name" => $vehi_name, 
																	"vehicle_description" => $description,
																	"v_model_id" => $vmodel,
																	"v_brand_id" => $vbrand,
																	"type"  => $type,
																	"color"	=> $color,
																	"year" => $year,
																	"car_maker_pn"  => $car_maker_pn,
																	"denso_pn" => $denso_pn,
																	"cool_gear_pn1" => $cool_gear_pn,
																	"v_image"	=> $compose_image,
																	"date_modified" => date("Y-m-d H:i:s")
																)
															);
														$count++;
												}
												else{
													$duplicate++;
												}
										 }
										 else{
											 $not_inserted++;
										 }
											$info=""; 
											$info .="Total <b>$count</b> record(s) are inserted<br/>";
											$info .="Total <b>$not_inserted</b> record(s) has been rejected<br/>"; 
											$info .="Total <b>$duplicate</b> record(s) are duplicate";
										
										}
									}
									$k++;
								endforeach;
								
							}
							
								$this->session->set_flashdata('message', $info);
								redirect('administration/importcsvproduct');
							
						}
						
					}
					else{
						$data['validation_message'] = "The file is invalid/empty"; 
						$this->load->view("administrator/import_csv",$data);
					}
				}
				else
				{
					
					$this->load->view("administrator/import_csv",$data);
				}
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
		else{
			redirect(); exit;
		}  
		ob_end_flush();
	}
	
	
	public function getDownloadStatistics()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$topdownloadpdf = $this->administration_model->getTopVisitedResourcePDF();
				$data['downloadpdf'] = '';
				$j=1;
				foreach($topdownloadpdf as $tppdf):
					$data['downloadpdf'] .= '<br/><b>'.$j.'</b>.'.$tppdf['download_pdf'];
					$j++;
				endforeach;
				$downloadai = $this->administration_model->getTopVisitedResourceAI();
				$data['downloadai'] = '';
				$k=1;
				foreach($downloadai as $tpai):
					$data['downloadai'] .= '<br/><b>'.$k.'</b>.'.$tpai['download_ai'];
					$k++;
				endforeach;
				
				$topv = $this->administration_model->getTopVisitedPage();
				
				$data['topvisited'] = '';
				$i=1;
				foreach($topv as $tp): 
					$data['topvisited'] .= '<b>'.$i.'</b>.'.ucwords($tp['page_name']).'<br/>';
					$i++;
				endforeach;
				$cond = array("session_id !="=>'');
				$like = array(); 
				$data['usercount'] = $this->administration_model->getUsersCount($cond,$like);
				$this->load->view("administrator/download_statistics",$data);
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	
	function download_statistics(){
		
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getUsersCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("first_name","last_name","email_address");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "user_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$users = $this->administration_model->getUsersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="2%"></th>
								<th class="text-center '.$css["0"].'" width="10%" onclick="changePaginate(0,\'first_name\',\''.$corder[0].'\')">First Name</th>
								<th class="'.$css[1].'" onclick="changePaginate(0,\'last_name\',\''.$corder[1].'\')">Last Name</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'email_address\',\''.$corder[2].'\')">Email Address</th>
								<th width="10%">Login Count</th> 
								<th class="text-center">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($users)>0)
				{
						if(count($users) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						$date_a = strtotime(date("Y-m-d H:i:s")); 
						foreach($users as $user) {   
							
							//$cond_act = array("user_id"=>$user["id"]);
							//$users_activity = $this->administration_model->getUsersLastLoginActivityInFormat($cond_act);
							///print_r($users_activity);
							if($user["last_activity"] != "" && $user["last_activity"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$last_activity_duration= date('d/m/Y H:i:s',strtotime($user["last_activity"]));
							}
							else
							{
								$last_activity_duration = "-";
							}
							if($user["in_time"] != "" && $user["in_time"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$in_time= date('d/m/Y H:i:s',strtotime($user["in_time"]));
							}
							else
							{
								$in_time = "-";
							}
							//$user["last_login"]; 
							
					$table .= '<tr id="row_'.$user["id"].'">
							<td></td>
							<td class="text-center">'.$user["first_name"].'</td>
							<td>'.$user["last_name"].'</td>
							<td>'.$user["email_address"].'</td>
							<td>'.$user["login_count"].'</td> 
							<td class="text-center">
								<a title="Edit" alt="Edit" id="status" class="viewstatistics" href="'.base_url().'administration/viewdownloadstatistics/?uid='.$user['id'].'&sort_by=1">View/Search Statistics Download</a>
							</td>						  
						  </tr>
						  ';
						}
						//die;
				}
				if($postData["start"] == 0)
				{
						if(count($users)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				//echo json_encode(array('table' => $table,'start' => $postData["start"],'paginate' => $paginate)); exit;
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($users),'paginate' => $paginate)); exit;
			}
		}
	}
	
	function relativeTime($time, $short = false){
			$SECOND = 1;
			$MINUTE = 60 * $SECOND;
			$HOUR = 60 * $MINUTE;
			$DAY = 24 * $HOUR;
			$MONTH = 30 * $DAY; 
			//$t = strtotime(date("Y-m-d H:m:s"));
			$before = time() - $time;
			
			if ($before <= 0)
			{
				return "not yet";
			}

			if ($short){
				if ($before < 1 * $MINUTE)
				{
					return ($before <5) ? "Just now" : $before . " ago";
				}

				if ($before < 2 * $MINUTE)
				{
					return "1m ago";
				}

				if ($before < 45 * $MINUTE)
				{
					return floor($before / 60) . "m ago";
				}

				if ($before < 90 * $MINUTE)
				{
					return "1h ago";
				}

				if ($before < 24 * $HOUR)
				{

					return floor($before / 60 / 60). "h ago";
				}

				if ($before < 48 * $HOUR)
				{
					return "1d ago";
				}

				if ($before < 30 * $DAY)
				{
					return floor($before / 60 / 60 / 24) . "d ago";
				}


				if ($before < 12 * $MONTH)
				{
					$months = floor($before / 60 / 60 / 24 / 30);
					return $months <= 1 ? "1mo ago" : $months . "mo ago";
				}
				else
				{
					$years = floor  ($before / 60 / 60 / 24 / 30 / 12);
					return $years <= 1 ? "1y ago" : $years."y ago";
				}
			}

			if ($before < 1 * $MINUTE)
			{
				return ($before <= 1) ? "just now" : $before . " seconds ago";
			}

			if ($before < 2 * $MINUTE)
			{
				return "a minute ago";
			}

			if ($before < 45 * $MINUTE)
			{
				return floor($before / 60) . " minutes ago";
			}

			if ($before < 90 * $MINUTE)
			{
				return "an hour ago";
			}

			if ($before < 24 * $HOUR)
			{

				return (floor($before / 60 / 60) == 1 ? 'about an hour' : floor($before / 60 / 60).' hours'). " ago";
			}

			if ($before < 48 * $HOUR)
			{
				return "yesterday";
			}

			if ($before < 30 * $DAY)
			{
				return floor($before / 60 / 60 / 24) . " days ago";
			}

			if ($before < 12 * $MONTH)
			{

				$months = floor($before / 60 / 60 / 24 / 30);
				return $months <= 1 ? "one month ago" : $months . " months ago";
			}
			else
			{
				$years = floor  ($before / 60 / 60 / 24 / 30 / 12);
				return $years <= 1 ? "one year ago" : $years." years ago";
			}

			return "$time";
		}
		
	function viewdownloadstatistics(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				$getData = $this->input->get();
				if(isset($_GET['uid']) && $_GET['uid'] !=""){
					$cond = array("user_id"=>$getData['uid']);
					$cond_sort = array("user_id"=>$getData['uid'],"date_modified >=" => $fromdate,"date_modified <=" => $todate);
					$user_details = $this->administration_model->getAllUsers($cond);
					$data['user_name'] = $user_details["0"]["first_name"]." ".$user_details["0"]["last_name"]; 
					
					$this->load->view("administrator/view_download_statistics",$data);
				}
				else{
					redirect("administration/getDownloadStatistics"); exit;
				}
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	function getUserDownloadStatistics(){
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			
			/*Check permission for AMW And SKW */
			if($this->checkPermission() == 1){
				
			$postData = $this->input->post(); 
			
			$cond_sort = array("user_id"=>$postData['uid'],"date(date_modified) >=" => $postData['fromdate'],"date(date_modified) <=" => $postData['todate']);
			$user_details = $this->administration_model->getAllUsers($cond);
			
			$user_name = $user_details["0"]["first_name"]." ".$user_details["0"]["last_name"];
			$user_resource_downloads = $this->administration_model->getDownloadByUserId($cond_sort);
			
			$cond_news = array("user_id"=>$postData['uid'],"date(dn_news_viewed.date_modified) >=" => $postData['fromdate'],"date(dn_news_viewed.date_modified) <=" => $postData['todate']);
			$join_news = array(TB_NEWS => TB_VIEWED_NEWS.".news_id = ".TB_NEWS.".news_id");
			
			$user_viewed_news = $this->administration_model->getViewedNewsByUserId($cond_news,$join_news);
			$cond_search = array("user_id"=>$postData['uid'],"date(dn_viewed_search.date_modified) >=" => $postData['fromdate'],"date(dn_viewed_search.date_modified) <=" => $postData['todate']);
			$join_search = array(TB_VEHICLE => TB_VIEWED_SEARCH.".search_part_id = ".TB_VEHICLE.".vehicle_id");
			$user_viewed_search_part = $this->administration_model->getViewedSearchPartByUserId($cond_search,$join_search);
			
			$cond_user = array("dn_user_statistics.user_id"=>$postData['uid'],"date(dn_user_statistics.date_modified) >=" => $postData['fromdate'],"date(dn_user_statistics.date_modified) <=" => $postData['todate']);
			$join_user = array(TB_USERS => TB_USER_STAT.".user_id = ".TB_USERS.".user_id");
			$user_history = $this->administration_model->getLoggedInUser($cond_user,$join_user);
			
			
			$table='<div class="col-md-12"><div class="tab-block mb25">
					<ul class="nav tabs-left">
			
						<li class="active">
							<a href="#tab12_1" data-toggle="tab">Downloaded Resources</a>
						</li>
						<li>
							<a href="#tab12_2" data-toggle="tab">Viewed News</a>
						</li>
						<li>
							<a href="#tab12_3" data-toggle="tab">Viewed Search Part</a>
						</li>
						<li>
							<a href="#tab12_4" data-toggle="tab">Logged In History</a>
						</li>
					</ul>
					<div class="tab-content" style="margin-top:10px;">';
			$table .= '<div id="tab12_1" class="tab-pane active">
			           <table class="table table-striped table-hover dataTable no-footer" width="100%" height="100%">';
						$table .= '<thead>
							  <tr> 
								<th width="40%" style="background:#F9F9F9 !important;" class="text-center">PDF Download</th>
								<th width="40%" class="text-center">AI File Download</th>
								<th width="20%" class="text-center">Download Date</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_resource_downloads)>0)
				{
					foreach($user_resource_downloads as $download) {
							$table .='<tr> 
										<td class="text-center">'.($download["download_pdf"]?$download["download_pdf"]:'-').'</td>
										<td class="text-center">'.($download["download_ai"]?$download["download_ai"]:'-').'</td>
										<td class="text-center">'.date("d/m/Y H:i:s",strtotime($download["date_modified"])).'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="4">No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';
			
			$table .='<div id="tab12_2" class="tab-pane">';
			
			$table .= '<table class="table table-striped table-hover dataTable no-footer" width="100%" style="width:100%" height="100%">';
						$table .= '<thead>
							  <tr>  
								<th style="background:#F9F9F9 !important;" width="70%" class="text-center">News Heading</th> 
								<th class="text-center"  width="30%">Viwed Date</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_viewed_news)>0)
				{
					foreach($user_viewed_news as $news) {
							$table .='<tr> 
										<td class="text-center">'.($news["news_heading"]?$news["news_heading"]:'-').'</td> 
										<td class="text-center">'.date("d/m/Y H:i:s",strtotime($news["date_modified"])).'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="3" >No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';
			 
			$table .='<div id="tab12_3" class="tab-pane">
			          <table class="table table-striped table-hover dataTable no-footer" width="100%" height="100%">';
						$table .= '<thead>
							  <tr> 
								<th style="background:#F9F9F9 !important;" class="text-center"  width="40%">Product Name</th> 
								<th class="text-center"  width="40%">COOLGEAR Part</th>  
								<th class="text-center"  width="20%">Viwed Date</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_viewed_search_part)>0)
				{
					foreach($user_viewed_search_part as $search) {
							$table .='<tr> 
										<td class="text-center">'.($search["vehicle_name"]?$search["vehicle_name"]:'-').'</td> 
										<td class="text-center">'.($search["cool_gear_pn1"]?$search["cool_gear_pn1"]:'-').'</td> 
										<td class="text-center">'.date("d/m/Y H:i:s",strtotime($search["date_modified"])).'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="4">No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';	
			
			$table .='<div id="tab12_4" class="tab-pane">
			          <table class="table table-striped table-hover dataTable no-footer" width="100%"  height="100%">';
						$table .= '<thead>
							  <tr> 
								<th style="background:#F9F9F9 !important;"  width="50%" class="text-center">Email Address</th> 
								<th  class="text-center"  width="25%">Login Time</th>  
								<th class="text-center"  width="25%">Logout Time</th>
							  </tr>
							</thead>
							<tbody>';
			if(count($user_history)>0)
				{
					foreach($user_history as $user) {
						
							
							if($user["login_time"] != "" && $user["login_time"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$in_time= date('d/m/Y H:i:s',strtotime($user["login_time"]));
							}
							else
							{
								$in_time = "-";
							}
							if($user["logout_time"] != "" && $user["logout_time"] != "0000-00-00 00:00:00"){
								//$last_activity_duration= $this->relativeTime(strtotime($user["last_activity"]));
								$out_time= date('d/m/Y H:i:s',strtotime($user["logout_time"]));
							}
							else
							{
								$out_time = "-";
							}
						
							$table .='<tr> 
										<td class="text-center" >'.($user["email_address"]?$user["email_address"]:'-').'</td> 
										<td class="text-center" >'.$in_time.'</td> 
										<td class="text-center" >'.$out_time.'</td>
									</tr>';
					}
				} 
				else{
					$table .='<tr><td class="text-center" colspan="4">No Records Available</td></tr>';
				}	
			$table .='</tbody></table></div>';
			
			$table .='</div></div></div>';
			
			echo json_encode(array('table' => $table)); exit;		
			}
			else{
				redirect("administration/getPagesListSK");
			}
		}
	}
	
	/*
	 * 
	 * 
	 * DENSO PHASE - II Start from here
	 * 
	 * */
	 
	
	public function getResourcesSK()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*
			 * Check permission for AMW And SKW 
			 * */
			 
			if($this->checkPermission() == 1){
				
				$data["country"] = $this->administration_model->getCountry();
				$data["categories"] = $this->administration_model->getAllVehicleCategory();
				$data["subcategories"] = $this->administration_model->getAllVehicleSubCategory();
				$this->load->view("administrator/resources_sk",$data);
			}
			else{
				redirect('administration/getPagesListSK');
			}
			
		}
	}
	
	public function getUsersList(){
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			
			$country_expl ="";
			if($postData['cid'] != ""){
				$country = $postData['cid'];
				$country_expl = explode(',',$country);
			}
			else{
				$country_expl = "";
			}
			
			if($postData["keyword"] != ""){
				$like = array("concat(first_name,' ',last_name)" => $postData["keyword"]);
			}
			else{
				$like = "";
			}
			
			 
			if(isset($postData['to_list']) && $postData['to_list'] != ""){
				$data_user1='';
						
						if(!empty($postData["to_list"])){ 
								foreach($postData["to_list"] as $data1):
										if($data1 != ""){
											$data_user1 .= $data1.',';
										}
								endforeach;
						}
			}
			else{
				$data_user1 = "";
			}
			$res = $this->administration_model->getUserByKeyword($like,$country_expl,$data_user1);
			$options = "";
			$options_to_list = "";
			
			if($postData['cid'] != ""){
					if(isset($postData['to_list']) && $postData['to_list'] != ""){
						$data_user='';
						
						if(!empty($postData["to_list"])){ 
								foreach($postData["to_list"] as $data1):
							
										$data_user .= $data1.',';
								endforeach;
						}
						 
						$res_to_list = $this->administration_model->getUserByUserToListId($data_user,$country);
						foreach($res_to_list as $user):
							$options_to_list .= "<option value='".$user['id']."'>".$user['first_name']." ".$user['last_name']."</option>";
						endforeach;
					}
					
					foreach($res as $user):
						$options .= "<option value='".$user['id']."'>".$user['first_name']." ".$user['last_name']."</option>";
					endforeach;
					//print_r($users);die;
					//$options_to_list1 = "<option selected value='1'>ABC</option>";
					echo json_encode(array("options" => $options,"to_list_options" => $options_to_list));exit;
				
			}
			else{
				if(isset($postData['to_list']) && $postData['to_list'] != ""){
					$data_user='';
					if($cid!=""){
						if(!empty($postData["to_list"])){ 
								foreach($postData["to_list"] as $data1):
							
										$data_user .= $data1.',';
								endforeach;
						} 
						
						$res_to_list = $this->administration_model->getUserByUserToListId($data_user,$country);
						foreach($res_to_list as $user):
							$options_to_list .= "<option value='".$user['id']."'>".$user['first_name']." ".$user['last_name']."</option>";
						endforeach;
					}
					
				}
				
				echo json_encode(array("options" => "","to_list_options" => $options_to_list));exit;
			}
			 
		}
	}
	
	
	
	
	public function getResourcesSKById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("res_sk_id" => $this->encrypt->decode($postData["id"]));
			$res = $this->administration_model->getResourceSKById($cond);
			//print_r($res);die;
			$options ="";
			//echo $res[0]['user_id'];
			if($res[0]['user_id'] != ""){
				$user_id = explode(',',$res[0]['user_id']);
				$i=0;
				$cnt = count($user_id);
				
				$options = "";
				
					foreach($user_id as $row):
					if($row != ""){
						if($i<$cnt){
							$cond = array("user_id" => $row);
							$res_user = $this->administration_model->getUserById($cond);
							if($res_user[0]["id"] != ""){
								$options .= "<option value='".$res_user[0]["id"]."'>".$res_user[0]["first_name"]." ".$res_user[0]["last_name"]."</option>";
							}
						}
					}
					$i++;
					endforeach;
				
			} 
			
			if($res[0]['country'] != ""){
				$country = $res[0]['country'];
				$country_expl = explode(',',$country);
			}
			else{
				$country_expl = "";
			}
			
			$data_user1 = '';
			if($res[0]['user_id'] != ""){
				$data_user1 .= $res[0]['user_id'].",";		
			}
			else{
				$data_user1 = "";
			}
			$like = array();
			$options_user ="";
			
			$res_user_list = $this->administration_model->getUserByKeyword($like,$country_expl,$data_user1);
			
			if($res_user_list){
				
				foreach($res_user_list as $user):
							$options_user .= "<option value='".$user['id']."'>".$user['first_name']." ".$user['last_name']."</option>";
				endforeach;
				
				echo json_encode(array("res"=>$res[0],"from_list" => $options_user, "options_user" => $options));exit;
				
			}
			
			echo json_encode(array("res"=>$res[0], "from_list" => "", "options_user" => $options));exit;
				 
		}
	}
	
	public function getResourcesSKDetailsById() // Get Single resources Sk
	{
		if(is_ajax_request())
		{ 
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("res_sk_id" => $this->encrypt->decode($postData["id"]));
			$res = $this->administration_model->getResourceSKById($cond);
			//print_r($res);die;
			$html_users = "";
			$html_country = "";
			if($res[0]['user_id'] != ""){
				$user_id = explode(',',$res[0]['user_id']);
				$i=0;
				$cnt = count($user_id); 
				foreach($user_id as $row):
				if($i<$cnt){
					$cond = array("user_id" => $row);
					$res_user = $this->administration_model->getUserById($cond);
					$html_users .= $res_user[0]["first_name"]." ".$res_user[0]["last_name"].", ";
				}
				$i++;
				endforeach;
				
			}
			
			if($res[0]['country'] != ""){
				$country_id = explode(',',$res[0]['country']);
				$i=0;
				$cnt = count($country_id); 
				foreach($country_id as $row):
				if($i<$cnt-1){
						$cond = array("country_id" => $row);
						$res_country = $this->administration_model->getCountryById($cond);
						$html_country .= $res_country[0]["name"].", ";
					}
				$i++;
				endforeach;
				
			}
			$file_html ="";
			if($res[0]['resources_sk_pdf'] != ""){
				$file_html .= "<a target='_blank' href='".base_url()."sk_files/".$res[0]['resources_sk_pdf'] ."'>".$res[0]['resources_sk_pdf']."</a>";
			}
			else{
				$file_html .= "No File available";
			}
			
			
			echo json_encode(array("file" => $file_html,"users" => $html_users,"country" => $html_country));exit;
				 
		}
	}
	
	
	public function list_resources_sk() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->administration_model->getResourcesSKCount($cond,$like);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("resources_sk_pdf");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "res_sk_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$resources = $this->administration_model->getResourcesSKPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th> 
								<th class="'.$css[0].'" width="20%" onclick="changePaginate(0,\'resources_sk_pdf\',\''.$corder[0].'\')">Resources PMK Files</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($resources)>0)
				{
						if(count($resources) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($resources as $resource) { 
							 
							 
							 
							//$arr[3] = $types["id"];
					$table .= '<tr id="row_'.$resource["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($resource["id"]).'" name="check" id="check" class="chk"></td>
								<td><a target="_blank" href="'.base_url().'sk_files/'.$resource["resources_sk_pdf"].'">'.$resource["resources_sk_pdf"].'</a></td> 
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="" data-option="'.$this->encrypt->encode($resource["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|
									<a title="View" alt="View" id="view" class="" data-option="'.$this->encrypt->encode($resource["id"]).'" href="javascript:void(0)">View Details</a>
								</td>
							  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($resources)==0)
						{
							$table .= '<tr  class="text-center"><td colspan="5">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getResorcesSKList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($resources),'paginate' => $paginate)); exit;
			}
		}
	} 
	
	/*
	 * Save Resources Sales Kit
	 * */
	 
	public function save_resources_sk() // Add/Edit Payment Type 
	{	
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["resourceId"] == "")
				{   
					$isValid = $this->validateResourceSK($postData,$_FILES);
					if($isValid["status"] == 1)
					{ 
						$cond = array();
						
						$resources = $this->administration_model->getAllResourcesSK($cond);
						$data["resources"] = $resources;
						
						$data_country = "";
						$data_user_ids = "";
						//print_r($postData["dd_country"]);
						
						if(!empty($postData["dd_country"])){ 
							foreach($postData["dd_country"] as $data1):
									$data_country .= $data1.',';
							endforeach;
						}
						
						/*if(!empty($postData["sel_users"])){ 
							$arr_sel_users = array_unique($postData["sel_users"]);
							foreach($arr_sel_users as $data_user):
									$data_user_ids .= $data_user.',';
							endforeach;
						}*/
						
						
						if(!empty($postData["sel_users"])){  
							$data_user_ids = $postData["sel_users"]; 
						}
						
						if($postData["text_pdf_file"] !=""){
							$filename = $this->skpdfpath."/".$postData["text_pdf_file"];
							if (!file_exists($filename)) {
									
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>The file '.$filename.' does not exists in destination folder.</div>')); exit;
							}
						}
						$insertId = $this->common_model->insert(TB_RESOURCES_SK,array("country" => $data_country, "user_id" => $data_user_ids, "date_modified" => date('Y-m-d H:i:s')));
						
						if($insertId)
						{ 
							
							$pdf_update  = array();
							$ai_update = array();
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								 
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$array_chars = array("~","^","!","!","#","$","%","^","&","*"," ");
								$act_pdf_name = str_replace($array_chars,"_",$isValid["0"]["act_pdf_file"]);
								$pdf_update = array("resources_sk_pdf"=>$act_pdf_name);
								
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/sk_files/'.$act_pdf_name.'">'.$act_pdf_name.'</a>';		
								$updateArr = $this->common_model->update(TB_RESOURCES_SK,array("res_sk_id" => $insertId),$pdf_update);
							}
							else if($postData["text_pdf_file"] !=""){
								$updateArr = $this->common_model->update(TB_RESOURCES_SK,array("res_sk_id" => $insertId),array("resources_sk_pdf" => $postData["text_pdf_file"]));
							}
							
							echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Resources Sales Kit has been added successfully...!!!</div>')); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
						} 
					}
					else
					{
						echo json_encode($isValid);exit;
					}  
				}
				else
				{ 
					$isValid = $this->validateResourceSK($postData,$_FILES);
					//print_r($isValid);
					 
					if($isValid["status"] == 1)
					{
						//print_r($postData);die;
						$cond1 = array("res_sk_id" => $this->encrypt->decode($postData["resourceId"])); 
						$resourcebyid = $this->administration_model->getResourceSKById($cond1); 
						$data["resources"] = $resourcebyid;  
						
						$data_country = "";
						$data_user_ids = "";
						if(!empty($postData["dd_country"])){ 
							foreach($postData["dd_country"] as $data1):
									$data_country .= $data1.',';
							endforeach;
						}
						
						/*if(!empty($postData["sel_users"])){ 
							$arr_sel_users = array_unique($postData["sel_users"]);
							foreach($arr_sel_users as $data_user):
									$data_user_ids .= $data_user.',';
							endforeach;
						}*/
						
						if(!empty($postData["sel_users"])){  
							$data_user_ids = $postData["sel_users"]; 
						}
						
						if($postData["text_pdf_file"] !=""){
							$filename = $postData["text_pdf_file"];
							$filepath = $this->skpdfpath."/".$postData["text_pdf_file"];
							if (!file_exists($filepath)) {
									
								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>The file '.$filename.' does not exists in destination folder.</div>')); exit;
							}
						}
						
						$arrEdit = array();
						//print_r($postData["chkId"]);die;
						$expl_id = explode(",",$postData["chkId"]);
						$cnt = count($expl_id);
						foreach($expl_id as $chkid):
							//$cond = array("res_sk_id" => $this->encrypt->decode($postData["ids"][$i])); 
							$this->common_model->update(TB_RESOURCES_SK,array("res_sk_id" => $this->encrypt->decode($chkid)),array("country" => $data_country, "user_id" => $data_user_ids));
						endforeach;
						//die;
						//echo $data_user_ids;die;
						$txt_update = array("user_type" => $postData["user_type"], "country" => $data_country, "user_id" => $data_user_ids, "date_modified" => date('Y-m-d H:i:s'));
						//print_r($txt_update);die;
						$pdf_update = array();
						if($postData["text_pdf_file"] !="" ){
							
							if($postData["text_pdf_file"] !=""){
								$pdf_update = array("resources_sk_pdf" => $postData["text_pdf_file"]);
								$act_pdf_name = $postData["text_pdf_file"];
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/pdf_files/'.$act_pdf_name.'">'.$act_pdf_name.'</a>';
							}
							
							
							$merge_update  = array_merge($pdf_update,$txt_update);
							$updateArr = $this->common_model->update(TB_RESOURCES_SK,array("res_sk_id" => $this->encrypt->decode($postData["resourceId"])),$merge_update);
						}
						elseif($_FILES){
							$this->validatefile->data(); 
							
							
							
							//echo $_FILES['pdf_file']['size'];
							if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != "" && $_FILES['pdf_file']['size']!=0){ 
								
								//@unlink($this->pdfpath.'/'.$resourcebyid["0"]["pdf_file"]);
								
								$pdf_file = $isValid["0"]["pdf_file"]; 
								$array_chars = array("~","^","!","!","#","$","%","^","&","*"," ");
								$act_pdf_name = str_replace($array_chars,"_",$isValid["0"]["act_pdf_file"]);
								$pdf_update = array("resources_sk_pdf" => $act_pdf_name);
								
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/sk_files/'.$act_pdf_name.'">'.$act_pdf_name.'</a>';		
							}
							else{
								$pdf_file_update  = '<a target="_blank" href="'.base_url().'/sk_files/'.$resourcebyid["0"]["resources_sk_pdf"].'">'.$resourcebyid["0"]["resources_sk_pdf"].'</a>';		
							}
							
							
							
						} 
						$merge_update  = array_merge($pdf_update,$txt_update);
							//print_r($merge_update);die;
						$updateArr = $this->common_model->update(TB_RESOURCES_SK,array("res_sk_id" => $this->encrypt->decode($postData["resourceId"])),$merge_update);
							
							if($updateArr)
							{
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["resourceId"].'"></td>
										
										<td>'.$pdf_file_update.'</td> 
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["resourceId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["resourceId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Resources Sales Kit has been updated successfully...!!!</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>You didn\'t make any change.</div>')); exit;
							}  
					}
					else
					{
						echo json_encode($isValid);exit;
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	} 
	
	public function validateResourceSK($postData,$FILES)
	{   
		$_FILES = $FILES;
		
		if($_FILES!=""){
			$bool = false;
			$actual_pdf_file = array();
			$actual_ai_file = array();
			$merge_update = array();
			$cond1 = array("res_sk_id" => $this->encrypt->decode($postData["resourceId"])); 
			$resourcebyid = $this->administration_model->getResourceSKById($cond1);
			if(isset($_FILES['pdf_file']['name']) && $_FILES['pdf_file']['name'] != ""){
				//echo $_FILES['pdf_file']['size'];die;
				if($_FILES['pdf_file']['size']>0){
				
				$ext = pathinfo($_FILES['pdf_file']['name'], PATHINFO_EXTENSION); 
				$pdf = 'pdf_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$array_chars = array("~","^","!","!","#","$","%","^","&","*"," ");
				$pdf_name = str_replace($array_chars,"_",$_FILES['pdf_file']['name']); 
				//die;
				//echo $fname;die;
				$actual_pdf_file = $_FILES['pdf_file']['name'];
				
				$config = array( 
					'allowed_types' => 'pdf|PDF|xls|XLS|xlsx|XLSX|ppt|PPT|doc|DOC|docx|DOCX|zip|ZIP',
					'upload_path'   => $this->skpdfpath,
					'file_name'		=> $pdf_name,
					'max_size'      => 500000
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('pdf_file');
				$actual_pdf_file = array("act_pdf_file" => $actual_pdf_file,"pdf_file" => $pdf);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>'.$pdf_name.':'.$this->validatefile->display_errors().'</div>');exit;
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					$bool = true;
					@unlink($this->skpdfpath.'/'.$resourcebyid["0"]["resources_sk_pdf"]);
					$this->validatefile->do_upload('pdf_file');
					$upload_data  = $this->validatefile->data();
					$actual_pdf_file = array("act_pdf_file" => $upload_data["file_name"],"pdf_file" => $pdf);
				}
			}
			else{
				return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>The blank file is now allowed to upload</div>');exit;
			}
		}
		
			$merge_update = array_merge($actual_pdf_file,$actual_ai_file);
			//print_r($data);die;
			//print_r($merge_update);die;
			if($bool){ 
				return array("status" => 1,$merge_update);
			}
		}
		
		/*if(!ctype_alpha($postData["txt_product_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This fields require character only.</div>');
		}*/
		 
		return array("status" => 1);
	}
	
	
	
	public function delete_resources_sk() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("res_sk_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_RESOURCES_SK,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	/*
	 * 
	 * Get pages 
	 * 
	 * */
	
	/*
	 * Save Resources Sales Kit
	 * */
	 
	public function save_pages_sk() // Add/Edit Payment Type 
	{	
			
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				$postData['txt_description'] = urldecode($postData['txt_description1']);
				//print_r($postData['txt_description']);die;
				if($postData["pageId"] == "")
				{   
					$pgid = $postData["pgId"];
					$isValid = $this->validatePagesSK($postData);
					//$postData['txt_description'] = urldecode($postData['txt_description1']);
					if($isValid["status"] == 1)
					{ 
						//$cond = array("section_name" => $postData['sel_section'],"lang_id" => $postData['sel_lang'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat']);
						
						//$pages = $this->administration_model->getAllPagesSK($cond);
						$cond_cat = array("v_cat_id" => $postData['sel_cat']);
						$check_cat = $this->administration_model->getAllVehicleCategory($cond_cat);
						//echo count($check_cat);die;
						$this->session->set_userdata('page1','');
						if(count($check_cat)>0){
							if($postData['sel_subcat'] !="" && $postData['sel_subcat'] != NULL){
								$cond = array("section_name" => $postData['sel_section'],"lang_id" => $postData['sel_lang'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat']);
								$pages = $this->administration_model->getAllPagesSK($cond);
							}
							else{
								$cond = array("section_name" => $postData['sel_section'],"lang_id" => $postData['sel_lang'],"category" => $postData['sel_cat']);
								$pages = $this->administration_model->getAllPagesSubCatSK($cond);
							}
							
							//echo count($pages);die;						
							$data["resources"] = $resources;
							
							
							if(count($pages)==0){
								$sub_cat = ($postData["sel_subcat"]?$postData["sel_subcat"]:NULL);
								$insertId = $this->common_model->insert(TB_PAGES,array("lang_id" => $postData["sel_lang"],"section_name" => $postData["sel_section"],"category"=>$postData["sel_cat"],"sub_category"=>$sub_cat,"page_description"=>trim($postData["txt_description"])));
								
								if($insertId)
								{ 
									$this->session->set_flashdata("message",'Translated Section has been added successfully.');
									//echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Page has been added successfully...!!!</div>')); exit;
									redirect("administration/getPagesSK?pgid=".$pgid."&mode=add");
								}
								else
								{
									$this->session->set_flashdata("message_error",'Opps, there is problem please try again.');
									//echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
									redirect("administration/getPagesSK?pgid=".$pgid."&mode=add");
								} 
							}
							else{
									$this->session->set_flashdata("message_error",'This translated section is already exists.Please try another section name.');
									//echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
									$tempdata = array("message_error"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This translated section is already exists.Please try another section name.</div>',"lang_id" => $postData["sel_lang"],"page_description"=>trim($postData["txt_description"]));
									//$this->session->set_userdata('page1',$data);
									//$this->session->set_tempdata($tempdata,NULL);
									//print_r($this->session->userdata('page'));die;
									//redirect("administration/getPagesSK?pgid=".$pgid."&mode=add"); 
									$this->load->view("administrator/pages",$data);
									//print_r($data);die;
									
							}
						}
						else{
							$this->session->set_flashdata("message_error",'This category does not exists in database.');
							$data = array("message_error"=>'<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>This category does not exists in database.</div>',"section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat'],"page_data"=>$postData["txt_description"],"resources_id" => $data_files,"public_access" => $chk_public);
							$this->session->set_userdata('page',$data);
							redirect("administration/getPagesSK?pgid=".$pgid."&mode=add");
						
						}
					}
					else
					{
						$this->session->set_flashdata("message",$isValid);
						//echo json_encode($isValid);exit;
						redirect("administration/getPagesSK");
					}  
				}
				else
				{ 
					$isValid = $this->validatePagesSK($postData);
					//print_r($isValid);
					if($isValid["status"] == 1)
					{
						//print_r($postData);die;
						
						$cond_cat = array("v_cat_id" => $postData['sel_cat']);
						$check_cat = $this->administration_model->getAllVehicleCategory($cond_cat);
						//echo count($check_cat);die;
						if(count($check_cat)>0){
						
						//$cond = array("page_id !="  => $postData["pageId"],"lang_id" => $postData['sel_lang'],"section_name" => $postData['sel_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat']);
						//$pages = $this->administration_model->getAllPagesSK($cond);
						
						
							if($postData['sel_subcat'] !="" && $postData['sel_subcat'] != NULL){
								$cond = array("page_id !="  => $postData["pageId"],"lang_id" => $postData['sel_lang'],"section_name" => $postData['sel_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat']);
								$pages = $this->administration_model->getAllPagesSK($cond);
							}
							else{
								$cond = array("page_id !="  => $postData["pageId"],"lang_id" => $postData['sel_lang'],"section_name" => $postData['sel_section'],"category" => $postData['sel_cat']);
								$pages = $this->administration_model->getAllPagesSubCatSK($cond);
							}
						
						//echo count($pages);die;
						
						$data_files = ""; 
						//print_r($postData["resources_sk_files"]);
							if(count($pages)==0){
								if(!empty($postData["resources_sk_files"])){ 
									foreach($postData["resources_sk_files"] as $data1):
								
											$data_files .= $data1.',';
									endforeach;
								}
								//echo $data_user_ids;die;
								$sub_cat = ($postData["sel_subcat"]?$postData["sel_subcat"]:NULL);
								$txt_update = array("lang_id" => $postData["sel_lang"],"section_name" => $postData["sel_section"],"category" => $postData["sel_cat"],"sub_category"=>$sub_cat ,"page_description"=>trim($postData["txt_description"]));
								//print_r($txt_update);die;
								$updateArr = $this->common_model->update(TB_PAGES,array("page_id" => $postData["pageId"]),$txt_update);
								if($updateArr)
								{
									
									if($postData["sel_section"] !=""){
										$txt_update_sk = array("section_name" => $postData["sel_section"]);
										$this->common_model->update(TB_PAGES,array("section_name" => $postData["sel_section"]),$txt_update_sk);
									}
									
									
									$this->session->set_flashdata("message",'Translated section has been updated successfully.');
									redirect("administration/getPagesSK?pid=".$postData["pageId"]);
								}
								else
								{
									$this->session->set_flashdata("message",'Translated section has been updated successfully.');
									redirect("administration/getPagesSK?pid=".$postData["pageId"]);
								}  
							}
							else{
									$this->session->set_flashdata("message_error",'This translated section is already exists.Please try another section name.');
									redirect("administration/getPagesSK?pid=".$postData["pageId"]);
							}
						}
						else{
							$this->session->set_flashdata("message_error",'This category does not exists in database.');
									 
							redirect("administration/getPagesSK?pid=".$postData["pageId"]);
						}
					}
					else
					{
						$this->session->set_flashdata("message",$isValid);
						//echo json_encode($isValid);exit;
						redirect("administration/getPagesSK");
					}
					
				}
			}
			else
			{
				//echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			} 
	}  

	

	public function getCategoryBySecId() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				$cond = array();
				$cond= array('s_id'=>$postData['sec_id']);
				// pr($postData);
				// die();
			$sectionCat = $this->administration_model->getCategoryBySecId($cond);
			echo json_encode($sectionCat[0]);exit;	
			}
		}
	}
	 
	public function list_pages() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				//pr($postData);
				$cond = array();
				$like = array();
				$join = array();
				//$join = array(TB_CATEGORY => TB_PAGES.".category = ".TB_CATEGORY.".v_cat_id",TB_SUBCATEGORY => TB_PAGES.".sub_category = ".TB_SUBCATEGORY.".v_sub_cat_id",TB_LANG => TB_PAGES.".lang_id = ".TB_LANG.".lang_id");

				if(count($postData["searchBy"]) >= 0 )
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
						// if($postData["searchBy"][$x] != "")
						// {
						// 	$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						// } 
						if($postData["searchBy"][$x] == "cat_id")
						{
							$like['cat_id'] = trim($postData["cat_id"]);
						}
						if($postData["searchBy"][$x] == "section_id")
						{
							$like['section_id'] = trim($postData["section_id"]);
						}
					}
				} 
				// pr($like);
				// die();
				$count = $this->administration_model->getPagesSKCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("P.category","P.sub_category","P.section_name","P.lang_id");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "page_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$userdata = $this->session->userdata('auth_user');	
				$cond_user = array("email_address" => $userdata['username']);
				$user = $this->administration_model->getUserById($cond_user);
				
				if($user[0]['lang_translator'] == '1'){
					$translator_access = 1;
				}
				else{
					$translator_access = 0;
				}

				$cond=array('P.lang_id'=>'1');
				
				$resources = $this->administration_model->getPagesSKPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
								<th class="'.$css[2].'" width="25%" onclick="changePaginate(0,\'name\',\''.$corder[2].'\')">Section</th>
								
								<th class="'.$css[0].'" width="20%" onclick="changePaginate(0,\'category\',\''.$corder[0].'\')">Category</th>
								<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'sub_category\',\''.$corder[1].'\')">Sub Category</th>
								
								<th class="'.$css[3].'" width="10%" onclick="changePaginate(0,\'lang_id\',\''.$corder[3].'\')">Languages</th>
								<th class="text-center" width="20%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($resources)>0)
				{  
						if(count($resources) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($resources as $resource) { 
							 
						$table .= '<tr id="row_'.$resource["page_id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$resource["page_id"].'" name="check" id="check" class="chk"></td>
								<td>'.ucfirst($resource["name"]).'</td> 
								<td>'.ucfirst($resource["category"]).'</td> 
								<td>'.($resource["sub_category"]?ucfirst($resource["sub_category"]):'NA').'</td> 
								<td>'.ucfirst($resource["language"]).'</td>
								<td class="text-center">';
									if($resource["language"] == 'English'){
										if($translator_access == '0'){
											$table .='<a title="Edit English Version" alt="Edit English Version" id="edit" class="editPayment" data-option="'.$resource["page_id"].'" href="'.base_url().'administration/getPagesDataSK?pid='.$resource["page_id"].'">Edit</a>|<a title="Other Languages" alt="Other Languages" id="view_question" class="editPayment" data-option="'.$resource["id"].'" href="http://103.224.243.154/denso/administration/getOtherLanguages/'.$resource["section_id"].'"> Other Languages </a>';
										}
										// $table .='<a title="Add Translation" alt="Add Translation" id="edit" class="editPayment" data-option="'.$resource["id"].'" href="'.base_url().'administration/   getPagesSK?pgid='.$resource["page_id"].'&mode=add">View|</a>';

										// $table .='<a title="Add Translation" alt="Add Translation" id="edit" class="editPayment" data-option="'.$resource["id"].'" href="'.base_url().'administration/getPagesSK?pgid='.$resource["page_id"].'&mode=add">Other Languages</a>';
									}
									else{
										$table .='<a title="Edit Translation" alt="Edit Langauage" id="edit" class="editPayment" data-option="'.$resource["id"].'" href="'.base_url().'administration/getPagesSK?pid='.$resource["page_id"].'&mode=edit">Edit</a>';
									}
								$table .='</td>
							  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($resources)==0)
						{
							$table .= '<tr class="text-center"><td  colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getResorcesSKList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($resources),'paginate' => $paginate)); exit;
			}
		}
	} 
	 
	public function getPagesSK()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$cond = array();
			$data['message'] = $this->session->flashdata('message'); 
			$data['page'] = $this->session->userdata('page1');
			///echo "test";
			//print_r($this->session->userdata('page1'));die;
			
			$data['message_error'] = $this->session->flashdata('message_error');
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);
			$cond_lang = array("lang_id !=" => '1');
			$data['langauages'] = $this->administration_model->getLanguages($cond_lang);
			$data['res_sk'] = $this->administration_model->getResourceSKById($cond);
			if((isset($_GET["pid"]) && $_GET["pid"]!="") || (isset($_GET["pgid"]) && $_GET["pgid"]!="")){
				if($_GET["pgid"] != "" ){
					$cond1 = array("page_id" => $_GET["pgid"]);
				}
				else{
					$cond1 = array("page_id" => $_GET["pid"]);
				}
				$data['res'] = $this->administration_model->getAllPagesSK($cond1);
				
				$cond_cat = array("v_cat_id" => $data['res'][0]['category']);
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond_cat);
				$cond_sub_cat = array("v_sub_cat_id" => $data['res'][0]['sub_category']);
				$data["sub_categories"] = $this->administration_model->getAllVehicleSubCategory($cond_sub_cat); 
				$data['sections']=$this->administration_model->getAllSections();

				//print_r($data['categories']);
				//print_r($data['sub_categories']);
				//die;
			} 
			 
			$this->load->view("administrator/pages",$data);
			
		}
	}
	
	public function getPagesListSK()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$cond = array();
			$cond_cat = array("status_pmk"=>"Active");
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond_cat);
			$userdata = $this->session->userdata('auth_user');
			
			$cond_user = array("email_address" => $userdata['username']);
			$user = $this->administration_model->getUserById($cond_user);
			
			//echo $user[0]['lang_translator'];die;
			if($user[0]['lang_translator'] == '1'){
				$data['translator_access'] = '1';
			}
			else{
				$data['translator_access'] = '0';
			}
			
			$cond_lang = array();
			$data['langauages'] = $this->administration_model->getLanguages($cond_lang);
			//print_r($data['langauages']);die;
			$data['res_sk'] = $this->administration_model->getResourceSKById($cond);
			$data['sections']=$this->administration_model->getAllSections();
			$this->load->view("administrator/pages_list",$data);
			//$this->load->view("administrator/v_pages_list",$data);
		}
	}
	 
	
	function getSubCategoryByCatId(){
		if(is_ajax_request())
		{
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("v_cat_id" => $postData["v_cat_id"]);
			$res = $this->administration_model->getAllVehicleSubCategory($cond);
			//print_r($res);die;
			$html ="";
			if(count($res)>0){
				$html .='<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Sub Category<span class="validationerror">*</span></label></label>
						 <div class="col-lg-8">';
				$html .='<select name="sel_subcat" id="sel_subcat" class="select-sm arrow_new form-control">
					<option value="">Select Sub Category</option>
				';
				foreach($res as $row):
					$selected = "";
					if($postData["sub_cat_id"] == $row['id']){
						$selected = "selected='selected'";
					}
					$html .= "<option $selected value='".$row['id']."'>".$row['sub_category']."</option>";
				endforeach;
				$html .='</select><span id="selsubCatInfo" class="text-danger"></span></div>';
			}
			echo json_encode(array("res" => $html));exit;
		}
	}
	
	function getSectionsDataByCatId(){
		
		$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("v_cat_id" => $postData["catid"]);
			$res = $this->administration_model->getAllVehicleSubCategory($cond);
			//print_r($res);die;
			$html ="";
			if(count($res)>0){
				$html .='<label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Sub Category</label></label>
						 <div class="col-lg-4">';
				$html .='<select name="sel_subcat" id="sel_subcat" class="select-sm arrow_new form-control">
					<option value="">Select sub Category</option>
				';
				foreach($res as $row):
					$selected = "";
					if($postData["sub_cat_id"] == $row['id']){
						$selected = "selected='selected'";
					}
					$html .= "<option $selected value='".$row['id']."'>".$row['sub_category']."</option>";
				endforeach;
				$html .='</select></div>';
			}
			
			
						
			$cond = array("category" => $postData["catid"]);
			$res = $this->administration_model->getAllPagesSK($cond);
			//print_r($res);die;
			$html_section ="";
			
			if(count($res)>0){
				$html_section .='<label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Section Name</label><span class="validationerror">*</span></label>
						 <div class="col-lg-4">';
				$html_section .='<select name="sel_section" id="sel_section" class="select-sm arrow_new form-control">
					<option value="">Select Section</option>
				';
				foreach($res as $row):
					$selected = "";
					if($postData["section_name"] == $row['section_name']){
						$selected = "selected='selected'";
					}
					$html_section .= "<option $selected value='".$row['section_name']."'>".$row['section_name']."</option>";
					
				endforeach;
				$html_section .='</select><span id="selSectionInfo" class="text-danger"></div>';
			}
			echo json_encode(array("res" => $html,"res_section" => $html_section));exit;
	}
	
	
	function getSectionsDataBySubCatId(){
		
		$postData = $this->input->post(); 
			//print_r($postData);die;
			
			$cond = array("category" => $postData["sub_cat_id"]);
			$res = $this->administration_model->getAllPagesSK($cond);
			//print_r($res);die;
			$html_section ="";
			
			if(count($res)>0){
				$html_section .='<label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first"><label>Select Section Name</label><span class="validationerror">*</span></label>
						 <div class="col-lg-4">';
				$html_section .='<select name="sel_section" id="sel_section" class="select-sm arrow_new form-control">
					<option value="">Select Section</option>
				';
				foreach($res as $row):
					$selected = "";
					if($postData["section_name"] == $row['section_name']){
						$selected = "selected='selected'";
					}
					$html_section .= "<option $selected value='".$row['section_name']."'>".$row['section_name']."</option>";
					
				endforeach;
				$html_section .='</select><span id="selSectionInfo" class="text-danger"></div>';
			}
			echo json_encode(array("res_section" => $html_section));exit;
	}
	
	
	function getSectionsPageDataBySectionId(){
		if(is_ajax_request()){
			$postData = $this->input->post();
			$sub_cat = ($postData['sel_subcat']?$postData['sel_subcat']:NULL);
			$cond = array("section_name" => $postData["sectionid"], "category" => $postData['sel_cat'],"sub_category" => $sub_cat);
				$res = $this->administration_model->getAllPagesSK($cond); 
				//print_r($res);die;
				$html_data ="";
				
				if(count($res)>0){
					
					$html_data .= $res[0]['page_data'];
				}
				echo json_encode(array("res_data" => $html_data));exit;
		}
	}
	
	public function validatePagesSK($postData)
	{   
		
		return array("status" => 1);
	}
	
	public function delete_pages_sk() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond_check = array("page_id" => $postData["ids"][$i],"lang_id"=>1);

					$pages = $this->administration_model->checkPagesSectionLangauages($cond_check);
					if(count($pages)>0){
					
						foreach($pages as $row):
							$cond_row = array("section_id" => $row["section_id"]);
							$isdelete = $this->common_model->delete(TB_PAGES,$cond_row);

							// echo $this->db->last_query();
							// exit;
							$arrDelete[] = $row['page_id'];
						endforeach;	
					}
					
					$cond = array("page_id" => $postData["ids"][$i]); 
					$isdelete = $this->common_model->delete(TB_PAGES,$cond);
					
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("status" => 1,"ids" => $arrDelete,"msg"=>"Record (s) deleted successfully."));exit;
			}
		}
	}
	
	public function getPagesKById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			
			//print_r($postData);die;
			$cond = array("page_id" => $_GET["pid"]);
			$res = $this->administration_model->getResourceSKById($cond);
			//print_r($res);die;
			
			echo json_encode(array("res"=>$res[0],"options_user"=>$options));exit;
				 
		}
	}

	
	
	/*
	 * 
	 * get English version data that should see at the time of Actual Page creation
	 * 
	 * */

	//function to save other language
	public function save_otherLanguages() // Add/Edit Payment Type 
	{	
		if(is_user_logged_in())
		{
			$postData = $this->input->post();
			//pr($postData);die;
			$postData['txt_description'] = urldecode($postData['txt_description1']);

			if($postData["pageId"] == "")
			{   
				$isValid = $this->validatePagesDataSK($postData);
				if($isValid["status"] == 1)
				{ 
					$cond_sec = array("section_id" => $postData['select_section'],"lang_id"=>$postData['select_language']);
					$check_sec = $this->administration_model->checkOtherLanguageIsExist($cond_sec);
					//pr($check_sec);
					
				if(count($check_sec)==0){   //if section is not added
					$data_files = ""; 
					$chk_public = (isset($postData["chk_public"])?$postData["chk_public"]:'0');
	  			    $chk_offline = (isset($postData["chk_offline"])?$postData["chk_offline"]:'0');
						
					$insertId = $this->common_model->insert(TB_PAGES,array("lang_id"=>$postData['select_language'],"page_data"=>trim($postData["txt_description"]),"section_id"=>$postData['section_id'],"page_description"=>trim($postData["txt_description"]),"public_access" => $chk_public,"offline_reading" => $chk_offline));
		
					if($insertId)
					{ 
						echo json_encode(array("status" => 1,"action" => "add","msg" =>'<div class="alert alert-success">Page has been added successfully...!!!</div>')); exit;
						//redirect("administration/getPagesDataSK");
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "add", "msg" =>'<div class="alert alert-danger">Opps, there is problem please try again.</div>')); exit;
						//redirect("administration/getPagesDataSK");
					} 
				}else{
					//$this->session->set_flashdata("message_error",'This page is already exists.Please try another section name.');
					echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger">This page is already exists.Please try another section name.</div>')); exit;
					
					//redirect("administration/getPagesDataSK");
				}
				}
			}
			else{

				$isValid = $this->validatePagesDataSK($postData);
				if($isValid["status"] == 1)
				{ 
					$cond_sec = array("section_id" => $postData['section_id'],"lang_id"=>$postData['lang_id']);
					$check_sec = $this->administration_model->checkOtherLanguageIsExist($cond_sec);
					//pr($postData);

					if(count($check_sec)==1){  
						$chk_public = (isset($postData["chk_public"])?$postData["chk_public"]:'0');
		  			    $chk_offline = (isset($postData["chk_offline"])?$postData["chk_offline"]:'0');
				  	$cond=array("page_id"=>$postData['pageId']);

					$updateArr = $this->common_model->update_data(TB_PAGES,$cond,array(
						"lang_id"=>$postData["lang_id"],
						"page_data"=>trim($postData["txt_description"]),
						"section_id" =>$postData['section_id'],
						"page_description"=>trim($postData["txt_description1"]),
						"public_access" => $chk_public,
						"offline_reading"=>$chk_offline)); 
					// echo $this->db->last_query();
					// die();
					if($updateArr)
					{ 
						echo json_encode(array("status" => 1,"action" => "add","msg" =>'<div class="alert alert-success">Record has been updated successfully</div>')); exit;
						//redirect("administration/getPagesDataSK");
					}
					else
					{
						echo json_encode(array("status" => 2,"action" => "add", "msg" =>'<div class="alert alert-danger">Opps, there is problem please try again.</div>')); exit;
						//redirect("administration/getPagesDataSK");
					} 
				}else{

						echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger">This page is already exists.Please try another section name.</div>')); exit;
						
						//redirect("administration/getPagesDataSK");
					}
				}
			}//end edit
		}
	}
	
	//added by pallavi for english 
	public function save_pages_data_sk() // Add/Edit Payment Type 
	{	
		if(is_user_logged_in())
		{
			$postData = $this->input->post();
			//print_r($postData);die;
			$postData['txt_description'] = urldecode($postData['txt_description1']);
			//echo htmlentities($this->input->post("txt_description"));die;
			
			if($postData["pageId"] == "")
			{   
				$isValid = $this->validatePagesDataSK($postData);
				if($isValid["status"] == 1)
				{ 
					//pr($postData);
					$cond_sec = array("section_id" => $postData['sel_section'],"lang_id"=>"1");
					$check_sec = $this->administration_model->checkSectionIsExist($cond_sec);
					
				if(count($check_sec)==0){   //if section is not added
					
					$data_files = ""; 
					if(!empty($postData["resources_sk_files"])){ 
						foreach($postData["resources_sk_files"] as $data1):
								$data_files .= $data1.',';
						endforeach;
				    }

					$chk_public = (isset($postData["chk_public"])?$postData["chk_public"]:'0');
	  				$chk_offline = (isset($postData["chk_offline"])?$postData["chk_offline"]:'0');
						$insertId = $this->common_model->insert(TB_PAGES,array("lang_id"=>'1',"page_data"=>trim($postData["txt_description"]),"section_id"=>$postData['sel_section'],"resources_id" => $data_files,"public_access" => $chk_public,"offline_reading" => $chk_offline));

					if($insertId)
					{ 
					   $this->session->set_flashdata("message",'Record has been added successfully.');
						redirect("administration/getPagesDataSK");
						//redirect("administration/getPagesListSK");
						
					}
					else
					{
						$this->session->set_flashdata("message_error",'Opps, there is problem please try again.');
						redirect("administration/getPagesDataSK");
					} 
				}else{
					$this->session->set_flashdata("message_error",'This page is already exists.Please try another section name.');
					//$data = array("section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat'],"page_data"=>$postData["txt_description"],"resources_id" => $data_files,"public_access" => $chk_public);
					//print_r($data);die;
					//$this->session->set_flashdata('page',$data);
					redirect("administration/getPagesDataSK");
				}
				}
			}
			else{  // for edit 
				
				$isValid = $this->validatePagesDataSK($postData);
				//print_r($postData);
				if($isValid["status"] == 1)
				{
					//$cond = array("page_id" => $postData['pageId']);
					$cond = array("page_id =" => $postData['pageId']);
					$cond_sec = array("section_id =" => $postData['section_id'],"page_id ="=> $postData["pageId"]);
				    $check_sec = $this->administration_model->checkPageSectionIsExist($cond_sec);

				   // print_r($check_sec);die;
				    if(count($check_sec)==1){ 
						if(!empty($postData["resources_sk_files"])){ 
								foreach($postData["resources_sk_files"] as $data1):
							
										$data_files .= $data1.',';
								endforeach;
						    }
						$chk_public = (isset($postData["chk_public"])?$postData["chk_public"]:'0');
		  			    $chk_offline = (isset($postData["chk_offline"])?$postData["chk_offline"]:'0');
						$updateArr = $this->common_model->update_data(TB_PAGES,$cond,array("lang_id" =>'1', "page_data" => trim($postData["txt_description"]),"section_id" => $postData['section_id'],"resources_id" => $data_files,"public_access" => $chk_public,"offline_reading"=>$chk_offline));
						// echo $this->db->last_query();
						// die();
						if($updateArr)
						{   
							$this->session->set_flashdata("message",'Record has been updated successfully.');
							redirect("administration/getPagesDataSK");
						}
						else
						{
							$this->session->set_flashdata("message_error",'Opps, there is problem please try again.');
							redirect("administration/getPagesDataSK");
						} 
					}else{
						$this->session->set_flashdata("message_error",'This page is already exists.Please try another section name.');
					   redirect("administration/getPagesDataSK");
					}
				}
				else
				{
					$this->session->set_flashdata("message",$isValid);
					//echo json_encode($isValid);exit;
					redirect("administration/getPagesDataSK");
				}
			}

		}
	}

	
	 
	 // renamed by pallavi
	public function save_pages_data_sk_old() // Add/Edit Payment Type 
	{	
	
		if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//print_r($postData);die;
				$postData['txt_description'] = urldecode($postData['txt_description1']);
				//echo htmlentities($this->input->post("txt_description"));die;
				
				if($postData["pageId"] == "")
				{   
					$isValid = $this->validatePagesDataSK($postData);
					if($isValid["status"] == 1)
					{ 
						$cond_cat = array("v_cat_id" => $postData['sel_cat']);
						$check_cat = $this->administration_model->getAllVehicleCategory($cond_cat);
						//echo count($check_cat);die;
						if(count($check_cat)>0){
							if($postData['sel_subcat'] !="" && $postData['sel_subcat'] != NULL){
								$cond = array("section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat']);
								$pages = $this->administration_model->getAllPagesSK($cond);
							}
							else{
								$cond = array("section_name" => $postData['txt_section'],"category" => $postData['sel_cat']);
								$pages = $this->administration_model->getAllPagesSubCatSK($cond);
							}
							//echo count($pages);die;
							$data["resources"] = $resources;
							$data_files = ""; 
							//print_r($postData["resources_sk_files"]);
							
							if(!empty($postData["resources_sk_files"])){ 
								foreach($postData["resources_sk_files"] as $data1):
							
										$data_files .= $data1.',';
								endforeach;
							}
							if(count($pages)==0){
							
								$sub_cat = ($postData["sel_subcat"]?$postData["sel_subcat"]:NULL);
								$chk_public = (isset($postData["chk_public"])?$postData["chk_public"]:'0');
								$chk_offline = (isset($postData["chk_offline"])?$postData["chk_offline"]:'0');
								$insertId = $this->common_model->insert(TB_PAGES,array("lang_id"=>'1',"section_name"=>$postData["txt_section"],"category"=>$postData["sel_cat"],"sub_category"=>$sub_cat,"page_data"=>trim($postData["txt_description"]),"resources_id" => $data_files,"public_access" => $chk_public,"offline_reading" => $chk_offline));
								
								if($insertId)
								{ 
									$this->session->set_flashdata("message",'Section has been added successfully.');
									//echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">�</button>Page has been added successfully...!!!</div>')); exit;
									redirect("administration/getPagesDataSK");
								}
								else
								{
									$this->session->set_flashdata("message_error",'Opps, there is problem please try again.');
									//echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
									redirect("administration/getPagesDataSK");
								} 
							}
							else{
									$this->session->set_flashdata("message_error",'This page is already exists.Please try another section name.');
									//echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
									//print_r($postData["txt_description"]);die;
									$data = array("section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat'],"page_data"=>$postData["txt_description"],"resources_id" => $data_files,"public_access" => $chk_public);
									//print_r($data);die;
									$this->session->set_flashdata('page',$data);
									redirect("administration/getPagesDataSK");
							}
						}
						else{
							$this->session->set_flashdata("message_error",'This category does not exists in database.');
									//echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
									//print_r($postData["txt_description"]);die;
									$data = array("section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat'],"page_data"=>$postData["txt_description"],"resources_id" => $data_files,"public_access" => $chk_public);
									//print_r($data);die;
									$this->session->set_flashdata('page',$data);
									redirect("administration/getPagesDataSK");
						}
					}
					else
					{
						$this->session->set_flashdata("message",$isValid);
						//echo json_encode($isValid);exit;
						redirect("administration/getPagesDataSK");
					}  
				}
				else
				{ 
					$isValid = $this->validatePagesDataSK($postData);
					//print_r($postData);die;
					if($isValid["status"] == 1)
					{
						//print_r($postData);die; 
						
						$cond_cat = array("v_cat_id" => $postData['sel_cat']);
						$check_cat = $this->administration_model->getAllVehicleCategory($cond_cat);
						//echo count($check_cat);die;
						if(count($check_cat)>0){
							$cond = array("page_id !="  => $postData["pageId"],"lang_id" => "1" ,"section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat']);
							$pages = $this->administration_model->getAllPagesSK($cond);
							//print_r($pages);die;
							$data_files = ""; 
							//print_r($postData["resources_sk_files"]);
							
							if(!empty($postData["resources_sk_files"])){ 
								foreach($postData["resources_sk_files"] as $data1):
							
										$data_files .= $data1.',';
								endforeach;
							} 
							//print_r($data_files);die;
							//echo count($pages);die;
							
							if(count($pages)==0){
								//print_r($data_files);die; 
								//echo $data_user_ids;die;
								//echo $postData["pageId"];die;
								$cond = array("page_id"  => $postData["pageId"]);
								$pages = $this->administration_model->getAllPagesSK($cond);
								//
								//$chk_access = (isset($postData["public_access"])?'1':'0');
								//die;
								$sub_cat = ($postData["sel_subcat"]?$postData["sel_subcat"]:NULL);
								$chk_offline = (isset($postData["chk_offline"])?$postData["chk_offline"]:'0');
								$chk_public = (isset($postData["chk_public"])?$postData["chk_public"]:'0');
								
								$txt_update = array("lang_id"=>'1',"section_name" => $postData["txt_section"],"category" => $postData["sel_cat"],"sub_category"=>$sub_cat,"page_data"=>trim($postData["txt_description"]),"resources_id" => $data_files,"public_access" => $chk_public,"offline_reading" => $chk_offline);
								$updateArr = $this->common_model->update(TB_PAGES,array("page_id" => $postData["pageId"]),$txt_update);
								if($postData["txt_section"] !=""){
										$txt_update_sk = array("section_name" => $postData["txt_section"],"category" => $postData["sel_cat"],"sub_category" => $sub_cat);
										//print_r($txt_update_sk);die;
										$this->common_model->update(TB_PAGES,array("section_name" => $pages[0]['section_name'],"category" => $pages[0]["category"],"sub_category" =>$pages[0]["sub_category"]),$txt_update_sk);
										//echo $this->db->last_query();die;
								}
								//die;
								if($updateArr)
								{
									$this->session->set_flashdata("message",'Section has been updated successfully.');
									//echo "Success";die;
									redirect("administration/getPagesDataSK?pid=".$postData["pageId"]);
								}
								else
								{
									$this->session->set_flashdata('message','Page has been updated successfully.');
									//echo "Success1";die;
									redirect("administration/getPagesDataSK?pid=".$postData["pageId"]);
									//redirect("administration/getPagesDataSK");
								}  
							}
							else{
									$this->session->set_flashdata("message_error",'This page is already exists.Please try another section name.');
									redirect("administration/getPagesDataSK?pid=".$postData["pageId"]);
							}
						}
						else{
							$this->session->set_flashdata("message_error",'This category does not exists in database.');
									//echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">�</button>Opps, there is problem please try again.</div>')); exit;
									//print_r($postData["txt_description"]);die;
									$data = array("section_name" => $postData['txt_section'],"category" => $postData['sel_cat'],"sub_category" => $postData['sel_subcat'],"page_data"=>$postData["txt_description"],"resources_id" => $data_files,"public_access" => $chk_public);
									//print_r($data);die;
									$this->session->set_flashdata('page',$data);
									redirect("administration/getPagesDataSK");
						}
					}
					else
					{
						$this->session->set_flashdata("message",$isValid);
						//echo json_encode($isValid);exit;
						redirect("administration/getPagesDataSK");
					}
					
				}
			}
			else
			{
				//echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			} 
	}
	 
	public function list_data_pages() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				
				$cond = array();
				$like = array();
				$join = array(TB_CATEGORY => TB_PAGES_DATA.".category = ".TB_CATEGORY.".v_cat_id",TB_SUBCATEGORY => TB_PAGES_DATA.".sub_category = ".TB_SUBCATEGORY.".v_sub_cat_id");
				
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				
				$count = $this->administration_model->getPagesDataSKCount($cond,$like,$join);
				//echo $this->db->last_query();
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("section_name","category","sub_category");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "pd_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$resources = $this->administration_model->getPagesDataSKPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				//echo $this->db->last_query();
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="10%"><input type="checkbox" id="selecctall"></th>
								<th class="'.$css[0].'" width="25%" onclick="changePaginate(0,\'section_name\',\''.$corder[0].'\')">Section</th>
								<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'category\',\''.$corder[1].'\')">Category</th>
								<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'sub_category\',\''.$corder[2].'\')">Sub Category</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($resources)>0)
				{
						if(count($resources) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($resources as $resource) { 
							 
						$table .= '<tr id="row_'.$resource["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($resource["id"]).'" name="check" id="check" class="chk"></td>
								<td>'.$resource["section_name"].'</td> 
								<td>'.$resource["category"].'</td> 
								<td>'.($resource["sub_category"]?$resource["sub_category"]:'-').'</td> 
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($resource["id"]).'" href="'.base_url().'administration/getPagesDataSK?pid='.$resource["id"].'"><i class="fa fa-edit"></i></a>
								</td>
							  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($resources)==0)
						{
							$table .= '<tr class="text-center"><td colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getResorcesSKList");
				$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($resources),'paginate' => $paginate)); exit;
			}
		}
	} 
	 
	public function getPagesDataSK()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$cond = array();
			$data['message'] = $this->session->flashdata('message');
			$data['page'] = $this->session->flashdata('page');

			$data['message_error'] = $this->session->flashdata('message_error');
			$cond_cat = array("status_pmk"=>"Active");
			$data["categories"] = $this->administration_model->getAllVehicleCategory($cond_cat);  
			$data['res_sk'] = $this->administration_model->getResourceSKById($cond);

			$cond_all_sec=array('lang_id'=>'1');
			$data['addedSections'] = $this->administration_model->getAllreadyAddedSessions($cond_all_sec);
			$session_idArr=array_map(function($e1){ return $e1['section_id']; },$data['addedSections']);

			$countSection = count(array_filter($session_idArr));
			
			if($countSection==0){
				$data['sections']=$this->administration_model->getAllSections();
			}else{
				 $cond_id = array('s_id' =>array_filter($session_idArr));
				 $data['sections']=$this->administration_model->getOtherSections($cond_id);
			}
			
			
			
			$cond_lang = array();
			$data['langauages'] = $this->administration_model->getLanguages($cond_lang);
			if(isset($_GET["pid"]) && $_GET["pid"]!=""){
				$cond = array("page_id" => $_GET["pid"]);
				$data['res'] = $this->administration_model->getAllPagesSK($cond);
				//print_r($data['res']);die;
			} 

			// pr($data['res']);
			// die();
			
			
			$this->load->view("administrator/pages_data",$data);
		}
	}

	
	public function getEnglishDescription()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			$postData=$this->input->post();

			$cond=array('section_id'=>$postData['section_id'],'lang_id'=>'1');
			$description = $this->administration_model->getEnglishDescription($cond);
			// pr($description);
			// die();
			echo json_encode($description[0]);exit;	
	
		}

	}
	
	public function getPagesDataListSK()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{  
			/*
			 * Check permission for AMW And SKW 
			 * */
			 
			if($this->checkPermission() == 1){
				$cond = array();
				$data["categories"] = $this->administration_model->getAllVehicleCategory($cond);

				$this->load->view("administrator/pages_data_list",$data);
			}
			else{
				redirect('administration/getPagesListSK');
			}
		}
	}
	
	
	public function validatePagesDataSK($postData)
	{   
		
		return array("status" => 1);
	}
	
	public function delete_pages_data_sk() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("pd_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_PAGES_DATA,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
}
?>
