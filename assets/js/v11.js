$(document).ready(function(){
	
	$.ajaxSetup({
		beforeSend: function(jqXHR, settings) {
				//$("#loading").show();
				setTimeout(function(){ }, 3000);
			}
	});
	/*
	$.ajaxSetup({
		complete: function (data) {
			$("#loading").hide();
		}
	});
	$.ajaxSetup({
		error: function (xhr, ajaxOptions, thrownError) {
			alert(xhr.status+" "+thrownError);
		  }
    });
    */
    //$.ajaxSetup({async:false});
    
    $("#loading").hide();
    
    
 });

function checkAll()
{
	$('#selecctall').click(function(event) {
		if(this.checked) {
			$('.chk').each(function() {
				this.checked = true;           
			});
		}else{
			$('.chk').each(function() {
				this.checked = false;                    
			});         
		}
	});
}

function commonDelete(ajaxURL,msg)
{
	if($('.chk:checked').length) {
		chkId = new Array();
		var i = 0;
		$('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		});
	  
		if(confirm(msg) == true)
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: ajaxURL,
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				alert((json.ids).length+" records has been deleted.");
			});
		}
	}
	else {
	  alert('Please Select at Least One Element.');
	}
}
