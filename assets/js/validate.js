function validateEmpty(field, fieldName, msg){	
	
	//if it's NOT valid
	if(field.val().trim().length == 0){
		//fname.addClass("validation");
		fieldName.text("Please enter "+msg);
		//fieldName.addClass("validation");
		//field.focus();
		return false;
	}
	else{
		fieldName.text("");
		return true;
	}
}

// validation for Image
function validateEmptyImg(field, fieldName, msg){
	if(field.val().trim().length == 0){
		//fname.addClass("validation");
		fieldName.text("Please select "+msg);
		//fieldName.addClass("validation");
		//field.focus();
		return false;
	}
	else{
		fieldName.text("");
		return true;
	}
}

//validation for date
function validateEmptyDate(field, fieldName, msg){
	if(field.val().trim().length == 0){
		//fname.addClass("validation");
		fieldName.text("Please select "+msg);
		//fieldName.addClass("validation");
		//field.focus();
		return false;
	}
	else{
		fieldName.text("");
		return true;
	}
}

function CheckAlphanumericWithSomeChars(field, fieldName){	
	//if(!RegExp(/^([a-zA-Z]+\s)*[a-zA-Z]+$/).test(field.val()))
	//if(!RegExp(/^([a-zA-Z]+[.-']+\s)*[a-zA-Z]+$/).test(field1))
	field1 = field.val().trim();
	if(!RegExp(/^([a-zA-Z0-9]+\s)*[a-zA-Z0-9 ()',.-]+$/).test(field1))
	{
		fieldName.text("Invalid entry, please enter alphanumeric(dot,comma,parenthesis and hyphen are allowed)");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
}

function CheckAlphabates_accept_space(field, fieldName, msg)
{
	field1 = field.val();
	if(!RegExp(/^([a-zA-Z])*[a-zA-Z]/).test(field1))
	{
		fieldName.text("Invalid entry, please enter characters only");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
}

function CheckAlphabates(field, fieldName, msg){	
	//if(!RegExp(/^([a-zA-Z]+\s)*[a-zA-Z]+$/).test(field.val()))
	field1 = field.val();
	if(!RegExp(/^([a-zA-Z])*[a-zA-Z]+$/).test(field1))
	{
		fieldName.text("Invalid entry, please enter characters only");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
}
function CheckPasswordText(field,fieldName){
	//alert(field.val());
	//if(field.val().indexOf(invalid) > -1
	//if(RegExp(/^[\s]*[\d+]$/).test(field.val()))
	if(!RegExp(/^[\S]*$/).test(field.val()))
	{
		fieldName.text("Spaces are not allowed");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else if(field.val().trim().length < 6 ){
		fieldName.text("Please enter minimun 6 characters.");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
	
}

function CheckSearchText(field,fieldName){
	//alert(field.val());
	//if(field.val().indexOf(invalid) > -1
	if(RegExp(/^[\s]*[\d+]$/).test(field.val()))
	//if(!RegExp(/^([0-9a-zA-Z !@#$%^&*()'.-]+\s)*[0-9a-zA-Z !@#,$%^&*()'._-]+$/).test(field.val().trim()))
	{
		fieldName.text("Spaces are not allowed");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
	
}


 
function CheckAlphanumeric(field, fieldName){	
	//if(!RegExp(/^([a-zA-Z]+\s)*[a-zA-Z]+$/).test(field.val()))
	//if(!RegExp(/^([a-zA-Z]+[.-']+\s)*[a-zA-Z]+$/).test(field1))
	field1 = field.val().trim();
	if(!RegExp(/^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/).test(field1))
	{
		fieldName.text("Invalid entry, please enter alphanumeric only");
		//fieldName.addClass("validation");	
		field.focus();
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
}

function validateWebsite(field, fieldName){
	field1 = field.val().trim();
	if(!RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/).test(field1))
	{		
		//email.addClass("validation");
		fieldName.text("Please enter the correct url");
		//fieldName.addClass("validation");
		return false;
	}
	else
	{	
		fieldName.text("");
		fieldName.removeClass("validation");
		return true;
	}
}
 
function validateEmail(field, fieldName)
{
	//if(!RegExp(/^\w[\w\-\.]+\@\w[\w\-]+(\.\w[\w\-]+)+$/).test(field.val()))
	if(!RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(field.val()))
	{		
		//email.addClass("validation");
		fieldName.text("Invalid email address, please try again.");
		fieldName.addClass("validation");
		return false;
	}
	else
	{	
		fieldName.text("");
		fieldName.removeClass("validation");
		return true;
	}
}

function checkCombo(field, fieldName, msg){
	
	//if(field.val() < 1 || field.val() == ""){
	if(field.val() == ""){
		
		fieldName.text("Please select "+msg);
		
		return false;
	}
	else
	{
		fieldName.text("");
		return true;
	}
}


function validateTerm(field,fieldName){	
	//if it's NOT valid 
	if(!field.prop('checked', true)){		 
		fieldName.text("Please tick the checkbox if 'agree'."); 
		return false;
	}	
	//if it's valid
	else{		 
		fieldName.text("");
		return true;
	}
}

function CheckNumerals(field, fieldName, msg){	
	if(field.val() != ""){
		if(!(field.val()).match(/^\d+$/ ))
		{
			fieldName.text("Invalid entry. "+msg+" should be numeric.");
			//fieldName.addClass("validation");	
			field.focus();
			return false;
		}
		else
		{
			fieldName.text("");
			return true;
		}
	}
}
 

function validateFName(){	
	//if it's NOT valid
	if(fname.val().length == 0){
		//fname.addClass("validation");
		fnameInfo.text("Please enter your first name.");
		fnameInfo.addClass("validation");		
		return false;
	}
	else if(!RegExp("^[A-Za-z][\w]*[A-Za-z]+$").test(fname.val()))
	{
		fnameInfo.text("Please enter in alphabets");
		fnameInfo.addClass("validation");
		return false;
	}
	//if it's valid
	else{
		fnameInfo.text("");
		fnameInfo.removeClass("validation");
		return true;
	}
}
function validateLName(){	
	//if it's NOT valid
	if(lname.val().length == 0){
		//lname.addClass("validation");
		lnameInfo.text("Please enter your last name.");
		lnameInfo.addClass("validation");
		return false;
	}
	else if(!RegExp("^[A-Za-z][\w]*[A-Za-z]+$").test(lname.val()))
	{
		lnameInfo.text("Please enter in alphabets");
		lnameInfo.addClass("validation");
		return false;
	}
	//if it's valid
	else{
		lnameInfo.text("");
		lnameInfo.removeClass("validation");
		return true;
	}
}

function validatePass1(pass1, pass1Info){	
	//it's NOT valid	
	//alert(pass1.val());
	if(pass1.val().length == 0){
		//pass1.addClass("validation");		
		pass1Info.text("Please enter your password.");		
		return false;
	}
	else if(pass1.val().length < 6){		
		pass1Info.text("You need to enter a password that is at least 6 characters long.");
		return false;
	}
	else if(pass1.val().length > 15){		
		pass1Info.text("Please enter a atmost of fifteen characters.");
		return false;
	}
	/*//	Validation for atleast 1 small character
	else if(!RegExp("[a-z]+").test(c))
	{
		passwordInfo.text("Your password must contain at least one lower case letter(a,b,etc).");
		passwordInfo.addClass("validation");
		return false;
	}
	//	Validation for atleast 1 upper case letter
			
	else if(!RegExp("[A-Z]+").test(c))
	{
		passwordInfo.text("Your password must contain at least one upper case letter(A,B,etc).");
		passwordInfo.addClass("validation");
		return false;
	}

	//	Validation for atleast 1 digit ie 0-9
			
	else if(!RegExp("[0-9]+").test(c))
	{
		passwordInfo.text("Your password must contain at least one digit(1,2,etc).");
		passwordInfo.addClass("validation");
		return false;
	}

	//	Validation for atleast 1 special character
			
	else if(!RegExp("[!@#$%^&*+=]+").test(c))
	{
		passwordInfo.text("Your password must contain at least one special character(*,?,etc).");
		passwordInfo.addClass("validation");
		return false;
	}*/
	// Check against email Id.
	/*else if(email.val() == c)
	{
		passwordInfo.text("Password must be different from your email!");
		passwordInfo.addClass("validation");
		return false;
	}*/
	else{
		pass1Info.text("");
		return true;
	}
}



// function validateContact(contact,contactInfo){	
// 	//if it's NOT valid
// 	var c = contact.val();	
// 	if(!RegExp(/^[0-9][\d\- ]*[0-9]$/).test(c)){		
// 		contactInfo.text("Contact number don't appear to be valid!");
// 		contactInfo.addClass("validation");
// 		return false;
// 	}	
// 	//if it's valid
// 	else{		
// 		contactInfo.text("");
// 		contactInfo.removeClass("validation");
// 		return true;
// 	}
// }

// function validateMobile(mobile,mobInfo){	
// 	//if it's NOT valid
// 	var c = mobile.val();
// 		//if(!RegExp(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/).test(c)){
// 		if(!RegExp(/^((91)|(\+91)|0?)[789]{1}\d{9}$/).test(c)){

// 			mobInfo.text("Invalid mobile number,please try again.");
// 			mobInfo.addClass("validation");
// 			return false;
// 		}
// 		//if it's valid
// 		else{		
// 			mobInfo.text("");
// 			mobInfo.removeClass("validation");
// 			return true;
// 		}	
// }

function checkContact(field,fieldName){
	
	if(field.val().length < 11 || field.val().length > 15 )
		{
			fieldName.text('').append("Invalid contact number, please try again.");
			fieldName.addClass("validation");
			return false;
		}
		else
		{	
			fieldName.text("");
			fieldName.removeClass("validation");
			return true;
		}
}


function validateAddress(){	
	//if it's NOT valid
	if(address.val().length == 0){
		//lname.addClass("validation");
		addressInfo.text("Please enter address!");
		addressInfo.addClass("validation");
		return false;
	}	
	//if it's valid
	else{		
		addressInfo.text("");
		addressInfo.removeClass("validation");
		return true;
	}
}

function validateCity(){	
	//if it's NOT valid
	if(city.val().length == 0){
		//lname.addClass("validation");
		cityInfo.text("Please enter city!");
		cityInfo.addClass("validation");
		return false;
	}	
	//if it's valid
	else{
		cityInfo.text("");
		cityInfo.removeClass("validation");
		return true;
	}
}

function validateState(){	
	//if it's NOT valid
	if(state.val().length == 0){
		//lname.addClass("validation");
		stateInfo.text("Please enter state!");
		stateInfo.addClass("validation");
		return false;
	}	
	//if it's valid
	else{		
		stateInfo.text("");
		stateInfo.removeClass("validation");
		return true;
	}
}

function validateCountry(){	
	//if it's NOT valid
	if(country.val().length == 0){
		//lname.addClass("validation");
		countryInfo.text("Please enter country!");
		countryInfo.addClass("validation");
		return false;
	}	
	//if it's valid
	else{		
		countryInfo.text("");
		countryInfo.removeClass("validation");
		return true;
	}
}

function validateZip(){	
	//if it's NOT valid
	if(!RegExp(/^[0-9][\d\- ]*[0-9]$/).test(zip.val())){
		//lname.addClass("validation");
		zipInfo.text("ZIP/PIN don't appear to be valid!");
		zipInfo.addClass("validation");
		return false;
	}	
	//if it's valid
	else{		
		zipInfo.text("");
		zipInfo.removeClass("validation");
		return true;
	}
}


